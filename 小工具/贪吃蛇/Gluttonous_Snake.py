#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2021-06-07
"""
# 导入需要用到的模块
import pygame, sys, random, time
# 从pygame模块导入常用的函数和常量
from pygame.locals import *

''' 
@ 功能：实现贪吃蛇游戏
https://blog.csdn.net/weixin_43173447/article/details/109357112
'''

'''第一步，先导入需要的模块，初始化Pygame'''
# 初始化Pygame库pygame.init()
pygame.init()
# 初始化一个游戏界面窗口
DISPLAY = pygame.display.set_mode((640, 480))
# 设置游戏窗口的标题
pygame.display.set_caption('Python贪吃蛇')
# 定义一个变量来控制游戏速度
FPSCLOCK = pygame.time.Clock()
# 初始化游戏界面内使用的字体,及字体大小
BASICFONT = pygame.font.SysFont("SIMYOU.TTF", 40)

# 定义颜色变量
BLACK = pygame.Color(0, 0, 0)
WHITE = pygame.Color(255, 255, 255)
RED = pygame.Color(255, 0, 0)
GREY = pygame.Color(150, 150, 150)

''' 
初始化贪吃蛇的大小，位置，以及贪吃蛇要吃的食物的大小和位置。以一个20x20的小格子做为基本大小素材，
贪吃蛇初始长三个小格子，每个食物1个小格子。并定义贪吃蛇的初始移动方向，注意定义的方向是字符串
'''
# 贪吃蛇的初始位置
snake_Head = [100, 100]
# 初始化贪吃蛇的长度 （注：这里以20*20为一个标准小格子）
snake_Body = [[80, 100], [60, 100], [40, 100]]
# 定义贪吃蛇的行动方向，初始向右。
direction = "RIGHT"

# 给定第一枚食物的位置
food_Position = [300, 300]
# 食物标记：0 代表食物已被吃掉， 1代表未被吃掉.
food_flag = 1

'''用Pygame里的draw函数分别绘制出贪吃蛇和食物'''


# 绘制贪吃蛇、 食物、 分数等信息
# 绘制贪吃蛇
def drawSnake(snake_Body):
    for i in snake_Body:
        pygame.draw.rect(DISPLAY, WHITE, Rect(i[0], i[1], 20, 20))


# 绘制食物的位置
def drawFood(food_Position):
    pygame.draw.rect(DISPLAY, RED, Rect(food_Position[0], food_Position[1], 20, 20))


'''做两个函数，用两个矩形文本分别显示游戏速度和分数。然后根据分数来控制游戏速度，分数越高游戏速度越快'''


# 打印当前分数
def drawScore(score):
    # 设置分数的显示颜色
    score_Surf = BASICFONT.render('score:%s' % (score), True, GREY)
    # 设置分数的位置,显示在屏幕右上角
    score_Rect = score_Surf.get_rect()
    score_Rect.midtop = (550, 10)

    # 绑定以上设置到句柄
    DISPLAY.blit(score_Surf, score_Rect)


# 显示游戏速度
def drawGamespeed(Gamespeed):
    # 设置游戏速度的显示颜色
    Gamespeed_Surf = BASICFONT.render(f'Speed:{Gamespeed}', True, GREY)
    # 设置游戏速度的位置
    Gamespeed_Rect = Gamespeed_Surf.get_rect()
    Gamespeed_Rect.midtop = (80, 10)
    # 绑定以上设置到句柄
    DISPLAY.blit(Gamespeed_Surf, Gamespeed_Rect)


'''定义一个函数，当游戏失败时显示信息，退出游戏'''


# 定义函数，用于展示游戏结束的画面并退出程序
# 游戏结束并退出
def GameOver():
    # 设置GameOver的显示颜色
    GameOver_Surf = BASICFONT.render('Game Over!', True, RED)
    # 设置GameOver的位置
    GameOver_Rect = GameOver_Surf.get_rect()
    GameOver_Rect.midtop = (320, 10)
    # 绑定以上设置到句柄
    DISPLAY.blit(GameOver_Surf, GameOver_Rect)

    pygame.display.flip()
    time.sleep(3)
    # 退出游戏
    pygame.quit()
    # 退出程序
    sys.exit()


'''游戏的主程序'''
# 设置一个游戏标志，以便于退出循环，初始为True
game_flag = True
while game_flag:
    # 渲染底色
    DISPLAY.fill(BLACK)
    # 画出贪吃蛇
    drawSnake(snake_Body)
    # 画出食物位置
    drawFood(food_Position)
    # 打印出玩家的分数
    drawScore(len(snake_Body) - 3)
    # 控制游戏速度, 根据贪吃蛇的蛇身长度来控制游戏速度
    # 玩家每吃三次，增加一次游戏速度，初始为2。可以根据需要调整。
    game_speed = 1 + len(snake_Body) // 3
    # 打印出游戏速度
    drawGamespeed(game_speed)

    # 刷新pygame的显示层，贪吃蛇与食物的每一次移动，都会刷新显示层的操作来显示
    pygame.display.flip()
    # print (game_speed)
    FPSCLOCK.tick(game_speed)
    for event in pygame.event.get():
        if event.type == QUIT:
            # 接收到退出事件后，退出程序
            pygame.quit()
            sys.exit()

        # 通过检测键盘的上下左右或者ASDW来控制贪吃蛇的移动
        # 检测按键等pygame事件
        # 判断键盘事件， 用 方向键 或 WASD 来表示上下左右
        elif event.type == KEYDOWN:
            if (event.key == K_UP or event.key == K_w) and direction != "DOWN":
                direction = 'UP'
            if (event.key == K_DOWN or event.key == K_s) and direction != "UP":
                direction = 'DOWN'
            if (event.key == K_LEFT or event.key == K_a) and direction != "RIGHT":
                direction = 'LEFT'
            if (event.key == K_RIGHT or event.key == K_d) and direction != "LEFT":
                direction = 'RIGHT'
            # 点击空格退出游戏
            if (event.key == K_SPACE):
                game_flag = False
                GameOver()
            # break

    # 根据键盘的输入，改变蛇的头部方向， 进行转弯操作
    if direction == 'LEFT':
        snake_Head[0] -= 20
    if direction == 'RIGHT':
        snake_Head[0] += 20
    if direction == 'UP':
        snake_Head[1] -= 20
    if direction == 'DOWN':
        snake_Head[1] += 20

    # 将蛇的头部当前位置加入到蛇身的列表中
    snake_Body.insert(0, list(snake_Head))

    '''
    如果蛇头与食物的位置重合，则判定吃到食物，将食物数量清零；
    而没吃到食物的话，蛇身就会跟着蛇头运动，蛇身的最后一节将被踢出列表。
    '''
    # 判断是否吃掉食物
    if snake_Head[0] == food_Position[0] and snake_Head[1] == food_Position[1]:
        food_flag = 0
    # if direction == "RIGHT" or direction == "LEFT":
    # 	snake_Body.insert(0, (snake_Head[0] + 20, snake_Head[1]))
    # else:
    # 	snake_Body.insert(0, (snake_Head[0], snake_Head[1] + 20))
    else:
        snake_Body.pop()

    # 当游戏界面中的食物数量为0时， 需要重新生成食物，利用random函数来生成随机位置
    # 生成新的食物
    if food_flag == 0:
        # 随机生成x, y
        x = random.randrange(1, 32)
        y = random.randrange(1, 24)
        food_Position = [int(x * 20), int(y * 20)]
        food_flag = 1

    # 判断游戏是否结束
    # 贪吃蛇触碰到边界
    if snake_Head[0] < 0 or snake_Head[0] > 640:
        GameOver()
    if snake_Head[1] < 0 or snake_Head[1] > 460:
        GameOver()

    # 贪吃蛇碰到自己
    for i in snake_Body[1:]:
        if snake_Head[0] == i[0] and snake_Head[1] == i[1]:
            GameOver()
