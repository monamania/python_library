#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2022-01-11
"""

# https://blog.csdn.net/qq_38161040/article/details/88578799
import smtplib
from email.header import Header
from email.mime.text import MIMEText

sender = '1301519350@qq.com'  # 发送账号
sender_Passwd = 'onbkixlewmyqifdi'  # 授权码
'''
receivers = ['1830309412@qq.com']  # 接收邮件，可设置为你的QQ邮箱或者其他邮箱

# 三个参数：第一个为文本内容，第二个 plain 设置文本格式，第三个 utf-8 设置编码
# message = MIMEText('Python 邮件发送测试...', 'plain', 'utf-8')
message = MIMEText('Hello Boy!')  # 邮件内容
message['From'] = Header('小爱', 'utf-8')  # 邮件发送者名字
message['To'] = Header('小蓝枣', 'utf-8')  # 邮件接收者名字
subject = 'Python SMTP 邮件测试'  # subject
message['Subject'] = Header(subject, 'utf-8')  # subject


try:
    mail = smtplib.SMTP()
    # mail = smtplib.SMTP_SSL()
    mail.connect("smtp.qq.com")  # 连接 qq 邮箱
    mail.login(sender, sender_Passwd)  # 账号和授权码
    mail.sendmail(sender, receivers, message.as_string())  # 发送账号、接收账号和邮件信息
    # smtpObj.sendmail(sender, receivers, message.as_string())
    print("邮件发送成功")
except smtplib.SMTPException:
    print("Error: 无法发送邮件")

'''
def send_email(title: str = 'Python SMTP 邮件测试', body: str = '测试', receivers: list = ['1830309412@qq.com']):
    # 三个参数：第一个为文本内容，第二个 plain 设置文本格式，第三个 utf-8 设置编码
    # message = MIMEText('Python 邮件发送测试...', 'plain', 'utf-8')
    message = MIMEText(body)  # 邮件内容
    message['From'] = Header('Monomania_CJK', 'utf-8')  # 邮件发送者名字
    message['To'] = Header('Monomania', 'utf-8')  # 邮件接收者名字
    message['Subject'] = Header(title, 'utf-8')  # subject

    try:
        mail = smtplib.SMTP()
        # mail = smtplib.SMTP_SSL()
        mail.connect("smtp.qq.com")  # 连接 qq 邮箱
        mail.login(sender, sender_Passwd)  # 账号和授权码
        mail.sendmail(sender, receivers, message.as_string())  # 发送账号、接收账号和邮件信息
        # smtpObj.sendmail(sender, receivers, message.as_string())
        print("邮件发送成功")
    except smtplib.SMTPException:
        print("Error: 无法发送邮件")


if __name__ == '__main__':
    # receivers = ['1830309412@qq.com']  # 接收邮件，可设置为你的QQ邮箱或者其他邮箱
    receivers = ['1301519350@qq.com']  # 接收邮件，可设置为你的QQ邮箱或者其他邮箱
    send_email(title="test", body="test", receivers=receivers)