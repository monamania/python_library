#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2022-11-17
"""
import os
import re

import docx
from aip import AipNlp

''' 
@ 功能：https://console.bce.baidu.com/ai/?_=1652794810218&fromai=1#/ai/nlp/app/list
'''

""" 你的 APPID AK SK """
APP_ID = '28488620'
API_KEY = 'hojiPlr9dimUzYFQogVgGBs5'
SECRET_KEY = 'AYtD101OvC93OVLubr0uPqUqbh4r7gWw'
client = AipNlp(APP_ID, API_KEY, SECRET_KEY)


class Corrector:
    def __init__(self, filePath:str):
        if not os.path.exists(filePath):
            print('=' * 80)
            print("文件不存在!")
            print("Input file path：%s" % filePath)
            print('=' * 80)
            input('按任意键结束....')
            exit()
        self.filePath = filePath
        self.__name__ = "Corrector"
        # 清空
        f = open("log.txt", "w")
        f.close()
        pass

    def getText(self, fileName):
        # 获取文档对象
        try:
            doc = docx.Document(fileName)
        except:
            print('=' * 80)
            print("docx文件打开失败!")
            print("Input file path：%s" % fileName)
            print('=' * 80)
            input('按任意键结束....')
            exit()
        print("段落数:" + str(len(doc.paragraphs)))  # 段落数为，每个回车隔离一段
        # 输出每一段的内容
        TextList = []
        for paragraph in doc.paragraphs:
            TextList.append(paragraph.text)

        return '\n'.join(TextList)

    def run(self):
        text = self.getText(self.filePath)
        lineNum = 0
        # data = re.split('[，。、；]', text)
        data = re.split('[。；]', text)
        for sentence in data:
            lineNum += 1
            # text="百度是一家人工智能公斯,中国石油大徐"
            # print(sentence)
            if len(sentence) == 0:
                continue
            try:
                result = client.ecnet(sentence)
            except:
                continue
            # print(result)
            for item in result.items():
                if item[0] == 'item':
                    # for item0 in item[1]:
                    #    print(item0)
                    if float(item[1]['score']) >= 0 and float(item[1]['score']) < 1:
                        print("LineNum" + str(lineNum) + ": " + sentence, flush=True)
                        # print(item)
                        if len(item[1]['vec_fragment']) != 0:
                            print("-" * 80)
                            print("LineNum" + str(lineNum) + ": " + sentence)
                            print(item[1]['vec_fragment'])
                            logList = ["\nLineNum" + str(lineNum) + ": " + sentence]
                            logList.append(str(item[1]['vec_fragment']))
                            logList.append("-" * 80)
                            print("-" * 80)
                            f = open("log.txt", "a")
                            f.write('\n'.join(logList))
                            f.close()


if __name__ == '__main__':
    print('*' * 5 + '1.修改指定目录下的文件' + '*' * 5)
    print('*' * 5 + '2.修改脚本目录下的file.docx' + '*' * 5)
    choose = input('请输入您的选择：')
    if choose == '1':
        filePath = input(r'请输入文件路径：')
        while not os.path.exists(filePath):
            filePath = input(r'目录不存在，请重新输入文件路径：')
    else:
        filePath = "file.docx"
    corrector = Corrector(filePath)
    corrector.run()

    print('=' * 40, "over", '=' * 40)
    input('按任意键结束....')
