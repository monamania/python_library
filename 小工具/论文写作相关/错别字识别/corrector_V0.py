#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2022-11-14
"""

''' 
@ 功能：
git库：https://github.com/shibing624/pycorrector
测试：https://www.mulanai.com/product/corrector/#trial  和   https://huggingface.co/spaces/shibing624/pycorrector 
https://blog.bfw.wiki/user1/15989140925880750099.html
'''
import pycorrector
corrected_sent, detail = pycorrector.correct('少先队员因该为老人让坐')
print(corrected_sent, detail)