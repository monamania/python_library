#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2021-07-15
"""
# getpass.getpass(prompt='Password: ', stream=None)模块用于输入密码时，隐藏密码字符
# getpass.getuser()获取当前用户名
import getpass
import logging
import os
import platform
import sys
import time
''' 
https://blog.csdn.net/weixin_44861659/article/details/122951087
@ 功能：自己编写的日志模块，可以用于生成日志文件，方便调试
# getpass.getpass(prompt='Password: ', stream=None)模块用于输入密码时，隐藏密码字符
# getpass.getuser()获取当前用户名

logging等级---数字越大等级越高，默认为WARNING
logging.NOTSET      10
logging.DEBUG       20
logging.INFO        30
logging.WARNING     40
logging.ERROR       50
logging.CRITICAL    60
'''


class MyLog:
    """这个类用于创建一个自用的log
    @ 大于等于DEBUG等级的会输出到屏幕
    @ 大于等于ERROR等级的会输出到日志文件中
    @ 日志文件以程序名字加.log后缀命名
    """

    def __init__(self):
        # 获取当前用户名
        user = getpass.getuser()
        # 设置使用者信息，比如我的电脑当前账户用户名叫Monomania
        self.logger = logging.getLogger(user)
        # 设置显示等级，只要是大于等于DEBUG的等级的都会显示道屏幕上
        self.logger.setLevel(logging.DEBUG)
        # 日志文件名，及其路径
        logFile = self._set_fileName()
        # formatter = logging.Formatter('%(asctime)-12s %(levelname)-8s %(name)-10s %(message)-12s')
        formatter = logging.Formatter(
            '%(asctime)-12s | %(levelname)-8s | %(name)-10s[%(filename)-10s:%(module)s:%(funcName)s:%(lineno)d] - %(message)-12s')  # 日志输出格式

        '''日志显示到屏幕上并输出道日志文件内, eval()--转义'''
        logHand = logging.FileHandler(eval(logFile))
        logHand.setFormatter(formatter)
        # 大于等于ERROR等级的会输出到日志文件中
        logHand.setLevel(logging.ERROR)

        logHandSt = logging.StreamHandler()
        logHandSt.setFormatter(formatter)
        self.logger.addHandler(logHand)
        self.logger.addHandler(logHandSt)

    def _set_fileName(self):
        # OS = sys.platform  # 获取操纵系统类型  Win32
        OS = platform.system()  # 获取操纵系统类型  Windows
        print(OS)
        if OS == 'Windows':
            path = os.path.dirname((os.path.abspath(__file__)))
            # log_path = os.path.join(os.path.dirname(path), 'logs')
            # if not os.path.exists(log_path):
            #     os.mkdir(log_path)  # 如果不存在这个logs文件夹，就自动创建一个
            # timestamp = time.strftime("%Y-%m-%d", time.localtime())
            # logfile_name = '%s.log' % timestamp

            bsname = os.path.basename(__file__)  # 脚本文件的文件名
            logFile = path + os.sep + bsname[0:-3] + '.log'
        else:
            logFile = './' + sys.argv[0][0:-3] + '.log'

        print(repr(logFile))  # 取消转义：repr
        return repr(logFile)

    '''日志的5个级别对应以下5个函数'''

    def debug(self, msg):
        self.logger.debug(msg)

    def info(self, msg):
        self.logger.info(msg)

    def warning(self, msg):
        self.logger.warning(msg)

    def error(self, msg):
        self.logger.error(msg)

    def critical(self, msg):
        self.logger.critical(msg)


if __name__ == '__main__':
    mylog = MyLog()
    mylog.debug("I'm debug")
    mylog.info("I'm info")
    mylog.warning("I'm warn")
    mylog.error("I'm error")
    mylog.critical("I'm critical")
