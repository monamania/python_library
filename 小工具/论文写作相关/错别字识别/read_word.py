#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2022-11-17
"""
import docx
''' 
@ 功能：pip install python-docx
https://cloud.tencent.com/developer/article/1898865
'''


def getText(fileName):
    # 获取文档对象
    doc = docx.Document(fileName)
    print("段落数:" + str(len(doc.paragraphs)))  # 段落数为，每个回车隔离一段
    # 输出每一段的内容
    TextList = []
    for paragraph in doc.paragraphs:
        TextList.append(paragraph.text)

    return '\n'.join(TextList)


if __name__ == '__main__':
    fileName = r'毕业论文_V1.3.docx'
    print(getText(fileName))