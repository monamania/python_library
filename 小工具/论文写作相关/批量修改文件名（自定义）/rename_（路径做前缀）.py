#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2021-05-19
"""
import os
from pprint import pprint
# import sys
bsname = os.path.basename(__file__)  # 脚本文件的文件名
''' 
@ 功能：批量修改文件名，并且不会修改文件的扩展名
'''

def getAllFileList(fatherPath):
    """
    递归获取fatherPath目录下的所有文件
    """
    all_file_list = []
    all_dir_list = []
    file_list = os.listdir(fatherPath)
    for parent, dirnames, filenames in os.walk(fatherPath):
        # Case1: traversal the directories
        for dirname in dirnames:
            # print("Parent folder:", parent)
            # print("Dirname:", dirname)
            file_address = os.path.join(parent, dirname)
            all_dir_list.append(file_address)
        for filename in filenames:
            # print("Parent folder:", parent)
            # print("Filename:", filename)
            file_address = os.path.join(parent, filename)
            all_file_list.append(file_address)
    return all_dir_list, all_file_list

def getCurFileList(fatherPath):
    """
    去除当前脚本文件
    """
    fileList = os.listdir(fatherPath)
    fileList.sort()  # 由小到大排序
    fileList = [name for name in fileList if os.path.isfile(fatherPath + os.sep + name)]
    if bsname in fileList:
        fileList.remove(bsname)
    return fileList


def getDirList(fatherPath):
    """
    """
    fileList = os.listdir(fatherPath)
    fileList.sort()  # 由小到大排序
    dirList = [name for name in fileList if os.path.isdir(fatherPath + os.sep + name)]
    return dirList


def changeName(prefix, filePath):
    # 批量修改
    # 设置旧文件名（就是路径+文件名）
    oldname = fatherPath + os.sep + filePath  # os.sep添加系统分隔符
    # print(oldname)
    suffix = os.path.splitext(filePath)[1]  # 获取后缀名
    # #设置新文件名
    newname = fatherPath + os.sep + prefix + suffix
    os.rename(oldname, newname)  # 用os模块中的rename方法对文件改名
    print(oldname, '======>', newname)


def run():
    pass


if __name__ == '__main__':
    # print(sys.executable)
    print('*' * 5 + '1.修改指定目录下的文件' + '*' * 5)
    print('*' * 5 + '2.修改脚本目录下的文件' + '*' * 5)

    choose = input('请输入您的选择：')
    if choose == '1':
        fatherPath = input(r'请输入文件路径(结尾加上\)：')
        while not os.path.exists(fatherPath):
            fatherPath = input(r'目录不存在，请重新输入文件路径(结尾加上\)：')
    else:
        fatherPath = os.path.dirname((os.path.abspath(__file__)))
        # path = os.path.dirname(os.path.realpath(sys.executable))  # 发布为exe时使用

    print('即将修改的路径为：' + fatherPath)

    # 获取该目录下所有文件，存入列表中
    print("=" * 80)
    pprint(getCurFileList(fatherPath))
    print("=" * 80)
    pprint(getDirList(fatherPath))
    print("=" * 80)
    pprint(getAllFileList(fatherPath)[0])
    print("=" * 80)
    pprint(getAllFileList(fatherPath)[1])
    run()
    # input('输入任意键结束……')
