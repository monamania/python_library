#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2022-11-13
"""

import hashlib
import http.client
import json
import random
import sys
import urllib
from time import sleep

''' 
@ 功能：通过翻译实现论文降重----每月100w字免费
appid和secretKey若为空请去https://api.fanyi.baidu.com/申请通用翻译API，免费
在这里可以查看key，https://api.fanyi.baidu.com/api/trans/product/desktop?req=developer
'''

appid = "20221113001450556"  # 填写你的appid
secretKey = "qqECoH7kAV3XCqLBAJb3"  # 填写你的密钥

httpClient = None
myurl = "/api/trans/vip/translate"


def geturl(fromLang='auto', toLang='zh', q="test"):
    salt = random.randint(32768, 65536)
    sign = appid + q + str(salt) + secretKey
    sign = hashlib.md5(sign.encode()).hexdigest()
    myurl = "/api/trans/vip/translate" + '?appid=' + appid + '&q=' + urllib.parse.quote(
        q) + '&from=' + fromLang + '&to=' + toLang + '&salt=' + str(salt) + '&sign=' + sign
    return myurl


def fanyi(myurl):
    try:
        httpClient = http.client.HTTPConnection('api.fanyi.baidu.com')
        httpClient.request('GET', myurl)
        # response是HTTPResponse对象
        response = httpClient.getresponse()
        result_all = response.read().decode("utf-8")
        result = json.loads(result_all)
        return result.get('trans_result')[0].get("dst")
    except Exception as e:
        print(e)
    finally:
        if httpClient:
            httpClient.close()


def run(to_lang_list=['en', 'de', 'zh']):
    result = str(input("#请输入论文原句:\n"))
    print("#请等待%d秒钟..." % len(to_lang_list), flush=True)
    for to in to_lang_list:
        url1 = geturl(fromLang='auto', toLang=to, q=result)
        sleep(1)  # 限制访问时间，收费用户可以去除
        result = fanyi(url1)
    print('=' * 80)
    print("#去重结果：\n" + result)


def help():
    help_text = """
用法：
    python3 lunwenqcv1.py [level]
    level可选1（默认）、2、3分别对应初级、中级、高级去重
    例如：python3 lunwenqcv1.py 2， 然后回车输入需要去重的段落，然后等待将结果稍稍改动使其语气通顺即可！
    """
    print(help_text)


def main(to_lang_list=None):
    choose = ''
    while(choose != 'q'):
        if to_lang_list:
            run(to_lang_list)
        else:
            run()
        choose = input('回车继续，q退出：')


if __name__ == '__main__':
    to_lang_list = None
    if len(sys.argv) == 1:
        to_lang_list = None
    elif len(sys.argv) == 2:
        if sys.argv[1] == '1':
            to_lang_list = None
        elif sys.argv[1] == '2':
            to_lang_list = ['en', 'de', 'jp', 'pt', 'zh']
            run(to_lang_list)
        elif sys.argv[1] == '3':
            to_lang_list = ['en', 'de', 'jp', 'pt', 'it', 'pl', 'bul', 'est', 'zh']
            run(to_lang_list)
        elif sys.argv[1] == '-h' or sys.argv[1] == '--help':
            help()
        else:
            help()
    else:
        help()

    main(to_lang_list)
    input('按任意键结束....')
