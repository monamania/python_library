import time
from datetime import datetime

import requests


def queryStatus(uid, password):
    session = requests.session()
    data = {'uid': uid,
            'password': password,
            'actionFlag': 'loginAuthenticate',
            'lang': 'en_US',
            'redirect': 'https%3A%2F%2Fcareer.huawei.com%2Freccampportal%2Flogin_index.html%3Fredirect%3Dhttps%3A%2F'
                        '%2Fcareer.huawei.com%2Freccampportal%2Fportal5%2Findex.html%3Fi%3D78302',
            'loginFlag': 'byUid',
            'deviceFingerInfo': 'd36a786626c0c6417af75105301d425b',
            'redirect_local': '',
            'redirect_modify': '',
            'getloginMethod': 'null',
            'selectedAccount': ''}
    headers = {
        'User-agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'}
    login_url = 'https://uniportal.huawei.com/uniportal/login.do'
    r = session.post(login_url, headers=headers, data=data, timeout=5)
    cur_time = datetime.now().timestamp()
    print('当前时间为' + datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
    time = int(cur_time * 1000)
    url = 'https://career.huawei.com/reccampportal/services/portal/portaluser/queryMyJobInterviewPortal5?reqTime='
    url = url + str(time)
    html_src = session.get(url, timeout=5, headers=headers)
    res = html_src.content.decode('utf-8')

    res_list = res.split('{')[1][:-2].split(',')
    for res in res_list:
        print(res)
    print('=' * 80)


if __name__ == "__main__":
    uid = '15058487457'
    password = 'jianfei@123'
    queryInterval = 10  # 设置轮询时间，单位秒
    try:
        while True:
            queryStatus(uid, password)
            time.sleep(queryInterval)
    except:
        print('出错了请关闭梯子后重新运行')
