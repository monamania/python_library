#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2022-05-19
"""
import logging
import os
import time
from logging.handlers import RotatingFileHandler
import getpass

import colorlog

''' 
@ 功能：
https://blog.csdn.net/weixin_44861659/article/details/122951087

效果：按照 日期/时间/日志等级/文件名称/类/方法名称/代码行数展示（这里可以自己手动调整formatter参数 如果感觉展示太长的话）
%(levelno)s: 打印日志级别的数值
%(levelname)s: 打印日志级别名称
%(pathname)s: 打印当前执行程序的路径，其实就是sys.argv[0]
%(filename)s: 打印当前执行程序名
%(funcName)s: 打印日志的当前函数
%(lineno)d: 打印日志的当前行号
%(asctime)s: 打印日志的时间
%(thread)d: 打印线程ID
%(threadName)s: 打印线程名称
%(process)d: 打印进程ID
%(message)s: 打印日志信息
'''

# 创建文件目录----文件目录的上一级目录
cur_path = os.path.dirname(os.path.realpath(__file__))  # log_path是存放日志的路径
log_path = os.path.join(os.path.dirname(cur_path), 'logs')

if not os.path.exists(log_path):
    os.mkdir(log_path)  # 如果不存在这个logs文件夹，就自动创建一个

# 修改log保存位置
timestamp = time.strftime("%Y-%m-%d", time.localtime())
logfile_name = '%s.log' % timestamp
logfile_path = os.path.join(log_path, logfile_name)
# 定义不同日志等级颜色
log_colors_config = {
    'DEBUG': 'bold_cyan',
    'INFO': 'bold_green',
    'WARNING': 'bold_yellow',
    'ERROR': 'bold_red',
    'CRITICAL': 'red',
}


class Logger(logging.Logger):
    def __init__(self, name, level='DEBUG', file=None, encoding='utf-8'):
        super().__init__(name)
        self.encoding = encoding
        self.file = file
        self.level = level
        # 针对所需要的日志信息 手动调整颜色
        formatter_to_file = logging.Formatter('%(asctime)s | %(levelname)-8s | %(name)-10s[%(filename)s:%(module)s:%(funcName)s:%(lineno)d] - %(message)s')  # 日志输出格式
        # formatter_to_file = logging.Formatter('%(asctime)-12s | %(levelname)-8s | %(name)-10s[%(filename)-10s:%(module)s:%(funcName)s:%(lineno)d] - %(message)-12s')  # 日志输出格式

        formatter = colorlog.ColoredFormatter(
            '%(log_color)s%(levelname)1.1s %(asctime)s %(reset)s| %(message_log_color)s%(levelname)-8s %(reset)s| %('
            'log_color)s[%(filename)s%(reset)s:%(log_color)s%(module)s%(reset)s:%(log_color)s%(funcName)s%('
            'reset)s:%(log_color)s%(''lineno)d] %(reset)s- %(white)s%(message)s',
            reset=True,
            log_colors=log_colors_config,
            secondary_log_colors={
                'message': {
                    'DEBUG': 'blue',
                    'INFO': 'blue',
                    'WARNING': 'blue',
                    'ERROR': 'red',
                    'CRITICAL': 'bold_red'
                }
            },
            style='%'
        )  # 日志输出格式
        # 创建一个FileHandler，用于写到本地
        rotatingFileHandler = logging.handlers.RotatingFileHandler(filename=logfile_path,
                                                                   maxBytes=1024 * 1024 * 50,
                                                                   backupCount=5)
        rotatingFileHandler.setFormatter(formatter_to_file)
        rotatingFileHandler.setLevel(logging.DEBUG)
        self.addHandler(rotatingFileHandler)
        # 创建一个StreamHandler,用于输出到控制台
        console = colorlog.StreamHandler()
        console.setLevel(logging.DEBUG)
        console.setFormatter(formatter)
        self.addHandler(console)
        self.setLevel(logging.DEBUG)

user = getpass.getuser()
logger = Logger(name=user, file=logfile_path)

if __name__ == '__main__':
    logger.debug("I'm debug")
    logger.info("I'm info")
    logger.warning("I'm warn")
    logger.error("I'm error")
    logger.critical("I'm critical")
