#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2021-06-09
"""
import os

import matplotlib.pyplot as plt
from PIL import Image
import numpy as np
from wordcloud import WordCloud
from bs4 import BeautifulSoup
import jieba
import requests

''' 
@ 功能：对2019年政府工作报告，进行词云分析
https://blog.csdn.net/weixin_44562468/article/details/88880807
https://blog.csdn.net/weixin_38753213/article/details/107438027
'''


# 获取网页中的正文文本
def extract_text(url):
    page_source = requests.get(url).content
    bs_source = BeautifulSoup(page_source, "lxml")
    report_text = bs_source.find_all('p')
    text = ''
    for p in report_text:
        text += p.get_text()
        text += '\n'
    return text

# 词频分析
def word_frequency(text):
    from collections import Counter
    words = [word for word in jieba.cut(text, cut_all=True) if len(word) >= 2]
    c = Counter(words)
    for word_freq in c.most_common(35):
        word, freq = word_freq
        print(word, freq)

# 生成词频
url_2019 = 'http://news.sina.com.cn/c/xl/2019-03-05/doc-ihsxncvf9915493.shtml'
text_2019 = extract_text(url_2019)
word_frequency(text_2019)

# 词云分析
words = jieba.lcut(text_2019, cut_all=True)
exclude_words = ["我们", "提高", "国家"]
for word in words:
    if word in exclude_words:
        words.remove(word)

cuted = ' '.join(words)
path = 'C:\Windows\Fonts\ELEPHNT.TTF'  # 字体目录
#预检测，没有这个目录就先生成
if not os.path.exists(path):
    os.mkdir(path)

abel_mask = np.array(Image.open(r'ML.png'))  # 背景画布
wc = WordCloud(font_path=path, background_color='black', mask=abel_mask, max_words=30, width=800, height=400, margin=2, max_font_size=250, min_font_size=40).generate(cuted)

# 作图
plt.figure(dpi=300)  # 通过分辨率放大或缩小图片
plt.imshow(wc)
plt.axis('off')
plt.show()



# def read_deal_text():
#     with open("ciyun.txt", "r") as f:
#         txt = f.read()
#     re_move = ["，", "。", " ", '\n', '\xa0']
#     # 去除无效数据
#     for i in re_move:
#         txt = txt.replace(i, " ")
#     word = jieba.lcut(txt)  # 使用精确分词模式
#
#     with open("txt_save.txt", 'w') as file:
#         for i in word:
#             file.write(str(i) + ' ')
#     print("文本处理完成")

