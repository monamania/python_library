#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2021-05-28
"""
import os
import sys

import you_get
''' 
@ 功能：批量下载视频
https://www.cnblogs.com/jiangyuzhen/p/10926919.html
'''


class YouGet:
    """利用you-get实现视频下载"""
    def __init__(self, path):
        self.path = path  # 存放视频文件的路径

    def rmfile(self, path):
        """
        删除文件
        :param path: 文件路径
        :return: None
        """
        try:
            os.remove(path)
        except Exception as e:
            print(e)

    def download(self, url):
        """下载文件 sys.argv you_get.main()方式"""
        # 提示这个的时候you-get: This is a multipart video. (use --playlist to download all parts.),使用下面的
        # sys.argv = ['you-get', '-o', self.path, url, '--playlist']
        sys.argv = ['you-get', '-o', self.path, url]
        you_get.main()

        # 下载完成，删除xml文件
        for file in os.listdir(self.path):
            if file[-3:] == 'xml':
                self.rmfile(os.path.join(self.path, file))

    def download_another(self, url):
        """下载文件 os.system方式 """
        # 提示这个的时候you-get: This is a multipart video. (use --playlist to download all parts.),使用下面的
        os.system('you-get -o {0} --format=flv {1} --playlist'.format(self.path, url))   # --format=flv表示高清 用-i选项可以查到
        # os.system('you-get -o {0} -format=mp4hd {0} --playlist'.format(self.path, url))
        # os.system('you-get -format=mp4hd {}'.format(url))

        # 下载完成，删除xml文件
        for file in os.listdir(self.path):
            if file[-3:] == 'xml':
                self.rmfile(os.path.join(self.path, file))


if __name__ == "__main__":
    # 用于保存视频的目录
    path = r"E:\My_Documents\bilibili资源\赵左虚ROS教程"
    '''
    稳定性不是很好
    url = "https://www.bilibili.com/video/BV1Ci4y1L7ZZ?p=1"
    # 序列视频下载
    yg = YouGet(path)
    # yg.download(url)
    yg.download_another(url)
    print('下载结束')
    '''


    # 非序列视频，而是一页一个视频的使用下面的方法
    # urls = ["待下载的视频列表"]
    urls = []
    base_url = "https://www.bilibili.com/video/BV1Ci4y1L7ZZ?p="
    first_page = 1
    end_page = 367
    print("总共有", end_page - first_page + 1, "个视频需要下载")
    for page in range(first_page, end_page+1):    # 从15开始的
        url_item = base_url + str(page)
        urls.append(url_item)

    yg = YouGet(path)
    for url in urls:
        # print('开始下载第', urls.index(url)+first_page, '个视频')
        # yg.download(url)
        # print('第', urls.index(url)+first_page, '个视频下载结束')
        # print('-'*50)
        print('开始下载第', urls.index(url)+1, '个视频', flush=True)
        yg.download(url)
        print('第', urls.index(url)+1, '个视频下载结束')
        print('还剩', end_page - urls.index(url) - first_page, '个视频需要下载')
        print('-'*50)





# import requests
# import re
# aid = input('请输入av号：')
# # 获取bilibili视频地址
# url = 'https://www.bilibili.com/video/av'+aid
# headers = {'Host': 'www.bilibili.com',
#            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36',
#             'Connection': 'keep-alive',
#            }
# res = requests.get(url=url, headers=headers).text
# # 获取视频的真正地址
# vid = re.compile(r'"url":"http(.+?)","backup_url":.+?')
# tittle = re.findall(r'<h1 title="(.+?)".*?>', res)[0]
# vid_url = 'https'+re.findall(vid, res)[0]
# print(tittle, '\n', vid_url)
# vid_headers = {
#     'Origin': 'https://www.bilibili.com',
#     'Referer': url,
#     'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36',
# }
# video = requests.get(url=vid_url, headers=vid_headers).content
# # 视频保存在与py文件同级的video文件夹下
# with open('./video/'+tittle+'.mp4', 'wb+') as f:
#     f.write(video)