#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2023-02-08
"""

''' 
@ 功能：https://blog.csdn.net/zhu6201976/article/details/103942005
'''
"""
改写互亿无线官方语音通知代码类实现
同一手机号一分钟之内发送超过1条报错
"""
import time

import requests


class CallPhone(object):
    """
    语音通知类,提供功能:语音通知
    """

    def __init__(self, account, password):
        self.url = "http://api.vm.ihuyi.com/webservice/voice.php?method=Submit"  # 接口地址
        self.account = account  # APIID
        self.password = password  # APIkey

    def call_phone(self, mobiles, content):
        """
        语音通知
        :param mobiles: 手机号列表
        :param content: 语音通知内容
        :return:None
        """
        # 定义请求的头部
        headers = {
            "Content-type": "application/x-www-form-urlencoded",
            "Accept": "text/plain"
        }
        for mobile in mobiles:
            # 定义请求的数据
            params = {
                'account': self.account,
                'password': self.password,
                'content': content,
                'mobile': mobile,
                'format': 'json'
            }
            # 发起数据
            response = requests.post(self.url, params=params, headers=headers, timeout=30)
            response_str = response.content.decode('utf-8')
            print(response_str)
            time.sleep(5)


if __name__ == '__main__':
    c = CallPhone('', '')
    c.call_phone(['13888888888', '13999999999'], '您的订单号是：0648。已由顺风快递发出，请注意查收。')