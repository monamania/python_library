#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2023-02-08
"""
import os
from time import sleep
import sys
sys.path.append('D:\\A_Code\\A_Python\\adb安卓调试工具')
os.getcwd()   #查看当前工作路径
os.chdir('D:\\A_Code\\A_Python\\adb安卓调试工具')   #更改当前路径
''' 
@ 功能：https://blog.csdn.net/weixin_39614521/article/details/109618556
'''

def test_call(number1=10086, number2=0, number3=0, number4=0, number5=0):
    # 拨打电话
    print("拨打电话")
    call = os.popen("adb shell am start -a android.intent.action.CALL -d tel:{}".format(number1))
    sleep(15)
    # 挂断电话
    print("挂断电话")
    Hangup = os.popen("adb shell input keyevent 26")
    Hangup = os.popen("adb shell input keyevent KEYCODE_ENDCALL")
    Hangup = os.popen("adb shell input keyevent 6")
    sleep(2)
    # call = os.popen("adb shell am start -a android.intent.action.CALL -d tel:{}".format(number2))
    # sleep(5)
    # # 挂断电话
    # Hangup = os.popen("adb shell input keyevent 26")
    # sleep(5)
    # call = os.popen("adb shell am start -a android.intent.action.CALL -d tel:{}".format(number3))
    # sleep(20)
    # # 挂断电话
    # Hangup = os.popen("adb shell input keyevent 26")
    # sleep(5)
    # call = os.popen("adb shell am start -a android.intent.action.CALL -d tel:{}".format(number4))
    # sleep(20)
    # # 挂断电话
    # Hangup = os.popen("adb shell input keyevent 26")
    # sleep(5)
    # call = os.popen("adb shell am start -a android.intent.action.CALL -d tel:{}".format(number5))
    # sleep(20)
    # # 挂断电话
    # Hangup = os.popen("adb shell input keyevent 26")
    # sleep(2)

# 你可以修改你要通话的号码如下：
number1 = 18257858747
number2 = 0
number3 = 10011
number4 = 12580
number5 = 114

i = 0
# 执行代码：
while True:
    print('='*50, "第 %d 轮" % i, '='*50)
    test_call(number1, number2, number3, number4, number5)
    i += 1

# # 截图
#
# ScreenShot = os.popen("adb shell /system/bin/screencap -p /sdcard/xie.png")
#
# # 将截图保存到电脑
#
# SaveScreenShot = os.popen("adb pull /sdcard/xie.png")
