#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2022-11-03
"""
import glob
import uuid
from io import BytesIO
import os

import requests
from PIL import Image

''' 
@ 功能：
'''


def change(fatherPath, src:str='*.jpg', dist:str='.tif'):
    for f in glob.glob(os.path.join(fatherPath, src)):
        print(f)
        img = Image.open(f)
        num = len(src) - 1
        img.load(), img.save(f[0:-1 * num] + dist)
        # remove(f) # 删除原来的图



if __name__ == '__main__':
    fatherPath = r"C:\Users\Monomania\OneDrive\文档\【重要】毕业论文\img\\"
    src_type_list = ['*.jpg', '*.png', '*.jpeg', '*.jpg']
    dist_type = '.tif'
    for src in src_type_list:
        change(fatherPath, src, dist_type)

    print("over")
    # url = r"https://www.mdpi.com/applsci/applsci-10-01135/article_deploy/html/images/applsci-10-01135-g003.png"
    # webp2jpg_url(url)
    # print("over")
