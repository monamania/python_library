#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2022-11-03
"""
import glob
import uuid
from io import BytesIO
import os

import requests
from PIL import Image

''' 
@ 功能：
'''


def webp2jpg(fatherPath):
    for f in glob.glob(os.path.join(fatherPath, '*.webp')):
        print(f)
        img = Image.open(f)
        img.load(), img.save(f[0:-5] + ".jpg")
        # remove(f) # 删除原来的图


def webp2jpg_url(url: str, filePath="./", fileName=None):
    print("url: ", url)
    res = requests.get(url)
    byte_stream = BytesIO(res.content)
    img = Image.open(byte_stream)
    if fileName:
        img.load(), img.save(filePath + os.sep + fileName + ".jpg")
    else:
        img.load(), img.save(filePath + os.sep + str(uuid.uuid1()) + ".jpg")
    print("over")


if __name__ == '__main__':
    fatherPath = r"D:\My_Documents\A4_研究生相关材料\【重要】毕业论文\img\\"
    webp2jpg(fatherPath)
    print("over")
    # url = r"https://www.mdpi.com/applsci/applsci-10-01135/article_deploy/html/images/applsci-10-01135-g003.png"
    # webp2jpg_url(url)
    # print("over")
