#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2021-10-13
"""
import matplotlib.pyplot as plt
import numpy as np

''' 
@ 功能：
# https://www.zhihu.com/question/55058172
'''
from scipy.spatial.distance import pdist


def Curvature(x, y):
    k = 0
    for i in range(len(x) - 2):
        tX = (x[i] - x[i - 1], y[i] - y[i - 1])
        ty = (x[i + 1] - x[i], y[i + 1] - y[i])
        d = 1 - pdist([tX, ty], 'cosine')
        sin = np.sqrt(1 - d ** 2)
        dis = np.sqrt((x[i - 1] - x[i + 1]) ** 2 + (y[i - 1] - y[i + 1]) ** 2)
        k = 2 * sin / dis
        print(k)
    return k


if __name__ == '__main__':
    x2 = np.array(
        [-856.622, -846.59, -836.505, -826.398, -816.279, -806.156, -796.024, -785.891, -775.748, -765.604, -755.451,
         -745.297, -735.135, -724.973, -714.802])
    y2 = np.array(
        [1519.23, 1501.71, 1484.29, 1466.81, 1449.36, 1431.89, 1414.44, 1396.98, 1379.54, 1362.08, 1344.64, 1327.2,
         1309.76, 1292.32, 1274.88])

    plt.plot(x2, y2)
    k = Curvature(x2, y2)
    print(k)
    plt.show()

    # 曲线
    # coordinates = np.array([[1, 1], [1.5, 2], [2, 3], [2.5, 3.5], [3, 4], [3.5, 4.25], [4, 4.5]])
    # plt.plot(coordinates[:, 0], coordinates[:, 1])
    # k = Curvature(coordinates[:, 0], coordinates[:, 1])
    # print(k)
    # plt.show()
