#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2021-08-23
"""
import time
from alive_progress import *
''' 
@ 功能：
安装pip install alive-progress
可通过其showtime()函数，获取可用的动态进度条
show_bars()函数查看所有进度条样式
'''
# show_bars()

def alive_progress_show():
        with alive_bar(10, bar='blocks') as bar:
            for i in range(1):
                time.sleep(0.2)
                bar()

if __name__ == '__main__':
    alive_progress_show()
