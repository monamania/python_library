#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2021-08-23
"""
from tqdm import tqdm
from tqdm import trange
import pandas as pd
import time
''' 
@ 功能：
https://blog.csdn.net/weixin_36670529/article/details/88868032
【推荐】https://blog.csdn.net/qq_33472765/article/details/82940843
'''


"""【常用】进度条高级使用，融合进其他内容  基础进度条1也很有用

'''method 1'''
with tqdm(total=100) as pbar:
    for i in range(10):
        time.sleep(0.1)
        pbar.update(10)

'''method 2'''
pbar = tqdm(total=100)
for i in range(10):
    time.sleep(0.1)
    pbar.update(10)
pbar.close()
"""

'''基础进度条1---很有用'''
for i in tqdm(range(10), desc='这是一个进度条'):
    time.sleep(0.2)

'''基础进度条2'''
tmep = [time.sleep(0.2) for i in tqdm(range(10))]

'''基础进度条3'''
for i in trange(10):
    time.sleep(0.2)

'''
基础进度条4
如果想要在迭代过程中变更说明文字
还可以预先实例化进度条对象，在需要刷新说明文字的时候执行相应的程序
'''
df = pd.DataFrame({'a': range(10)})

for row in tqdm(df.itertuples()):
    time.sleep(0.2)

for row in df.itertuples():
    print(row)


'''
配合jupyter notebook/jupyter lab的美观进度条
需要：pip install ipywidgets
'''
from tqdm.notebook import trange
for i in trange(10):
    time.sleep(0.2)


