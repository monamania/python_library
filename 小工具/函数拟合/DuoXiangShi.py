#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2021-07-14
"""
import matplotlib.pyplot as plt

''' 
@ 功能：采用多项式的方式，进行拟合。与泰勒展开类似
https://blog.csdn.net/mao_hui_fei/article/details/103821601
'''
plt.rcParams['font.sans-serif'] = ['SimHei']  # 指定默认字体
# font = {'family': 'SimHei',
#         'weight': 'bold',
#         'size': '16'}
#
# plt.rc('font', **font)  # 步骤一（设置字体的更多属性）
plt.rcParams['axes.unicode_minus'] = False  # 解决负数坐标显示问题
# ------------------------------------------------
# t, x, y = [], [], []
#
# f1 = np.polyfit(t, x, 10)  # 5代表用的5次多项式，f1代表系数
# p1 = np.poly1d(f1)
# yvals = p1(t)  # 拟合y值
#
# plot1 = plt.plot(t, x, 's', label='Time (days)')
# plot2 = plt.plot(t, yvals, 'r', label='Matching')
# plt.xlabel('t')
# plt.ylabel('x')
# plt.legend(loc=4)  # 指定legend的位置右下角
# plt.title('polyfitting')
# plt.show()

# import numpy as np
# import matplotlib.pyplot as plt
#
# x_data = np.random.rand(100).astype(np.float32)
# y_data = x_data * 0.1 + 0.3
#
# poly = np.polyfit(x_data, y_data, deg=1)
#
# plt.plot(x_data, y_data, 'o')
# plt.plot(x_data, np.polyval(poly, x_data))
# plt.show()


import numpy as np

# x = np.arange(1, 17, 1)
# y = np.array([4.00, 6.40, 8.00, 8.80, 9.22, 9.50, 9.70, 9.86, 10.00, 10.20, 10.32, 10.42, 10.50, 10.55, 10.58, 10.60])
x = np.array([0.181, 0.268, 0.351, 0.448, 0.536, 0.545, 0.577, 0.652])
y = np.array([10, 15, 20, 25, 30, 32, 35, 40])
# x = np.array([0, 1, 1.9, 2.1, 3, 3.9, 4.1, 4.8,   5,  6, 7])
# y = np.array([2, 2,   2,   1, 1,   1, 2.3, 2.8, 3.5, 3.5, 3.5])

# 第一个拟合，自由度为3   ploy表示系数
ploy = np.polyfit(x, y, deg=8)
# 生成多项式对象
p1 = np.poly1d(ploy)
print(ploy)
print(p1)
plt.plot(x, y, 'o')
plt.plot(x, np.polyval(ploy, x))
plt.show()
