#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2021-05-19
"""
import os
import sys

''' 
@ 功能：批量修改文件名，并且不会修改文件的扩展名
'''
print('*' * 5 + '1.修改指定目录下的文件' + '*' * 5)
print('*' * 5 + '2.修改脚本目录下的文件' + '*' * 5)

choose = input('请输入您的选择：')
if choose == '1':
    path = input(r'请输入文件路径(结尾加上\)：')
    while not os.path.exists(path):
        path = input(r'目录不存在，请重新输入文件路径(结尾加上\)：')
else:
    # path = os.path.dirname((os.path.abspath(__file__)))
    path = os.path.dirname(os.path.realpath(sys.executable))  # 发布为exe时使用

print('即将修改的路径为：' + path)

# 获取该目录下所有文件，存入列表中
fileList = os.listdir(path)
fileList.sort()  # 由小到大排序
# bsname = os.path.basename(__file__)  # 脚本文件的文件名
"""发布为exe文件时使用"""
bsname = sys.executable  # exe文件的文件名
bsname = bsname.split('\\')[-1]
# print(bsname)


# 如果当前脚本在所指定的目录下的话，就从列表里删除，防止把脚本名字也改了
if bsname in fileList:
    fileList.remove(bsname)

print('当前目录下的文件为：')
for name in fileList:
    print(name)

prefix = input('请输入要设置的前缀：')

# suffix = input('请输入要设置的后缀(比如.jpg)：')
# while len(suffix) == 0:
#     suffix = input('后缀为空，请重新输入(比如.jpg)：')

# 批量修改
for i in range(len(fileList)):
    # 设置旧文件名（就是路径+文件名）
    oldname = path + os.sep + fileList[i]  # os.sep添加系统分隔符
    # print(oldname)

    suffix = os.path.splitext(fileList[i])[1]  # 获取后缀名[扩展名]
    # #设置新文件名
    newname = path + os.sep + prefix + str(i) + suffix
    try:
        os.rename(oldname, newname)  # 用os模块中的rename方法对文件改名
    except Exception as e:
        print('您要改的的前缀名和原来的一样啦！改不了啦！重新给个前缀名吧！')
        tmp = input('请输入要设置的前缀：')
        while tmp == prefix:
            tmp = input('重复啦！请输入要设置的前缀：')
        prefix = tmp
        # #设置新文件名
        newname = path + os.sep + prefix + str(i) + suffix
        os.rename(oldname, newname)  # 用os模块中的rename方法对文件改名

    print(oldname, '======>', newname)

input('输入任意键结束……')
# os.system('pause')
