#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2021-05-27
"""
import time
from random import choice
from random import randrange

import win32clipboard as w
import win32con
import win32gui

''' 
@ 功能：
'''


class SendMsg():
    def __init__(self, receiver, msg):
        self.receiver = receiver
        self.msg = msg
        self.setText()

    # 设置剪贴版内容
    def setText(self):
        w.OpenClipboard()
        w.EmptyClipboard()
        w.SetClipboardData(win32con.CF_UNICODETEXT, self.msg)
        w.CloseClipboard()

    # 发送消息
    def sendmsg(self):
        qq = win32gui.FindWindow(None, self.receiver)
        win32gui.SendMessage(qq, win32con.WM_PASTE, 0, 0)
        win32gui.SendMessage(qq, win32con.WM_KEYDOWN, win32con.VK_RETURN, 0)
        print("sucessfuly send", self.msg)


def getmessage(fileName):
    f = open(fileName, 'r', encoding='utf-8')
    lines = f.readlines()
    f.close()
    return choice(lines)


def main():
    receiver = '曹捞捞'  # 这里填入接收者的备注名
    while True:
        msg = getmessage('message.txt')
        qq = SendMsg(receiver, msg)
        qq.sendmsg()
        time.sleep(randrange(60, 10 * 60, 60))


if __name__ == '__main__':
    main()
