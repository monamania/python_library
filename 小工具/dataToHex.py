'''
/** 
 * 函数：Float2Hex
 * 描述：获取浮点数的十六进制字符串表示(以本机内存的字节存储顺序)
 * 参数：fDec        待转换浮点数
 *         pstrBuf    字符串缓冲区，存储转换结果，转换结果呈现的是数值在本机内存中高低字节的存储顺序，根据本机大小端模式的不同，
 *                    呈现的不一定是该数值的字节顺序(大端是一样的，而小端是逆序的)。在windows下，由于是小端，如果想要pstrBuf返回
 *                    数值的字节顺序，需要首先把fDec转换为大端再传入。
 * 返回：字符串缓冲区首地址
 */
unsigned char* Float2Hex(float fDec, unsigned char *pstrBuf)
{
    int i;
    
    int* pIVal = (int*)&fDec;

    for (i = 0; i < 8; i++)
    {
        int t = *pIVal >> (4 * (7 - i));
        t = t & 0x0000000F;

        switch (t)
        {
        case 0:
            pstrBuf[i] = '0';
            break;
        case 1:
            pstrBuf[i] = '1';
            break;
        case 2:
            pstrBuf[i] = '2';
            break;
        case 3:
            pstrBuf[i] = '3';
            break;
        case 4:
            pstrBuf[i] = '4';
            break;
        case 5:
            pstrBuf[i] = '5';
            break;
        case 6:
            pstrBuf[i] = '6';
            break;
        case 7:
            pstrBuf[i] = '7';
            break;
        case 8:
            pstrBuf[i] = '8';
            break;
        case 9:
            pstrBuf[i] = '9';
            break;
        case 10:
            pstrBuf[i] = 'A';
            break;
        case 11:
            pstrBuf[i] = 'B';
            break;
        case 12:
            pstrBuf[i] = 'C';
            break;
        case 13:
            pstrBuf[i] = 'D';
            break;
        case 14:
            pstrBuf[i] = 'E';
            break;
        case 15:
            pstrBuf[i] = 'F';
            break;
        default:
            break;
        }

    }

    return pstrBuf;
}
'''
