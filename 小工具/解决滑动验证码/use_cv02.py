#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2021-08-30
"""
import cv2

''' 
@ 功能：
https://blog.csdn.net/mixintu/article/details/109733585
'''


# 获取滑块的大小
def fix_img(filename):
    #  1.为了更高的准确率，使用二值图像
    img = cv2.imread(filename)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    _, thresh = cv2.threshold(gray, 127, 255, cv2.THRESH_BINARY)
    # 2.将轮廓提取出来
    contours, _ = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
    # 3.用绿色(0, 255, 0)来画出最小的矩形框架
    x, y, w, h = cv2.boundingRect(contours[0])
    rect_x = x + w
    rect_y = y + h
    # print(x, y, rect_x, rect_y)  # x，y是矩阵左上点的坐标，w，h是矩阵的宽和高
    rect_img = cv2.rectangle(img, (x, y), (rect_x, rect_y), (0, 255, 0), 1)
    # 3.根据滑块的高度和宽度进行截图
    mixintu = rect_img[y:rect_y, x:rect_x]
    return mixintu


# 获取移动距离
async def compute_gap(fadebg, fullbg):
    # 1.对滑块进行图片处理
    tp_img = fix_img(fadebg)  # 裁掉透明部分，找出滑块的大小
    tp_pic = cv2.cvtColor(cv2.Canny(tp_img, 100, 200), cv2.COLOR_GRAY2BGR)
    # 2.对背景进行图片处理
    bg_pic = cv2.cvtColor(cv2.Canny(cv2.imread(fullbg), 100, 200), cv2.COLOR_GRAY2BGR)
    # 3.模板匹配matchTemplate
    res = cv2.matchTemplate(bg_pic, tp_pic, cv2.TM_CCOEFF_NORMED)
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
    # print(min_val, max_val, min_loc, max_loc)  # 最小值，最大值，最小值的位置，最大值的位置
    return max_loc[0]  # 返回滑块移动距离/


if __name__ == '__main__':
    pass
