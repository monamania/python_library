#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2021-06-18
"""
import pdfkit

''' 
@ 功能：实现将url链接转换为pdf文件
豆丁网、百度文库等无法使用
'''

# url = r"https://www.csdn.net/"
# pdfkit.from_url(url, r'不告而取即为偷，当面强拿则为抢.pdf')
url = r"http://www.autolabor.com.cn/book/ROSTutorials/di-7-zhang-ji-qi-ren-dao-822a28-fang-771f29/72-dao-hang-shi-xian/724-dao-hang-shi-xian-04-lu-jing-gui-hua.html"
pdfkit.from_url(url, r'测试.pdf')

# def save_to_pdf(url):
#     '''
#     根据url，将文章保存到本地
#     :param url:
#     :return:
#     '''
#     title = get_title(url)
#     body = get_Body(url)
#     filename = author + '-' + title + '.pdf'
#     if '/' in filename:
#         filename = filename.replace('/', '+')
#     if '\\' in filename:
#         filename = filename.replace('\\', '+')
#     print(filename)
#     options = {
#         'page-size': 'Letter',
#         'encoding': "UTF-8",
#         'custom-header': [
#             ('Accept-Encoding', 'gzip')
#         ]
#     }
#     # 本来直接调用pdfkid的from方法就可以了，但是由于我们的wkhtmltopdf安装包有点问题，一直没法搜到，所以只能用本办法，直接配置了wk的地址
#     # 尴尬了，主要是一直没法下载到最新的wk，只能在网上down了旧版本的。有谁能下到的话发我一份。。。
#     config = pdfkit.configuration(wkhtmltopdf=r'C:\Program Files\wkhtmltopdf\bin\wkhtmltopdf.exe')
#     pdfkit.from_string(body, filename, options=options, configuration=config)
#     print('打印成功！')




# import pdfkit
# pdfkit.from_url('http://baidu.com','out.pdf')
# pdfkit.from_file('test.html','out.pdf')
# pdfkit.from_string('Hello!','out.pdf')
# pdfkit.from_url(['google.com', 'yandex.ru', 'engadget.com'], 'out.pdf')
# pdfkit.from_file(['file1.html', 'file2.html'], 'out.pdf')
# with open('file.html') as f:
#     pdfkit.from_file(f, 'out.pdf')

# # Use False instead of output path to save pdf to a variable
# 如果我们想继续操作pdf，可以将其读取成一个变量，其实就是一个string变量。
# pdf = pdfkit.from_url('http://google.com', False)


# 以指定各种选项，就是上面三个方法中的options。
# options = {
#     'page-size': 'Letter',
#     'margin-top': '0.75in',
#     'margin-right': '0.75in',
#     'margin-bottom': '0.75in',
#     'margin-left': '0.75in',
#     'encoding': "UTF-8",
#     'custom-header' : [
#         ('Accept-Encoding', 'gzip')
#     ]
#     'cookie': [
#         ('cookie-name1', 'cookie-value1'),
#         ('cookie-name2', 'cookie-value2'),
#     ],
#     'no-outline': None
# }
#
# pdfkit.from_url('http://google.com', 'out.pdf', options=options)
