#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2022-05-19
"""

''' 
@ 功能：
模拟dict的功能--减少{}的内存
'''
class Dict:
    def __init__(self):
        self.key_list = []
        self.value_list = []

    def __len__(self):
        return len(self.key_list)

    def __getitem__(self, key):
        if key in self.key_list:
            idx = self.key_list.index(key)
            return self.value_list[idx]
        else:
            return None

    def __setitem__(self, key, value):
        if key not in self.key_list:
            self.key_list.append(key)
            self.value_list.append(value)
        else:
            idx = self.key_list.index(key)
            self.value_list[idx] = value

    def __delitem__(self, key):
        idx = self.key_list.index(key)
        del self.key_list[idx]
        del self.value_list[idx]

    def update(self, key, value):
        self.__setitem__(key, value)

    def values(self):
        return self.value_list

    def keys(self):
        return self.key_list

    def __repr__(self):
        return f'len: {self.__len__()} \nkey_list: {self.key_list} \nvalue_list: {self.value_list}'

    def __str__(self):
        ss = ''
        for idx, key in enumerate(self.key_list):
            ss += (f'{type(key).__name__} - {key}: {self.value_list[idx]}\n')
        return ss


if __name__ == '__main__':
    dc = Dict()
