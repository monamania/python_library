#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2021-08-22
"""
import os

''' 
@ 功能：
获取文件数据
使用说明：
path = 文件路径
bd = BaseData(path)
test_data = bd.get_data_by_ID(需要查找的股票ID)
test_data = bd.get_data_by_FileName(需要查找的文件名)
print(bd.get_Place_By_ID(需要查找的股票ID))

获取股票ID：test_data.id         # 股票id
获取股票名字：self.name       # 股票名字
获取日期列表：self.date       # 日期
获取开盘列表：self.start      # 开盘
获取最高列表：self.max        # 最高
获取最低列表：self.min        # 最低
获取收盘列表：self.close      # 收盘
获取成交量：self.trading_numb       # 成交量
获取成交额：self.trading_volume     # 成交额
'''

class BaseData:
    def __init__(self, path):
        """
        可通过ID，或者文件名获取对应的数据
        :param path:
        """
        self.path = path
        # 获取该目录下所有文件，存入列表中
        self.fileList = [d for d in os.listdir(path)]
        self.SH_ID = []  # 上海的ID
        self.SZ_ID = []  # 深圳的ID
        # 数据处理，处理成地域+ID
        self._handleFileName()

    def get_data_by_ID(self, id):
        """
        通过股票ID获取数据
        :param id:
        :return:
        """
        if id in self.SH_ID:
            fileName = 'SH' + '#' + id + '.txt'
            return TableData(self.path, fileName)
        elif id in self.SZ_ID:
            fileName = 'SZ' + '#' + id + '.txt'
            return TableData(self.path, fileName)
        else:
            raise ValueError('ID输入有误，没有这个ID的文件：', id)

    def get_data_by_FileName(self,fileName):
        """
        通过文件名获取数据
        :param fileName:
        :return:
        """
        if fileName in fileList:
            return TableData(self.path, fileName)
        else:
            raise ValueError('文件名输入有误：', fileName)


    def get_Place_By_ID(self, id):
        """获取位置，是深圳的还是上海的"""
        if id in self.SH_ID:
            return 'SH'
        elif id in self.SH_ID:
            return 'SZ'


    def _handleFileName(self):
        tmp_list = []
        for item in self.fileList:
            tmp_list.append(item.split('.')[0])

        for item in tmp_list:
            tmp_list = item.split('#')
            if tmp_list[0] == 'SH':
                self.SH_ID.append(tmp_list[1])
            elif tmp_list[0] == 'SZ':
                self.SZ_ID.append(tmp_list[1])
            else:
                raise ValueError('出现了未定义的名字：', tmp_list[0])

    def getFileList(self):
        """
        当前路径下的文件名
        :return:文件名列表
        """
        return self.fileList


class TableData:
    def __init__(self, path, fileName):
        """
        用于单个文件的数据读取，返回数据对象
        self.id         # 股票id
        self.name       # 股票名字
        self.date       # 日期
        self.start      # 开盘
        self.max        # 最高
        self.min        # 最低
        self.close      # 收盘
        self.trading_numb       # 成交量
        self.trading_volume     # 成交额
        :param path: 文件路径
        :param fileName: 文件名（不是ID名）
        """
        self.path = path
        self.fileName = fileName
        self.id = ''  # 股票id
        self.name = ''  # 股票名字
        self.date = []  # 日期
        self.start = []  # 开盘
        self.max = []  # 最高
        self.min = []  # 最低
        self.close = []  # 收盘
        self.trading_numb = []  # 成交量
        self.trading_volume = []  # 成交额
        self._readTxt()


    def _readTxt(self):
        """
        读取单个文件中的数据
        """
        with open(self.path + os.sep + self.fileName, "r") as f:
            lines = f.readlines()
        for i, line in enumerate(lines):
            # print(i, line)
            line = line.split()
            if i == 0:
                self.id = line[0]
                self.name = line[1]
            elif i >= len(lines) - 1 or i == 1:
                # 丢弃第二行与最后一行的没用的数据
                pass
            else:
                self.date.append(line[0])
                self.start.append(float(line[1]))
                self.max.append(float(line[2]))
                self.min.append(float(line[3]))
                self.close.append(float(line[4]))
                self.trading_numb.append(float(line[5]))
                self.trading_volume.append(float(line[6]))



if __name__ == '__main__':
    path = r'E:\【重要】数学建模\第三次作业\题目\2021年8月11日后复权数据'
    bd = BaseData(path)
    fileList = bd.getFileList()
    print('目录：', path)
    print('目录下文件列表：', fileList)
    print('上海股票ID列表：', bd.SH_ID)
    print('深圳股票ID列表：', bd.SZ_ID)
    # 通过文件名获取数据
    td = bd.get_data_by_FileName(fileList[0])
    # 通过ID获取数据
    # td = bd.get_data_by_ID('600000')
    print('股票ID:', td.id)
    print('股票名字:', td.name)
    print('日期:', td.date)
    print('开盘:', td.start)
    print('最高:', td.max)
    print('最低:', td.min)
    print('收盘:', td.close)
    print('成交量:', td.trading_numb)
    print('成交额:', td.trading_volume)

