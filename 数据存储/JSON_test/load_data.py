#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2021-07-12
"""
import json
''' 
@ 功能：通过json格式进行数据的读取
json.loads()针对数据操作
json.load()针对文件操作
'''
json_str = '[{"name": "小王", "score": 95}, {"name": "小明", "score": 89}, {"name": "小红", "score": 99}]'

users = json.loads(json_str)
print(type(users))
print(users)


with open('test.json', 'r') as fp:
    """由于test.json采用ASCII编码，所以不一定要指定编码格式"""
    users = json.load(fp)
    print(type(users))
    print(users)


with open('test2.json', 'r', encoding='utf-8') as fp:
    """由于test2.json采用utf-8编码，所以一定要指定编码格式"""
    users = json.load(fp)
    print(type(users))
    print(users)

