#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2021-07-12
"""
import json

''' 
@ 功能：通过json格式进行数据的存储
json.dumps()针对数据操作
json.dump()针对文件操作
'''
users = [
    {
        'name': '小王',
        'score': 95
    },
    {
        'name': '小明',
        'score': 89
    },
    {
        'name': '小红',
        'score': 99
    }
]  # 定义列表数据

json_str = json.dumps(users, ensure_ascii=False)  # 将列表编码为JSON字符串
print(json_str)

with open('test.json', 'w') as fp:
    json.dump(users, fp)   # 会保存为默认编码后的内容


with open('test2.json', 'w', encoding='utf-8') as fp:
    # 会保存为当前内容—————一定要指定utf-8不然保存的就是GBK格式了
    json.dump(users, fp, ensure_ascii=False)


