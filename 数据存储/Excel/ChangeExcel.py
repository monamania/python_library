#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2021-05-21
"""
# from pprint import pprint
from openpyxl import Workbook
import time

from openpyxl import load_workbook
from win32com.client import Dispatch
import os

'''
@ 功能：
'''
key = ['冷负荷', '湿球温度']


def vba_open(filename, sheet_name):
    try:
        xlApp = Dispatch("Excel.Application")
        xlApp.Visible = False  # 不可见
        xlBook = xlApp.Workbooks.Open(filename)
        sheets = xlBook.Sheets
        sheet1 = sheets.Item(sheet_name)
        # data = sheet1.Cells(i,j).value
        data = sheet1.Range('W16').value
        xlBook.Save()  # 读的时候不要保存，修改记得保存
        xlBook.Close()
        # xlApp.Quit()

        return data

    except Exception as e:
        print("vba_open失败，e:", e)
        input("输入回车退出……")
        exit(1)


def writeData(data_path, data, sheetname1='Sheet1', sheetname2='Sheet2'):
    wb_result = Workbook()
    ws_result = wb_result.active  # 获取表
    print('\n---------开始分组数据写入--------------', flush=True)
    print('-----------共', (len(data) + 1), '组--------------')
    for grp in range(len(data)):
        # for grp in range(2):
        print('\n-----------第', grp + 1, '组开始-----------', flush=True)
        print('-------------稍等哦~-------------', flush=True)
        time_start = time.time()
        global key
        try:
            wb = load_workbook(data_path, data_only=False)
            # print(wb.sheetnames)
            ws = wb[sheetname1]  # 获取表
            # print('Title: ', ws.title)

            k = 0
            for i in range(3, 6, 2):  # 列
                for j in range(0, len(data[grp][key[k]])):
                    ws.cell(row=j + 3, column=i).value = data[grp][key[k]][j]
                    # print("row", j + 3, "col", i, 'k ', k, 'cell', data[grp][key[k]][j])
                k = k + 1

            wb.save(data_path)

        except Exception as e:
            print("write中加载文件", data_path, "失败，e:", e)
            input("输入回车退出……")
            exit(1)

        value = vba_open(data_path, sheetname2)  # 用VBA库读取结果

        # 保存到txt文件
        with open('result.txt', 'a') as f:
            f.write(str(data[grp]['城市']) + ': ' + str(value) + '\n')
            print(str(data[grp]['城市']) + ': ' + str(value))

        # 保存到excel中
        ws_result.cell(grp + 1, 1).value = data[grp]['城市']  # 保存
        ws_result.cell(grp + 1, 2).value = value  # 保存
        # print(str(data[grp]['城市']) + str(value))

        time_end = time.time()
        print('耗时: ', time_end - time_start)

    wb_result.save('result.xlsx')


def readExcel(data_path, sheetname='Sheet1'):
    print('-----------打开excel表-----------')
    wb = load_workbook(data_path, data_only=False)
    print(wb.sheetnames)
    ws = wb[sheetname]  # 获取表
    print('Title: ', ws.title)
    print('--------------------------------', flush=True)

    # 获取行列数
    row = ws.max_row
    col = ws.max_column
    print('row: ', row)
    print('col: ', col)
    print('-----------数据读取中-----------', flush=True)

    datas = []
    global key
    for grp in range(3, col, 3):
        # 定义一个空字典
        sheet_data = {}
        for sub_grp in range(0, 2):
            sub_data = []
            for cell_row in range(3, row + 1):
                # 获取单元格数据
                c_cell = ws.cell(row=cell_row, column=(grp + sub_grp)).value
                # print("row", j, "col", (i+cnt), 'cell', c_cell)
                sub_data.append(c_cell)
            sheet_data[key[sub_grp]] = sub_data
            sheet_data['城市'] = ws.cell(row=1, column=grp).value
            # 再将字典追加到列表中
        datas.append(sheet_data)

    wb.save(data_path)
    # 返回从excel中获取到的数据：以列表存字典的形式返回
    return datas


def main():
    proPath = os.path.dirname(os.path.abspath(__file__))
    time_start = time.time()

    # =================读地址========================================
    # data_path1 = proPath + '\\' + r'test1.xlsx'
    data_path1 = proPath + '\\' + r'负荷部分.xlsx'
    path1_sheet_name1 = r'温和地区'
    # =================写地址========================================
    # data_path2 = proPath + '\\' + r'test2.xlsx'
    data_path2 = proPath + '\\' + r'1-标准计算表格.xlsx'
    path2_sheet_name1 = r'输入参数'
    path2_sheet_name2 = r'结果概况'

    # =================开始读取数据===================================
    try:
        excel_1 = readExcel(data_path1, path1_sheet_name1)  # 读取数据
    except Exception as e:
        print("加载文件", data_path1, "失败，e:", e)
        input("输入回车退出……")
        exit(1)
    # show(data)
    # print(excel_1)
    print("read over")
    time_end = time.time()
    print('read cost: ', time_end - time_start)
    # =================开始写入数据===================================
    try:
        writeData(data_path2, excel_1, path2_sheet_name1, path2_sheet_name2)
    except Exception as e:
        print("加载文件", data_path2, "失败，e:", e)
        input("输入回车退出……")
        exit(1)

    print("write over")
    print("程序运行结束！")


if __name__ == '__main__':
    main()
    input("输入回车退出……")
