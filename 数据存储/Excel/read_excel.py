#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2021-05-17
"""
import datetime
from pprint import pprint

import xlrd
from xlrd import xldate_as_tuple

''' 
@ 功能：
xlrd中单元格的数据类型
数字一律按浮点型输出，日期输出成一串小数，布尔型输出0或1，所以我们必须在程序中做判断处理转换
成我们想要的数据类型
0 empty,1 string, 2 number, 3 date, 4 boolean, 5 error
'''


class ReadExcelData():
    # 初始化方法
    # def __init__(self, data_path, sheetname):
    def __init__(self, data_path):
        # 定义一个属性接收文件路径
        self.data_path = data_path
        # 定义一个属性接收工作表名称
        # self.sheetname = sheetname
        # 使用xlrd模块打开excel表读取数据
        with xlrd.open_workbook(self.data_path) as self.data:
            # 根据工作表的名称获取工作表中的内容（方式①）
            # self.table = self.data.sheet_by_name(self.sheetname)
            # 根据工作表的索引获取工作表的内容（方式②）
            self.table = self.data.sheet_by_index(0)
        # 获取第一行所有内容,如果括号中1就是第二行，这点跟列表索引类似
        self.keys = self.table.row_values(0)
        # 获取工作表的有效行数
        self.__rowNum = self.table.nrows
        # 获取工作表的有效列数
        self.__colNum = self.table.ncols

    def get_rowNum(self):
        """获取数据表行数"""
        return self.__rowNum

    def get_colNum(self):
        """获取数据表行数"""
        return self.__colNum

    # 定义一个读取excel表的方法
    def readExcel(self):
        # 定义一个空列表
        datas = []
        for i in range(1, self.__rowNum):
            # 定义一个空字典
            sheet_data = {}
            for j in range(self.__colNum):
                # 获取单元格数据类型
                c_type = self.table.cell(i, j).ctype
                # 获取单元格数据
                c_cell = self.table.cell_value(i, j)
                # 对单元格数据元素进行数据处理
                c_cell = self.__handleCell(c_type, c_cell)

                sheet_data[self.keys[j]] = c_cell

                # 循环每一个有效的单元格，将字段与值对应存储到字典中
                # 字典的key就是excel表中每列第一行的字段
                # sheet_data[self.keys[j]] = self.table.row_values(i)[j]
            # 再将字典追加到列表中
            datas.append(sheet_data)
        # 返回从excel中获取到的数据：以列表存字典的形式返回
        return datas

    def showExcel(self):
        for i in range(0, self.__rowNum):
            for j in range(0, self.__colNum):
                # 获取单元格数据类型
                c_type = self.table.cell(i, j).ctype
                # 获取单元格数据
                c_cell = self.table.cell_value(i, j)
                # 对单元格数据元素进行数据处理
                c_cell = self.__handleCell(c_type, c_cell)
                print(c_cell, end='\t\t')
            print()

    def __handleCell(self, c_type, c_cell):
        """对元素进行处理"""
        if c_type == 2 and c_cell % 1 == 0:  # 如果是整形
            c_cell = int(c_cell)
        elif c_type == 3:
            # 转成datetime对象
            date = datetime.datetime(*xldate_as_tuple(c_cell, 0))
            c_cell = date.strftime('%Y/%d/%m %H:%M:%S')
        elif c_type == 4:  # boolean型
            c_cell = True if c_cell == 1 else False
        return c_cell


if __name__ == "__main__":
    data_path = "D:\My_Documents\B报销\经费使用情况.xlsx"
    # sheetname = "My_Worksheet"
    excel = ReadExcelData(data_path)
    datas = excel.readExcel()
    pprint(datas)
    excel.showExcel()

# #!/usr/bin/env python
# # -*- coding:utf-8 -*-
# """
# @ Author：CJK_Monomania
# @ Data：2021-05-17
# """
# import xlrd
# from xlrd import xldate_as_tuple
# import datetime
# from pprint import pprint

# ''' 
# @ 功能：
# '''

# '''
# xlrd中单元格的数据类型
# 数字一律按浮点型输出，日期输出成一串小数，布尔型输出0或1，所以我们必须在程序中做判断处理转换
# 成我们想要的数据类型
# 0 empty,1 string, 2 number, 3 date, 4 boolean, 5 error
# '''
# class ExcelData():
#     # 初始化方法
#     def __init__(self, data_path, sheetname):
#         #定义一个属性接收文件路径
#         self.data_path = data_path
#         # 定义一个属性接收工作表名称
#         self.sheetname = sheetname
#         # 使用xlrd模块打开excel表读取数据
#         self.data = xlrd.open_workbook(self.data_path)
#         # 根据工作表的名称获取工作表中的内容（方式①）
#         self.table = self.data.sheet_by_name(self.sheetname)
#         # 根据工作表的索引获取工作表的内容（方式②）
#         # self.table = self.data.sheet_by_name(0)
#         # 获取第一行所有内容,如果括号中1就是第二行，这点跟列表索引类似
#         self.keys = self.table.row_values(0)
#         # 获取工作表的有效行数
#         self.rowNum = self.table.nrows
#         # 获取工作表的有效列数
#         self.colNum = self.table.ncols

#     # 定义一个读取excel表的方法
#     def readExcel(self):
#         # 定义一个空列表
#         datas = []
#         for i in range(1, self.rowNum):
#             # 定义一个空字典
#             sheet_data = {}
#             for j in range(self.colNum):
#                 # 获取单元格数据类型
#                 c_type = self.table.cell(i,j).ctype
#                 # 获取单元格数据
#                 c_cell = self.table.cell_value(i, j)
#                 if c_type == 2 and c_cell % 1 == 0:  # 如果是整形
#                     c_cell = int(c_cell)
#                 elif c_type == 3:
#                     # 转成datetime对象
#                     date = datetime.datetime(*xldate_as_tuple(c_cell,0))
#                     c_cell = date.strftime('%Y/%d/%m %H:%M:%S')
#                 elif c_type == 4:  #boolean型
#                     c_cell = True if c_cell == 1 else False
#                 sheet_data[self.keys[j]] = c_cell
#                 # 循环每一个有效的单元格，将字段与值对应存储到字典中
#                 # 字典的key就是excel表中每列第一行的字段
#                 # sheet_data[self.keys[j]] = self.table.row_values(i)[j]
#             # 再将字典追加到列表中
#             datas.append(sheet_data)
#         # 返回从excel中获取到的数据：以列表存字典的形式返回
#         return datas

#     # def showExcel(self):
#     #     for i in range(0, self.rowNum):
#     #         for j in range(0, self.colNum):
#     #             c_cell = self.table.cell_value(i, j)
#     #             print(c_cell, end='\r')
#     #         print('\n')

# if __name__ == "__main__":
#     data_path = "Excel_test.xls"
#     sheetname = "My_Worksheet"
#     excel = ExcelData(data_path, sheetname)
#     datas = excel.readExcel()
#     pprint(datas)
#     # excel.showExcel()
