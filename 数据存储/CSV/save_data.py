#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2021-07-12
"""
import csv

''' 
@ 功能：保存数据
'''

# 普通方式
headers = ['name', 'price', 'author']
values = [
    ('三国演义', 48, '罗贯中'),
    ('红楼梦', 50, '曹雪芹'),
    ('西游记', 45, '吴承恩'),
    ('三体', 20, '刘慈欣')
]

with open('data_write.csv', 'w', encoding='utf-8', newline='') as fp:
    """一定要指定newline=''，不然会空一行"""
    writer = csv.writer(fp)
    writer.writerow(headers)
    writer.writerows(values)


# 字典方式
headers = ['name', 'price', 'author']
values = [
    {"name": '三国演义', "price": 48, "author": '罗贯中'},
    {"name": '红楼梦', "price": 50, "author": '曹雪芹'},
    {"name": '西游记', "price": 45, "author": '吴承恩'},
    {"name": '三体', "price": 20, "author": '刘慈欣'}
]
with open('data_write2.csv', 'w', encoding='utf-8', newline='') as fp:
    """一定要指定newline=''，不然会空一行"""
    writer = csv.DictWriter(fp, headers)
    writer.writerow({"name": 'name', "price": 'price', "author": 'author'})
    writer.writerow({"name": '按照一行的', "price": 48, "author": '测试'})
    writer.writerows(values)