#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
作者：CJK_Monomania
日期：2021-05-08
"""
from random import randint

''' 
@ 功能：创建Die类，模拟掷骰子
'''


class Die:
    """表示一个骰子的类"""

    def __init__(self, num_sides=6):
        """默认骰子为6面"""
        self.num_sides = num_sides

    def roll(self):
        """返回一个位于1和投资面数之间的随机值"""
        return randint(1, self.num_sides)
