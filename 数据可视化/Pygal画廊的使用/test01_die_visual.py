#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
作者：CJK_Monomania
日期：2021-05-08
"""
import pygal

from test01_die import Die

''' 
@ 功能：模拟掷骰子，并利用pygal进行可视化
'''

# 创建一个D6---6面的骰子
die = Die()

# 掷几次骰子，并将结果存储一个列表中
results = []

for roll_num in range(1000):
    result = die.roll()
    results.append(result)

# print(results)

# 分析结果
frequencies = []
for value in range(1, die.num_sides + 1):
    """遍历可能的点数（这里为1~6），计算每种点数在results中出现了多少次"""
    frequency = results.count(value)
    frequencies.append(frequency)

print(frequencies)

# 对数据进行可视化
hist = pygal.Bar()
hist.title = "Results of rolling one D6 1000 times."    # 表头
hist.x_labels = ['1', '2', '3', '4', '5', '6']          # 将D6的可能结果作为横坐标
hist.x_title = "Result"
hist.y_title = "Frequency of Result"

# 我们使用add()将一系列值添加到图表中（向它传递要给添加的值指定的标签，还有一个列表，其中包含将出现在图表中的值）
hist.add('D6', frequencies)
hist.render_to_file('die_visual.svg')   # 渲染成.svg文件（可以使用浏览器打开）
