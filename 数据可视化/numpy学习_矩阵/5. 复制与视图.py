#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2021-08-12
"""
import numpy as np

''' 
@ 功能：
'''

'''不复制'''
# a = np.arange(12)
# b = a
# print(b is a)
# b.shape = 3, 4
# print(a.shape)

'''视图与浅复制'''
# a = np.arange(12)
# a.shape = 3, 4
# print('a:', a)
# c = a.view()
# print('c:', c)
# print(c is a)
# print(c.base is a)  # c是a的数据的视图
# print('c.base:', c.base)
# print(c.flags.owndata)
# c.shape = 6, 2
# print(a.shape)  # a的形状没有改变
# c[4, 1] = 1234  # a的数据改变了
# print(a)

'''深复制'''
a = np.arange(12)
a.shape = 3, 4
d = a.copy()
print(d is a)
print(d.base is a)
d[0, 0] = 9999
print(a)
