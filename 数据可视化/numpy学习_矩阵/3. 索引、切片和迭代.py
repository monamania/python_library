#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2021-08-12
"""
import numpy as np

''' 
@ 功能：
'''

'''一维数组'''
# a = np.arange(10) ** 3
# print(a)
# print('获取索引为2的元素:', a[2])
# print('获取索引为2到5的子串:', a[2:5])
# a[:6:2] = -1000
# print('从索引0开始到索引6每隔2个(不包括6)，依次减1000:')  # 0,2,4
# print(a)
# print('逆序显示：')
# print(a[::-1])
# print('遍历')
# for i in a:
#     print(i)


'''多维数组'''


def f(x, y): return 10 * x + y


b = np.fromfunction(f, (5, 4), dtype=int)
print(b)
print('2行3列的元素（从0行0列开始）：')
print(b[2, 3])
print('第0~4行，第1列的元素（从0行0列开始）：')
print(b[0:4, 1])
print('所有行，第1列的元素（从0行0列开始）：')
print(b[:, 1])
print('第1~3行，所有列的元素（从0行0列开始）：')
print(b[1:3, :])

print('对每一行进行遍历：')
for row in b:
    print(row)

print('使用flat属性对每一个元素进行遍历：')
for element in b.flat:
    print(element)

print('...表示省略，特别是维数很多时，常用')
c = np.array([[[0, 1, 2],  # 三维数组
               [10, 12, 13]],
              [[100, 101, 102],
               [110, 112, 113]]])
print(c)
print('数组形状结构')
print(c.shape)
print('取三维数组第一维的索引为1的')
print(c[1, ...])  # 和 c[1,:,:] 、 c[1]效果相同
print('取三维数组最后一维的索引为2的')
print(c[..., 2])  # 和c[:,:,2]效果相同
