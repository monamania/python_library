#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2021-08-12
"""
import numpy as np

''' 
@ 功能：
数组的算数计算是在元素层级运算的【即各个元素分别计算】。计算结果会存在一个新创建的数组中。
点乘：np.dot
叉乘：np.cross
'''
'''基本运算'''
# print('-------直接乘除----------')
# a = np.array([20, 30, 40, 50])
# b = np.arange(4)
# print(a)
# print(b)
# c = a - b
# print(c)
# print(b ** 2)
# print(10 * np.sin(a))
# print(a < 35)
#
# print('-------向量点乘与叉乘----------')
# A = np.array([20, 30, 40, 50])
# B = np.arange(4)  # [0,1,2,3]
# # 元素相乘
# print(A * B)
# # 向量点乘
# print(A.dot(B))  # 20*0 + 30*1 + 40*2 + 50*3
# print(np.dot(A, B))
# # 向量叉乘【注意得用列表】
# x = [1, 2, 3]
# y = [4, 5, 6]
# print(np.cross(x, y))
#
# print('------矩阵乘法----------')
# A = np.array([(1, 1), (0, 1)])
# B = np.array([(2, 0), (3, 4)])
# print('矩阵A：')
# print(A)
# print('矩阵B：')
# print(B)
# print('各元素相乘： A * B=')
# print(A * B)
# print('点乘：A .* B=')
# print(A.dot(B))
# print(np.dot(A, B))
# print('叉乘：A x B=')
# print(np.cross(A, B))


'''矩阵的行列式'''
# A_matrix = np.array([[4, 6], [3, 8]])
# det = np.linalg.det(A_matrix)
# print(det)


'''矩阵的逆'''
# a = np.array([[1, 2], [3, 4]])  # 初始化一个非奇异矩阵(数组)
# print(np.linalg.inv(a))  # 对应于MATLAB中 inv() 函数
#
# # 矩阵对象可以通过 .I 更方便的求逆
# A = np.matrix(a)
# print(A.I)
# print('转置')
# print(a.T)
# print(A.T)


'''奇异阵求伪逆'''
# # 定义一个奇异阵 A
# A = np.zeros((4, 4))
# A[0, -1] = 1
# A[-1, 0] = -1
# A = np.matrix(A)
# print(A)
# # print(A.I)  将报错，矩阵 A 为奇异矩阵，不可逆
# print(np.linalg.pinv(A))   # 求矩阵 A 的伪逆（广义逆矩阵），对应于MATLAB中 pinv() 函数


'''一元操作：求和、平均、最大、最小'''
# print('-----一元操作----')
# a = np.random.random((2, 3))
# print(a)
# print('求和')
# print(a.sum())    	# 求和【cumsum为累加求和】
# print('求平均')
# print(a.mean())   	# 平均值
# print('求最大值')
# print(a.max())		# 最大值
# print('求最小值')
# print(a.min())		# 最小值
# # 指定维数
# print('-----指定维数----')
# b=np.arange(12).reshape(3,4)
# print(b)
# print('对第0维的元素求和【对列】')
# print(b.sum(axis=0)) #对第0维的元素求和
# print('对第1维的元素求和【对行】')
# print(b.sum(axis=1)) #对第1维的元素求和
# print('对第1维的元素求最小【对行】')
# print(b.min(axis=1))
# print('对第1维的元素累加求和【对行】')
# print(b.cumsum(axis=1)) #对第1维的元素累加求和


'''数学函数'''
B = np.arange(3)
print(B)
print('-----e的次方-----')
print(np.exp(B))
print('-----开根号-----')
print(np.sqrt(B))
print('-----加法-----')
C = np.array([2, -1, 4])
print(np.add(B, C))
print(B + C)
