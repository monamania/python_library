#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2021-08-12
"""
import numpy as np

''' 
@ 功能：
使用numpy进行数组的创建（向量、矩阵）—— ndarray
下列是几个`ndarray`中的重要属性:
- **ndarray.ndim**
  　数组的维数——矩阵的秩。
- **ndarray.shape**
  　数组的形状。
- **ndarray.size**
  　数组的元素个数。
- **ndarray.dtype**
  　数组的元素类型。
'''

'''np.array创建数组'''
# 创建一维数组 -- 行向量
# print('-------一维数组--------')
# a = np.array([1.1, 2.2, 3.3])
# print(a)
# print('类型：', a.dtype)
# print('-------二维数组--------')
# c = np.array([(1, 2, 3), (4.5, 5, 6)])  # 创建二维数组
# print(c)
# print('-------二维数组(显示声明类型complex复数类型)--------')
# d = np.array([(1+2j, 2), (3, 4)], dtype=complex)  # 数组的类型可以在创建时显式声明 complex-复数
# print(d)

'''zeros、ones、eye、empty创建数组'''
# print('-------全是零的数组--------')
# e = np.zeros((3, 4))
# print(e)
# print('-------全是1的数组--------')
# f = np.ones((2, 3, 4), dtype=np.int16)  # 可以更改数据类型
# print(f)
# print('-------单位矩阵--------')
# E = np.eye(4)
# print(E)
# print('-------全是随机数的数组--------')
# g = np.empty((2, 3))
# print(g)

'''
arange、linspace创建列表
np.arange(start,end,step)
np.linspace(start, stop, num=50, endpoint=True, retstep=False, dtype=None)
'''
# print('-------arange--------')
# a = np.arange(10, 30, 5)
# print(a)
# b = np.arange(0, 2, 0.3)  # 同样可以接收浮点数
# print(b)
# print('-------linspace--------')
# c = np.linspace(0, 2, 9)
# print(c)
# d = np.linspace(0, 2, 9, endpoint=False)
# print(d)


