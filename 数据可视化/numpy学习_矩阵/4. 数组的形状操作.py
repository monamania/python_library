#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2021-08-12
"""
import numpy as np

''' 
@ 功能：
numpy数组的形状操作
'''

'''更改数组形状'''
# a = np.floor(10 * np.random.random((3, 4)))
# print('铺平变为一维数组：')
# print(a.ravel())  # 返回铺平后的数组
# print('由(3，4)变为(6，2)：')
# print(a.reshape(6, 2))  # 按照指定的形状更改


'''堆砌不同的数组'''
# a = np.floor(10 * np.random.random((2, 2)))
# print(a)
# b = np.floor(10 * np.random.random((2, 2)))
# print(b)
# print('垂直方向堆砌：')
# print(np.vstack((a, b)))  # 垂直方向堆砌
# print('水平方向堆砌：')
# print(np.hstack((a, b)))  # 水平方向堆砌

'''利用newaxis插入新的维度'''
# x = np.arange(4)
# print(x)  # 初始的一维数组
# print(x[np.newaxis, :])  # 得到的二维数组
# print(x[:, np.newaxis])  # 得到的另一个二维数组


'''拆分数组'''
a = np.floor(10 * np.random.random((2, 12)))
print(a)
print(np.hsplit(a, 3))
print(np.hsplit(a, (1, 2, 3)))  # 在第一列，第二列，第三列进行划分
