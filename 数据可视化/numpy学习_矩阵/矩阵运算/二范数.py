'''numpy函数介绍'''
# np.linalg.norm(x, ord=None, axis=None, keepdims=False)
# np.linalg.norm：linalg=linear(线性)+algebra(代数)，norm则表示范数

# x：表示矩阵(也可以是一维)

# ord：范数类型

# axis：轴向
# axis=1表示按行向量处理，求多个行向量的范数
# axis=0表示按列向量处理，求多个列向量的范数
# axis=None表示矩阵范数。

# keepdims：是否保持矩阵的二维特性
# True表示保持矩阵的二维特性，False相反

'''向量'''
#  1>>>import numpy as np
#  2>>>x=np.array([1,2,3,4])
#  3>>> np.linalg.norm(x) #默认是二范数，所有向量元素绝对值的平方和再开方
#  45.477225575051661
#  5>>> np.sqrt(1**2+2**2+3**2+4**2)
#  65.477225575051661
#  7>>> np.linalg.norm(x,ord=1) #所有向量元素绝对值之和
#  810.0
#  9>>> 1+2+3+4
# 1010
# 11>>> np.linalg.norm(x,ord=np.inf) #max(abs(x_i))，所有向量元素绝对值中的最大值
# 124.0
# 13>>> np.linalg.norm(x,ord=-np.inf) #min(abs(x_i))，所有向量元素绝对值中的最小值
# 141.0

'''矩阵'''
import numpy as np

x = np.arange(1,17).reshape((4,4))
#x = x.reshape((3,3))
print(np.linalg.norm(x))  #默认是二范数，最大特征值的算术平方根

