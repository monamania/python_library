''' 施密特标准正交化 '''
# from sympy import *

# L = [Matrix([-2,1,1,1]), Matrix([1,1,-1,-1]), Matrix([-2,1,0,1])]

# o1=GramSchmidt(L)   #正交化

# print(o1)
# o2 = GramSchmidt(L, True)  # 标准化
# print('--------------------------')
# print(o2)


''' 求矩阵的逆、转置、特征值 '''
import numpy as np
 
kernel = np.matrix([[2,0,0],[1,1,1],[1,-1,3]])
print(kernel)
print('矩阵的逆\n',np.linalg.inv(kernel)) #矩阵的逆


eigvials = np.linalg.eigvals(kernel)
print('特征值', eigvials)

eig = np.linalg.eig(kernel)
print('特征向量\n', eig[1],'\n')

P = eig[1]
P_ni_T = np.linalg.inv(P).T #矩阵的逆的转置

print('矩阵的逆的转置\n',P_ni_T,'\n') 