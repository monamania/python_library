import numpy as np
from sympy import Matrix
import sympy

print('\n矩阵乘积--------------------------')
#矩阵乘积
matrix1 = [[0,0,0],[-1,1,-1],[0,0,0]]
matrix2 = [[0,0,0],[-1,1,-1],[0,0,0]]
print("矩阵相乘（不是内积）：\n",np.dot(matrix1,matrix2),"\n")

#求特征值、矩阵的逆
print('\n求特征值、矩阵的逆--------------------------')
matrix1 = [[7/2,6/2,9/2],[-5/2,-6/2,-7/2],[-7/2,-4/2,-7/2]]
eigvials = np.linalg.eigvals(matrix1)
print('特征值', eigvials, '\n')

print('矩阵的逆\n',np.linalg.inv(matrix1)) #矩阵的逆

#求Jordan标准型
print('\n求Jordan标准型--------------------------')
A = np.array([[2,0,0],[1,1,1],[1,-1,3]])
a = Matrix(A)
P, Ja = a.jordan_form()

print('P：', P,'\n')
print(np.matrix(P))
print('Jordan标准型:', Ja,'\n')
print( np.matrix(Ja))


#Jordan矩阵验证
print('\nJordan矩阵验证--------------------------')
matrixP = [[1,-1,0],[2,-1,0],[-1,0,1]]
matrixA = [[-1,1,0],[-4,3,0],[1,0,2]]
matrixP_inv = np.linalg.inv(matrixP)
print('P的逆\n',matrixP_inv) #矩阵的逆
jordan = np.dot(matrixP_inv, matrixA)
jordan = np.dot(jordan, matrixP)
print("Jordan矩阵：\n", jordan, "\n")
#print(Matrix(jordan))

"""
python 矩阵有两种形式：array 和 matrix 对象（它们的区别在这里就不说了），下面介绍相关乘法
1. np.multiply

对 array 和 matrix 对象的操作相同
(1) a 和 b 维度相同
都是每行对应元素相乘（即对应内积的第一步，不求和）

>>> a = np.array([[1,2],[1,2]])
>>> a*a
>>> array([[1, 4],
       [1, 4]])
（2）对于两个矩阵元素 a 和 b 维度不一的情况（array 和 matrix 对象都适用），则需将相应的行和列进行扩充，需要扩充的行或列的维度必须为 1。
对列扩充则补 1， 对行扩充则将第一行复制到每一行。比如，a：3 * 1， b: 1 * 2，则 a 扩充为 3 * 2，b 扩充为 3 * 2。
如下所示：

>>> a = np.array([[1],[1],[1]])
>>> b = np.array([1,2])
>>> np.multiply(a, b)
>>> array([[1, 2],
       [1, 2],
       [1, 2]])
       
（3）a 和 b为标量：则标量直接相乘
2. *号
（1）对于 matrix 对象，代表矩阵乘法（维度必须满足相应规则）;
（2）对于array对象，则是每行对应元素相乘。当 array 对象的 shape 不同时（matrix 对象不行） ，其规则和 np.multiply 一样；
3. np.matmul
该函数对 array 和 matrix 对象的操作是不一样的。
（1）对于 matrix 对象，对应矩阵乘法，对象维度必须满足矩阵乘法规则。
（2）对于 array 对象，对应内积，但对象维度必须相同，不支持维度扩展。
（3）不支持标量运算。

在array 中，与 multiply 一样，每行对应元素相乘
4. np.dot
对于matrix 对象，对应矩阵乘法。
对于两个 array 类型的元素：a，b，有如下可能:
（1）a 和 b 都是一维 array，那么 dot 就是它们的内积（点乘）；
（2）a 和 b 都是二维 array，那么 dot 就是它们的矩阵乘积（即按矩阵乘法规则来计算），也可以用 matmul 或 a @ b；
（3）如果a 和 b 都是标量（scalar），那么 dot 就是两个数的乘积，也可以用 multiply 或 a * b；
（4）若 a：N * D，b：1 * D，那么 dot 为 a 的每一行和 b （只有一行）的 内积；
>>>a = np.array([[1,2], [3, 4]])
>>>b = np.array([1, 2])
>>>np.dot(a, b)
>>>array([ 5, 11])
"""


# import numpy as np

# # 2-D array: 2 x 3
# two_dim_matrix_one = np.array([[1 ,-3,-2],[-1-5-2],[2,14,6]])
# # 2-D array: 3 x 2
# two_dim_matrix_two = np.array([[1 ,-3,-2],[-1-5-2],[2,14,6]])

# two_multi_res = np.dot(two_dim_matrix_one, two_dim_matrix_two)
# print('two_multi_res: %s' %(two_multi_res))

# # 1-D array
# one_dim_vec_one = np.array([1, 2, 3])
# one_dim_vec_two = np.array([4, 5, 6])
# one_result_res = np.dot(one_dim_vec_one, one_dim_vec_two)
# print('one_result_res: %s' %(one_result_res))