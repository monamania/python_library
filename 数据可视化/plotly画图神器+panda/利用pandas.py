#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2022-04-11
"""
import pandas as pd
import cufflinks as cf
from IPython.display import display,HTML
cf.set_config_file(sharing='public',theme='ggplot',offline=True)
# 运行命令 cf.getThemes() 以获取所有可用的主题
# print(cf.getThemes())
''' 
@ 功能：
https://blog.csdn.net/qq_34160248/article/details/122466879
https://blog.csdn.net/cxd3341/article/details/105016903

https://www.jb51.net/article/240151.htm
'''
df_population = pd.read_csv('population_total.csv')


# 删除空值，重新调整，然后选择几个国家来测试交互式绘图。
# dropping null values
df_population = df_population.dropna()
# reshaping the dataframe
df_population = df_population.pivot(index='year', columns='country',
                                    values='population')
# selecting 5 countries
df_population = df_population[['United States', 'India', 'China',
                               'Indonesia', 'Brazil']]

# 绘制可视化
"""line"""
df_population.iplot(kind='line',xTitle='Years', yTitle='Population',title='Population (1955-2020)')

"""bar"""
df_population_2020 = df_population[df_population.index.isin([2020])]
df_population_2020 = df_population_2020.T
df_population_2020.iplot(kind='bar', color='lightgreen', xTitle='Years', yTitle='Population',title='Population in 2020')

# 多个变量分组的条形图
# filter years out
df_population_sample = df_population[df_population.index.isin([1980, 1990, 2000, 2010, 2020])]
# plotting
df_population_sample.iplot(kind='bar', xTitle='Years',yTitle='Population')


"""箱形图"""
df_population['United States'].iplot(kind='box', color='green',
                                     yTitle='Population')

df_population.iplot(kind='box', xTitle='Countries',yTitle='Population')

"""直方图"""
df_population[['United States', 'Indonesia']].iplot(kind='hist',xTitle='Population')

"""饼图"""
# transforming data
df_population_2020 = df_population_2020.reset_index()
df_population_2020 =df_population_2020.rename(columns={2020:'2020'})# plotting
df_population_2020.iplot(kind='pie', labels='country',values='2020', title='Population in 2020 (%)')


"""散点图"""
df_population.iplot(kind='scatter', mode='markers')