#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
作者：CJK_Monomania
日期：2021-05-06
重要参考！！https://blog.csdn.net/qq_42871249/article/details/104781281
同时展示不同大小的图，显示中文，常用函数介绍等：https://blog.csdn.net/Leon_winter/article/details/90202239
一个图的不同区域！！ https://www.cnblogs.com/SupremeBoy/p/12247671.html
"""
import matplotlib.pyplot as plt
import numpy as np
from itertools import cycle  ##python自带的迭代器模块
colors = cycle('bgrcmykbgrcmykbgrcmykbgrcmyk')    ##python自带的迭代器模块
bight = cycle(( '-','solid','--',':','-.','dashed','dashdot','dotted'))    ##python自带的迭代器模块
markers = ['.', ',', '1', '2', '3', '4', '+', 'x', '|', '_', 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 'o', 'v', '^', '<', '>', '8', 's', 'p', '*', 'h', 'H', 'D', 'd', 'P', 'X']
# https://zhuanlan.zhihu.com/p/157350270

# plt.autoscale(axis='x',tight=True)  # 坐标缩放
# plt.grid()   # 网格
plt.rcParams['font.sans-serif'] = ['SimHei']  # 指定默认字体
# font = {'family': 'SimHei',
#         'weight': 'bold',
#         'size': '16'}
#
# plt.rc('font', **font)  # 步骤一（设置字体的更多属性）
plt.rcParams['axes.unicode_minus'] = False  # 解决负数坐标显示问题
plt.rcParams['figure.figsize'] = (15.0, 8.0)   # 单位是inches
# plt.xticks(rotation=120)   # 设置横坐标显示的角度，角度是逆时针，自己看
# tick_spacing = 3    # 设置密度，比如横坐标9个，设置这个为3,到时候横坐标上就显示 9/3=3个横坐标，
# ------------------------------------------------
''' 
# plt.gcf()：获取当前图表
# plt.gca()：获取当前子图
# 按esc退出
plt.gcf().canvas.mpl_connect(
            'key_release_event',
            lambda event: [sys.exit(0) if event.key == 'escape' else None])
此程序用于绘制——简单的折线图
实现修改标题、线条粗细、横纵坐标、字体大小的功能
最全教程：https://www.delftstack.com/zh/howto/matplotlib/
可变参数，属性说明：https://zhuanlan.zhihu.com/p/259878627
参数说明：
重要参考！！https://blog.csdn.net/qq_42871249/article/details/104781281
颜色配置，及legend图例大小 https://blog.csdn.net/mmc2015/article/details/72829107
legend图例：https://blog.csdn.net/helunqu2017/article/details/78641290/
https://zhuanlan.zhihu.com/p/111108841
https://blog.csdn.net/hanling1216/article/details/84964884   处理时间 改变坐标格式
横众坐标间隔显示：https://www.cnblogs.com/z1141000271/p/11628079.html

fig1, ax = plt.subplots()
# 解决ticklabel字重叠——横众坐标间隔显示：
xticks = list(range(0, len(X), 12))  # 这里设置的是x轴点的位置（40设置的就是间隔了）
xlabels = [X[x] for x in xticks]  # 这里设置X轴上的点对应在数据集中的值（这里用的数据为totalSeed）
xticks.append(len(X))
xlabels.append(X[-1])
ax.set_xticks(xticks)
ax.set_xticklabels(xlabels, rotation=12)

'''

input_values = [1, 2, 3, 4, 5]  # 输入值，相当于横坐标
squares = np.arange(6)
squares *= squares
squares = squares[1:]  # 生成1，4，8，16，25的数据，输出值，相当于纵坐标

fig = plt.figure()
fig.suptitle('整体测试', fontsize=18)
plt.subplot(131)  # 设置子图位置
plt.plot(input_values, squares, color='b', linestyle='-', linewidth=5, label='测试曲线')  # lw参数设置线宽，lw是line width的缩写

# 设置图表标题，并给坐标轴加上标签
plt.title("Square Numbers", fontsize=24)
plt.xlabel("Value", fontsize=14)
plt.ylabel("Square of Value", fontsize=14)

# plt.gca().invert_xaxis()  # x值改为由大到小
# 设置轴的刻度标记的大小 axis（轴）——x、y、both ；
plt.tick_params(axis='both', labelsize=14)
# plt.tick_params(axis='x', labelsize=14)
# plt.tick_params(axis='y', labelsize=14)

plt.legend(labels=['测试'], loc='best')  # 0则为指定legend的位置左上角
plt.show()



# 宋体     SimSun
# 黑体     SimHei
# 微软雅黑     Microsoft YaHei
# 微软正黑体     Microsoft JhengHei
# 新宋体     NSimSun
# 新细明体     PMingLiU
# 细明体     MingLiU
# 标楷体     DFKai-SB
# 仿宋     FangSong
# 楷体     KaiTi
# 隶书：LiSu
# 幼圆：YouYuan
# 华文细黑：STXihei
# 华文楷体：STKaiti
# 华文宋体：STSong
# 华文中宋：STZhongsong
# 华文仿宋：STFangsong
# 方正舒体：FZShuTi
# 方正姚体：FZYaoti
# 华文彩云：STCaiyun
# 华文琥珀：STHupo
# 华文隶书：STLiti
# 华文行楷：STXingkai
# 华文新魏：STXinwei
