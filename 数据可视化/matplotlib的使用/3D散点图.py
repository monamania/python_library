#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2021-09-30
"""
import numpy as np
import matplotlib.pyplot as mp
from mpl_toolkits.mplot3d import axes3d

''' 
@ 功能： 绘制三维散点图
'''


# 1.生成数据
n = 200
x = np.random.normal(0, 1, n)
y = np.random.normal(0, 1, n)
z = np.random.normal(0, 1, n)
d = np.sqrt(x ** 2 + y ** 2 + z ** 2) # 距离

# 2.绘制图片
mp.figure("3D Scatter", facecolor="lightgray")
ax3d = mp.gca(projection="3d")  # 创建三维坐标

mp.title('3D Scatter', fontsize=20)
ax3d.set_xlabel('x', fontsize=14)
ax3d.set_ylabel('y', fontsize=14)
ax3d.set_zlabel('z', fontsize=14)
mp.tick_params(labelsize=10)

ax3d.scatter(x, y, z, s=20, c=d, cmap="jet", marker="o")

mp.show()

