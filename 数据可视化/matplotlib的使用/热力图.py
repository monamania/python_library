#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2022-11-26
"""

''' 
@ 功能：
Python-Seaborn热图绘制：https://blog.csdn.net/sunchengquan/article/details/78573244
使用pyheatmap.heatmap绘制热力图：https://blog.csdn.net/weixin_43289135/article/details/104669856
python画热力图：https://blog.csdn.net/ltochange/article/details/118416330
3D：https://blog.csdn.net/weixin_51723388/article/details/125348664
'''
# -*- coding: utf-8 -*-
from pyheatmap.heatmap import HeatMap
import numpy as np
N = 10000
X = np.random.rand(N) * 255  # [0, 255]
Y = np.random.rand(N) * 255
data = []
for i in range(N):
  tmp = [int(X[i]), int(Y[i]), 1]
  data.append(tmp)

heat = HeatMap(data)
heat.clickmap(save_as="1.png") #点击图
heat.heatmap(save_as="2.png") #热图
