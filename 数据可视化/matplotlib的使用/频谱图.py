#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2021-10-16
"""

''' 
@ 功能：
https://www.delftstack.com/zh/howto/matplotlib/matplotlib.pyplot.specgram-in-python/
'''

""" 使用 matplotlib.pyplot.specgram() 方法绘制频谱图 """
'''
matplotlib.pyplot.specgram(x, 
                           NFFT=None, 
                           Fs=None, 
                           Fc=None, 
                           detrend=None, 
                           window=None, 
                           noverlap=None, 
                           cmap=None, 
                           xextent=None, 
                           pad_to=None, 
                           sides=None, 
                           scale_by_freq=None, 
                           mode=None, 
                           scale=None, 
                           vmin=None, 
                           vmax=None, *,
                            data=None, 
                            **kwargs)
'''
import math
import numpy as np
import matplotlib.pyplot as plt

dt=0.0001
w=2
t=np.linspace(0,5,math.ceil(5/dt))
A=20*(np.sin(3 * np.pi * t))

plt.specgram(A,Fs=1)
plt.title('Spectrogram Using matplotlib.pyplot.specgram() method')
plt.show()
# 它使用 matplotlib.pyplot.specgram() 方法为函数 A=20sin(3*np.pi*t) 创建一个频谱图。该方法中的参数 fs 代表采样频率。


""" 使用 scipy.signal.spectrogram() 方法绘制频谱图 """
import math
import numpy as np
import matplotlib.pyplot as plt
from scipy import signal

dt=0.0001
w=2
t=np.linspace(0,5,math.ceil(5/dt))
A=2*(np.sin(1 * np.pi *300* t))

f, t, Sxx = signal.spectrogram(A, fs=1, nfft=514)
plt.pcolormesh(t, f, Sxx)
plt.ylabel('Frequency')
plt.xlabel('Time')
plt.title('Spectrogram Using scipy.signal.spectrogram() method')
plt.show()

# 它使用 scipy.signal.spectrogram() 方法为函数 A=2sin(300*np.pi*t) 创建一个频谱图。该方法中的参数 fs 代表采样频率，ntft 代表所用 FFT 的长度。
#
# 该方法返回三个值 f、t 和 Sxx。f 代表采样频率数组，t 代表采样时间数组，Sxx 代表 A 的频谱图。
# 这种方法并不能生成输入信号的频谱图。我们可以使用 matplotlib.pyplot.colormesh() 来生成图形。