#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
作者：CJK_Monomania
日期：2021-05-07
"""
# 在每次做决策时都使用choice()来决定使用哪种选择
from random import choice

''' 
@ 功能：此程序生成随机漫步的数据
@ 随机漫步：每次行走都完全是随机的，没有明确的方向，结果是由一系列随机决策决定的
会被test03_rw_visual所导入，并用matplotlib以引人瞩目的方式呈现数据
'''


class RandomWalk:
    """一个生成随机漫步数据的类"""

    def __init__(self, num_points=5000):
        """初始化随机漫步的属性 num_points表示随机漫步的次数，其他两个表示随机漫步经过的x和y坐标"""
        self.num_points = num_points
        # 所有随机漫步都始于(0, 0)
        self.x_values = [0]
        self.y_values = [0]

    def fill_walk(self):
        """计算随机漫步包含的所有点"""
        # 不断漫步，直到列表达到指定的长度
        while len(self.x_values) < self.num_points:
            # 决定前进方向以及沿这个方向前进的距离
            # x_direction = choice([1, -1])  # 向右走还是向左走 1向右 -1向左
            # x_distance = choice([0, 1, 2, 3, 4])  # 沿指定的方向走多远
            # x_step = x_direction * x_distance
            #
            # y_direction = choice([1, -1])  # 向上走还是向下走 1向上 -1向下
            # y_distance = choice([0, 1, 2, 3, 4])  # 沿指定的方向走多远
            # y_step = y_direction * y_distance
            # 改进后
            x_step = self.get_step()
            y_step = self.get_step()

            # 拒绝原地踏步
            if x_step == 0 and y_step == 0:
                continue

            # 计算下一个点的x和y值
            next_x = self.x_values[-1] + x_step
            next_y = self.y_values[-1] + y_step
            self.x_values.append(next_x)
            self.y_values.append(next_y)

    @staticmethod
    def get_step() -> int:
        """计算前进步长，用于确定每次漫步的距离和方向"""
        tmp_direction = choice([1, -1])  # 向右走还是向左走 1向右 -1向左
        tmp_distance: int = choice([0, 1, 2, 3, 4])  # 沿指定的方向走多远
        return tmp_direction * tmp_distance
