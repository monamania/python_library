#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2021-10-12
"""
# from  PIL import Image
import cv2
import matplotlib.image as mpimg  # mpimg 用于读取图片
# import matplotlib.image as mpimg # mpimg 用于读取图片
import matplotlib.pyplot as plt  # plt 用于显示图片
import numpy as np

plt.rcParams['font.sans-serif'] = ['SimHei']  # 指定默认字体
plt.rcParams['axes.unicode_minus']=False       #显示负号
''' 
@ 功能：
https://www.cnblogs.com/yinxiangnan-charles/p/5928689.html

图像的膨胀（白的变大）与腐蚀（黑的变大）：https://blog.csdn.net/weixin_39128119/article/details/84172385
图像腐蚀与膨胀：https://www.cnblogs.com/angle6-liu/p/10704970.html
'''

"=================  1. 读取数据  ================="
map = mpimg.imread('map_Simple.png')  # 读取和代码处于同一目录下的 lena.png
# 此时 lena 就已经是一个 np.array 了，可以对它进行任意处理
print(map.shape)  # (512, 512, 3)

"=================  2. 原始图像  ================="
plt.figure()
plt.imshow(map)  # 显示图片
plt.title("原始图像")

"=================  2. 图像腐蚀  erosion 黑的变大  ================="
plt.figure()
## b.设置卷积核5*5
# kernel = np.array([[0,1,0],
#    [1,1,1],
#    [0,1,0]])
kernel = np.ones((20, 20), np.uint8)

## c.图像的腐蚀，默认迭代次数
erosion = cv2.erode(map, kernel)
plt.imshow(erosion)  # 显示图片
plt.title("腐蚀后")
"=================  3. 图像膨胀  Dilation 白的变大  ================="
plt.figure()
## b.设置卷积核5*5
# kernel = np.array([[0,1,0],
#    [1,1,1],
#    [0,1,0]])
kernel = np.ones((20, 20), np.uint8)

# ## c.图像的腐蚀，默认迭代次数
# erosion = cv2.erode(map, kernel)

## c.图像的膨胀，默认迭代次数
dst = cv2.dilate(map,kernel)

plt.imshow(dst)  # 显示图片
plt.title("膨胀后")
"=================  其他  ================="
# plt.axis('off') # 不显示坐标轴
plt.show()
#
# img = Image.open('map.png')
# img.show()
# im_array = np.array(img)
# plt.imshow(im_array)
