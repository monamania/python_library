#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
作者：CJK_Monomania
日期：2021-05-07
"""
from time import sleep

import matplotlib.pyplot as plt

from test03_random_walk import RandomWalk

''' 
@ 功能：此程序生成随机漫步的数据，导入test03_random_walk，并用matplotlib以引人瞩目的方式呈现数据
@ 注意：plt.show()是阻塞方式的，无法动态显示
动态显示可以用
* plt.ion() : 交互式绘图模式（动态显示）
* plt.pause(0.05): 显示绘图
# plt.ioff()
'''

'''基础版本'''
# while True:
#     """多次循环，模拟多个随机漫步"""
#     # 创建一个RandomWalk实例，并将其包含的点都绘制出来
#     rw = RandomWalk()
#     rw.fill_walk()
#
#     plt.scatter(rw.x_values, rw.y_values, s=15)
#     plt.show()            # 静态显示
#     # plt.ion()           # 交互式绘图模式（动态显示）
#     # plt.pause(0.05)     # 间隔一段时间更新
#
#     keep_running = input("Make another walk? (y/n): ")
#     if keep_running == 'n':
#         break


'''
进阶版本：
        给点着色——渐变后面的步数颜色更深
        突出起点和终点
        隐藏坐标轴
        设置窗口尺寸
'''
while True:
    """多次循环，模拟多个随机漫步"""
    # 创建一个RandomWalk实例，并将其包含的点都绘制出来
    rw = RandomWalk()
    rw.fill_walk()

    # 设置绘图窗口的尺寸 参数：dpi表示系统分辨率（默认为80），figsize表示窗口大小
    plt.figure(dpi=80, figsize=(10, 6))

    # 给点着色——渐变，后面的步数颜色更深
    point_numbers = list(range(rw.num_points))
    plt.scatter(rw.x_values, rw.y_values, c=point_numbers, cmap=plt.cm.Blues,
                edgecolor='none', s=15)

    # 突出起点和终点
    plt.scatter(0, 0, c='green', edgecolors='none', s=100)
    plt.scatter(rw.x_values[-1], rw.y_values[-1], c='red', edgecolors='none',
                s=100)

    # # 隐藏坐标轴
    # plt.axes().get_xaxis().set_visible(False)
    # plt.axes().get_yaxis().set_visible(False)
    plt.xticks([])  # 去掉x轴
    plt.yticks([])  # 去掉y轴
    plt.axis('off')  # 去掉坐标轴

    plt.show()  # 静态显示
    # plt.ion()           # 交互式绘图模式（动态显示）
    # plt.pause(0.05)     # 显示绘图
    sleep(0.1)

    keep_running = input("Make another walk? (y/n): ")
    if keep_running == 'n':
        break
