#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2021-11-07
"""
import matplotlib.pyplot as plt

''' 
@ 功能：
plt.boxplot   或者 pd的DataFrom中df.boxplot
https://blog.csdn.net/weixin_40683253/article/details/87857194


参数说明：https://www.cnblogs.com/shanger/p/13041426.html
'''
