#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
作者：CJK_Monomania
日期：2021-05-07
"""
import matplotlib.pyplot as plt

''' 
https://blog.csdn.net/gaocui883/article/details/108136081

此程序用于绘制——简单的散点图
实现设置样式的功能
c的取值可改变填充颜色，edgecolor属性用于设置边界颜色
b——blue
c——cyan
g——green
k——black
m——magenta
r——red
w——white
y——yellow
'''

# x_values = [1, 2, 3, 4, 5]
# # y_values = [1, 4, 9, 16, 25]
x_values = list(range(1, 1001))
y_values = [x ** 2 for x in x_values]

# 并设定这个点的数据点轮廓颜色为无（相当于无轮廓），像素大小为40，数据点颜色为红色（默认为蓝色）
# plt.scatter(x_values, y_values, edgecolor='none', s=40, c='red')
# # 与上面相同，唯一不同的就是颜色采用RGB来定义
# plt.scatter(x_values, y_values, c=(0.8, 0, 0), edgecolor='none', s=40)
# # 根据每个点的y值来设置其颜色映射来绘数据点————有一种渐变的效果！！！
plt.scatter(x_values, y_values, c=y_values, cmap=plt.cm.Blues, edgecolor='none', s=40)
# # 用于绘制一个点，并设定这个点的像素大小为200
# plt.scatter(2, 4, s=200)

# 设置图表标题并给坐标轴加上标签
plt.title("Square Numbers", fontsize=24)
plt.xlabel("Value", fontsize=14)
plt.ylabel("Square of Value", fontsize=14)

# 设置轴的刻度标记的大小 axis（轴）——x、y、both ；which——
plt.tick_params(axis='both', which='major', labelsize=14)

# 设置每个坐标轴的取值范围，参数：x的最小值，x的最大值，y的最小值，y的最大值
plt.axis([0, 1100, 0, 1100000])

plt.show()
"""
要让程序自动将图表保存到文件中，可将对plt.show()的调用替换为对plt.savefig()的调用：
@ 注意是替换
@ 参数1：指定文件名，参数2：指定将图标多余的空白区域裁剪掉
"""
# plt.savefig('squares_plot.png', bbox_inches='tight')
