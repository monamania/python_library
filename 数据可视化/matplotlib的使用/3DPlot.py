#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2021-09-30
"""

''' 
@ 功能：绘制3D曲面图
https://blog.csdn.net/weixin_45875105/article/details/107119204
https://blog.csdn.net/kiripeng/article/details/88571122
将绘图代码接口化，是自己学习的一份记录
'''
"""
    绘制3D曲面图
"""
import numpy as np
import matplotlib.pyplot as mp
from mpl_toolkits.mplot3d import Axes3D

# 准备数据
n = 1000
x, y = np.meshgrid(np.linspace(-3, 3, n),
                   np.linspace(-3, 3, n))

z = (1 - x / 2 + x ** 5 + y ** 3) * \
    np.exp(-x ** 2 - y ** 2)

# 绘制图片
fig = mp.figure("3D Surface", facecolor="lightgray")
mp.title("3D Surface", fontsize=18)

# 设置为3D图片类型
ax3d = Axes3D(fig)
# ax3d = mp.gca(projection="3d")    # 同样可以实现

ax3d.set_xlabel("X")
ax3d.set_ylabel("Y")
ax3d.set_zlabel("Z")
mp.tick_params(labelsize=10)

ax3d.plot_surface(x, y, z, cstride=20, rstride=20, cmap="jet")

mp.show()


# if __name__=='__main__':
