[toc]

# 协程

## 1. 理解

**子程序（或者称为函数）**：在所有语言中都是层级调用，比如A调用B，B在执行过程中又调用了C，C执行完毕返回，B执行完毕返回，最后是A执行完毕。所以**子程序调用是通过栈实现的，一个线程就是执行一个子程序**。子程序调用总是一个入口，一次返回，调用顺序是明确的。

**协程**：协程看上去也是子程序，但执行过程中，在子程序内部可中断，然后转而执行别的子程序，在适当的时候再返回来接着执行。

注意：在一个子程序中中断，去执行其他子程序，不是函数调用，有点类似CPU的中断

```python
# 协程在执行A的过程中，可以随时中断去执行B，B也可以在执行过程中，再去执行A；
# 但是在A中是没有调用B的
def A():
    print('1')
    print('2')
    print('3')

def B():
    print('x')
    print('y')
    print('z')

#结果
1
2
x
y
3
z
```

- **特点**：协程属于一个线程内执行；即一个线程内可以运行多个协程
- **协程的优势**
  - 执行效率高；【子程序切换由程序自身控制，没有线程切换的开销，和多线程比，线程数量越多，协程的性能优势就越明显】
  - 不需要多线程的锁机制；【由于只有一个线程，不存在同时写变量的冲突，在协程中控制共享资源不加锁，只需要判断状态就好了】
- 如何利用多核CPU：多进程+协程



## 2. 协程的实现

python通过`generator（生成器）`实现协程

在generator中，我们不但可以通过`for`循环来迭代，还可以不断调用`next()`函数获取由`yield`语句返回的下一个值。

但是Python的`yield`不但可以返回一个值，它还可以接收调用者发出的参数。

来看例子：

传统的生产者-消费者模型是一个线程写消息，一个线程取消息，通过锁机制控制队列和等待，但一不小心就可能死锁。

如果改用协程，生产者生产消息后，直接通过`yield`跳转到消费者开始执行，待消费者执行完毕后，切换回生产者继续生产，效率极高：

```python
def consumer():
    print("[Consumer] Started")
    r = ''
    while True:
        n = yield r  # send(None)后代码执行到这里
        print("[Consumer] Yield return: %s" % str(n))
        if not n:
            return
        print('[CONSUMER] Consuming %s...' % n)
        r = '200 OK'


def produce(c):
    initY = c.send(None)  # 若不发送None，会得到`TypeError: can't send non-None value to a just-started generator`
    # 这个None并不会作为第一次yield的返回值，它的作用仅仅是让生成器启动
    # consumer启动, 打印"[Consumer] Started"，yield出r的初始值
    print("[PRODUCER] Init Y: %s" % str(initY))
    n = 0
    while n < 5:
        n = n + 1
        print('[PRODUCER] Producing %s...' % n)
        r = c.send(n)
        print('[PRODUCER] Consumer return: %s' % r)
    c.close()

c = consumer()
produce(c)

#-------------------------------------
执行结果：

[Consumer] Started
[PRODUCER] Init Y: 
[PRODUCER] Producing 1...
[Consumer] Yield return: 1
[CONSUMER] Consuming 1...
[PRODUCER] Consumer return: 200 OK
[PRODUCER] Producing 2...
[Consumer] Yield return: 2
[CONSUMER] Consuming 2...
[PRODUCER] Consumer return: 200 OK
[PRODUCER] Producing 3...
[Consumer] Yield return: 3
[CONSUMER] Consuming 3...
[PRODUCER] Consumer return: 200 OK
[PRODUCER] Producing 4...
[Consumer] Yield return: 4
[CONSUMER] Consuming 4...
[PRODUCER] Consumer return: 200 OK
[PRODUCER] Producing 5...
[Consumer] Yield return: 5
[CONSUMER] Consuming 5...
[PRODUCER] Consumer return: 200 OK
```

注意到`consumer`函数是一个`generator`，把一个`consumer`传入`produce`后：

1. 首先调用`c.send(None)`启动生成器；【`c.send(None)`，其功能类似于``next(c)`】
2. 然后，一旦生产了东西，通过`c.send(n)`切换到`consumer`执行；
3. `consumer`通过`yield`拿到消息，处理，又通过`yield`把结果传回；
4. `produce`拿到`consumer`处理的结果，继续生产下一条消息；
5. `produce`决定不生产了，通过`c.close()`关闭`consumer`，整个过程结束。



> `return`的确是跳出函数，这是为`consumer`生成器提供了另一种终止方式`c.send(None)`。
>
> 当生成器**第一次**调用`c.send(None)`时，作用是预激，即**启动生成器**，效果等同于`next(c)`，`consumer`函数执行到`yield r`后停止，生成第一个值''，只有在生成器启动之后，调用`send`方法才会赋值给`n`；
>
> 当生成器**第二次**及以后调用`c.send(None)`时，作用是**终止生成器**，效果等同于`c.close()`，但有所不同的是，`c.send(None)`会抛出`StopIteration`异常，且异常中包含返回值，而`c.close()`方法则不会抛出异常。
>
> 
>
> **在示例中，从未执行至此**。是的，
>
> > `c.send(None)`
>
> 时也不会执行到这个条件判断里。为何？
>
> 因为程序执行`c.send(None)`的目的是生成器完成预激，让其执行到`n = yield r`，以便下一次生成器可以接受到send过来的参数。
>
> 于是当下一次 `r = c.send(1)`调用生成器时 ，`consumer`会执行赋值语句
>
> > `n = yield r`，即 n = 1
>
> **注意**：这时的yield r里的r还是上一次的空值。 而后继续执行下去，这时n已为1，当然不会被return 接着循环到下一次的 n = yield r，执行完 `yield r(r = '200 ok')`，然后停留在赋值语句前（即 n = ...），直到下次`c.send(n)`完成赋值后继续执行。
>
> 所以不要被`c.send(None)`迷惑，yield抛出生成器的值后，就会挂起。n也不会通过赋值获得yield r的抛出值，n获得的只能是调用者发出的send参数。
>
> 理解python理解步骤用`pdb`细步命令s可以一目了然，虽然麻烦但方便理解python执行逻辑。



整个流程无锁，由一个线程执行，`produce`和`consumer`协作完成任务，所以称为“协程”，而非线程的抢占式多任务。

最后套用Donald Knuth的一句话总结协程的特点：

“子程序就是协程的一种特例。”