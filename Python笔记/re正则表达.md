[toc]

# 正则表达式

## 字符组

字符组：[字符组]

![img](https://gitee.com/monamania/picBed/raw/master/img/20210613160656)

## 字符（元字符）

![img](https://gitee.com/monamania/picBed/raw/master/img/20210613160744)

```
\w 包括：字母、数字、下划线；如果验证要求只允许输入：“数字和字母” 不能单独 \w 解决，
还要考虑到下划线，所以只能写成[0-9a-zA-Z] 
```

```python
import re
# 输入数字或者字母则显示输入正确

strd = input("please:")
if re.search("^[0-9a-zA-Z]+$", strd):
    print("输入正确")
else:
    print("错误")
```

![img](https://gitee.com/monamania/picBed/raw/master/img/20210613161147)

```
# [/s/S] 、[/d/D]、[/w/W] 这类的可以匹配任意字符
```

## 量词（个数）`*+？{}`

![在这里插入图片描述](https://gitee.com/monamania/picBed/raw/master/img/20210613161309)

![img](https://gitee.com/monamania/picBed/raw/master/img/20210613161330)

> `.`表示除了换行符外的任意字符



## 边界字符（也属于元字符）`.^$`

![在这里插入图片描述](https://gitee.com/monamania/picBed/raw/master/img/20210613161351)

> `.`表示除了换行符外的任意字符

```python
 有一些有特殊意义的元字符进入字符组中会恢复它本来的意义如： . | [ ] ( )
^ 匹配字符串的开始 如: ^x ,表示字符串必须以x开头, 注意:与 [^ ] 意义不同
$ 匹配字符串的结束 如: x$ , 表示字符串必须以x结尾

^,$ : 如果字符有多行, 且在 re.M 模式下, ^,$ 会将每一行当作一个字符串，
	即换行后会重新匹配

\A 匹配字符串的开始, 与^ 类似, 如: \Ax ,表示字符串必须以x开头
\Z 匹配字符串的结尾, 与$类似, 如: x\Z,表示字符串必须以x结尾
# \A ,\Z , 在re.M模式下不会影响多行匹配

\b 用来匹配单词的结尾, 如: er\b 表示单词是否以 er结束
\B 用来匹配非单词的结尾, 如: er\B 表示单词中含有er,单独的以er结尾的词不行
# 注意:\b 需要转意 即写成 \\b

import re

res=re.findall("海.","海燕海桥海石")   # ['海燕', '海桥', '海石']
res=re.findall("^海.","海燕海桥海石")  # 海燕
res=re.findall("海.$","海燕海桥海石")  # 海石
res=re.findall("^海.$","海燕海桥海石") # []
res=re.findall("^海.$","海燕") # ['海燕']  这就能明白为什么 . 也属于边界字符了

print(res)
```

## 分组（）与或 ｜［^］

身份证号码是一个长度为15或18个字符的字符串，如果是15位则全部由数字组成，首位不能为0；
如果是18位，则前17位全部是数字，末位可能是数字或x，下面我们尝试用正则来表示：

![img](https://gitee.com/monamania/picBed/raw/master/img/20210613162130)

```python
# 匹配任意一个邮箱 min@163.com
# x|y 表示匹配 x 或者 y
# (xyz) 加上括号表示将 xyz 看成一个整体
mailPattern = "\w+@\w+\.\w+"
# mailPattern = "(\w+@\w+\.((com)|(cn)))" # 在re.match / re.search 下能找出

```

```python
# 匹配日期 1800-01-01 --- 2020-12-31
# 1899 1999 -- 2020
# 0-[1-9] 1[0-1-2]
# 0-[1-9] 1 2 - 0-9 3 0,1


result=re.search("^((1[89]\d{2})|(20[01]\d)|(2020))-((0[1-9])|(1[012]))-((0[1-9])|([12]\d)|(3[01]))$","2020-12-31")
# 注意括号分组，或 | 的时候，每一种可能给一个括号，
# 判断的每一项（年、月、日）再给一个括号
print(result)


import re

strd = input("请按格式输入出生年月日：")
if re.search("((1[89]\d{2})|(20[01]\d)|(2020))-((0[1-9])|(1[012]))-((0[1-9])|([12]\d)|(3[01]))", strd):
    print("输入正确")
else:
    print("输入格式有误，或者数值有误")
```

注意：转义字符的问题

![img](https://gitee.com/monamania/picBed/raw/master/img/20210613162523)

## 贪婪模式和非贪婪模式

> 贪婪匹配：在满足匹配时，匹配尽可能长的字符串，默认情况下，采用贪婪匹配

```python
import re

res = re.findall("a?", "min is a good man")  # 非贪婪模式
# 结果：['', '', '', '', '', '', '', 'a', '', '', '', '', '', '', '', 'a', '', '']
res = re.findall("a?", "min is a good maan")  # 非贪婪模式
# 结果：['', '', '', '', '', '', '', 'a', '', '', '', '', '', '', '', 'a', 'a', '', '']
res = re.findall("a*", "min is a good maan")  # 贪婪模式
# 结果：['', '', '', '', '', '', '', 'a', '', '', '', '', '', '', '', 'aa', '', '']
res = re.findall("a+", "min is a good maan")  # 贪婪模式
# 结果：['a','aa']
res = re.findall("a{3}", "min is a good maaan")  # 贪婪模式
# 结果：['aaa']
res = re.findall("a{3}", "min is a good maaaan")  # 贪婪模式
# 结果：['aaa']
res = re.findall("a{3,5}", "min is a good maaaaan")  # 贪婪模式
# 结果：['aaaaa'] 包后
res = re.findall("a{3,}", "min is a good maaaaaaan")  # 贪婪模式
# 结果：['aaaaaaa']
res = re.findall("a{,5}", "min is a good maaaaaaan")  # 贪婪模式
# 结果：['', '', '', '', '', '', '', 'a', '', '', '', '', '', '', '', 'aaaaa', 'aa', '', '']
print(res)
```

> 单独出现 ？ 非贪婪模式
> 出现 的第二个范围量词是“ ？” 就会变成 非贪婪模式



> 常用的非贪婪匹配Pattern

```python
*? 重复任意次，但尽可能少重复
+? 重复1次或更多次，但尽可能少重复
?? 重复0次或1次，但尽可能少重复
{n,m}? 重复n到m次，但尽可能少重复
{n,}? 重复n次以上，但尽可能少重复
```



> . * ? 的用法

```python
. 是任意字符
* 是取 0 至 无限长度
? 是非贪婪模式。
合在一起就是：取尽量少的任意字符，一般不会这么单独写，他大多用在：
.*?x

就是取前面任意长度的字符，直到一个 x 出现
```



# re模块下的常用方法

> 三种模式：
> `re.match()` 是从源字符串的 **起始位置**开始查找**一个**匹配项，如果在开头没有匹配到就返回空
> `re.search()` 是从源字符串中找到**一个**匹配项
> `re.findall()` 是从源字符串中找到**所有**的匹配项

```
flag：真正的含义
	re.I 	使匹配对大小写不敏感
 	re.M 	多行匹配，是否影响 ^ 和 $  
	re.S    使 . 匹配包含换行符在内的任意字符
```

```python
# re.M 实例——进行多行匹配
import re

res = re.findall("^min","min is a good man\nmin is a good man") # ['min']
res = re.findall("^min", "min is a good man\nmin is a good man", re.M)  # ['min', 'min']

# 如果后面，没有加上re.M 表示看做一行字符串（不会进行换行），只能找到前面的一个“min”,加了，加了能找到两个

res = re.findall("\Amin","min is a good man\nmin is a good man")  # ['min']
res = re.findall("\Amin","min is a good man\nmin is a good man",re.M) # ['min', 'min']


# print(res)
```

## `re.match()` 

```python
re.match(pattern,string,flag) # 是从源字符串的 起始位置开始查找一个匹配项，如果在开头没有匹配到就返回空

     pattern 要进行匹配的正则表达式
     string 表示的是源字符串
     flag 标记, 可以不写
         re.I 使匹配对大小写不敏感
         re.M 多行匹配，是否影响 ^ 和 $
         re.S 使 . 匹配包含换行符在内的任意字符

# 如果匹配成功会返回一个对象
# 如果匹配失败 会 返回None
# 可以根据 结构是否为 None 来判断是否匹配成功
# 可以通过这个变量的group方法来获取结果;
# 如果没有匹配到,会返回None,此时如果使用group会报错

```

```python
>>> 匹配手机号码
import re
phone_number = input('please input your phone number ： ')
if re.match('^(13|14|15|18)[0-9]{9}$',phone_number): # 如果成功则不是None
        print('是合法的手机号码')
else:
        print('不是合法的手机号码')
        

```

## `re.search()`

```python
re.search(pattern,string,flag) # 是从源字符串中（从左往右）找到第一个匹配项

     pattern 要进行匹配的正则表达式
     string 表示的是源字符串
     flag 标记, 可以不写
         re.I 使匹配对大小写不敏感
         re.M 多行匹配,是否影响 ^ 和 $
         re.S 使 . 匹配包含换行符在内的任意字符
         
     # 多用于表单验证    

res = re.search("www","www.baidu.com")
res = re.search("www","ww.baiduwww.com")
res = re.search("www","www.baiduwww.com")
print(res)
print（res.group()）
# 只匹配从左到右的第一个,得到的不是直接的结果,而是一个变量,
# --需要通过这个变量的group方法来获取结果;
# 如果没有匹配到,会返回None,此时如果使用group会报错
```

## `re.findall()`！！最常用

```
re.findall(pattern,string,flag) # 是从源字符串中找到所有的匹配项

     pattern 要进行匹配的正则表达式
     string 表示的是源字符串
     flag 标记, 可以不写
         re.I 使匹配对大小写不敏感
         re.M 多行匹配,是否影响 ^ 和 $
         re.S 使 . 匹配包含换行符在内的任意字符


# findall的结果是列表的形式,会将找到的多个结果放到列表中去
# 注意: 如果找不到,会返回一个空列表
# 注意：不能直接使用 group 方法
res = re.findall("www","www.baidu.com")
res = re.findall("wwwa","www.baiduwww.com") # 结果：[]
print(res)
print(type(res))
```

> **==`findall` 的优先级查询：==**

```python
import re

ret = re.findall('www.(baidu|oldboy).com', 'www.oldboy.com')
print(ret)  # ['oldboy'] 这是因为findall会优先把匹配结果组里内容返回

# 如果想要匹配结果,取消权限即可，格式：在分组内加上 ？：即 (?:正则表达式)

ret = re.findall('www.(?:baidu|oldboy).com', 'www.oldboy.com')
print(ret)  # ['www.oldboy.com']

```

```python
>>> 三种模式练习
import re

ret = re.findall('a', 'eva egon yuan') # 返回所有满足匹配条件的结果,放在列表里
print(ret) # 结果 : ['a', 'a']

ret = re.search('a', 'eva egon yuan').group()
print(ret) # 结果 : 'a'
# 函数会在字符串内 查找模式匹配,只到找到第一个匹配然后返回一个包含匹配信息的对象,
# 该对象可以通过调用group()方法得到匹配的字符串,如果字符串没有匹配，则返回None。

ret = re.match('a', 'abc').group() # 同search,不过在字符串开始处进行匹配
print(ret)
#结果 : 'a'

```

## `re.split()`字符串分割

```python
# re.split(patter,string,maxsplit=0,flags=0)
# 参数：
# pattern,正则表达式, 即以某个正则来拆分
# string, 被拆分的源字符串
# maxsplit=0, 最大拆分次数
# flags=0 标识


# 正则表达式来拆分字符串
strData ="wen1 is2 a3 old4 man5"
lis_re = re.split(" ",strData)
# list_re=re.split("\d",strData)
# 结果：['wen', ' is', ' a', ' old', ' man', '']

# list_re=re.split("[0-9]",strData)
# 结果：['wen', ' is', ' a', ' old', ' man', '']

# list_re=re.split(" +",strData) #注意，+前面有个空格
# print(list_re) 结果：['wen1', 'is2', 'a3', 'old4', 'man5']
```

```python
# 如果想保留用来作切割标准的字符串：只需给它添加分组即可
ret = re.split('\d+','alex83taibai40egon25')
print(ret) # ['alex', 'taibai', 'egon', ''] # 最后一次切割 留下末尾一个空
ret = re.split('(\d+)','alex83taibai40egon25aa')
print(ret) # ['alex', '83', 'taibai', '40', 'egon', '25', 'aa']
```

## `re.sub()` 与`re.subn()`  替换

```python
str= "蔡徐篮球 是 xxx\n蔡徐坤歌手 是 yyy\n 肖战 是 帅哥"

# re.sub()
# 参数 
	pattern
	repl, 用来替换的新字符串
	string
	count=0, 替换的次数,默认是全部替换
	flags=0  # 不能修改，默认可以不写
	# 默认是 re.M 模式，会换行匹配所有

res = re.sub("蔡徐.{,2}","肖战",str)
'''
肖战 是 xxx
肖战手 是 yyy
 肖战 是 帅哥
'''

# subn与sub功能一致, 但是 sub是返回替 换后的新字符串,

# subn返回的是元组,元组有2个元素, 元素1代表替换后的新字符串, 元素2代表的替换的次数
res = re.subn("蔡徐.{,2}","肖战",str)
# ('肖战 是 xxx\n肖战手 是 yyy\n 肖战 是 帅哥', 2) 
    
print(res)
```

## 提取与分组！！有用

正则表达式不仅仅有强大的匹配功能,还有强大提取功能
`(xyz) 将xyz看作一个整体`
`(xyz) 将xyz看成一个小组`

- `re.search()` 模式下的分组

```python
import re

s = '<a>wahaha</a>'  # 标签语言 html 网页
ret = re.search('<(\w+)>(\w+)</(\w+)>',s)
print(ret.group(0))  # 返回所有的结果  输出 <a>wahaha</a>
print(ret.group(1))  # 数字参数代表的是取对应分组中的内容  输出 a
print(ret.group(2))  # 输出 wahaha
print(ret.group(3))  # 输出 a
```

- `re.findall()` 模式的分组

```python
# 为了findall也可以顺利取到分组中的内容,有一个特殊的语法,就是优先显示分组中的内容
import re

s = '<a>wahaha</a>'  # 标签语言 html 网页
ret = re.findall('(\w+)',s)
print(ret) #['a', 'wahaha', 'a']
ret = re.findall('>(\w+)<',s)
print(ret) #['wahaha']

strData = "010-34545546"
numberPattern = "(\d{3})-(\d{8})" 结果：[('010', '34545546')]
numberPattern = "((\d{3})-(\d{8}))" 结果：[('010-34545546', '010', '34545546')]  -- 列表内为一整个元组
# findall会将所有的组作为一个元组的元素，放在列表中，组是靠括号来分的
res = re.findall(numberPattern,strData)
print(res) # print（res[0]） 结果：('010', '34545546')
```

- `re.findall`模式下，取消分组优先

```python
ret = re.findall('\d+(\.\d+)?','1.234*4.3')
print(ret) # 结果是：['.234', '.3']
# 为什么只显示了 小数位，因为findall优先显示分组中的内容

# 取消分组优先做法：在分组内加上？：(?:正则表达式)
ret = re.findall('\d+(?:\.\d+)?','1.234*4.3')
print(ret)


>>>可以给每一个分组取一个别名, 格式：(?P<别名>正则)
str_date = "010-34545546"
numberPattern = "(?P<First>\d{3})-(?P<Last>\d{8})"
res = re.match(numberPattern,str_date)
print(res)

# group可以提取完整匹配的字符
# () 可以将字符串分成多个组, 分组顺序是由外到里, 由前到后
print(res.group()) # 默认是0
print(res.group(1)) # 第一组
print(res.group(2)) # 第二组
#print(res.group(3)) # 第三组

>>>可以给每一个分组取一个别名, 格式：(?P<别名>正则)
# 获取的时候可以直接根据别名来获取
print(res.group("First"))
print(res.group("Last"))

# groups 将() 包裹的内容进行分组提取, 将所有的分组作为元组的元素,并作为结果返回
     # 它只看括号
res=re.match("\d{3}-\d{8}",strDate) # 这里没括号，输出结果为：()空元祖
print(res.groups())
```

- 分组命名的用法

```python
s = '<a>wahaha</b>' # 加入div标签不一致
pattern = '<(\w+)>(\w+)</(\w+)>'
ret = re.search(pattern,s)
# print(ret.group(1) == ret.group(3)) False

# 使用前面的分组 要求使用这个名字的分组和前面同名分组中的内容匹配的必须一致
pattern = '<(?P<tab>\w+)>(\w+)</(?P=tab)>'
ret = re.search(pattern,s)
print(ret) #False
```

## 总结（`re.compile`）

```python
import re

ret = re.findall('a', 'eva egon yuan')  # 返回所有满足匹配条件的结果,放在列表里
print(ret) # 结果 : ['a', 'a']

ret = re.search('a', 'eva egon yuan').group()
print(ret) # 结果 : 'a'
# 函数会在字符串内查找模式匹配,只到找到第一个匹配然后返回一个包含匹配信息的对象,该对象可以
# 通过调用group()方法得到匹配的字符串,如果字符串没有匹配，则返回None。

ret = re.match('a', 'abc').group()  # 同search,不过尽在字符串开始处进行匹配
print(ret)
# 结果 : 'a'

ret = re.split('[ab]', 'abcd')  # 先按'a'分割得到''和'bcd',在对''和'bcd'分别按'b'分割
print(ret)  # ['', '', 'cd']

ret = re.sub('\d', 'H', 'eva3egon4yuan4', 1) # 将数字替换成'H'，参数1表示只替换1个
print(ret) # evaHegon4yuan4

ret = re.subn('\d', 'H', 'eva3egon4yuan4') # 将数字替换成'H'，返回元组(替换的结果,替换了多少次)
print(ret)

obj = re.compile('\d{3}')  # 将正则表达式编译成为一个 正则表达式对象，规则要匹配的是3个数字
ret = obj.search('abc123eeee') # 正则表达式对象调用search，参数为待匹配的字符串
print(ret.group())  # 结果 ： 123

import re
ret = re.finditer('\d', 'ds3sy4784a')   # finditer返回一个存放匹配结果的迭代器
print(ret)  # <callable_iterator object at 0x10195f940>
print(next(ret).group())  # 查看第一个结果
print(next(ret).group())  # 查看第二个结果
print([i.group() for i in ret])  # 查看剩余的左右结果

'''
先编译后使用

re模块的进阶 : 时间/空间
compile 节省你使用正则表达式解决问题的时间
编译 正则表达式 编译成 字节码
在多次使用的过程中 不会多次编译
ret = re.compile('\d+')   # 已经完成编译了
print(ret)

res = ret.findall('alex83taibai40egon25')
print(res) # 类型是“list”
res = ret.search('sjkhk172按实际花费928')
print(res.group())



finditer 节省你使用正则表达式解决问题的空间/内存
ret = re.finditer('\d+','alex83taibai40egon25')
#print（ret）这是一个迭代器
for i in ret:
     print(i.group())


findall 返回列表 找所有的匹配项
search  匹配就 返回一个变量,通过group取匹配到的第一个值,不匹配就返回None,group会报错
match   相当于search的正则表达式中加了一个'^'

spilt   返回列表,按照正则规则切割,默认匹配到的内容会被切掉
sub/subn 替换,按照正则规则去寻找要被替换掉的内容,subn返回元组,第二个值是替换的次数

compile  编译一个正则表达式,用这个结果去search match findall finditer 能够节省时间
finditer 返回一个迭代器,所有的结果都在这个迭代器中,需要通过循环+group的形式取值 能够节省内存
'''
```



# 其他字符串的分割与组合方法

## `str.split()`内置函数

```python
# 语法
str.split(s=' ', num=-1)[n]

参数说明：
s：指定的分割符，不写默认按照空格分割
num：表示分割次数。如果指定了参数num，就会将字符串分割成num+1个子字符串，并且每一个子字符串可以赋给新的变量；-1表示全部分割
n：表示选取分割后的第几个分片
```

例子

```python
#定义一个字符串str1
>>> str1 = "3w.gorly.test.com.cn"
 
#使用默认分隔符分割字符串str1
>>> print str1.split()
['3w.gorly.test.com.cn']
 
#指定分隔符为'.'，进行分割字符串str1
>>> print str1.split('.')
['3w', 'gorly', 'test', 'com', 'cn']
 
#指定分隔符为'.'，并且指定切割次数为0次
>>> print str1.split('.',0)
['3w.gorly.test.com.cn']
 
#指定分隔符为'.'，并且指定切割次数为1次
>>> print str1.split('.',1)
['3w', 'gorly.test.com.cn']
 
#指定分隔符为'.'，并且指定切割次数为2次
>>> print str1.split('.',2)
['3w', 'gorly', 'test.com.cn']
 
#这种分割等价于不指定分割次数str1.split('.')情况
>>> print str1.split('.',-1)
['3w', 'gorly', 'test', 'com', 'cn']
 
#指定分隔符为'.'，并取序列下标为0的项
>>> print str1.split('.')[0]
3w
 
#指定分隔符为'.'，并取序列下标为4的项
>>> print str1.split('.')[4]
cn
```

## `os.path.split(‘PATH’)`分割路径与文件名

参数说明：
PATH指一个文件所在的绝对路劲

例子

```python
>>> import os
>>> print os.path.split("d:\test\a.txt")
('d:', '\test\x07.txt')
>>> print os.path.split('d:/test/a.txt')
('d:/test', 'a.txt')
>>> print os.path.split('d:\\test\\a.txt')
('d:\\test', 'a.txt')

# 从上面的结果可以看出，如果我们路劲写成d:\test\a.txt，是得不到我们想要的结果，必须将再加一个’\’来转义第二个’\’才行，或者直接写成d:/test/a.txt这样。
```

##  **`str.join(seq)`**将序列组合成字符串函数

```python
语法：s.join(seq)
参数说明：
s：给定的连接符
seq：代表要连接的序列，如list、tuple、str的序列
```

实例普通字符串的连接（只能针对字符或字符串进行连接）

```python
>>> '-'.join("abdcd")
'a-b-d-c-d'
>>> list1 = ['a','b','c']
>>> ''.join(list1)
'abc'
```

