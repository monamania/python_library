[toc]

## 问题描述

​		在openpyxl对Excel读写操作过程中，发现内存没有马上释放，如果得多次读取大文件，内存爪机，后续代码就无法运行。

尝试：各种wb.save()或者with open等途径无法解决。

发现：因为python的回收机制，导致内存无法马上释放，于是乎就有了

```python
import gc 
del wb,ws      #wb为打开的工作表
gc.collect()     #马上内存就释放了。
```



## 1. Garbage collection垃圾回收

​		现在的高级语言如java，c#等，都采用了垃圾收集机制，而不再是c，c++里用户自己管理维护内存的方式。自己管理内存极其自由，可以任意申请内存，但如同一把双刃剑，为**大量内存泄露，悬空指针**等bug埋下隐患。 对于一个字符串、列表、类甚至数值都是对象，且定位简单易用的语言，自然不会让用户去处理如何分配回收内存的问题。 python里也同java一样采用了垃圾收集机制，不过不一样的是: python采用的是引用计数机制为主，分代收集两种机制为辅的策略

**引用计数机制：**

python里每一个东西都是对象，它们的核心就是一个结构体：PyObject

```C++
typedef struct_object {
    int ob_refcnt;
    struct_typeobject *ob_type;
} PyObject;
```

PyObject是每个对象必有的内容，其中`ob_refcnt`就是做为引用计数。当一个对象有新的引用时，它的`ob_refcnt`就会增加，当引用它的对象被删除，它的`ob_refcnt`就会减少

```C++
#define Py_INCREF(op)   ((op)->ob_refcnt++) //增加计数
#define Py_DECREF(op) \ //减少计数
    if (--(op)->ob_refcnt != 0) \
        ; \
    else \
        __Py_Dealloc((PyObject *)(op))
```

当引用计数为0时，该对象生命就结束了。

**引用计数机制的优点：**

- 简单
- 实时性：一旦没有引用，内存就直接释放了。不用像其他机制等到特定时机。
  - 实时性还带来一个好处：处理回收内存的时间分摊到了平时。

**引用计数机制的缺点：**

- 维护引用计数消耗资源
- 循环引用

```python
list1 = []
list2 = []
list1.append(list2)
list2.append(list1)
```

​		<font color="pink">list1与list2相互引用，如果不存在其他对象对它们的引用，list1与list2的引用计数也仍然为1，所占用的内存永远无法被回收，这将是致命的</font>。 对于如今的强大硬件，缺点1尚可接受，但是循环引用导致内存泄露，注定python还将引入新的回收机制。(标记清除和分代收集)



## 2. Ruby和python的内存管理区别

**程序源码**

python

```python
class Node:
    def __init__(self, val):
        sefl.value = val
        
print(Node(1))
print(Node(2))
```

Ruby

```ruby
class Node
    def initialize(val)
        @value = val
    end
end

p Node.new(1)
p Node.new(2)
```

### 2.1 Ruby的对象分配

​		当我们执行上面的Node.new(1)时，Ruby到底做了什么？Ruby是如何为我们创建新的对象的呢？ 出乎意料的是它做的非常少。实际上，早在代码开始执行前，Ruby就提前创建了成百上千个对象，并把它们串在链表上，名曰：<font color="blue">可用列表</font>。下图所示为可用列表的概念图：

![img](https://img-blog.csdn.net/20180327190814526?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM3NjE2MDY5/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)

​		想象一下每个白色方格上都标着一个"未使用预创建对象"。当我们调用 Node.new ,Ruby只需取一个预创建对象给我们使用即可：

![img](https://img-blog.csdn.net/20180327190836385?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM3NjE2MDY5/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)

​		上图中左侧灰格表示我们代码中使用的当前对象，同时其他白格是未使用对象。

> 请注意：无疑我的示意图是对实际的简化。实际上，Ruby会用另一个对象来装载字符串"ABC",另一个对象装载Node类定义，还有一个对象装载了代码中分析出的抽象语法树等等……

如果我们再次调用 `Node.new`，Ruby将递给我们另一个对象：

![img](https://img-blog.csdn.net/2018032719085954?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM3NjE2MDY5/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)


​		这个简单的用链表来预分配对象的算法已经发明了超过50年，而发明人这是赫赫有名的计算机科学家John McCarthy，一开始是用Lisp实现的。Lisp不仅是最早的函数式编程语言，在计算机科学领域也有许多创举。其一就是利用垃圾回收机制自动化进行程序内存管理的概念。

​		标准版的Ruby，也就是众所周知的"Matz's Ruby Interpreter"(MRI),所使用的GC算法与McCarthy在1960年的实现方式很类似。无论好坏，Ruby的垃圾回收机制已经53岁高龄了。像Lisp一样，Ruby预先创建一些对象，然后在你分配新对象或者变量的时候供你使用。

### 2.2 Python 的对象分配

​		尽管由于许多原因Python也使用可用列表(用来回收一些特定对象比如 list)，但在为新对象和变量分配内存的方面Python和Ruby是不同的。

例如我们用Pyhon来创建一个Node对象：

![img](https://img-blog.csdn.net/20180327190933149?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM3NjE2MDY5/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)



​		与Ruby不同，当创建对象时Python立即向操作系统请求内存。(Python实际上实现了一套自己的内存分配系统，在操作系统堆之上提供了一个抽象层。但是我今天不展开说了。)

当我们创建第二个对象的时候，再次像OS请求内存：

![img](https://img-blog.csdn.net/20180327190951937?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM3NjE2MDY5/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)

## 2.3 python的循环引用问题

​		在Python中，每个对象都保存了一个称为引用计数的整数值，来追踪到底有多少引用指向了这个对象。无论何时，如果我们程序中的一个变量或其他对象引用了目标对象，Python将会增加这个计数值，而当程序停止使用这个对象，则Python会减少这个计数值。一旦计数值被减到零，Python将会释放这个对象以及回收相关内存空间。

​		从六十年代开始，计算机科学界就面临了一个严重的理论问题，那就是针对引用计数这种算法来说，如果一个数据结构引用了它自身，即如果这个数据结构是一个循环数据结构，那么某些引用计数值是肯定无法变成零的。为了更好地理解这个问题，让我们举个例子：

![img](https://img-blog.csdn.net/20180327204718913?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM3NjE2MDY5/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)


​	我们有一个"构造器"(在Python中叫做 __init__ )，在一个实例变量中存储一个单独的属性。在类定义之后我们创建两个节点，ABC以及DEF，在图中为左边的矩形框。两个节点的引用计数都被初始化为1，因为各有两个引用指向各个节点(n1和n2)。

​	现在，让我们在节点中定义两个附加的属性，next以及prev：

![img](https://img-blog.csdn.net/20180327204735583?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM3NjE2MDY5/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)




​	跟Ruby不同的是，Python中你可以在代码运行的时候动态定义实例变量或对象属性。我们设置 n1.next 指向 n2，同时设置 n2.prev 指回 n1。现在，我们的两个节点使用循环引用的方式构成了一个双向链表。同时请注意到 ABC 以及 DEF 的引用计数值已经增加到了2。这里有两个指针指向了每个节点：首先是 n1 以及 n2，其次就是 next 以及 prev。

​		现在，假定我们的程序不再使用这两个节点了，我们将 n1 和 n2 都设置为null(Python中是None)。

![img](https://img-blog.csdn.net/20180327204750967?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM3NjE2MDY5/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)




​		好了，Python会像往常一样将每个节点的引用计数减少到1。

​		请注意在以上刚刚说到的例子中，我们以一个不是很常见的情况结尾：我们有一个“孤岛”或是一组未使用的、互相指向的对象，但是谁都没有外部引用。换句话说，我们的程序不再使用这些节点对象了，所以我们希望Python的垃圾回收机制能够足够智能去释放这些对象并回收它们占用的内存空间。但是这不可能，因为所有的引用计数都是1而不是0。Python的引用计数算法不能够处理互相指向自己的对象。

详情参考：[(Python3 的垃圾回收机制（GC）](https://blog.csdn.net/qq_37616069/article/details/79717704)

## 3. gc模块

​		gc module是python垃圾回收机制的接口模块，可以通过该module启停垃圾回收、调整回收触发的阈值、设置调试选项。使用gc module、objgraph可以定位内存泄露，定位之后，解决很简单。
​		如果没有禁用垃圾回收，那么Python中的内存泄露有两种情况：要么是对象被生命周期更长的对象所引用，比如global作用域对象；要么是循环引用中存在`__del__`

​		垃圾回收比较耗时，因此在对性能和内存比较敏感的场景也是无法接受的，如果能解除循环引用，就可以禁用垃圾回收。使用gc module的DEBUG选项可以很方便的定位循环引用，解除循环引用的办法要么是手动解除，要么是使用weakref



## 参考链接

- [(Python3 的垃圾回收机制（GC）](https://blog.csdn.net/qq_37616069/article/details/79717704)
- [使用gc、objgraph干掉python内存泄露与循环引用！ - xybaby - 博客园 (cnblogs.com)](https://www.cnblogs.com/xybaby/p/7491656.html)
- [gc --- 垃圾回收器接口 — Python 3.10.1 文档](https://docs.python.org/zh-cn/3/library/gc.html)

