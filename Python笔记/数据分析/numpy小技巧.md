[Python 中计算欧几里得距离 | D栈 - Delft Stack](https://www.delftstack.com/zh/howto/numpy/calculate-euclidean-distance/)



[Python 中的曲线曲率 | D栈 - Delft Stack](https://www.delftstack.com/zh/howto/numpy/curvature-formula-numpy/)



[Python Numpy 中的协方差 | D栈 - Delft Stack](https://www.delftstack.com/zh/howto/numpy/python-covariance/)



[Python 中 NumPy 数组的一位有效编码 | D栈 - Delft Stack](https://www.delftstack.com/zh/howto/numpy/one-hot-encoding-numpy/)



[教程 | D栈 - Delft Stack](https://www.delftstack.com/zh/tutorial/)





[算法 | D栈 - Delft Stack](https://www.delftstack.com/zh/tutorial/algorithm/)

