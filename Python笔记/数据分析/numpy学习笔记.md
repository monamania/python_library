[toc]

手册：[Routines — NumPy v1.21 Manual](https://numpy.org/doc/stable/reference/routines.html)

https://github.com/cxylpy/self_tutorial/blob/master/numpy/quickstart/Quickstart.md

# 基本概念

Numpy 的核心是连续的多维数组。

Numpy中的数组叫做`np.ndarray`，也可以使用别名`np.array`。

但这里的np.array与Python标准库中的array是不同的。 下列是几个`ndarray`中的重要属性：

- **ndarray.ndim**
  　数组的维数【0表示列的那一维，1表示行的那一维】。
- **ndarray.shape**
  　数组的形状。
- **ndarray.size**
  　数组的元素个数。
- **ndarray.dtype**
  　数组的元素类型。

例子

```python
import numpy as np
data=np.arange(15).reshape(3,5)
print(data)
print(data.shape)
print(data.ndim)
print(data.size)
print(data.dtype.name)

#---------输出----------#
[[ 0  1  2  3  4]
 [ 5  6  7  8  9]
 [10 11 12 13 14]]
# 形状
(3, 5)
# 维数【0表示列的那一维，1表示行的那一维】
2
# 元素个数
15
# 元素类型
int32
```



# 数组的创建（向量、矩阵）

[cei()、linspace()、arrange()、full()、eye()、empty()、random()的区别](https://www.cnblogs.com/wmy-ncut/p/9915681.html)

- **`np.array()`——配合Python的元组、列表创建**

```python
import numpy as np
# 创建一维数组 -- 行向量
print('-------一维数组--------')
a = np.array([1.1, 2.2, 3.3])
print(a)
print('类型：', a.dtype)
print('-------二维数组--------')
c = np.array([(1, 2, 3), (4.5, 5, 6)])  # 创建二维数组
print(c)
print('-------二维数组(显示声明类型complex复数类型)--------')
d = np.array([(1+2j, 2), (3, 4)], dtype=complex)  # 数组的类型可以在创建时显式声明 complex-复数
print(d)


#==========输出=============#
-------一维数组--------
[1.1 2.2 3.3]
类型： float64
-------二维数组--------
[[1.  2.  3. ]
 [4.5 5.  6. ]]
-------二维数组(显示声明类型complex复数类型)--------
[[1.+2.j 2.+0.j]
 [3.+0.j 4.+0.j]]
```

- **`np.zeros()`——创建全是0的数组**
- **`np.ones()` 创建全是1的数组**
- **`np.eye(n)`创建单位矩阵**
- **`np.empty()` 创建初始值是==随机数==的数组。**

```python
''''zeros、ones、empty创建数组'''
print('-------全是零的数组--------')
e = np.zeros((3, 4))
print(e)
print('-------全是1的数组--------')
f = np.ones((2, 3, 4), dtype=np.int16)  # 可以更改数据类型
print(f)
print('-------单位矩阵--------')
E = np.eye(4)
print(E)
print('-------全是随机数的数组--------')
g = np.empty((2, 3))
print(g)

#==========输出=============#
-------全是零的数组--------
[[0. 0. 0. 0.]
 [0. 0. 0. 0.]
 [0. 0. 0. 0.]]
-------全是1的数组--------
[[[1 1 1 1]
  [1 1 1 1]
  [1 1 1 1]]

 [[1 1 1 1]
  [1 1 1 1]
  [1 1 1 1]]]
-------单位矩阵--------
[[1. 0. 0. 0.]
 [0. 1. 0. 0.]
 [0. 0. 1. 0.]
 [0. 0. 0. 1.]]
-------全是随机数的数组--------
[[6.23042070e-307 1.89146896e-307 6.33628451e-307]
 [2.22518251e-306 1.33511969e-306 1.33511290e-306]]
```

- **`np.arange(start,end,step)`——创建列表【指定步长】**
- **`np.linspace(start, stop, num=50, endpoint=True, retstep=False, dtype=None)`——创建列表【指定个数--浮点数常用】**

```python
'''
arange、linspace创建列表
np.arange(start,end,step)
np.linspace(start, stop, num=50, endpoint=True, retstep=False, dtype=None)
'''
print('-------arange--------')
a = np.arange(10, 30, 5)
print(a)
b = np.arange(0, 2, 0.3)  # 同样可以接收浮点数
print(b)
print('-------linspace--------')
c = np.linspace(0, 2, 9)
print(c)
d = np.linspace(0, 2, 9, endpoint=False)
print(d)

#==========输出=============#
-------arange--------
[10 15 20 25]
[0.  0.3 0.6 0.9 1.2 1.5 1.8]
-------linspace--------
[0.   0.25 0.5  0.75 1.   1.25 1.5  1.75 2.  ]
[0.         0.22222222 0.44444444 0.66666667 0.88888889 1.11111111
 1.33333333 1.55555556 1.77777778]
```

- `numpy.random.randint(low, high=None, size=None, dtype='l')`：返回一个随机整数矩阵
  - 范围：从低（包括）到高（不包括），即[low, high)
  - 若没写参数high值则：[0, low)
  - dtype: dtype(可选)：想要输出的格式。如`int64`、`int`等等
-  `np.random.random((3,2))`：返回一个3行4列的矩阵
- **通过自定义函数创建数组：`fromfunction`**

```python
def f(x, y): return 10 * x + y


b = np.fromfunction(f, (5, 4), dtype=int)
print(b)

#==========输出=============#
[[ 0  1  2  3]
 [10 11 12 13]
 [20 21 22 23]
 [30 31 32 33]
 [40 41 42 43]]
```



## 基本运算

数组的算数计算是在元素层级运算的【即各个元素分别计算】。计算结果会存在一个新创建的数组中。

#### **直接使用`+、-、*、/`**：各个元素分别计算

```python
a = np.array([20, 30, 40, 50])
b = np.arange(4)  	# [0,1,2,3]
print(a)   			# [20, 30, 40, 50]
print(b)   			# [0, 1, 2, 3]
print(a - b)		# [20 29 38 47]
print(b ** 2)       # [0 1 4 9]
print(10 * np.sin(a)) 
print(a < 35)		# [ True  True False False]
```

#### 向量点乘 与 叉乘

```python
A = np.array([20, 30, 40, 50])
B = np.arange(4)  	# [0,1,2,3]
# 元素相乘
print(A*B)
# 向量点乘
print(A.dot(B))     # 20*0 + 30*1 + 40*2 + 50*3
print(np.dot(A, B))
# 向量叉乘【注意得用列表】
x = [1, 2, 3]
y = [4, 5, 6]
print(np.cross(x, y))  # 得到一个同时垂直于x和y的向量

#==========输出=============#
[  0  30  80 150]
260
260
[-3  6 -3]
# 相当于 -3i + 6j -3k
```

#### 矩阵乘法：使用dot

```python
print('------矩阵乘法----------')
A = np.array([(1, 1), (0, 1)])
B = np.array([(2, 0), (3, 4)])
print('矩阵A：')
print(A)
print('矩阵B：')
print(B)
print('各元素相乘： A * B=')
print(A * B)
print('点乘：A .* B=')
print(A.dot(B))
print(np.dot(A, B))
print('叉乘：A x B=')
print(np.cross(A, B))

#==========输出=============#
------矩阵乘法----------
矩阵A：
[[1 1]
 [0 1]]
矩阵B：
[[2 0]
 [3 4]]
各元素相乘： A * B=
[[2 0]
 [0 4]]
点乘：A .* B=
[[5 4]
 [3 4]]
[[5 4]
 [3 4]]
叉乘：A x B=
[-2 -3]
```

#### 矩阵的行列式

```python
'''矩阵的行列式'''
A_matrix = np.array([[4, 6], [3, 8]])
det = np.linalg.det(A_matrix)
print(det)

#==========输出=============#
14.000000000000004
```

#### 矩阵的逆

```python
a = np.array([[1, 2], [3, 4]])  # 初始化一个非奇异矩阵(数组)
print(np.linalg.inv(a))  # 对应于MATLAB中 inv() 函数

# 矩阵对象可以通过 .I 更方便的求逆
A = np.matrix(a)
print(A.I)
print('转置')
print(a.T)
print(A.T)

#==========输出=============#
[[-2.   1. ]
 [ 1.5 -0.5]]
[[-2.   1. ]
 [ 1.5 -0.5]]
转置
[[1 3]
 [2 4]]
[[1 3]
 [2 4]]
```

#### 奇异阵求伪逆

```python
'''奇异阵求伪逆'''
# 定义一个奇异阵 A
A = np.zeros((4, 4))
A[0, -1] = 1
A[-1, 0] = -1
A = np.matrix(A)  # 矩阵对象
print(A)
# print(A.I)  将报错，矩阵 A 为奇异矩阵，不可逆
print(np.linalg.pinv
```

#### 其他：矩阵的迹、解方程组、计算特征值

```python
import numpy as np
a = np.array([[1.0, 2.0], [3.0, 4.0]])
print(a)
a.transpose()
np.linalg.inv(a)
u = np.eye(2) # unit 2x2 matrix; "eye" represents "I"
j = np.array([[0.0, -1.0], [1.0, 0.0]])
np.dot (j, j) # 点积
np.trace(u)  # 矩阵的迹
y = np.array([[5.], [7.]])
print(np.linalg.solve(a, y))#解线性方程组
print(np.linalg.eig(j))#计算特征值
```





## 矩阵对象

**矩阵对象的属性：**【通过`np.matrix()`可以转换为矩阵对象】

###### matrix.T transpose：返回矩阵的转置矩阵

###### matrix.H hermitian (conjugate) transpose：返回复数矩阵的共轭元素矩阵

###### matrix.I inverse：返回矩阵的逆矩阵

###### matrix.A base array：返回矩阵基于的数组



## 一元操作

- **数组求和、平均值、求最大最小值**【必须是数组】

```python
print('-----一元操作----')
a = np.random.random((2, 3))
print(a)
print('求和')
print(a.sum())    	# 求和【cumsum为累加求和】
print('求平均')
print(a.mean())   	# 平均值
print('求最大值')
print(a.max())		# 最大值
print('求最小值')
print(a.min())		# 最小值
#==========输出=============#
-----一元操作----
[[0.65188952 0.82232872 0.02885442]
 [0.15634995 0.85316126 0.31207017]]
求和
2.824654042075561
求平均
0.47077567367926015
求最大值
0.8531612552391274
求最小值
0.028854424481340568
```

- **指定维数**

```python
print('-----指定维数----')
b=np.arange(12).reshape(3,4)
print(b)
print('对第0维的元素求和【对列】')
print(b.sum(axis=0)) #对第0维的元素求和
print('对第1维的元素求和【对行】')
print(b.sum(axis=1)) #对第1维的元素求和
print('对第1维的元素求最小【对行】')
print(b.min(axis=1))
print('对第1维的元素累加求和【对行】')
print(b.cumsum(axis=1)) #对第1维的元素累加求和

#==========输出=============#
print('-----指定维数----')
b=np.arange(12).reshape(3,4)
print(b)
print('对第0维的元素求和【对列】')
print(b.sum(axis=0)) #对第0维的元素求和
print('对第1维的元素求和【对行】')
print(b.sum(axis=1)) #对第1维的元素求和
print('对第1维的元素求最小【对行】')
print(b.min(axis=1))
print('对第1维的元素累加求和【对行】')
print(b.cumsum(axis=1)) #对第1维的元素累加求和
```



## 数学函数sin、cos等

NumPy提供了熟知的数学方法，如：sin、cos、exp等。在NumPy中，这些方法被称作广播函数。这些函数会对数组中的**每个元素**进行计算，返回计算后的数组

```python
B = np.arange(3)
print(B)
print('-----e的次方-----')
print(np.exp(B))
print('-----开根号-----')
print(np.sqrt(B))
print('-----加法-----')
C = np.array([2, -1, 4])
print(np.add(B, C))
print(B + C)

#==========输出=============#
[0 1 2]
-----e的次方-----
[1.         2.71828183 7.3890561 ]
-----开根号-----
[0.         1.         1.41421356]
-----加法-----
[2 0 6]
[2 0 6]
```



## 索引、切片和迭代

**基本操作**

- 一维数组

```python
a = np.arange(10) ** 3
print(a)
print('获取索引为2的元素:', a[2])
print('获取索引为2到5的子串:', a[2:5])
a[:6:2] = -1000
print('从索引0开始到索引6每隔2个(不包括6)，依次减1000:')  # 0,2,4
print(a)
print('逆序显示：')
print(a[::-1])
print('遍历')
for i in a:
    print(i)

#==========输出=============#
[  0   1   8  27  64 125 216 343 512 729]
获取索引为2的元素: 8
获取索引为2到5的子串: [ 8 27 64]
从索引0开始到索引6每隔2个(不包括6)，依次减1000:
[-1000     1 -1000    27 -1000   125   216   343   512   729]
逆序显示：
[  729   512   343   216   125 -1000    27 -1000     1 -1000]
遍历
-1000
1
-1000
27
-1000
125
216
343
512
729
```

- 多维数组
  - 可以在每一个维度有一个索引，这些索引构成元组来进行访问。

```python
def f(x, y): return 10 * x + y


b = np.fromfunction(f, (5, 4), dtype=int)
print(b)
print('2行3列的元素（从0行0列开始）：')
print(b[2, 3])
print('第0~4行，第1列的元素（从0行0列开始）：')
print(b[0:4, 1])
print('所有行，第1列的元素（从0行0列开始）：')
print(b[:, 1])
print('第1~3行，所有列的元素（从0行0列开始）：')
print(b[1:3, :])

print('对每一行进行遍历：')
for row in b:
    print(row)

print('使用flat属性对每一个元素进行遍历：')
for element in b.flat:
    print(element)

#==========输出=============#
[[ 0  1  2  3]
 [10 11 12 13]
 [20 21 22 23]
 [30 31 32 33]
 [40 41 42 43]]
2行3列的元素（从0行0列开始）：
23
第0~4行，第1列的元素（从0行0列开始）：
[ 1 11 21 31]
所有行，第1列的元素（从0行0列开始）：
[ 1 11 21 31 41]
第1~3行，所有列的元素（从0行0列开始）：
[[10 11 12 13]
 [20 21 22 23]]
##后面太长，知道就好
```

- `...`表示对索引的省略。如下所示：

```python
print('...表示省略，特别是维数很多时，常用')
c = np.array([[[0, 1, 2],  # 三维数组
               [10, 12, 13]],
              [[100, 101, 102],
               [110, 112, 113]]])
print(c)
print('数组形状结构')
print(c.shape)
print('取三维数组第一维的索引为1的')
print(c[1, ...])  # 和 c[1,:,:] 、 c[1]效果相同
print('取三维数组最后一维的索引为2的')
print(c[..., 2])  # 和c[:,:,2]效果相同

#==========输出=============#
...表示省略，特别是维数很多时，常用
[[[  0   1   2]
  [ 10  12  13]]

 [[100 101 102]
  [110 112 113]]]
数组形状结构
(2, 2, 3)
取三维数组第一维的索引为1的
[[100 101 102]
 [110 112 113]]
取三维数组最后一维的索引为2的
[[  2  13]
 [102 113]]
```



# 数组的形状操作

## 更改数组形状 `reshape`、`ravel`

```python
a = np.floor(10 * np.random.random((3, 4)))
print('铺平变为一维数组：')
print(a.ravel())  # 返回铺平后的数组
print('由(3，4)变为(6，2)：')
print(a.reshape(6, 2))  # 按照指定的形状更改

print('一个维度填的是-1，则该维度的形状会自动进行计算：')
print(a.reshape(3,-1))

#==========输出=============#
铺平变为一维数组：
[3. 5. 6. 6. 4. 1. 0. 2. 0. 3. 3. 3.]
由(3，4)变为(6，2)：
[[3. 5.]
 [6. 6.]
 [4. 1.]
 [0. 2.]
 [0. 3.]
 [3. 3.]]
一个维度填的是-1，则该维度的形状会自动进行计算：
[[3. 5.]
 [6. 6.]
 [4. 1.]
 [0. 2.]
 [0. 3.]
 [3. 3.]]
```



## 堆砌不同的数组【两两组合】

- `np.vstack(a,b)`：垂直堆砌——等价于 `np.r_[a,b]`
- `np.hstack`：水平堆砌——等价于 `np.c_[a,b]`

```python
a = np.floor(10 * np.random.random((2, 2)))
print(a)
b = np.floor(10 * np.random.random((2, 2)))
print(b)
print('垂直方向堆砌：')
print(np.vstack((a, b)))  # 垂直方向堆砌   
print('水平方向堆砌：')
print(np.hstack((a, b)))  # 水平方向堆砌

print('插入新维度--二维变为三维：')
print(a[:, np.newaxis])
```

- `np.newaxis`：插入新的维度，且新的维度为1

  - 一维变二维：newaxis将一个一维数组变成了二维数组，且扩的那一维的长度是1

  - ```python
    import numpy as np
    x=np.arange(4)
    print(x)  # 初始的一维数组
    print(x[np.newaxis, :])  # 得到的二维数组
    print(x[:, np.newaxis])  # 得到的另一个二维数组
    
    #==========输出=============#
    [0 1 2 3]
    [[0 1 2 3]]
    [[0]
     [1]
     [2]
     [3]]
    ```



## 拆分数组 `hsplit`、`vsplit`

```python
a = np.floor(10 * np.random.random((2, 12)))
print(a)
print(np.hsplit(a, 3))
print(np.hsplit(a, (1, 2, 3)))  # 在第一列，第二列，第三列进行划分

#==========输出=============#
[[5. 4. 1. 7. 1. 4. 3. 4. 5. 4. 5. 9.]
 [2. 2. 0. 9. 6. 1. 8. 6. 9. 0. 8. 0.]]
[array([[5., 4., 1., 7.],
       [2., 2., 0., 9.]]), array([[1., 4., 3., 4.],
       [6., 1., 8., 6.]]), array([[5., 4., 5., 9.],
       [9., 0., 8., 0.]])]
[array([[5.],
       [2.]]), array([[4.],
       [2.]]), array([[1.],
       [0.]]), array([[7., 1., 4., 3., 4., 5., 4., 5., 9.],
       [9., 6., 1., 8., 6., 9., 0., 8., 0.]])]
```



# 复制与视图

## 直接用 = 是不会复制的！

```python
a = np.arange(12)
b = a
print(b is a)
b.shape = 3, 4
print(a.shape)

#==========输出=============#
True
(3, 4)
```

## 视图与浅复制

不同的数组可以使用同一份数据，`view`函数在同一份数据上创建了新的数组对象。

【数组形状不同，但数据相同】

```python
'''视图与浅复制'''
a = np.arange(12)
a.shape = 3, 4
print('a:', a)
c = a.view()
print('c:', c)
print(c is a)
print(c.base is a)  # c是a的数据的视图
print('c.base:', c.base)
print(c.flags.owndata)
c.shape = 6, 2
print(a.shape)  # a的形状没有改变
c[4, 1] = 1234  # a的数据改变了
print(a)

#==========输出=============#
a: [[ 0  1  2  3]
 [ 4  5  6  7]
 [ 8  9 10 11]]
c: [[ 0  1  2  3]
 [ 4  5  6  7]
 [ 8  9 10 11]]
False
True
c.base: [[ 0  1  2  3]
 [ 4  5  6  7]
 [ 8  9 10 11]]
False
(3, 4)
[[   0    1    2    3]
 [   4    5    6    7]
 [   8 1234   10   11]]
```

> 对数组切片会返回数组的视图
>
> ```python
> s=a[:,1:3]
> s[:]=10
> print(a)
> [[ 0 10 10  3]
>  [ 4 10 10  7]
>  [ 8 10 10 11]]
> ```



## 深复制  `copy`

```python
a = np.arange(12)
a.shape = 3, 4
d = a.copy()
print(d is a)
print(d.base is a)
d[0, 0] = 9999
print(a)

#==========输出=============#
False
False
[[ 0  1  2  3]
 [ 4  5  6  7]
 [ 8  9 10 11]]
```





# 索引相关的小技巧

[self_tutorial/Quickstart.md at master · cxylpy/self_tutorial (github.com)](https://github.com/cxylpy/self_tutorial/blob/master/numpy/quickstart/Quickstart.md#多种多样的索引和索引的小技巧)



# 其他 函数

## np.random函数

### 生成[0,1)间随机数：rand

**`numpy.random.rand(d0,d1,…,dn)`**

- rand函数根据给定维度生成[0,1)之间的数据，包含0，不包含1
- dn表示每个维度
- 返回值为指定维度的array

```python
np.random.rand(4,2)
array([[ 0.02173903,  0.44376568],
       [ 0.25309942,  0.85259262],
       [ 0.56465709,  0.95135013],
       [ 0.14145746,  0.55389458]])

np.random.rand(4,3,2) # shape: 4*3*2
array([[[ 0.08256277,  0.11408276],
        [ 0.11182496,  0.51452019],
        [ 0.09731856,  0.18279204]],
 
       [[ 0.74637005,  0.76065562],
        [ 0.32060311,  0.69410458],
        [ 0.28890543,  0.68532579]],
 
       [[ 0.72110169,  0.52517524],
        [ 0.32876607,  0.66632414],
        [ 0.45762399,  0.49176764]],
 
       [[ 0.73886671,  0.81877121],
        [ 0.03984658,  0.99454548],
        [ 0.18205926,  0.99637823]]])
```

###### 除此之外生成[0,1)之间的浮点数的方法还有：

- numpy.random.random_sample(size=None)
- numpy.random.random(size=None)
- numpy.random.ranf(size=None)
- numpy.random.sample(size=None)



### 获取标准正态分布：randn

**`numpy.random.randn(d0,d1,…,dn)`**

- randn函数返回一个或一组样本，具有标准正态分布。
- dn表示每个维度
- 返回值为指定维度的array

```python
np.random.randn() # 当没有参数时，返回单个数据
-1.1241580894939212

np.random.randn(2,4)
array([[ 0.27795239, -2.57882503,  0.3817649 ,  1.42367345],
       [-1.16724625, -0.22408299,  0.63006614, -0.41714538]])

np.random.randn(4,3,2)
array([[[ 1.27820764,  0.92479163],
        [-0.15151257,  1.3428253 ],
        [-1.30948998,  0.15493686]],
 
       [[-1.49645411, -0.27724089],
        [ 0.71590275,  0.81377671],
        [-0.71833341,  1.61637676]],
 
       [[ 0.52486563, -1.7345101 ],
        [ 1.24456943, -0.10902915],
        [ 1.27292735, -0.00926068]],
 
       [[ 0.88303   ,  0.46116413],
        [ 0.13305507,  2.44968809],
        [-0.73132153, -0.88586716]]])
```

###### 标准正态分布介绍

- 标准正态分布—-standard normal distribution
- 标准正态分布又称为u分布，是以0为均值、以1为标准差的正态分布，记为N（0，1）。





### 获取随机整数：randint

**`numpy.random.randint(low, high=None, size=None, dtype=’l’)`**

- 返回随机整数，范围区间为[low,high），包含low，不包含high
- 参数：low为最小值，high为最大值，size为数组维度大小，dtype为数据类型，默认的数据类型是np.int
- high没有填写时，默认生成随机数的范围是[0，low)
- numpy.random.random_integers(low, high=None, size=None)
  - 返回随机整数，范围区间为[low,high]，包含low和high
  - 参数：low为最小值，high为最大值，size为数组维度大小
  - high没有填写时，默认生成随机数的范围是[1，low]

- `numpy.random.randint(low, high=None, size=None, dtype='l')`：返回一个随机整数矩阵
  - 范围：从低（包括）到高（不包括），即[low, high)
  - 若没写参数high值则：[0, low)
  - dtype: dtype(可选)：想要输出的格式。如`int64`、`int`等等



### 从序列中随机选：choice

**`numpy.random.choice(a, size=None, replace=True, p=None)`**

- 从给定的一维数组中生成随机数；
- 参数： a为一维数组类似数据或整数；size为数组维度(一维数组的长度)；p为数组中的数据出现的概率，参数p的长度与参数a的长度需要一致；参数p为概率，p里的数据之和应为1；
- a为整数时，对应的一维数组为np.arange(a)；

- `np.random.choice(a, size=None, replace=True, p=None)`：在列表中选择某元素
  - a：列表或者int型
    - 当为int时，相当于`np.random.arange()`
  - size：大小，输出形状
    - 默认为None即返回一个值
    - 可以给元组型
  - replace：样品是否具有替换
    - True表示可以多次选择【有放回的】
  - p：每个条目相关的概率
    - 默认为均匀分布

```python
np.random.choice(5,3)
[0 3 1]

np.random.choice(5, 3, replace=False) # 当replace为False时，生成的随机数不能有重复的数值

demo_list = ['lenovo', 'sansumg','moto','xiaomi', 'iphone']
np.random.choice(demo_list,size=(3,3))
array([['moto', 'iphone', 'xiaomi'],
       ['lenovo', 'xiaomi', 'xiaomi'],
       ['xiaomi', 'lenovo', 'iphone']],
      dtype='<U7')

demo_list = ['lenovo', 'sansumg','moto','xiaomi', 'iphone']
np.random.choice(demo_list,size=(3,3), p=[0.1,0.6,0.1,0.1,0.1])
array([['sansumg', 'sansumg', 'sansumg'],
       ['sansumg', 'sansumg', 'sansumg'],
       ['sansumg', 'xiaomi', 'iphone']],
      dtype='<U7')
```



### 使得随机数据可预测：seed

**`numpy.random.seed()`**

- np.random.seed()的作用：使得随机数据可预测。
- 当我们设置相同的seed，每次生成的随机数相同。如果不设置seed，则每次会生成不同的随机数

```python
np.random.seed(0)
np.random.rand(5)
array([ 0.5488135 ,  0.71518937,  0.60276338,  0.54488318,  0.4236548 ])

np.random.seed(1676)
np.random.rand(5)
array([ 0.39983389,  0.29426895,  0.89541728,  0.71807369,  0.3531823 ])

np.random.seed(1676)
np.random.rand(5)
array([ 0.39983389,  0.29426895,  0.89541728,  0.71807369,  0.3531823 ])
```

