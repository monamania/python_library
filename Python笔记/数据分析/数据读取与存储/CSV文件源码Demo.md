# 读取

```python
#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2021-07-12
"""
import csv

''' 
@ 功能：通过csv读取文件
https://blog.csdn.net/weixin_36279318/article/details/79078255
'''


# 读取表头——方式1
def read_path1():
    """通过next的方式读取表头及数据"""
    with open('data.csv', 'r', encoding='utf-8') as fp:
        reader = csv.reader(fp)
        titles = next(reader)
        print(titles)
        for x in reader:
            print(x)


# 读取表头——方式2
def read_path2_by_row():
    """通过行的方式的的方式读取表头"""
    with open('data.csv', 'r', encoding='utf-8') as fp:
        reader = csv.reader(fp)
        rows = [row for row in reader]
        print(rows[0])


# 通过字典的方式获取文件数据
def read_by_dict():
    """通过字典的方式获取文件数据"""
    with open("data.csv", 'r', encoding='utf-8') as fp:
        # 1.创建阅读器对象
        reader = csv.DictReader(fp)
        # 2.读取文件第一行数据
        titles = next(reader)
        print(titles)
        # 4.按照key的方式读取数据
        for x in reader:
            print(x['author'], end='\t')
            print(x['name'], end='\t')
            print(x['price'])


# 获取文件某一列数据
def read_by_col():
    """读取某一列"""
    with open("data.csv", 'r', encoding='utf-8') as f:
        reader = csv.reader(f)
        column = [row[0] for row in reader]
        print(column)


# 获取文件头及其索引
def get_index_and_header():
    """获取文件头及其索引"""
    with open("data.csv", 'r', encoding='utf-8') as f:
        # 1.创建阅读器对象
        reader = csv.reader(f)
        # 2.读取文件第一行数据
        head_row = next(reader)
        print(head_row)
        # 4.获取文件头及其索引
        for index, column_header in enumerate(head_row):
            print(index, column_header)


if __name__ == '__main__':
    # 读取表头——方式1——next的方式
    print("---------------------------")
    print('通过next的方式读取表头及数据')
    read_path1()

    # 按行读取的方式
    print("---------------------------")
    print('通过行的方式的的方式读取表头')
    read_path2_by_row()

    # 按行读取的方式
    print("---------------------------")
    print('通过字典的方式获取文件数据')
    read_by_dict()

    # 按行读取的方式
    print("---------------------------")
    print('读取某一列')
    read_by_col()

    # 按行读取的方式
    print("---------------------------")
    print('获取文件头及其索引')
    get_index_and_header()

```

# 保存

```python
#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2021-07-12
"""
import csv

''' 
@ 功能：保存数据
'''

# 普通方式
headers = ['name', 'price', 'author']
values = [
    ('三国演义', 48, '罗贯中'),
    ('红楼梦', 50, '曹雪芹'),
    ('西游记', 45, '吴承恩'),
    ('三体', 20, '刘慈欣')
]

with open('data_write.csv', 'w', encoding='utf-8', newline='') as fp:
    """一定要指定newline=''，不然会空一行"""
    writer = csv.writer(fp)
    writer.writerow(headers)
    writer.writerows(values)


# 字典方式
headers = ['name', 'price', 'author']
values = [
    {"name": '三国演义', "price": 48, "author": '罗贯中'},
    {"name": '红楼梦', "price": 50, "author": '曹雪芹'},
    {"name": '西游记', "price": 45, "author": '吴承恩'},
    {"name": '三体', "price": 20, "author": '刘慈欣'}
]
with open('data_write2.csv', 'w', encoding='utf-8', newline='') as fp:
    """一定要指定newline=''，不然会空一行"""
    writer = csv.DictWriter(fp, headers)
    writer.writerow({"name": 'name', "price": 'price', "author": 'author'})
    writer.writerow({"name": '按照一行的', "price": 48, "author": '测试'})
    writer.writerows(values)
```

