## (一)CSV格式文件

https://blog.csdn.net/weixin_36279318/article/details/79078255

1.说明

> CSV是一种以逗号分隔数值的文件类型，在数据库或电子表格中，常见的导入导出文件格式就是CSV格式，CSV格式存储数据通常以纯文本的方式存数数据表。

## (二)CSV库操作csv格式文本

操作一下表格数据：
![这里写图片描述](https://gitee.com/monamania/picBed/raw/master/img/20210712100042)

1.读取表头的2中方式

```python
#方式一
import csv
def read_path1():
    """通过next的方式读取表头及数据"""
    with open('data.csv', 'r', encoding='utf-8') as fp:
        reader = csv.reader(fp)
        titles = next(reader)
        print(titles)
        for x in reader:
            print(x)


----------
#方式二
import csv
def read_path2_by_row():
    """通过行的方式的的方式读取表头"""
    with open('data.csv', 'r', encoding='utf-8') as fp:
        reader = csv.reader(fp)
        rows = [row for row in reader]
        print(rows[0])

```

```python 
结果演示：['姓名', '年龄', '职业', '家庭地址', '工资']
```



2.读取文件某一列数据

```python
#1.获取文件某一列数据
import csv
def read_by_col():
    """读取某一列"""
    with open("data.csv", 'r', encoding='utf-8') as f:
        reader = csv.reader(f)
        column = [row[0] for row in reader]
        print(column)
```

```python 
结果演示：['姓名', '张三', '李四', '王五', 'Kaina']
```



3.向csv文件中写入数据

```python 
#1.向csv文件中写入数据
import csv
# 普通方式
headers = ['name', 'price', 'author']
values = [
    ('三国演义', 48, '罗贯中'),
    ('红楼梦', 50, '曹雪芹'),
    ('西游记', 45, '吴承恩'),
    ('三体', 20, '刘慈欣')
]

with open('data_write.csv', 'w', encoding='utf-8', newline='') as fp:
    """一定要指定newline=''，不然会空一行"""
    writer = csv.writer(fp)
    writer.writerow(headers)
    writer.writerows(values)

    
    
# 字典方式
headers = ['name', 'price', 'author']
values = [
    {"name": '三国演义', "price": 48, "author": '罗贯中'},
    {"name": '红楼梦', "price": 50, "author": '曹雪芹'},
    {"name": '西游记', "price": 45, "author": '吴承恩'},
    {"name": '三体', "price": 20, "author": '刘慈欣'}
]
with open('data_write2.csv', 'w', encoding='utf-8', newline='') as fp:
    """一定要指定newline=''，不然会空一行"""
    writer = csv.DictWriter(fp, headers)
    writer.writerow({"name": 'name', "price": 'price', "author": 'author'})
    writer.writerow({"name": '按照一行的', "price": 48, "author": '测试'})
    writer.writerows(values)
```

![这里写图片描述](https://gitee.com/monamania/picBed/raw/master/img/20210712100201)



4.获取文件头及其索引

```python 
import csv
def get_index_and_header():
    """获取文件头及其索引"""
    with open("data.csv", 'r', encoding='utf-8') as f:
        # 1.创建阅读器对象
        reader = csv.reader(f)
        # 2.读取文件第一行数据
        head_row = next(reader)
        print(head_row)
        # 4.获取文件头及其索引
        for index, column_header in enumerate(head_row):
            print(index, column_header)

```

```
结果演示：
['姓名', '年龄', '职业', '家庭地址', '工资']
0 姓名
1 年龄
2 职业
3 家庭地址
4 工资
```



5.获取某列的最大值

```python 
# ['姓名', '年龄', '职业', '家庭地址', '工资']
import csv
with open("D:\\test.csv") as f:
    reader = csv.reader(f)
    header_row=next(reader)
    # print(header_row)
    salary=[]
    for row in reader:
        #把第五列数据保存到列表salary中
         salary.append(int(row[4]))
    print(salary)
    print("员工最高工资为："+str(max(salary)))
```

```
结果演示：员工最高工资为：10000
```



6.复制CSV格式文件

原文件test.csv

![这里写图片描述](https://gitee.com/monamania/picBed/raw/master/img/20210712100335)

```python 
import csv
f=open('test.csv')
#1.newline=''消除空格行
aim_file=open('Aim.csv','w',newline='')
write=csv.writer(aim_file)
reader=csv.reader(f)
rows=[row for row in reader]
#2.遍历rows列表
for row in rows:
    #3.把每一行写到Aim.csv中
    write.writerow(row)
```

> 01.未添加关键字参数newline=’ '的结果：
>
> ![这里写图片描述](https://gitee.com/monamania/picBed/raw/master/img/20210712100411)
>
> 02添加关键字参数newline=’ '的Aim.csv文件的内容：
>
> ![这里写图片描述](https://gitee.com/monamania/picBed/raw/master/img/20210712100422)

### 使用csv.DictReader——直接存储为字典！比上面的方式方便

https://blog.csdn.net/tomatomas/article/details/81005503

读取

```python
with open(‘name.csv’) as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        #循环打印数据的id和class值，此循环执行7次
        print(row[‘id’],row[‘class’])
```

写入

```python
with open(‘name.csv’,’w’) as csvfile:
    writer = csv.DictWriter(csvfile,fieldnames=[‘id’,’class’])
    #写入列标题，即DictWriter构造方法的fieldnames参数
    writer.writeheader()
    for data in datas:
        writer.writerow({‘id’:data[0],’class’:data[1]})
        
或者：
import csv
```

