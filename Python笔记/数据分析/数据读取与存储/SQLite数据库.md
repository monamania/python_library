```python
#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2021-05-28
"""
import sqlite3

''' 
@ 功能：进行sqlite3的测试
https://blog.csdn.net/whatday/article/details/90766086/
https://blog.csdn.net/Leo_csdn_/article/details/80732047
'''


def init_db(dbpath='test.db'):
    """ 创建数据库 """
    # 数据库相关使用的语句
    sql = '''
        create table if not exists student(
        id integer  primary  key autoincrement,
        name text,
        sex text,
        score numeric
        )
    '''
    # 创建数据表
    conn = sqlite3.connect(dbpath)
    # cursor = conn.cursor()
    # cursor.execute(sql)    # 使用上面的sql语句创建数据库
    # 或者
    with conn:
        conn.execute(sql)
    conn.commit()
    conn.close()


def insert_db(dbpath):
    """ 插入数据 """
    # 连接数据库
    conn = sqlite3.connect(dbpath)
    # 创建游标
    cursor = conn.cursor()

    # 插入数据
    sql = "INSERT INTO student(name, sex) VALUES(\'张3\', '男')"
    cursor.execute(sql)

    # 插入数据 2
    data = ('张2', '女')  # or ['love2', 2221]
    sql = "INSERT INTO Student(name, sex) VALUES(?, ?)"
    cursor.execute(sql, data)

    # 提交事物
    conn.commit()


def clear_db(dbpath, tableName):
    """ 删除数据库里面的整个表 """
    # 连接数据库
    conn = sqlite3.connect(dbpath)
    # 创建游标
    cursor = conn.cursor()

    sql = "drop table " + tableName
    cursor.execute(sql)

    # 提交事物
    conn.commit()


def del_item_db(dbpath):
    """ 删除数据库中，指定表的指定数据 """
    # 连接数据库
    conn = sqlite3.connect(dbpath)
    # 创建游标
    cursor = conn.cursor()

    sql = '''
    delete from mytable where id = 2; 
    '''  # 删除表格中id为2的一条或多条记录
    cursor.execute(sql)

    # 提交事物
    conn.commit()


def find_data_db(dbpath, tableName):
    """数据查找"""
    # 连接数据库
    conn = sqlite3.connect(dbpath)
    # 创建游标
    cursor = conn.cursor()

    # 查询数据 1
    sql = "select * from " + tableName
    values = cursor.execute(sql)
    for i in values:
        print(i)

    # 查询数据 2
    sql = "select * from " + tableName + " where id=?"
    values = cursor.execute(sql, (1,))
    for i in values:
        print('id:', i[0])
        print('name:', i[1])
        print('sex:', i[2])

    # 提交事物
    conn.commit()


# 修改数据
# update mytable set id = 5 where name = 'leo'//将表格中"name"为"leo"的记录（行）的"id"字段的值改为"5"


def main():
    dbpath = 'student.db'
    init_db(dbpath)
    insert_db(dbpath)


'''
常见sqlite语句
1. 建表：
    （1）缺省写法，确定是新表
    create table mytable (id integer, name text not null)
    （2）标准写法，如果是已存在的表则不会创建
    create table if not exists mytable (id integer, name text not null);
    补充：integer为整形，text为字符型，这些是数据类型。not null为标识，意思非空即必须有值。
    除此之外的标识还有unique，意为唯一不可重复；
    primary key，作用是设为主键，相当于not null+unique，
    例：(id integer primary key,name text not null unique)
    
2. 删表：
    drop table mytable;  //表直接没了

3. 删除表中数据
    delete from mytable where id = 2;              //删除表格中id为2的一条或多条记录
    delete from 表名;        //清空表内所有数据，但是表还在，注意不要在delete后加‘*’

4. 查看表中数据
    select * from mytable   //简易写法，直接遍历显示表格内容
    select * from mytable order by asc/desc // 按照 升序/降序 显示表格内容
    select * from mytable where ... //where后面跟的是筛选条件，如" where id = 024"或者" where name = 'leo' "
    select * from mytable where id = 1 and name = 'leo'; //sqlite可以用and连接多个的where限定语句

5. 修改数据：
    update mytable set id = 5 where name = 'leo';      //将表格中"name"为"leo"的记录（行）的"id"字段的值改为"5"

7. 添加字段（列）：
    alter table mytable add column newcolumn text;         //新增一个类型text的字段newcolumn
    
8. 重命名表
    alter table mytable rename to newtable;          //将表格“mytable”重命名为“newtable”
    
9. 两张表关联查找
    select* from table1,table2 where table1.id = table2.id //将两张表中id相等的记录挑选出来，可以增加额外条件如“where id = 2”
    select * from table1 inner join table2 on XXX=XXX    //同1)

10. 两张表关联复制：
    insert into mytable select * from table2    ; 可加上‘where’的限定语句;
将table2中的数据拷贝到mytable中去，前提是两个表格结构相同，可以宽松，但不可缺省，即原字段是not null限制的，新表对应字段可以是普通可空字段，但是不可以缺省该字段(可以附加条件)
'''

#
# def saveData2DB(datalist, dbpath):
#     init_db(dbpath)
#     conn = sqlite3.connect(dbpath)
#     cur = conn.cursor()
#     for data in datalist:
#         for index in range(len(data)):
#             if index == 4 or index == 5:
#                 continue
#             data[index] = '"' + data[index] + '"'
#         sql = '''
#                     insert into movie250(
#                     info_link,pic_link,cname,ename,score,rated,instroduction,info)
#                     values (%s)''' % ",".join(data)
#         # print(sql)     #输出查询语句，用来测试
#         cur.execute(sql)
#         conn.commit()
#     cur.close
#     conn.close()
#
#
# def init_db(dbpath):
#     sql = '''
#         create table movie250(
#         id integer  primary  key autoincrement,
#         info_link text,
#         pic_link text,
#         cname varchar,
#         ename varchar ,
#         score numeric,
#         rated numeric,
#         instroduction text,
#         info text
#         )
#
#
#     '''  # 创建数据表
#     conn = sqlite3.connect(dbpath)
#     cursor = conn.cursor()
#     cursor.execute(sql)
#     conn.commit()
#     conn.close()

# 保存数据到数据库


if __name__ == '__main__':
    main()

```

