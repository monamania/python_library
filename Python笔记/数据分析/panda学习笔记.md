[Content — Joyful Pandas 1.0 documentation (datawhale.club)](http://joyfulpandas.datawhale.club/Content/index.html)

[toc]

# DataFrame初始化

## 1. 创建一个空的DataFrame

```python
import pandas as pd
import numpy as np
pd.DataFrame(columns=('id','name','grade','class'))
```

## 2. 利用字典初始化

格式：

```
pd.DataFrame(data[, index=, columns=])
```

例1：

```python
data={"one":np.random.randn(4),"two":np.linspace(1,4,4),"three":['zhangsan','李四',999,0.1]}
df=pd.DataFrame(data,index=[1,2,3,4])
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/20181027180352575.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3h0ZmdlMDkxNQ==,size_27,color_FFFFFF,t_70)

> https://blog.csdn.net/xtfge0915/article/details/52938740
>
> - 如果创建df时不指定索引，默认索引将是从0开时，步长为1的数组。
> - df的行、列可以是不同的数据类型，同行也可以有多种数据类型。
> - df创建完成后可以重新设置索引，通常用到3个函数：`set_index`、`reset_index`、`reindex`。
>
> **1. `set _index`用于将df中的一行或多行设置为索引。**
>
> `df.set_index(['one'],drop=False)` or`df.set_index('one)`
> `df.set_index(['one','two'])`
> 参数drop默认为True，意为将该列设置为索引后从数据中删除，如果设为False，将继续在数据中保留该行。
>
> ![img](https://img-blog.csdnimg.cn/20181027181302493.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3h0ZmdlMDkxNQ==,size_27,color_FFFFFF,t_70)
>
> **2.如果要设置的索引不在数据中，可以通过**
> `df.index=['a','b','c','d']`
>
> **3.`reset_index`用于将索引还原成默认值，即从0开始步长为1的数组。**
>
> `df.reset_index(drop=True)`
> 参数`drop`默认值为`False`，意为将原来的索引做为数据列保留，如果设为`True`，原来的索引会直接删除。
>
> ![在这里插入图片描述](https://img-blog.csdnimg.cn/2018102718194482.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3h0ZmdlMDkxNQ==,size_27,color_FFFFFF,t_70)
>
> 4. `reindex`比较复杂，看[官方文档]([pandas.DataFrame.reindex — pandas 1.3.3 documentation (pydata.org)](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.reindex.html))

例2：

```python
pd.DataFrame([{'A': 1, 'B': 2, 'C': 3}, {'A': 2, 'B': 3}])
pd.DataFrame([{'A': 1, 'B': 2, 'C': 3}, {'A': 2, 'B': 3}], index=['a', 'b'], columns=['A', 'B'])
pd.DataFrame({'A': [1, 2, 3], 'B': [2, 3, 4]})
pd.DataFrame({'A': [1, 2, 3], 'B': [2, 3, 4]}, index=['a', 'b', 'c'], columns=['A'])
pd.DataFrame({'A': {'a': 1, 'b': 2}, 'B': {'a': 3}}, columns=['A'], index=['a'])
```



例子3：dict{list} 和 list[dict]效果是一样的

```python
#%%
import pandas as pd
import numpy as np

data_dict1 = {
    "x":100,
    "y":10,
    "cost":20,
    "parent":None
}
data_dict2 = {
    "x":100,
    "y":10,
    "cost":50,
    "parent":0
}
data = [data_dict1, data_dict2]

df1 = pd.DataFrame(data)
#%%
data = {
    "x":[100,100],
    "y":[10,20],
    "cost":[20,50],
    "parent":[None,0]
}
df2 = pd.DataFrame(data)
```



## 3. 利用列表（数组）初始化

```
pd.DataFrame([1, 2, 3])
pd.DataFrame([1, 2, 3], columns=['A'], index=['a', 'b', 'c'])
pd.DataFrame([[1, 2, 3], [2, 3, 4]])
pd.DataFrame([[1, 2, 3], [2, 3, 4]], columns=['A', 'B', 'C'], index=['a', 'b'])
```



# pandas的to_csv()使用方法

**1.首先查询当前的工作路径：**

```
import os
os.getcwd() #获取当前工作路径
```

**2.to_csv()是DataFrame类的方法，read_csv()是pandas的方法**

```
dt.to_csv() #默认dt是DataFrame的一个实例，参数解释如下
```

- **路径** path_or_buf: A string path to the file to write or a StringIO

```
dt.to_csv('Result.csv') #相对位置，保存在getwcd()获得的路径下
dt.to_csv('C:/Users/think/Desktop/Result.csv') #绝对位置
```

- **分隔符** sep : Field delimiter for the output file (default ”,”)

```
dt.to_csv('C:/Users/think/Desktop/Result.csv',sep='?')#使用?分隔需要保存的数据，如果不写，默认是,
```

- **替换空值** na_rep: A string representation of a missing value (default ‘’)

```
dt.to_csv('C:/Users/think/Desktop/Result1.csv',na_rep='NA') #确实值保存为NA，如果不写，默认是空
```

- **格式** float_format: Format string for floating point numbers

```
dt.to_csv('C:/Users/think/Desktop/Result1.csv',float_format='%.2f') #保留两位小数
```

- **是否保留某列数据** cols: Columns to write (default None)

```
dt.to_csv('C:/Users/think/Desktop/Result.csv',columns=['name']) #保存索引列和name列
```

- **是否保留列名** header: Whether to write out the column names (default True)

```
dt.to_csv('C:/Users/think/Desktop/Result.csv',header=0) #不保存列名
```

- **==【常用】是否保留行索引==** index:  whether to write row (index) names (default True)

```
dt.to_csv(r'C:/Users/think/Desktop/Result1.csv',index=0) #不保存行索引
dt.to_csv(r'C:/Users/think/Desktop/Result1.csv',index=0, na_rep='NULL') #不保存行索引,且将nan保存为NULL
```





> 读取df后，将nan替换成其他的值：
>
> ```python
> # 替换为0
> df.fillna(0) 
> 
> # 替换为None
> # 将非空数据保留，空数据用None替换
> df = df.where(df.notnull(), None)
> ```
>
> pandas取dataframe特定行/列

https://blog.csdn.net/xtfge0915/article/details/52938740

```python
# DataFrame()   创建一个DataFrame对象
# df.shape    返回（行数，列数）   行df.shape[0]
# df.values    返回ndarray类型的对象
按照单元格读取
方法1：df[col][row]
或者 df.col[row]
# df[ 行,列 ]            按索引返回元素    lx既能用数字，也能用索引名
# df.iloc[ 行序,列序 ]      按序值返回元素    iloc只能用数字索引，不能用索引名
# df.loc[ 行索引,列索引 ]    按索引返回元素    loc只能通过index和columns来取，不能用数字
# df.index 获取行索引
# df.columns   获取列索引
# df.axes  获取行及列索引
# df.T 行与列对调
# df.info()   打印DataFrame对象的信息
# df.head(i)   显示前 i 行数据
# df.tail(i)   显示后 i 行数据
# df.describe()    查看数据按列的统计信息
```

## 1.按列取、按索引/行取、按特定行列取

按照单元格读取

**方法1：**`df[col][row]`
读取一个单元格的数据时推荐使用，也可以写成df.col[row]

```python
import numpy as np
from pandas import DataFrame
import pandas as pd
 
 
df=DataFrame(np.arange(12).reshape((3,4)),index=['one','two','thr'],columns=list('abcd'))
 
df['a']#取a列
df['a'][0] # 取0行a列，，a列的第0行
df[['a','b']]#取a、b列
 
#ix可以用数字索引，也可以用index和column索引
df.ix[0]#取第0行
df.ix[0:1]#取第0行
df.ix['one':'two']#取one、two行
df.ix[0:2,0]#取第0、1行，第0列
df.ix[0:1,'a']#取第0行，a列
df.ix[0:2,'a':'c']#取第0、1行，abc列
df.ix['one':'two','a':'c']#取one、two行，abc列
df.ix[0:2,0:1]#取第0、1行，第0列
df.ix[0:2,0:2]#取第0、1行，第0、1列
 
#loc只能通过index和columns来取，不能用数字
df.loc['one','a']#one行，a列
df.loc['one':'two','a']#one到two行，a列
df.loc['one':'two','a':'c']#one到two行，a到c列
df.loc['one':'two',['a','c']]#one到two行，ac列
 
#iloc只能用数字索引，不能用索引名
df.iloc[0:2]#前2行
df.iloc[0]#第0行
df.iloc[0:2,0:2]#0、1行，0、1列
df.iloc[[0,2],[1,2,3]]#第0、2行，1、2、3列
 
#iat取某个单值,只能数字索引
df.iat[1,1]#第1行，1列
#at取某个单值,只能index和columns索引
df.at['one','a']#one行，a列
```

按行读取

```python
for row in df.iloc:
    print(row.values)
```



## 2.按条件取行

```python
选取等于某些值的行记录 用 ==
df.loc[df['column_name'] == some_value]
df1=df[df['column_name']==0]
df2=df[df['column_name']==1]
 
选取某列是否是某一类型的数值 用 isin
df.loc[df['column_name'].isin(some_values)]
 
多种条件的选取 用 &
df.loc[(df['column'] == some_value) & df['other_column'].isin(some_values)]
 
选取不等于某些值的行记录 用 ！=
df.loc[df['column_name'] != some_value]
 
isin返回一系列的数值,如果要选择不符合这个条件的数值使用~
df.loc[~df['column_name'].isin(some_values)]
```

## 3.取完之后替换

```python
df = pd.DataFrame({"id": [25,53,15,47,52,54,45,9], "sex": list('mfmfmfmf'), 'score': [1.2, 2.3, 3.4, 4.5,6.4,5.7,5.6,4.3],"name":['daisy','tony','peter','tommy','ana','david','ken','jim']})
```

![img](https://img2018.cnblogs.com/blog/1252882/201903/1252882-20190325140237948-1207145883.png)

将男性(m)替换为1，女性(f)替换为0

**方法1：**

```python
df.ix[df['sex']=='f','sex']=0
df.ix[df['sex']=='m','sex']=1
```

![img](https://img2018.cnblogs.com/blog/1252882/201903/1252882-20190325140457680-1882598181.png)

**注：**在上面的代码中，逗号后面的‘sex’起到固定列名的作用

**方法2：**

```python
df.sex[df['sex']=='m']=1
df.sex[df['sex']=='f']=0　　
```

## 4.删除特定行

```python
# 要删除列“score”<50的所有行：
df = df.drop(df[df.score < 50].index)
 
df.drop(df[df.score < 50].index, inplace=True)
 
# 多条件情况
# 可以使用操作符： | 只需其中一个成立, & 同时成立, ~ 表示取反，它们要用括号括起来。
# 例如删除列“score<50 和>20的所有行
df = df.drop(df[(df.score < 50) & (df.score > 20)].index)
```

　

## 5. 选取特定标签列，生成新的df

```python
new_df = df[['id','name','sex']]
```



## 6. 修改dataframe的列名

```python
# 法一：
df.columns = ['A','B']

# 法二
df.rename(columns={'a':'A'})
```



## 7.将NaN替换为自定义数值

```python
values={"key01":0, "key02":''}
df.fillna(value=0,inplace=True)
```



# 显示DataFrame信息

```
df.info():          # 打印摘要
df.describe():      # 描述性统计信息----很有用
df.round(2)         # 将数据按照四舍五入显示，保留两位小数
df.values:          # 数据 <ndarray>
df.to_numpy()       # 数据 <ndarray> (推荐)
df.shape:           # 形状 (行数, 列数)
df.columns:         # 列标签 <Index>
df.columns.values:  # 列标签 <ndarray>
df.index:           # 行标签 <Index>
df.index.values:    # 行标签 <ndarray>
df.head(n):         # 前n行
df.tail(n):         # 尾n行
pd.options.display.max_columns=n: # 最多显示n列
pd.options.display.max_rows=n:    # 最多显示n行
df.memory_usage():                # 占用内存(字节B)
```



# DataFrame合并merge/concat

[merge](https://blog.csdn.net/lost0910/article/details/104814860)

[concat](https://blog.csdn.net/weixin_44208569/article/details/89676843)





# 按行列遍历

[(4条消息) pandas按行按列遍历Dataframe的几种方式_LuckyDucky的博客-CSDN博客_遍历dataframe](https://blog.csdn.net/sinat_29675423/article/details/87972498)

遍历数据有以下三种方法：

- 
  简单对上面三种方法进行说明：

- iterrows(): 按行遍历，将DataFrame的每一行迭代为(index, Series)对，可以通过row[name]对元素进行访问。
  itertuples(): 按行遍历，将DataFrame的每一行迭代为元祖，可以通过row[name]对元素进行访问，**比iterrows()效率高**。
  iteritems():按列遍历，将DataFrame的每一列迭代为(列名, Series)对，可以通过row[index]对元素进行访问。

示例数据

```python
import pandas as pd

inp = [{'c1':10, 'c2':100}, {'c1':11, 'c2':110}, {'c1':12, 'c2':123}]
df = pd.DataFrame(inp)

print(df)
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190227143422984.png)

## 按行遍历iterrows():

```python
for index, row in df.iterrows():
    print(index) # 输出每行的索引值
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190227143612870.png)

`row[‘name’]`取值

```python
# 对于每一行，通过列名name访问对应的元素
for row in df.iterrows():
    print(row['c1'], row['c2']) # 输出每一行

```

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190227143716567.png)

## 按行遍历itertuples():

`getattr(row, ‘name’)`取值

```python
for row in df.itertuples():
    print(getattr(row, 'c1'), getattr(row, 'c2')) # 输出每一行
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190227143835738.png)

## 按列遍历iteritems():

```python
for index, row in df.iteritems():
    print(index) # 输出列名
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190227144044993.png)

```python
for row in df.iteritems():
    print(row[0], row[1], row[2]) # 输出各列
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190227144037269.png)
