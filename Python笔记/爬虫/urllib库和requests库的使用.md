# `urllib`库的使用——python内部自带

## GET请求

```python
#!/usr/bin/env python
# -*- coding:utf-8 -*-
import urllib.request

response = urllib.request.urlopen("http://www.baidu.com/")
print(response.read().decode('utf-8'))  # 对获取到的网页源码进行utf-8解码
print(response.status)   # 返回状态码
print(response.getheaders())   # 返回响应头
print(response.getheader('Server'))   # 返回响应头里面的Server内容

```



## POST请求

> POST请求可以用[httpbin.org](http://httpbin.org/)这个网址作为服务器进行请求测试

### 法一：增加data数据的方式进行post请求

```python
#!/usr/bin/env python
# -*- coding:utf-8 -*-
import urllib.request
import urllib.parse   # 用于解析

# POST请求可以用[httpbin.org](http://httpbin.org/)这个网址作为服务器进行请求测试
# 提供的表单信息要求为二进制
data = bytes(urllib.parse.urlencode({"hello":"worlf"}), encoding='utf-8')
response = urllib.request.urlopen("http://httpbin.org/post", data=data)   # 加入了data内容就默认发起POST请求
print(response.read().decode('utf-8'))

```

输出

```
{
  "args": {}, 
  "data": "", 
  "files": {}, 
  "form": {
    "hello": "world"
  }, 
  "headers": {
    "Accept-Encoding": "identity", 
    "Content-Length": "11", 
    "Content-Type": "application/x-www-form-urlencoded", 
    "Host": "httpbin.org", 
    "User-Agent": "Python-urllib/3.8", 
    "X-Amzn-Trace-Id": "Root=1-60a11b2e-7a275e7b0e2278926e53653b"
  }, 
  "json": null, 
  "origin": "210.32.32.178", 
  "url": "http://httpbin.org/post"
}
```

### 法二：模拟浏览器，并使用人工指定的方式进行post请求

```python
#!/usr/bin/env python
# -*- coding:utf-8 -*-
import urllib.request
import urllib.parse   # 用于解析

url = "http://httpbin.org/post"
# 模拟浏览器发起请求
headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36 Edg/90.0.818.62"
}  # 用户代理，表示告诉豆瓣服务器，我们是什么类型的机器、浏览器（本质上是告诉浏览器，我们可以接收什么水平的文件内容）


data = bytes(urllib.parse.urlencode({"hello": "world"}), encoding='utf-8')

# 构建请求对象
req = urllib.request.Request(url=url, data=data, headers=headers, method="POST")
# 发起请求
response = urllib.request.urlopen(req)
print(response.read().decode('utf-8'))

```

输出

```
{
  "args": {}, 
  "data": "", 
  "files": {}, 
  "form": {
    "hello": "world"
  }, 
  "headers": {
    "Accept-Encoding": "identity", 
    "Content-Length": "11", 
    "Content-Type": "application/x-www-form-urlencoded", 
    "Host": "httpbin.org", 
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36 Edg/90.0.818.62", 
    "X-Amzn-Trace-Id": "Root=1-60a1210a-5c0fd4547bfe3bf776c48ea2"
  }, 
  "json": null, 
  "origin": "210.32.32.178", 
  "url": "http://httpbin.org/post"
}
```



## 异常处理

### 超时处理

```python
#!/usr/bin/env python
# -*- coding:utf-8 -*-
import urllib.request  # 用于发起http请求
import urllib.error   # 用于异常处理
# 超时处理
try:
    response = urllib.request.urlopen("http://httpbin.org/get", timeout=0.01)
    print(response.read().decode('utf-8'))
except urllib.error.URLError as e:
    print('time out!')
    if hasattr(e, "code"):
        print(e.code)  # 打印错误码
    if hasattr(e, "reason")
    	print(e.reason)  # 打印原因

```

> ### hasattr() 函数用于判断对象是否包含对应的属性。
>
> ```python
> hasattr(object, name)
> object -- 对象。
> name -- 字符串，属性名。
> return
> 如果对象有该属性返回 True，否则返回 False。
> ```



## 其他响应内容

