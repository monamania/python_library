# 步骤

1. `cd`进入项目文件夹，输入：`scrapy startproject 项目名`，其中`testDemo`为爬虫项目名称

   ```
   scrapy startproject testDemo
   ```

2. `cd`进入`testDemo`，输入：`scrapy genspider 爬虫名 域名`，其中`hangZhouMovieSpider` 为生成的基础爬虫名字，为其脚本搜索的域，比如`www.baidu.com`

   ```
   scrapy genspider baidu www.baidu.com
   ```

3. 生成的文件说明

   - `items.py`：决定爬取那些项目
   - `hangZhouMovieSpider.py`：决定怎么爬
   - `setting.py`：决定由谁去处理爬取的内容
   - `pipelines.py`：决定爬取后的内容怎么处理

