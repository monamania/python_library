#!/bin/sh
#@author: Monomania(mono_cjk@qq.com)
#显示黄色,且设置为高亮
#showYellow CONTENT
#CONTENT:显示的内容
showYellow()
{
  content=$1
  echo -e "\033[1;33m$content\033[0m"
}
echo -n "ROS_DISTRO: "
echo $ROS_DISTRO 

sudo apt update
showYellow "===================aptitude===================="
sudo apt-get install aptitude -y --fix-missing
showYellow "===================export===================="

export ROS_LOCALHOST_ONLY=1
export RMW_IMPLEMENTATION=rmw_fastrtps_cpp

source /home/work/ros2_nvidia/install/setup.bash
source /zdrive/install/x86_64/interfaces/share/interfaces/local_setup.bash

showYellow "===================python3-tk===================="
echo "Install python3-tk for matplotlib"
# echo "y" | sudo apt install python3-tk
sudo apt install python3-tk -y --fix-missing
sudo pip3 install matplotlib
sudo pip3 install pandas --default-timeout=100 -i https://pypi.tuna.tsinghua.edu.cn/simple
sudo pip3 install numpy
sudo pip3 install openpyxl --default-timeout=100 -i https://pypi.tuna.tsinghua.edu.cn/simple
sudo pip3 install xlrd

showYellow "====================plotly==================="
echo "Install plotly and cufflinks for plotly"
sudo pip3 install plotly
# sudo pip3 install cufflinks

# workdir=$(cd $(dirname $0); pwd)
# dname=$(dirname "$PWD")
# bname=$(basename '$PWD')
# cd $bname

# export XDG_RUNTIME_DIR=/usr/lib/
# export RUNLEVEL=3

showYellow "====================pyqt5==================="
echo "Install pyqt5"
sudo pip3 install pyqt5==5.12.0
# sudo pip3 install pyqt5
sudo apt install pyqt5*  -y #安装pyqt5的依赖项
sudo apt install qt5-default qttools5-dev-tools  -y # 安装qtdesigner

# pypy——————————————————————————————————————————————————————————————
showYellow "======================================="
# https://blog.csdn.net/SunStrongInChina/article/details/111238778
# https://blog.csdn.net/u013719339/article/details/84141010

source /zdrive/myScripts/source_export.sh

showYellow "===================OPRN CORE DUMMP==================="
set -v
ulimit -c unlimited
mkdir /tmp/corefile
echo 1 > /proc/sys/kernel/core_uses_pid   # 生成的 core 文件名将会变成 core.pid，其中 pid 表示该进程的 PID
echo "/tmp/corefile/core-%e-%p-%t" > /proc/sys/kernel/core_pattern  # 设置生成的 core 文件保存在 “/tmp/corefile” 目录下，文件名格式为 “core-命令名-pid-时间戳"
set +v 

showYellow "===================OVER==================="

