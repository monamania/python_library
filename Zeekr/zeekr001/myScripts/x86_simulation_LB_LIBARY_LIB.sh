#!/usr/bin/env bash

export RMW_IMPLEMENTATION=rmw_fastrtps_cpp
local_path="/home/mono/zpilot/pnc_simulation_rviz/pnc_x86_simulation_install"
pnc_path="/zdrive"
source /home/work/ros2_nvidia/install/setup.bash
source ${pnc_path}/install/x86_64/interfaces/share/interfaces/local_setup.bash

export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${pnc_path}/install/x86_64/thirdparty_lib
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${pnc_path}/install/x86_64/interfaces/lib
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${pnc_path}/install/x86_64/hdmap/nav_service/lib
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${pnc_path}/install/x86_64/pnc/common/lib
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${pnc_path}/install/x86_64/platform/common/lib

export MAP_DATA_DIR=/zdrive/mapdata
export HDMAP_CONFIG_DIR=${pnc_path}/install/x86_64/hdmap
