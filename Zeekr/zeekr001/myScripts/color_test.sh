#/bin/bash
# https://blog.csdn.net/bbccaaac/article/details/103199258

# *******作者：        payne                                    *********
# *******创建时间：  2018/7/27                                  *********
# *******功能：      带颜色的回显                               *********
# *******版本：      v1.0                                       *********
#echo 用法：
#　　\33[0m 关闭所有属性
#　　\33[1m 设置高亮度
#　　\33[4m 下划线
#　　\33[5m 闪烁
#　　\33[7m 反显
#　　\33[8m 消隐
#　　\33[30m — \33[37m 设置前景色   --30:黑色 31:红色 32:绿色 33:黄色 34:蓝色 35:紫色 36:深绿色 37:白色
#　　\33[40m — \33[47m 设置背景色   --40:黑色 41:红色 42:绿色 43:黄色 44:蓝色 45:紫色 46:深绿色 47:白色
#　　\33[nA 光标上移n行
#　　\33[nB 光标下移n行
#　　\33[nC 光标右移n行
#　　\33[nD 光标左移n行
#　　\33[y;xH设置光标位置
#　　\33[2J 清屏
#　　\33[K 清除从光标到行尾的内容
#　　\33[s 保存光标位置
#　　\33[u 恢复光标位置
#　　\33[?25l 隐藏光标
#　　\33[?25h 显示光标
 
#显示绿色
#showGreen CONTENT
#CONTENT:显示的内容
showGreen()
{
  content=$1
  echo -e "\033[32m$content\033[0m"
}
 
#显示红色
#showRed CONTENT
#CONTENT:显示的内容
showRed()
{
  content=$1
  echo -e "\033[31m$content\033[0m"
}
 
#显示黄色,且设置为高亮
#showYellow CONTENT
#CONTENT:显示的内容
showYellow()
{
  content=$1
  echo -e "\033[1;33m$content\033[0m"
}

#显示蓝色,且设置为高亮
#showBlue CONTENT
#CONTENT:显示的内容
showBlue()
{
  content=$1
  echo -e "\033[1;34m$content\033[0m"
}

showYellow "test"



for STYLE in 0 1 2 3 4 5 6 7; do
  for FG in 30 31 32 33 34 35 36 37; do
    for BG in 40 41 42 43 44 45 46 47; do
      CTRL="\033[${STYLE};${FG};${BG}m"
      echo -en "${CTRL}"
      echo -n "${STYLE};${FG};${BG}"
    #   echo -n "hello"
      echo -en "\033[0m"
    done
    echo
  done
  echo
done
# Reset
echo -e "\033[0m"

