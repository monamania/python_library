#!/bin/sh
#@author: Monomania(mono_cjk@qq.com)
#显示黄色,且设置为高亮
#showYellow CONTENT
#CONTENT:显示的内容
showYellow()
{
  content=$1
  echo -e "\033[1;33m$content\033[0m"
}

export ROS_LOCALHOST_ONLY=1
export RMW_IMPLEMENTATION=rmw_fastrtps_cpp

source /home/work/ros2_nvidia/install/setup.bash
source /zdrive/install/x86_64/interfaces/share/interfaces/local_setup.bash

showYellow "======================================="
echo "Install python3-tk for matplotlib"
# echo "y" | sudo apt install python3-tk
sudo apt install python3-tk -y
pip3 install matplotlib
pip3 install pandas
pip3 install numpy

showYellow "======================================="
echo "rqt_bag"
pip3 install pillow
pip3 install pydot

showYellow "======================================="
echo "Install plotly and cufflinks for plotly"
# pip3 install plotly
# pip3 install cufflinks

# workdir=$(cd $(dirname $0); pwd)
# dname=$(dirname "$PWD")
# bname=$(basename '$PWD')
# cd $bname

# export XDG_RUNTIME_DIR=/usr/lib/
# export RUNLEVEL=3

showYellow "======================================="
# echo "Install pyqt5"
# sudo pip3 install pyqt5==5.12.0 -y
# sudo apt install pyqt5*  -y #安装pyqt5的依赖项
# sudo apt install qt5-default qttools5-dev-tools  -y # 安装qtdesigner

# pypy——————————————————————————————————————————————————————————————
showYellow "======================================="
# https://blog.csdn.net/SunStrongInChina/article/details/111238778
# https://blog.csdn.net/u013719339/article/details/84141010

source /zdrive/myScripts/source_export.sh

