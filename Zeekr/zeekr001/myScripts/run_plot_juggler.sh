#!/usr/bin/env bash
#-------------------------------------------------------
export ROS_LOCALHOST_ONLY=1
export RMW_IMPLEMENTATION=rmw_fastrtps_cpp
export GLOG_minloglevel=0

# get root dir
DISPLAY_AND_SIMULATION_PACKAGE_DIR=/zdrive

source /home/work/ros2_nvidia/install/setup.bash
source /home/work/plotjuggler/install/local_setup.bash
source ${DISPLAY_AND_SIMULATION_PACKAGE_DIR}/install/x86_64/interfaces/share/interfaces/local_setup.bash

ros2 run plotjuggler plotjuggler