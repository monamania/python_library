#!/bin/bash
#@auther: Monomania_cjk(mono_cjk@qq.com)
showYellow()
{
  content=$1
  echo -e "\033[1;33m$content\033[0m"
}

# ——————————————————————————————————————————————————————————————————————————————

# 当前目录
CRTDIR=$(pwd)
echo $CRTDIR
# # 脚本目录
# workdir=$(cd $(dirname $0); pwd)
# echo $workdir

showYellow '========================'
cd "$CRTDIR/common" 
showYellow "start git pull $pwd"
git checkout develop ; git branch --set-upstream-to=origin/develop develop
git pull

cd "$CRTDIR/vehicle_config" 
showYellow "start git pull $pwd"
git checkout develop ; git branch --set-upstream-to=origin/develop develop
git pull

# ——————————————————————————————————————————————————————————————————————————————
foreachd(){
# 遍历参数1 
 echo $1
 echo $1/*
        for file in $1/*  
            do
                # 如果是目录就打印处理，然后继续遍历，递归调用
                if [ -d $file ]
                then
                    echo $file"是目录" 
                    cd $file
                    showYellow "start git pull $file"
                    git checkout develop ; git branch --set-upstream-to=origin/develop develop
                    git pull
                    # foreachd $file
                # elif [ -f $file ]
                # then
                #     ./dy.exe -f $file >>./log
                fi
            done
}

showYellow '========================'
echo 'start pathList'
pathList=(
    "$CRTDIR/perception" 
    "$CRTDIR/thirdparty"  
    "$CRTDIR/interfaces" 
    "$CRTDIR/pilotsim" 
    "$CRTDIR/perception"
    "$CRTDIR/platform"
    "$CRTDIR/platform/tools"
    "$CRTDIR/hdmap"
    "$CRTDIR/hdmap/tools"
    "$CRTDIR/mapdata"
    "$CRTDIR/pnc"
    "$CRTDIR/pnc/modules"
    "$CRTDIR/pnc/tests"
    "$CRTDIR/pnc/tools"
    )

for path in ${pathList[@]}; do
    showYellow '-----------------------------'
    # echo $path
    cd $path
    foreachd $path
done
