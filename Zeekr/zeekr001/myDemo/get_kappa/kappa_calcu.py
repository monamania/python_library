import os
from pathlib import Path
import sys
import math
import time
import copy

from rcl_interfaces.msg import Log
from rclpy.serialization import deserialize_message
from rosidl_runtime_py.utilities import get_message
from std_msgs.msg import String

import rosbag2_py  

import matplotlib.pyplot as plt
import numpy as np

import scipy as sp
from scipy.optimize import leastsq


if os.environ.get('ROSBAG2_PY_TEST_WITH_RTLD_GLOBAL', None) is not None:
    # This is needed on Linux when compiling with clang/libc++.
    # TL;DR This makes class_loader work when using a python extension compiled with libc++.
    #
    # For the fun RTTI ABI details, see https://whatofhow.wordpress.com/2015/03/17/odr-rtti-dso/.
    sys.setdlopenflags(os.RTLD_GLOBAL | os.RTLD_LAZY)

def get_rosbag_options(path, serialization_format='cdr'):
    storage_options = rosbag2_py.StorageOptions(uri=path, storage_id='sqlite3')
    converter_options = rosbag2_py.ConverterOptions(
        input_serialization_format=serialization_format,
        output_serialization_format=serialization_format)

    return storage_options, converter_options


def chassis_report_reader(bag_file):
    bag_path = bag_file
    storage_options, converter_options = get_rosbag_options(bag_path)
    reader = rosbag2_py.SequentialReader()
    reader.open(storage_options, converter_options)
    topic_types = reader.get_all_topics_and_types()
    type_map = {topic_types[i].name: topic_types[i].type for i in range(len(topic_types))}
    counter = 0
    storage_filter = rosbag2_py.StorageFilter(topics=['/pnc/chassis_report'])
    reader.set_filter(storage_filter)

    str_angle = []

    while reader.has_next():
        (topic, data, t) = reader.read_next()            
        msg_type = get_message(type_map[topic])
        msg = deserialize_message(data, msg_type)
        # timestamp = msg_planning.header.stamp.sec + msg_planning.header.stamp.nanosec*1e-9
        timestamp = t*1.0e-9

        steering_angle = msg.steer.steering_angle.value

        str_angle.append(steering_angle)

        counter += 1
        if counter > 1e5:
            print('chassis_report exceed max number, data lost!')
            break
    return(str_angle)


def loc_report_reader(bag_file):
    bag_path = bag_file
    storage_options, converter_options = get_rosbag_options(bag_path)
    reader = rosbag2_py.SequentialReader()
    reader.open(storage_options, converter_options)
    topic_types = reader.get_all_topics_and_types()
    type_map = {topic_types[i].name: topic_types[i].type for i in range(len(topic_types))}
    counter = 0
    storage_filter = rosbag2_py.StorageFilter(topics=['/loc/ego_tf'])
    reader.set_filter(storage_filter)
    loc_x = []
    loc_y = []
    while reader.has_next():
        (topic, data, t) = reader.read_next()            
        msg_type = get_message(type_map[topic])
        msg = deserialize_message(data, msg_type)
        # timestamp = msg_planning.header.stamp.sec + msg_planning.header.stamp.nanosec*1e-9
        timestamp = t*1.0e-9

        x = msg.world_x
        y = msg.world_y

        loc_x.append(x)
        loc_y.append(y)

        counter += 1
        if counter > 1e5:
            print('chassis_report exceed max number, data lost!')
            break
    return(loc_x, loc_y)


def locrelative_report_reader(bag_file):
    bag_path = bag_file
    storage_options, converter_options = get_rosbag_options(bag_path)
    reader = rosbag2_py.SequentialReader()
    reader.open(storage_options, converter_options)
    topic_types = reader.get_all_topics_and_types()
    type_map = {topic_types[i].name: topic_types[i].type for i in range(len(topic_types))}
    counter = 0
    storage_filter = rosbag2_py.StorageFilter(topics=['/loc/ego_tf_relative'])
    reader.set_filter(storage_filter)
    loc_x_relative = []
    loc_y_relative = []
    while reader.has_next():
        (topic, data, t) = reader.read_next()            
        msg_type = get_message(type_map[topic])
        msg = deserialize_message(data, msg_type)
        # timestamp = msg_planning.header.stamp.sec + msg_planning.header.stamp.nanosec*1e-9
        timestamp = t*1.0e-9

        x = msg.x
        y = msg.y

        loc_x_relative.append(x)
        loc_y_relative.append(y)

        counter += 1
        if counter > 1e5:
            print('chassis_report exceed max number, data lost!')
            break
    return(loc_x_relative, loc_y_relative)


def find_time_stamp(t, t_list):
    for i in range(0,len(t_list)):
        if t_list[i]>=t:
            return i


def kappa_identy(bag_file):
 
    # 读取定位数据
    # loc_x, loc_y = loc_report_reader(bag_file)
    loc_x, loc_y = locrelative_report_reader(bag_file)
    steer_angle = chassis_report_reader(bag_file)
    print('str_angl = ', np.mean(steer_angle))

    # 初始化矩阵
    data_length = len(loc_x)
    matrix_a = np.mat(np.zeros((data_length, 3)))
    matrix_b = np.mat(np.zeros((data_length, 1)))

    # 拟合圆形
    for i in range(len(matrix_a)):
        matrix_a[i, 0] = loc_x[i]
        matrix_a[i, 1] = loc_y[i]
        matrix_a[i, 2] = 1
        matrix_b[i, 0] = loc_x[i] * loc_x[i] + loc_y[i] * loc_y[i]
    # print(matrix_a)
    # print(matrix_b)

    x_abc = np.mat(np.zeros((3, 1)))
    x_abc = np.linalg.inv(matrix_a.conj().transpose() @ matrix_a) @ (matrix_a.conj().transpose() @ matrix_b)
    # print('x_abc_1 = ', x_abc[0, 0])
    # print('x_abc_2 = ', x_abc[1, 0])
    # print('x_abc_3 = ', x_abc[2, 0])

    org_x = x_abc[0, 0] / 2
    org_y = x_abc[1, 0] / 2
    r_c = math.sqrt(x_abc[0,0] * x_abc[0,0] + x_abc[1,0] * x_abc[1,0] + 4 * x_abc[2,0]) / 2
    print('org_x = ', org_x)
    print('org_y = ', org_y)
    print('radius = ', r_c)
    print('kappa = ', 1 / r_c)

    # 计算拟合圆坐标
    loc_x_polyfit = []
    loc_y_polyfit = []
    for i in range(360):
        x = org_x + r_c * math.cos(i / 180 * 3.1415926)
        y = org_y + r_c * math.sin(i / 180 * 3.1415926)
        loc_x_polyfit.append(x)
        loc_y_polyfit.append(y)


    # 绘图对比
    plt.subplot(211)
    plt.plot(loc_x, loc_y, 'r*', label = 'loc')
    plt.plot(org_x, org_y, 'b+')
    plt.plot(loc_x_polyfit, loc_y_polyfit, label = 'loc_polyfit')
    plt.axis('equal')
    plt.legend()
    plt.subplot(212)
    plt.plot(steer_angle, label = 'steer_angle')
    plt.legend()
    plt.show(block=True)
    

if __name__ == '__main__':
     import sys
     list_of_args = sys.argv
     if len(list_of_args)<2:
         print('Required parameter<bag_path> missing!')
     else:
         kappa_identy(list_of_args[1])