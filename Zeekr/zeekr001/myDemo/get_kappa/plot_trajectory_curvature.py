# read bag and plot trajectory
# usage: python3 plot_trajectory.py bag_path time_stamp
# 

import os
from pathlib import Path
import sys
import math
import time
from turtle import left

from rcl_interfaces.msg import Log
from rclpy.serialization import deserialize_message
from rosidl_runtime_py.utilities import get_message
from std_msgs.msg import String

import rosbag2_py  

import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, TextBox
import numpy as np
from vehicle_model_sim import VehicleModelSim


if os.environ.get('ROSBAG2_PY_TEST_WITH_RTLD_GLOBAL', None) is not None:
    # This is needed on Linux when compiling with clang/libc++.
    # TL;DR This makes class_loader work when using a python extension compiled with libc++.
    #
    # For the fun RTTI ABI details, see https://whatofhow.wordpress.com/2015/03/17/odr-rtti-dso/.
    sys.setdlopenflags(os.RTLD_GLOBAL | os.RTLD_LAZY)

def get_rosbag_options(path, serialization_format='cdr'):
    storage_options = rosbag2_py.StorageOptions(uri=path, storage_id='sqlite3')
    converter_options = rosbag2_py.ConverterOptions(
        input_serialization_format=serialization_format,
        output_serialization_format=serialization_format)

    return storage_options, converter_options

def ego_tf_reader(bag_file):
    bag_path = bag_file
    # bag_path = r'/home/zhiyuan/data/1202/rosbag2_2021_12_02/rosbag2_2021_12_02-23_19_03/rosbag2_2021_12_02-23_19_03_0.db3'
    storage_options, converter_options = get_rosbag_options(bag_path)
    reader = rosbag2_py.SequentialReader()
    reader.open(storage_options, converter_options)
    topic_types = reader.get_all_topics_and_types()
    # Create a map for quicker lookup
    type_map = {topic_types[i].name: topic_types[i].type for i in range(len(topic_types))}
    # Set filter for topic of string type
    msg_counter = 0
    t_loc = []
    delta_loc =[]
    storage_filter = rosbag2_py.StorageFilter(topics=['/loc/ego_tf'])
    reader.set_filter(storage_filter)
    while reader.has_next():
        (topic, data, t) = reader.read_next()
        # print('t'); print(type(t)); print(t*1e-9)
        msg_type = get_message(type_map[topic])
        msg_localizaztion = deserialize_message(data, msg_type)
        # print(msg_localizaztion)
        tem_loc = []
        # loc_timestamp = msg_localizaztion.header.stamp.sec + msg_localizaztion.header.stamp.nanosec*1e-9
        loc_timestamp = t*1.0e-9
        # print(msg_localizaztion.header.stamp.sec, msg_localizaztion.header.stamp.nanosec)
        world_x = msg_localizaztion.world_x
        world_y = msg_localizaztion.world_y
        world_yaw = msg_localizaztion.world_yaw
        tem_loc = [world_x, world_y, world_yaw]
        # print(tem_loc)
        # print(world_x)
        # print(world_y)
        # print(yaw)
        # print(planning_timestamp)
        # print(msg_counter)  
        msg_counter += 1
        # print(msg_counter)
        if msg_counter>2e5:
            print('ego_tf exceed max number, data lost!')
            break
        t_loc.append(loc_timestamp)
        delta_loc.append(tem_loc)
    # print(t_loc)
    # print(delta_loc)
    return (t_loc, delta_loc)


def ego_tf_relative_reader(bag_file):
    bag_path = bag_file
    # bag_path = r'/home/zhiyuan/data/1202/rosbag2_2021_12_02/rosbag2_2021_12_02-23_19_03/rosbag2_2021_12_02-23_19_03_0.db3'
    storage_options, converter_options = get_rosbag_options(bag_path)
    reader = rosbag2_py.SequentialReader()
    reader.open(storage_options, converter_options)
    topic_types = reader.get_all_topics_and_types()
    # Create a map for quicker lookup
    type_map = {topic_types[i].name: topic_types[i].type for i in range(len(topic_types))}
    # Set filter for topic of string type
    msg_counter = 0
    t_loc = []
    data_loc =[]
    storage_filter = rosbag2_py.StorageFilter(topics=['/loc/ego_tf_relative'])
    reader.set_filter(storage_filter)
    while reader.has_next():
        (topic, data, t) = reader.read_next()
        # print('t'); print(type(t)); print(t*1e-9)
        msg_type = get_message(type_map[topic])
        msg_localizaztion = deserialize_message(data, msg_type)
        # print(msg_localizaztion)
        tem_loc = []
        # loc_timestamp = msg_localizaztion.header.stamp.sec + msg_localizaztion.header.stamp.nanosec*1e-9
        loc_timestamp = t*1.0e-9
        # print(msg_localizaztion.header.stamp.sec, msg_localizaztion.header.stamp.nanosec)
        world_x = msg_localizaztion.x
        world_y = msg_localizaztion.y
        world_yaw = msg_localizaztion.yaw
        tem_loc = [world_x, world_y, world_yaw]
        # print(tem_loc)
        # print(world_x)
        # print(world_y)
        # print(yaw)
        # print(planning_timestamp)
        # print(msg_counter)  
        msg_counter += 1
        # print(msg_counter)
        if msg_counter>2e5:
            print('ego_tf exceed max number, data lost!')
            break
        t_loc.append(loc_timestamp)
        data_loc.append(tem_loc)
    # print(t_loc)
    # print(delta_loc)
    return (t_loc, data_loc)


def control_debug_reader(bag_file):
    bag_path = bag_file
    storage_options, converter_options = get_rosbag_options(bag_path)
    reader = rosbag2_py.SequentialReader()
    reader.open(storage_options, converter_options)
    topic_types = reader.get_all_topics_and_types()
    # Create a map for quicker lookup
    type_map = {topic_types[i].name: topic_types[i].type for i in range(len(topic_types))}
    # Set filter for topic of string type
    msg_counter = 0
    t_ = []
    data_ = []
    storage_filter = rosbag2_py.StorageFilter(topics=['/pnc/control_debug'])
    reader.set_filter(storage_filter)
    while reader.has_next():
        (topic, data, t) = reader.read_next()
        # print('t'); print(type(t)); print(t*1e-9)
        msg_type = get_message(type_map[topic])
        msg_ = deserialize_message(data, msg_type)
        # print(msg_localizaztion)
        # loc_timestamp = msg_localizaztion.header.stamp.sec + msg_localizaztion.header.stamp.nanosec*1e-9
        timestamp = t*1.0e-9
        # print(msg_localizaztion.header.stamp.sec, msg_localizaztion.header.stamp.nanosec)
        heading_error = msg_.heading_error
        lateral_error = msg_.lateral_error
        lat_mpc_debug = msg_.lat_mpc_debug
        current_speed = msg_.current_speed
        ref_point_x = lat_mpc_debug.ref_point_x
        ref_point_y = lat_mpc_debug.ref_point_y
        steer_angle_now = msg_.steering_position/100.0*7.7
        steer_angle_cmd = msg_.steer_angle/100.0*7.7
        steer_angle_fd = msg_.steer_angle_feedback/100.0*7.7
        control_cmd = []
        for pt in lat_mpc_debug.control_cmd:
            control_cmd.append(pt)
        curvature = []
        for pt in lat_mpc_debug.curvature:
            curvature.append(pt)
        e1 = []
        for pt in lat_mpc_debug.solution1:
            e1.append(pt)
        e1_dot = []
        for pt in lat_mpc_debug.solution2:
            e1_dot.append(pt)
        e2 = []
        for pt in lat_mpc_debug.solution3:
            e2.append(pt)
        e2_dot = []
        for pt in lat_mpc_debug.solution4:
            e2_dot.append(pt)
        traj_x = []
        for pt in lat_mpc_debug.trajectory_x:
            traj_x.append(pt)
        traj_y = []
        for pt in lat_mpc_debug.trajectory_y:
            traj_y.append(pt)
        traj_yaw = []
        for pt in lat_mpc_debug.trajectory_yaw:
            traj_yaw.append(pt)
        pred_x = []
        for pt in lat_mpc_debug.predicted_x:
            pred_x.append(pt)
        pred_y = []
        for pt in lat_mpc_debug.predicted_y:
            pred_y.append(pt)
        pred_yaw = []
        for pt in lat_mpc_debug.predicted_yaw:
            pred_yaw.append(pt)

        data_tem_ = [lateral_error, heading_error, ref_point_x, ref_point_y, control_cmd, curvature, 
                    e1, e1_dot, e2, e2_dot, current_speed, traj_x, traj_y, traj_yaw, pred_x, pred_y, pred_yaw,
                    steer_angle_now, steer_angle_cmd, steer_angle_fd]

        msg_counter += 1
        # print(msg_counter)
        if msg_counter>2e5:
            print('ego_tf exceed max number, data lost!')
            break
        t_.append(timestamp)
        data_.append(data_tem_)
    # print(t_loc)
    # print(delta_loc)
    return (t_, data_)


# planning_result
def planning_result_reader(bag_file):
    bag_path = bag_file
    # bag_path = r'/media/fxh/E/zpilot/rosbag2_py_example/rosbag2_py/resources/talker/rosbag2_2021_11_21-18_24_32_0.db3'
    storage_options, converter_options = get_rosbag_options(bag_path)
    reader = rosbag2_py.SequentialReader()
    reader.open(storage_options, converter_options)
    topic_types = reader.get_all_topics_and_types()
    type_map = {topic_types[i].name: topic_types[i].type for i in range(len(topic_types))}
    counter = 0
    storage_filter = rosbag2_py.StorageFilter(topics=['/pnc/planning_result'])
    reader.set_filter(storage_filter)
    t_plan = []
    delta_plan = []
    while reader.has_next():
        (topic, data, t) = reader.read_next()
        # print('t'); print(type(t)); print(t*1e-9)
        # if counter <500:
        #     counter+=1
        #     continue               
        msg_type = get_message(type_map[topic])
        msg_planning = deserialize_message(data, msg_type)
        # print(msg_planning)
        # print(msg.header.stamp.sec)
        # print(msg.header.stamp.nanosec)

        #  get point in PlaningTrajectory
        # planning_timestamp = msg_planning.header.stamp.sec + msg_planning.header.stamp.nanosec*1e-9
        planning_timestamp = t*1e-9
        # print(planning_timestamp)

        # print(counter)

        x = []
        y = []
        t_rel = []
        kappa = []
        tem_plan = []
        for pt in msg_planning.trajectory_point:
            x.append(pt.path_point.x)
            y.append(pt.path_point.y)
            t_rel.append(pt.relative_time)
            kappa.append(pt.path_point.kappa)
        # print(x)
        # print(y)
        tem_plan = [x, y, t_rel, kappa]
        t_plan.append(planning_timestamp)
        delta_plan.append(tem_plan) 
        counter += 1
        if counter>=1e5:
            print('planning_result exceed max number, data lost!')
            break
    # print(t_plan)
    # print(delta_plan)
    return (t_plan, delta_plan)


def find_time_stamp(t, t_list):
    # for i in range(0,len(t_list)):
    #     if t_list[i]>=t:
    #         return i
    return np.argmin(np.fabs(np.array(t_list) - t))

import copy
def trans_cord(x_0, y_0, yaw_0, plan_path):
    new_path = copy.deepcopy(plan_path)
    for i in range(0,len(plan_path[0])): 
        # xy = plan_path[i]
        xy_x = plan_path[0][i]
        xy_y = plan_path[1][i]
        x_new = xy_x - x_0
        y_new = xy_y - y_0
        cos_theta = math.cos(yaw_0)
        sin_theta = math.sin(yaw_0)
        x_new_final =  x_new*cos_theta + y_new*sin_theta
        y_new_final = -x_new*sin_theta + y_new*cos_theta
        # new_pt = [x_new_final, y_new_final]
        new_path[0][i] = x_new_final
        new_path[1][i] = y_new_final
    return(new_path)

def trans_traj(traj_x, traj_y, x_0, y_0, yaw_0):
    new_path = np.empty([len(traj_x), 2])
    new_path[:, 0] = np.array(traj_x)
    new_path[:, 1] = np.array(traj_y)
    new_path = new_path - np.array([x_0, y_0])
    mat_r = np.array([[math.cos(yaw_0), -math.sin(yaw_0)],
                      [math.sin(yaw_0),  math.cos(yaw_0)]])
    new_path = new_path @ mat_r
    return(new_path)

def plot_traj(bag_file, time_stamp):
    # show trajectory
    t_loc, delta_loc = ego_tf_relative_reader(bag_file)
    t_plan, delta_plan = planning_result_reader(bag_file)
    control_debug_reader(bag_file)
    plan_pt_0 = []
    plan_pt_0_x = []
    plan_pt_0_y = []
    for traj in delta_plan:
        x_tmp = traj[0]
        y_tmp = traj[1]
        t_tmp = traj[2]
        if  len(t_tmp)>0:
            indx = len(t_tmp)-1
            for i in range(len(t_tmp)):
                if   t_tmp[i]>=0.0:
                    indx = i
                    break
            plan_pt_0_x.append(x_tmp[indx])
            plan_pt_0_y.append(y_tmp[indx])
            plan_pt_0.append([x_tmp[indx], y_tmp[indx]])


    t1 = time_stamp # 1637644973.052 # 1637644972.602  # 1637644972.522  1637644973.052
    # t2 = 1637644972.732
    # import pdb; pdb.set_trace()
    indx_loc_1 =find_time_stamp(t1, t_loc)
    indx_pln_1 =find_time_stamp(t1, t_plan)

    loc_tmp = delta_loc[indx_loc_1]
    traj_tmp = delta_plan[indx_pln_1]
    new_path = trans_cord(loc_tmp[0],loc_tmp[1],loc_tmp[2],traj_tmp)
    loc_pt = np.array(delta_loc)

    # import pdb; pdb.set_trace()

    fig, ax = plt.subplots(2,1)
    # ax.plot(plan_pt_0_x, plan_pt_0_y, delta_loc[:][0], delta_loc[:][1])
    # plt.figure(2,1)
    ax[0].plot(loc_pt[:,0], loc_pt[:,1], delta_loc[indx_loc_1][0], delta_loc[indx_loc_1][1],'x')
    ax[0].axis('equal')
    ax[0].grid(True)
    ax[1].plot(-np.array(new_path[1]), np.array(new_path[0]), 'r')
    # ax[1].axis('equal')
    plt.xlim(-1, 1)
    ax[1].grid(True)
    ax[0].set_title(str(t_plan[indx_pln_1]))
    # print(str(t1))
    plt.show(block=True)



def plot_multi_traj(bag_file, time_stamp):
    # show trajectory
    # t_loc, delta_loc = ego_tf_reader(bag_file)
    t_loc, delta_loc = ego_tf_relative_reader(bag_file)
    t_plan, delta_plan = planning_result_reader(bag_file)
    t_debug, data_debug = control_debug_reader(bag_file)
        
    fig_row = 2
    fig_col = 6
    # fig, ax = plt.subplots(fig_row, fig_col)
    for i in range(0,12):
        t1 = time_stamp - 0.6 + 0.1*i # 1637644973.052 # 1637644972.602  # 1637644972.522  1637644973.052
        # t2 = 1637644972.732
        # import pdb; pdb.set_trace()
        indx_loc_1 =find_time_stamp(t1, t_loc)
        indx_pln_1 =find_time_stamp(t1, t_plan)
        # print(indx_pln_1)
        # print(indx_loc_1)

        loc_tmp = delta_loc[indx_loc_1]
        traj_tmp = delta_plan[indx_pln_1]
        new_path = trans_cord(loc_tmp[0],loc_tmp[1],loc_tmp[2],traj_tmp)
        # loc_pt = np.array(delta_loc)

        # import pdb; pdb.set_trace()

        ax_ = plt.subplot(fig_row, fig_col, i+1)
        ax_.plot(-np.array(new_path[1]), np.array(new_path[0]), 'r')
        # ax_.plot(np.array(traj_tmp[0]), np.array(traj_tmp[1]), 'r.', np.array(loc_tmp[0]), np.array(loc_tmp[1]), 'bx')
        # ax[m,n].axis('equal')
        # plt.xlim(-1, 1)
        ax_.grid(True)
        ax_.set_title(str(t_plan[indx_pln_1]))

    plt.show(block=True)


def plot_multi_traj_inner(bag_file, time_stamp):
    # show trajectory
    # t_loc, delta_loc = ego_tf_reader(bag_file)
    t_loc, delta_loc = ego_tf_relative_reader(bag_file)
    t_plan, delta_plan = planning_result_reader(bag_file)
    t_debug, data_debug = control_debug_reader(bag_file)

    t_slider_start = max([t_loc[0], t_plan[0],t_debug[0]])+0.5
    t_slider_end = max([t_loc[-1], t_plan[-1],t_debug[-1]])-0.5
    time_stamp = t_slider_start

    fig_row = 5
    fig_col = 5
    fig, _ = plt.subplots(fig_row, fig_col)

    def plot_inside(time_stamp):
        for i in range(0, fig_col):
            t1 = time_stamp - 0.2 + 0.03*i # 1637644973.052 # 1637644972.602  # 1637644972.522  1637644973.052
            # import pdb; pdb.set_trace()
            indx_loc_1 =find_time_stamp(t1, t_loc)
            indx_pln_1 =find_time_stamp(t1, t_plan)
            indx_debug_1 =find_time_stamp(t1, t_debug)

            loc_tmp = delta_loc[indx_loc_1]
            traj_tmp = delta_plan[indx_pln_1]
            new_path = trans_cord(loc_tmp[0],loc_tmp[1],loc_tmp[2],traj_tmp)
            debug_tmp = data_debug[indx_debug_1]
            traj_x_debug = debug_tmp[11]
            traj_y_debug = debug_tmp[12]
            if len(debug_tmp[14])>0:
                prdct_x_debug = debug_tmp[14]
                prdct_y_debug = debug_tmp[15]
            else:
                prdct_x_debug = debug_tmp[11]
                prdct_y_debug = debug_tmp[12]

            new_path_traj = trans_traj(traj_x_debug, traj_y_debug, loc_tmp[0], loc_tmp[1], loc_tmp[2])
            new_path_predct = trans_traj(prdct_x_debug, prdct_y_debug, loc_tmp[0], loc_tmp[1], loc_tmp[2])


            # import pdb; pdb.set_trace()
            N = len(debug_tmp[4])
            veh_sim = VehicleModelSim(0.05)
            spd = [debug_tmp[10]]*N
            x, y, _ = veh_sim.TrajSim(spd, np.array(debug_tmp[4][0:N])*14.6, 1.5, 0.0, 0.0)
            
            ax_ = plt.subplot(fig_row, fig_col, i+1)
            plt.cla()
            ax_.plot(-np.array(new_path[1]), np.array(new_path[0]), 'r', -np.array(y), np.array(x), 'b.-', 
                        -new_path_traj[:,1], new_path_traj[:,0], 'r.', -new_path_predct[:,1], new_path_predct[:,0], 'g.')
            # ax_.plot(np.array(traj_tmp[0]), np.array(traj_tmp[1]), 'r.', np.array(loc_tmp[0]), np.array(loc_tmp[1]), 'bx')
            ax_.axis('equal')
            plt.xlim(-2, 2)
            plt.ylim(-1, 10)
            ax_.grid(True)
            # time_str = ('%0.3f' %(t_plan[indx_pln_1]))
            time_str = ('%0.3f' %(t_debug[indx_debug_1]))
            ax_.set_title(time_str)

            # data_tem_ = [lateral_error, heading_error, ref_point_x, ref_point_y, control_cmd, curvature, 
            #                    0              1            2               3           4           5         
            #             e1, e1_dot, e2, e2_dot, current_speed, traj_x, traj_y, traj_yaw, pred_x, pred_y, pred_yaw]
                        #  6   7       8     9        10           11        12       13     14      15        16
                    # steer_angle_now, steer_angle_cmd, steer_angle_fd]
                            #    17         18                19                  
            t_debug_frm = np.array(list(range(N)))*0.05
            ax_ = plt.subplot(fig_row, fig_col, fig_col+i+1)
            plt.cla()
            ax_.plot(t_debug_frm, np.array(debug_tmp[4][0:N])*57.3*14.6, 'r.-', 
            np.array([0.0]), np.array([debug_tmp[17]])*57.3, 'bo',
            np.array([0.0]), np.array([debug_tmp[19]])*57.3, 'g+',
            np.array([0.0]), np.array([debug_tmp[18]])*57.3, 'rx',)
            # ax[m,n].axis('equal')
            # plt.xlim(-1, 1)
            ax_.grid(True)
            ax_.legend(['u [deg]','now','fb','cmd'])
            # ax_.legend()

            ax_ = plt.subplot(fig_row, fig_col, fig_col*2+i+1)
            plt.cla()
            ax_.plot(t_debug_frm, np.array(debug_tmp[6][0:N]), 'r.-')
            # ax[m,n].axis('equal')
            # plt.xlim(-1, 1)
            ax_.grid(True)
            # ax_.set_title(str(t_debug[indx_debug_1]))
            ax_.legend(['e1 [m]'])
            # ax_.legend()

            ax_ = plt.subplot(fig_row, fig_col, fig_col*3+i+1)
            plt.cla()
            ax_.plot(t_debug_frm, np.array(debug_tmp[8][0:N])*57.3, 'r.-')
            # ax[m,n].axis('equal')
            # plt.xlim(-1, 1)
            ax_.grid(True)
            # ax_.set_title(str(t_debug[indx_debug_1]))
            ax_.legend(['e2 [deg]'])
            # ax_.legend()

            ax_ = plt.subplot(fig_row, fig_col, fig_col*4+i+1)
            plt.cla()
            ax_.plot(t_debug_frm, np.array(debug_tmp[5][0:N]), 'r.-')
            # ax[m,n].axis('equal')
            # plt.xlim(-1, 1)
            ax_.grid(True)
            # ax_.set_title(str(t_debug[indx_debug_1]))
            ax_.legend(['kappa'])
            # ax_.legend()

    plot_inside(time_stamp)

    # Make a horizontal slider to control the frequency.
    axtime = plt.axes([0.15, 0.05, 0.65, 0.03])
    freq_slider = Slider(
        ax=axtime,
        label='t [s]',
        valmin=t_slider_start,
        valmax=t_slider_end,
        valinit=t_slider_start,
        valstep=0.01
    )
    # The function to be called anytime a slider's value changes
    def update(val):
        plot_inside(freq_slider.val)
        fig.canvas.draw_idle()

    # register the update function with each slider
    freq_slider.on_changed(update)

    def on_key_release(event):
        # print(event.key)
        step = 0.1
        if event.key == 'left':
            tmp = freq_slider.val - step
            tmp = max(tmp, t_slider_start)
            tmp = min(tmp, t_slider_end)
            freq_slider.set_val(tmp)
        if event.key == 'right':
            tmp = freq_slider.val + step
            tmp = max(tmp, t_slider_start)
            tmp = min(tmp, t_slider_end)
            freq_slider.set_val(tmp)

    fig.canvas.mpl_connect('key_release_event', on_key_release)

    # def on_submit(expression):
    #     tmp = float(expression)
    #     tmp = max(tmp, t_slider_start)
    #     tmp = min(tmp, t_slider_end)
    #     freq_slider.set_val(tmp)

    # axbox = fig.add_axes([0.05, 0.05, 0.07, 0.03])
    # text_box = TextBox(axbox, "t")
    # text_box.on_submit(on_submit)
    # text_box.set_val(('%0.3f' % (t_slider_start)))

    plt.show(block=True)



if __name__ == '__main__':
    # from tkinter import Tk
    # from tkinter.filedialog import askopenfilename
    # root = Tk()
    # root.withdraw()
    # bag_file = askopenfilename(title='Please choose a ros2 bag file', 
    #               initialdir='~', filetypes=[('ros2 bag file','*.db3')])
    # if bag_file:
    #     plot_multi_traj_inner(bag_file, 0.0)
    bag_file = r"/home/zhiyuan/zPilot/rosbag2_2022_04_08-18_59_03/rosbag2_2022_04_08-18_59_03_0.db3"
    t_loc, delta_loc = ego_tf_relative_reader(bag_file)
    t_plan, delta_plan = planning_result_reader(bag_file)
    t_debug, data_debug = control_debug_reader(bag_file)
    t1 = 1649415578.488
    indx_loc_1 =find_time_stamp(t1, t_loc)
    indx_pln_1 =find_time_stamp(t1, t_plan)
    indx_debug_1 =find_time_stamp(t1, t_debug)

    loc_tmp = delta_loc[indx_loc_1]
    traj_tmp = delta_plan[indx_pln_1]
    new_path = trans_cord(loc_tmp[0],loc_tmp[1],loc_tmp[2],traj_tmp)
    # debug_tmp = data_debug[indx_debug_1]
    # traj_x_debug = debug_tmp[11]
    # traj_y_debug = debug_tmp[12]
    # if len(debug_tmp[14])>0:
    #     prdct_x_debug = debug_tmp[14]
    #     prdct_y_debug = debug_tmp[15]
    # else:
    #     prdct_x_debug = debug_tmp[11]
    #     prdct_y_debug = debug_tmp[12]

    # new_path_traj = trans_traj(traj_x_debug, traj_y_debug, loc_tmp[0], loc_tmp[1], loc_tmp[2])
    # new_path_predct = trans_traj(prdct_x_debug, prdct_y_debug, loc_tmp[0], loc_tmp[1], loc_tmp[2])
    
    # f1 为各项的系数，3 表示想要拟合的最高次项是多少。
    x = np.array(new_path[0][0:20])
    y = np.array(new_path[1][0:20])
    k_org = np.array(new_path[3][0:20])

    f1 = np.polyfit(x, y, 3)
    # p1 为拟合的多项式表达式
    p1 = np.poly1d(f1)
    print('p1 is :\n',p1)
    ydot = np.poly1d([f1[0]*3, f1[1]*2, f1[2]])
    ydotdot = np.poly1d([f1[0]*6, f1[1]*2])
    k = ydotdot(x) / np.float_power((1 + ydot(x)*ydot(x)), 1.5)
    print("k_org", "k","diff")
    print(k_org)
    print(k)
    print(k-k_org)

    fig, _ = plt.subplots(2, 1)
    ax_ = plt.subplot(2, 1, 1)
    plt.cla()
    plt.plot(x, y, 's',label='original values')
    yvals = p1(x) #拟合y值
    plt.plot(x, yvals, 'r',label='polyfit values')

    ax_ = plt.subplot(2, 1, 2)
    plt.plot(x, k_org, 'b.-',label='original kappa')
    plt.plot(x, k, 'r.-',label='polyfit kappa')
    plt.show()
