[PNC小包换包脚本使用指南](https://doc.weixin.qq.com/doc/w3_AasAvgbOAEoqsZKOXQ6RPSxnmG2V7?scode=ANkACgc-AAwMNJkGXVAdAA1QZRAGQ&version=2.8.10.2010&platform=win)

## MPC是跟踪控制算法--纵向  LON_MPC_NEW_CONTROLLER

### MPC old
// 老版本-----新版本不是跟踪误差的
lon_mpc_controller_conf {
  ts: 0.01
  acceleration_deadzone : 0.05
  deceleration_deadzone : -0.05
  preview_window: 20.0
  standstill_acceleration: -3.0
  use_increment_lon_mpc: false
  use_increment_qpoases_lon_mpc: true
  eps: 0.01
  rho: 10
  max_iteration: 500
  prediction_horizon: 10
  control_horizon: 7
  time_tau: 1.49
  matrix_q_three_state: 10.0  // 跟踪距离s的误差
  matrix_q_three_state: 0.7   // 速度v的误差
  matrix_q_three_state: 0.1   // 加速度 a 的误差
  matrix_q_two_state: 10.0    // 无用
  matrix_q_two_state: 13.0    // 无用
  matrix_r: 0.05              // 控制量矩阵---纵向下发的为加速度指令，此为加速度控制矩阵
  matrix_g: 0.05              // 一般不动（干扰量矩阵）
  model_type: three_state_identi_model
  use_speed_station_preview: true
  cmd_mean_filter_window_size: 6
  station_error_cutoff_freq: 0.5
  speed_error_cutoff_freq: 3.0
  accele_error_mean_filter_window_size: 14
  use_digital_filter_station_error: false
  use_digital_filter_speed_error: false
  use_kalman_filter_accele: false
  use_mean_filter_cmd: true
  use_mean_filter_accele_error: false
  use_leadlag_lon_mpc: false
  use_slope_offset: false
  use_pitch_offset: false
  pitch_offset: -0.014
  use_preview_acceleration_reference: true
  use_acceleration_correction: false
  use_low_speed_estimate: false
  use_acceleration_for_error_calculation: true
  soft_station_error: 0.4
  soft_speed_error: 0.3
  soft_cutoff_speed: 3.5
  starting_distance_min: 3.0
  starting_speed_min: 1.0
  accele_jerk_limit: 20.0
  
}

### MPC new
lon_mpc_new_controller_conf {
  ts: 0.1
  acceleration_deadzone : 0.01
  deceleration_deadzone : -0.01
  preview_window: 0.0
  standstill_acceleration: -1.0
  use_increment_lon_mpc: false
  use_increment_qpoases_lon_mpc: false
  eps: 0.01
  rho: 10
  max_iteration: 100
  prediction_horizon: 10
  control_horizon: 10
  time_tau: 1.49
  matrix_q_three_state: 0.4    // 不用了---调参，看下面的 mpc_coefficients_scheduler
  matrix_q_three_state: 12.0
  matrix_q_three_state: 0.1
  matrix_q_two_state: 10.0
  matrix_q_two_state: 13.0
  matrix_r: 0.5
  matrix_g: 0.05
  use_speed_station_preview: false
  cmd_mean_filter_window_size: 6
  station_error_cutoff_freq: 0.5
  speed_error_cutoff_freq: 3.0
  accele_error_mean_filter_window_size: 14
  use_digital_filter_station_error: false
  use_digital_filter_speed_error: false
  use_kalman_filter_accele: false
  use_mean_filter_cmd: true
  use_mean_filter_accele_error: false
  use_leadlag_lon_mpc: false
  use_slope_offset: false
  use_pitch_offset: false
  pitch_offset: -0.014
  use_preview_acceleration_reference: false
  use_acceleration_correction: false
  use_low_speed_estimate: false
  use_acceleration_for_error_calculation: false
  soft_station_error: 0.4
  soft_speed_error: 0.3
  soft_cutoff_speed: 3.5
  starting_distance_min: 3.0
  starting_speed_min: 1.0
  accele_jerk_limit: 20.0
  open_loop_stop_speed: 1.0
  mpc_coefficients_scheduler {
    scheduler {
      speed: 0.0
      value: 0.4    // q - s 
      value: 12.0   // q - v
      value: 0.1    // q - a
      value: 0.5    // r矩阵
    }
    // 25 m/s 以上用下面的参数
    scheduler {
      speed: 25.0
      value: 0.4
      value: 12.0
      value: 0.1
      value: 0.5
    }
  }
  speed_only_mpc_scheduler {
    scheduler {
      speed: 0.0
      value: 0.0
      value: 12.0
      value: 0.1
      value: 0.5
    }
    scheduler {
      speed: 50.0
      value: 0.4
      value: 12.0
      value: 0.1
      value: 0.5
    }
  }
  station_err_gain_scheduler {
    scheduler {
      speed: 1.4
      ratio: 1.0
    }
    scheduler {
      speed: 2.8
      ratio: 1.0
    }
    scheduler {
      speed: 4.2
      ratio: 1.0
    }
    scheduler {
      speed: 5.6
      ratio: 1.0
    }
    scheduler {
      speed: 7.0
      ratio: 1.0
    }
    scheduler {
      speed: 8.4
      ratio: 1.0
    }
    scheduler {
      speed: 9.8
      ratio: 1.0
    }
    scheduler {
      speed: 11.2
      ratio: 1.0
    }
    scheduler {
      speed: 12.6
      ratio: 1.0
    }
    scheduler {
      speed: 14.0
      ratio: 1.0
    }
    scheduler {
      speed: 15.4
      ratio: 1.0
    }
    scheduler {
      speed: 16.8
      ratio: 1.0
    }
    scheduler {
      speed: 18.0
      ratio: 1.0
    }
    scheduler {
      speed: 25.0
      ratio: 1.0
    }
  }
  speed_err_gain_scheduler {
    scheduler {
      speed: 1.4
      ratio: 0.85
    }
    scheduler {
      speed: 2.8
      ratio: 1.0
    }
    scheduler {
      speed: 4.2
      ratio: 1.0
    }
    scheduler {
      speed: 5.6
      ratio: 1.0
    }
    scheduler {
      speed: 7.0
      ratio: 1.0
    }
    scheduler {
      speed: 8.4
      ratio: 1.0
    }
    scheduler {
      speed: 9.8
      ratio: 1.0
    }
    scheduler {
      speed: 11.2
      ratio: 1.0
    }
    scheduler {
      speed: 12.6
      ratio: 1.0
    }
    scheduler {
      speed: 14.0
      ratio: 1.0
    }
    scheduler {
      speed: 15.4
      ratio: 1.0
    }
    scheduler {
      speed: 16.8
      ratio: 1.0
    }
    scheduler {
      speed: 18.0
      ratio: 1.0
    }
    scheduler {
      speed: 25.0
      ratio: 1.0
    }
  }
  pitch_angle_filter_conf {
    cutoff_freq: 5
  }
  speed_leadlag_conf {
    innerstate_saturation_level: 1000
    alpha: 1.0
    beta: 1.6
    tau: 0.004
  }
}



### 查看调参效果
1. contorl_debug下的station error、speed_error、acc--_error


## 横向--LAT_MPC_NEW_CONTROLLER


lat_mpc_new_controller_conf{
  ts: 0.05
  mass_fl: 599
  mass_fr: 591
  mass_rl: 587
  mass_rr: 579
  eps: 0.01
  rho: 10.0
  use_increment_format: false
  use_increment_qpoases_format: false
  use_osqp_ref: true
  matrix_q_kin: 0.01
  matrix_q_kin: 0.02
  matrix_q_kin_latency: 0.01
  matrix_q_kin_latency: 0.1
  matrix_q_kin_latency: 0.0001
  matrix_q_err_dyn: 3.45179532
  matrix_q_err_dyn: 0.19068215
  matrix_q_err_dyn: 130.72332909
  matrix_q_err_dyn: 55.41348172
  matrix_r: 1.0
  matrix_g: 1.0
  matrix_q_err_dyn_reverse: 67.0
  matrix_q_err_dyn_reverse: 0.00
  matrix_q_err_dyn_reverse: 60.0
  matrix_q_err_dyn_reverse: 0.00
  matrix_q_err_dyn_reverse_close: 1.0
  matrix_q_err_dyn_reverse_close: 0.0
  matrix_q_err_dyn_reverse_close: 62.0
  matrix_q_err_dyn_reverse_close: 0.0
  cost_weight_u_dot: 100.0
  steer_cutoff_freq: 0.7
  lateral_error_rate_cutoff_freq: 10.0
  heading_error_rate_cutoff_freq: 2.0
  heading_mean_filter_window_size: 6
  lateral_mean_filter_window_size: 6
  steer_mean_filter_window_size: 6
  max_iteration: 200
  prediction_horizon: 20
  control_horizon: 7
  steer_tau: 0.1
  preview_window: 20
  use_preview_point: false
  use_leadlag_lat_mpc: false
  distance_path_remain_reverse: 4.8
  parking_steer_rate_limit: 2.6
  filter_coefficient: 0.7
  delta_steer_angle_limit: 1.0
  delta_steer_angle_with_forward_limt: 30.0
  lat_mpc_leadlag_conf {
    innerstate_saturation_level: 1000
    alpha: 1.0
    beta: 1.4
    tau: 0.006
  }
  mpc_coefficients_scheduler {

  }
}

