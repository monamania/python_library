```
#!/bin/env python3
# -*- coding:utf-8 -*-
# @Author: monomania_cjk
# @Data: 2022.04.02
```

# 均值、方差、标准差

https://blog.csdn.net/robert_chen1988/article/details/102712946
1、均方差就是标准差，标准差就是均方差。。。方差是标准差的平方

2、方差 是各数据偏离平均值 差值的平方和 的平均数

3、均方误差（MSE）是各数据偏离真实值 差值的平方和 的平均数

4、方差是平均值，均方误差是真实值。




## 均值
```python
import numpy as np
# 一般的均值可以用 numpy 中的 mean 方法求得：
a = [5, 6, 16, 9]
np.mean(a)  # 9

# numpy 中的 average 方法不仅能求得简单平均数，也可以求出加权平均数。average 里面可以跟一个 weights 参数，里面是一个权数的数组，例如：
np.average(a)  # 9
np.average(a, weights = [1, 2, 1, 1]) # 8.4
```

## 方差
计算方差时，可以利用 numpy 中的 var 函数，默认是总体方差（计算时除以样本数 N），若需要得到样本方差（计算时除以 N - 1），需要跟参数 ddo f= 1，例如
```python
import numpy as np
a = [5, 6, 16, 9]
np.var(a) # 计算总体方差 18.5


np.var(a, ddof = 1) # 计算样本方差 24.666666666666668


b = [[4, 5], [6, 7]]
np.var(b) # 计算矩阵所有元素的方差  1.25


np.var(b, axis = 0) # 计算矩阵每一列的方差    array([1., 1.])


np.var(b, axis = 1) # 计算矩阵每一行的方差  array([0.25, 0.25])
```

## 标准差--就是均方差。。。正态分布的那个sigama   3*sigma说明为异常值

计算标准差时，可以利用 numpy 中的 std 函数，使用方法与 var 函数很像，默认是总体标准差，若需要得到样本标准差，需要跟参数 ddof =1，
```python
import numpy as np
a = [5, 6, 16, 9]
np.std(a) # 计算总体标准差  4.301162633521313

np.std(a, ddof = 1 ) # 计算样本标准差  4.96655480858378


np.std(b) # 计算矩阵所有元素的标准差  1.118033988749895


np.std(b, axis = 0) # 计算矩阵每一列的标准差  array([1., 1.])


np.std(b, axis = 1) # 计算矩阵每一列的标准差   array([0.5, 0.5])

```