// #include "/usr/include/mgl2/qt.h"

// int sample(mglGraph *gr)
// {
//   gr->Rotate(60,40);
//   gr->Box();
//   return 0;
// }
// //-----------------------------------------------------
// int main(int argc,char **argv)
// {
//   mglQT gr(sample,"MathGL examples");
//   return gr.Run();
// }



#include <mgl2/fltk.h>

int graph(mglGraph *gr) {
    gr->Title("MathGL Demo");
    gr->SetOrigin(0, 0);
    gr->SetRanges(0, 10, -2.5, 2.5);
    gr->FPlot("sin(1.7*2*pi*x) + sin(1.9*2*pi*x)", "r-4");
    gr->Axis();
    gr->Grid();
    return 0;
}

int main() {
    mglFLTK gr(graph, "MathGL demo window title");
    return gr.Run();
}