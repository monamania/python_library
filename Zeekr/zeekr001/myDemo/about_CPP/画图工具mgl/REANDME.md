<!--
https://doc.weixin.qq.com/doc/w3_AaUA1QZRAGQxUoLQ5SUR0C3rRjTLs?scode=ANkACgc-AAwmLl6325AbIASAbIAC0&version=2.8.10.2010&platform=win
 
https://cloud.tencent.com/developer/article/1588309

https://blog.csdn.net/chyuanrufeng/article/details/102808715
 -->
## 环境配置
1. 更换源
https://developer.aliyun.com/mirror/ubuntu
#备份
sudo cp /etc/apt/sources.list /etc/apt/sources.list.bk

#更换源
sudo gedit /etc/apt/sources.list

#替换默认的 http://cn.archive.ubuntu.com/
#为 mirrors.aliyun.com


2. 更新源
sudo apt update


3. 安装
sudo apt install libmgl-dev

4. 编译
头文件在：
`include "/usr/include/mgl2/mgl.h"`
链接库：
`g++ test_mgl.cpp -o mgl_ex1  -lmgl`
`g++ test_mgl.cpp -o mgl_ex1  -lmgl  -lmgl-qt`
有效：`g++ test_mgl.cpp -o mgl_ex1  -lmgl  -lmgl-fltk`


## 测试
```CPP
#include "/usr/include/mgl2/qt.h"

int sample(mglGraph *gr)
{
  gr->Rotate(60,40);
  gr->Box();
  return 0;
}
//-----------------------------------------------------
int main(int argc,char **argv)
{
  mglQT gr(sample,"MathGL examples");
  return gr.Run();
}
```