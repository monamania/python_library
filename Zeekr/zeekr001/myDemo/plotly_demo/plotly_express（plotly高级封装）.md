<!-- 
pip3 install plotly; pip3 install cufflinks
sudo apt install python3-tk -->

pip3 install plotly


## plotly
https://blog.csdn.net/qq_43634001/article/details/94981837
https://testcpongo3.blog.csdn.net/article/details/90235168
https://cloud.tencent.com/developer/article/1439757

https://blog.csdn.net/qq_43634001/article/details/94981837

https://daobook.github.io/plotly-book/README.html

https://blog.csdn.net/LaoChengZier/article/details/82862815

https://plotly.com/python/


layout: https://blog.csdn.net/qq_16381291/article/details/121518073

## plotly_express------plotly的高级封装
https://www.jianshu.com/p/41735ecd3f75


https://cloud.tencent.com/developer/article/1794856




## layout
https://blog.csdn.net/qq_16381291/article/details/121518073
```python
#尺寸、背景和全局设置：Paper、plot
fig.update_layout(
	font_size=16,
    paper_bgcolor='#E9E7EF',
    plot_bgcolor='black',
    width=1000,
    margin=dict(t=100,pad=10)
)

#图例
fig.update_layout(
    legend_title='我是图例',
    legend_title_font_color='red',
    legend_bordercolor='black',
    legend_valign='top',
    legend_borderwidth=1
)

#标题
fig.update_layout(
    title='我是标题',
    title_font_size=22,
    title_font_color='red',
    title_x=0.4)

#图形系列非数据相关的设置
fig.layout.bargap=0.5#(如针对bar图)

#x轴：轴、网格线、范围滚动条
fig.update_layout(
    xaxis=dict(
        title='我是X轴',
        title_font_color='red',
        gridcolor='cyan',
        rangeslider=dict(bgcolor='black',yaxis_rangemode='auto')
    ),
    yaxis=dict(
        title='我是y轴',
        title_font_color='red'
    )
    
)

#彩色轴
#动态改变系列为彩色轴
fig.update_traces(
    marker={'color': [4, 6, 5], 'coloraxis': 'coloraxis'},
    selector={'type':'bar','name':'y2'}#选择系列
)
#更新彩色轴
fig.update_coloraxes(
    colorbar_title='我是colorbar',
    colorbar_title_font_color='red',
    colorbar_x=1.02,
    colorbar_y=0,
    showscale=True,
    colorscale=[[0, 'rgb(0,0,255)'], [1, 'rgb(255,0,0)']]#或者用系统的命名配色方案，如’rdylbu'
)


#注释
fig.add_annotation(
    x=0,y=7,
    text='我是注释',
    font=dict(color='red')
)  #动态添加注释
fig.update_annotations(patch=dict(xref='x',yref='y',showarrow=True),
                      arrowcolor='red',
                      arrowhead=4)

```