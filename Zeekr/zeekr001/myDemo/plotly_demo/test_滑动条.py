#!/usr/bin/env python
# -*- coding: utf-8 -*-
# author：Zhang Kai time:2020/4/14
import plotly.graph_objects as go
import plotly.offline as ps
import plotly.express as px
import numpy as np

# Create figure
fig = go.Figure()

# Add traces, one for each slider step
for step in np.arange(0, 5, 0.1):
    fig.add_trace(
        go.Scatter(
            visible=False,
            line=dict(color="#00CED1", width=6),
            name="𝜈 = " + str(step),
            x=np.arange(0, 10, 0.01),
            y=np.sin(step * np.arange(0, 10, 0.01))))

# Make 10th trace visible
fig.data[10].visible = True

# Create and add slider
steps = []
for i in range(len(fig.data)):
    step = dict(
        method="restyle",
        args=["visible", [False] * len(fig.data)],
    )
    step["args"][1][i] = True  # Toggle i'th trace to "visible"
    steps.append(step)
    arg=["visible", [False] * len(fig.data)]
    print(step)
    print("--------------------")

sliders = [dict(
    active=10,   #默认值
    currentvalue={"prefix": "Frequency:"},    #滑动条显示的名称；
    pad={"t": 10},    # 调节滑动条的位置,单位像素；
    steps=steps
)]

fig.update_layout(
    sliders=sliders
)