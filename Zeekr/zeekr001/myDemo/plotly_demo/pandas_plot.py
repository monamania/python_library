import numpy as np
import pandas as pd
import matplotlib.pyplot as plt  # 引用画图库中的pyplot模块

#定义数据
x = np.arange(-10, 10, 0.05)  # 400
y1 = 1/(1 + np.exp(-x))  # 400  sigmoid 
y2 = np.maximum(0, x)   # 400  relu
y3 = np.tanh(x)  # 400
y = np.array([y1, y2, y3]).T  # [400,3]

# 真实数据时用来插值平滑曲线
from scipy import interpolate
xnew = np.linspace(x.min(), x.max(), 1000)  # 平滑到1000组值
func = interpolate.interp1d(x, y, kind='quadratic', axis=0)
ynew = func(xnew)  # [1000,3]

variables = pd.DataFrame(ynew,
                         columns=['sigmoid','relu','tanh'],
                         index=xnew)

variables.plot(color=['red','green','blue'])

# 标题
plt.title('Activation Function')
# 合适的位置加图例
plt.legend(loc='best')
# x、y轴范围
plt.xlim(-5, 5)
plt.ylim(-1.2, 1.2)
# x、y轴标签内容字体
plt.xlabel('Input', fontdict={'family': 'Times New Roman', 'size': 12})
plt.ylabel('Output', fontdict={'family': 'Times New Roman', 'size': 12})
# x、y轴刻度字体
plt.xticks(fontproperties='Times New Roman', size=10, rotation=0)
plt.yticks(fontproperties='Times New Roman', size=10, rotation=30)
# 刻度间隔设置
from matplotlib.pyplot import MultipleLocator
x_major_locator = MultipleLocator(2)
y_major_locator = MultipleLocator(1)
ax = plt.gca()
ax.xaxis.set_major_locator(x_major_locator)
ax.yaxis.set_major_locator(y_major_locator)
# 显示网格
plt.grid()
# 保存
plt.savefig('my_plot.png')
# 显示
plt.show()

