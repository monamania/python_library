# coding:utf-8
# https://cloud.tencent.com/developer/article/1676997
# 很不错：https://blog.csdn.net/weixin_43417472/article/details/89356461
# https://blog.csdn.net/qq_38025771/article/details/110336258

# 作者：州的先生
# 博客：https://zmister.com

from pyqtgraph.Qt import QtGui, QtCore
import numpy as np
import pyqtgraph as pg

# 实例化一个绘图窗口
win = pg.GraphicsWindow()
win.resize(1000,600)
win.setWindowTitle('PyQtGraph基础绘图示例 - zmister.com')

# 启用抗锯齿选项
pg.setConfigOptions(antialias=True)

# 添加一个图形
x2 = np.linspace(-100, 100, 1000)
data2 = np.sin(x2) / x2
p8 = win.addPlot(title="区域选择")
p8.plot(data2, pen=(255,255,255,200))
# 添加一个线区域选择项目，起始区间在400到700
lr = pg.LinearRegionItem([400,700])
p8.addItem(lr)

p9 = win.addPlot(title="放大区域选择")
p9.plot(data2)
# 更新绘图
def updatePlot():
    p9.setXRange(*lr.getRegion(), padding=0)

# 更新区域选择
def updateRegion():
    lr.setRegion(p9.getViewBox().viewRange()[0])
lr.sigRegionChanged.connect(updatePlot)
p9.sigXRangeChanged.connect(updateRegion)
updatePlot()

if __name__ == '__main__':
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()