# https://blog.csdn.net/qq_43634001/article/details/94981837
# https://testcpongo3.blog.csdn.net/article/details/90235168
import plotly
from  plotly.graph_objs import Scatter,Layout
import matplotlib.pyplot as plt 

# plotly.offline.init_notebook_mode(connected=True)
#%%writefile D:\基本折线图.py   #保存成py文件
import plotly.offline as py
from plotly.graph_objs import Scatter, Layout
import plotly.graph_objs as go

py.init_notebook_mode(connected=True)

trace1 = go.Scatter(
    x=[1,2,3],
    y=[1,2,4])

trace2 = go.Scatter(
    x=[1,2,5],
    y=[2,1,4])

# py.iplot([trace1, trace2])
py.plot([trace1, trace2])
