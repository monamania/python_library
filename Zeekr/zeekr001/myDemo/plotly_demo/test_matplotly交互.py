# https://blog.csdn.net/dongfuguo/article/details/118704477

import numpy as np

import matplotlib.pyplot as plt

from matplotlib.widgets import Slider, Button, RadioButtons

fig, ax = plt.subplots()

#设置图形区域位置

plt.subplots_adjust(left=0.1, bottom=0.25)

t = np.arange(0.0, 1.0, 0.001)

#初始振幅与频率，并绘制初始图形

a0, f0= 5, 3

s = a0*np.sin(2*np.pi*f0*t)

l, = plt.plot(t, s, lw=2, color='red')

#设置坐标轴刻度范围

plt.axis([0, 1, -10, 10])

axColor = 'lightgoldenrodyellow'

#创建两个Slider组件，分别设置位置/尺寸、背景色和初始值

# axfreq = plt.axes([0.1, 0.1, 0.75, 0.03], axisbg=axColor)
axfreq = plt.axes([0.1, 0.1, 0.75, 0.03])

sfreq = Slider(axfreq, 'Freq', 0.1, 30.0, valinit=f0)

# axamp = plt.axes([0.1, 0.15, 0.75, 0.03], axisbg=axColor)
axamp = plt.axes([0.1, 0.15, 0.75, 0.03])

samp = Slider(axamp, 'Amp', 0.1, 10.0, valinit=a0)

#为Slider组件设置事件处理函数

def update(event):

    #获取Slider组件的当前值，并以此来更新图形

    amp = samp.val

    freq = sfreq.val

    l.set_ydata(amp*np.sin(2*np.pi*freq*t))

    plt.draw()

sfreq.on_changed(update)

samp.on_changed(update)

#创建Adjust按钮，设置大小、位置和事件处理函数

def adjustSliderValue(event):

    ampValue = samp.val + 0.05

    if ampValue > 10:

        ampValue = 0.1

    samp.set_val(ampValue)

    freqValue = sfreq.val + 0.05

    if freqValue > 30:

        freqValue = 0.1

axAdjust = plt.axes([0.6, 0.025, 0.1, 0.04])

buttonAdjust = Button(axAdjust, 'Adjust', color=axColor, hovercolor='red')

buttonAdjust.on_clicked(adjustSliderValue)

#创建按钮组件，用来恢复初始值

resetax = plt.axes([0.8, 0.025, 0.1, 0.04])

button = Button(resetax, 'Reset', color=axColor, hovercolor='yellow')

def reset(event):

    sfreq.reset()

    samp.reset()

button.on_clicked(reset)

plt.show()




# https://blog.csdn.net/qq_38778838/article/details/89040722
# import matplotlib.pyplot as plt
# import numpy as np
	
# def Show(y):
# 	#参数为一个list
	
# 	len_y = len(y)
# 	x = range(len_y)
# 	_y = [y[-1]]*len_y
	
# 	fig = plt.figure(figsize=(960/72,360/72))
# 	ax1 = fig.add_subplot(1,1,1)
	
# 	ax1.plot(x, y, color='blue')
# 	line_x = ax1.plot(x, _y, color='skyblue')[0]
# 	line_y = ax1.axvline(x=len_y-1, color='skyblue')
	
# 	ax1.set_title('aaa')
# 	#标签
# 	text0 = plt.text(len_y-1,y[-1],str(y[-1]),fontsize = 10)
	
# 	def scroll(event):
# 		axtemp=event.inaxes
# 		x_min, x_max = axtemp.get_xlim()
# 		fanwei_x = (x_max - x_min) / 10
# 		if event.button == 'up':
# 			axtemp.set(xlim=(x_min + fanwei_x, x_max - fanwei_x))
# 		elif event.button == 'down':
# 			axtemp.set(xlim=(x_min - fanwei_x, x_max + fanwei_x))
# 		fig.canvas.draw_idle() 
# 	#这个函数实时更新图片的显示内容
# 	def motion(event):
# 		try:
# 			temp = y[int(np.round(event.xdata))]
# 			for i in range(len_y):
# 				_y[i] = temp
# 			line_x.set_ydata(_y)
# 			line_y.set_xdata(event.xdata)
# 			######
# 			text0.set_position((event.xdata, temp))
# 			text0.set_text(str(temp))
			
# 			fig.canvas.draw_idle() # 绘图动作实时反映在图像上
# 		except:
# 			pass

# 	fig.canvas.mpl_connect('scroll_event', scroll)
# 	fig.canvas.mpl_connect('motion_notify_event', motion)
	
# 	plt.show()
