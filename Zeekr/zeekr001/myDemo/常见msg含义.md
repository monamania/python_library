# /loc/ego_tf
<!-- string child_frame_id


#uint8 STATUS_INITLIZING=0
#uint8 STATUS_OK=1
#uint8 STATUS_WARN=2
#uint8 STATUS_ERROR=3
#uint8 STATUS_FATAL=4

#uint8 MODE_NONE=0
#uint8 MODE_HD=1
#uint8 MODE_SD=2
#uint8 MODE_ADAS=3

uint8 status 0
uint8 mode 0

uint32 fusion_sources #  bit[0]=1 chassis_speed fused, bit[1] imu fused, bit[2] gnss fused, bit[3] ins fused, bit[4] lidar fused, bit[5] vector loc fused. 


float64 longitude
float64 latitude
float64 altitude

# euler angles FLU to ENU x-roll, y-pitch, z-yaw, rotation-Order: Z-Y-X
# The roll, in [-pi, pi], corresponds to a rotation around the x-axis.
# The pitch, in [-pi/2, pi/2), corresponds to a rotation around the y-axis.
# The yaw, in [-pi, pi], corresponds to a rotation around the z-axis.
float32 yaw
float32 pitch
float32 roll

float64 world_x
float64 world_y
float64 world_z

float32 world_yaw
float32 world_pitch
float32 world_roll


float32 velocity_east
float32 velocity_north
float32 velocity_up

# velocity in vehicle frame
float32 velocity_x
float32 velocity_y
float32 velocity_z


# linear_acceleration in vehicle frame
float32 acc_x
float32 acc_y
float32 acc_z

# angular rate(gyro) in vehicle frame
float32 gyro_x
float32 gyro_y
float32 gyro_z

# linear_acceleration bias in vehicle frame
float32 acc_bias_x 0.0
float32 acc_bias_y 0.0
float32 acc_bias_z 0.0

# gyro bias in vehicle frame
float32 gyro_bias_x 0.0
float32 gyro_bias_y 0.0
float32 gyro_bias_z 0.0

float32 wheel_speed_fl_scale 1.0
float32 wheel_speed_fr_scale 1.0
float32 wheel_speed_rl_scale 1.0
float32 wheel_speed_rr_scale 1.0

float32[36] pose_covariance
float32[9] enu_velocity_covariance -->

# /loc/ego_tf_relative

# /pnc/control_debug
1. path_remain
   当前运行进度距离规划结束还有多远
2. heading_error
   前转向轴心，偏离轨迹的角度
3. lateral_error
   横向 侧面偏差
4. speed_reference
   参考速度---没有正负
5. current_speed
   当前速度

# /pnc/chassis_report
<!-- 
std_msgs/Header header

uint8 auto_driving_mode
uint8 take_over_status

Float32WithValid speed
Float32WithValid speed_disp

Brake brake
Drive drive
Gear gear
Steer steer
ImuMotion imu_motion_master
ImuMotion imu_motion_slave
float32[] reserved1
uint32[] reserved2
 -->
1. auto_driving_mode
    #COMPLETE_MANUAL = 0
    #COMPLETE_AUTO_DRIVE = 1
    #AUTO_STEER_ONLY = 2
    #AUTO_SPEED_ONLY = 3
    #AUTO_PARKING_ASSIST = 4
2. steer_enable
    #DISABLE = 0
    #ENABLE = 1
3. brake/standstill_status
   当前状态，0：UNHOLD，1：HOLD
4. speed/value
   当前底盘车速，有正负
5. gear/gear
   档位，0：无效，1：D驾驶，2：R倒车，3：停车制动，4：空档
    #GEAR_INVALID  = 0
    #GEAR_DRIVE = 1
    #GEAR_REVERSE = 2
    #GEAR_PARKING = 3
    #GEAR_NEUTRAL = 4


# /pnc/mop_status
1. park_status
    #PARKING_UNAVAILABLE = 0  停车不可用
    #PARKING_RUNNING = 1      运行
    #PARKING_SUSPEND = 2      挂起暂停 
    #PARKING_FAIL = 3         失败  
    #PARKING_FINISH = 4       结束
2. park_abnormal_reason
    #NO_REASON = 0
    #STOP_TEMPORARILY = 1
    #PARKING_TIMES_EXCEED = 2

3. available_direction
    #INVALID = 0
    #FL = 1
    #FR = 2
    #FD = 3
    #RL = 4
    #RR = 5
    #RD = 6

