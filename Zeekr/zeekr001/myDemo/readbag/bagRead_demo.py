#!/bin/env python3
# -*- coding:utf-8 -*-
# @Author: monomania_cjk
# @Data: 2022.04.06

import math
import numpy as np
import numpy.linalg as LA  # 计算曲率
import pandas as pd

import rclpy
from rclpy.node import Node

import os
import sys
import time

from rclpy.serialization import deserialize_message
from rosidl_runtime_py.utilities import get_message

import rosbag2_py



if os.environ.get('ROSBAG2_PY_TEST_WITH_RTLD_GLOBAL', None) is not None:
    # This is needed on Linux when compiling with clang/libc++.
    # TL;DR This makes class_loader work when using a python extension compiled with libc++.
    #
    # For the fun RTTI ABI details, see https://whatofhow.wordpress.com/2015/03/17/odr-rtti-dso/.
    sys.setdlopenflags(os.RTLD_GLOBAL | os.RTLD_LAZY)

def get_rosbag_options(path, serialization_format='cdr'):
    storage_options = rosbag2_py.StorageOptions(uri=path, storage_id='sqlite3')
    converter_options = rosbag2_py.ConverterOptions(
        input_serialization_format=serialization_format,
        output_serialization_format=serialization_format)
    return storage_options, converter_options


def eg_tf_reader(bag_path):
    storage_options, converter_options = get_rosbag_options(bag_path)
    reader = rosbag2_py.SequentialReader()
    reader.open(storage_options, converter_options)
    topic_types = reader.get_all_topics_and_types()
    # Create a map for quicker lookup
    type_map = {topic_types[i].name: topic_types[i].type for i in range(len(topic_types))}
    # Set filter for topic of string type

    msg_counter = 0
    
    t_loc = []
    world_x_loc =[]
    world_y_loc =[]
    world_yaw_loc =[]
    storage_filter = rosbag2_py.StorageFilter(topics=['/loc/ego_tf'])
    reader.set_filter(storage_filter)


    while reader.has_next():
        (topic, data, t) = reader.read_next()
        # print(topic)
        # print('t'); print(type(t)); print(t*1e-9)
        msg_type = get_message(type_map[topic])
        msg_localizaztion = deserialize_message(data, msg_type)
        # if topic == '/loc/ego_tf':
        # print(dir(msg_localizaztion))   # 获取属性与方法
        # print("="*50)

        # tem_loc = []
        
        # loc_timestamp = msg_localizaztion.header.stamp.sec + msg_localizaztion.header.stamp.nanosec*1e-9
        loc_timestamp = t*1.0e-9
        # print(msg_localizaztion.header.stamp.sec, msg_localizaztion.header.stamp.nanosec)
        world_x = msg_localizaztion.world_x
        world_y = msg_localizaztion.world_y
        world_yaw = msg_localizaztion.world_yaw

        msg_counter += 1
        # print(msg_counter)
        if msg_counter>2e5:
            print('ego_tf exceed max number, data lost!')
            break
        t_loc.append(loc_timestamp)
        world_x_loc.append(world_x)
        world_y_loc.append(world_y)
        world_yaw_loc.append(world_yaw)
    dataMap = {
        '__time':t_loc,
        'metadata/loc/ego_tf/world_x':world_x_loc,
        'metadata/loc/ego_tf/world_y':world_y_loc,
        'metadata/loc/ego_tf/world_yaw':world_yaw_loc
    }
    
    return dataMap



def eg_tf_relative_reader(bag_path):
    storage_options, converter_options = get_rosbag_options(bag_path)
    reader = rosbag2_py.SequentialReader()
    reader.open(storage_options, converter_options)
    topic_types = reader.get_all_topics_and_types()
    # Create a map for quicker lookup
    type_map = {topic_types[i].name: topic_types[i].type for i in range(len(topic_types))}
    # Set filter for topic of string type

    msg_counter = 0
    
    t_loc = []
    world_x_loc =[]
    world_y_loc =[]
    world_yaw_loc =[]
    storage_filter = rosbag2_py.StorageFilter(topics=['/loc/ego_tf_relative'])
    reader.set_filter(storage_filter)


    while reader.has_next():
        (topic, data, t) = reader.read_next()
        # print(topic)
        # print('t'); print(type(t)); print(t*1e-9)
        msg_type = get_message(type_map[topic])
        msg_localizaztion = deserialize_message(data, msg_type)
        # if topic == '/loc/ego_tf':
        # print(dir(msg_localizaztion))   # 获取属性与方法
        # print("="*50)

        # tem_loc = []
        
        # loc_timestamp = msg_localizaztion.header.stamp.sec + msg_localizaztion.header.stamp.nanosec*1e-9
        loc_timestamp = t*1.0e-9
        # print(msg_localizaztion.header.stamp.sec, msg_localizaztion.header.stamp.nanosec)
        x = msg_localizaztion.x
        y = msg_localizaztion.y
        yaw = msg_localizaztion.yaw

        msg_counter += 1
        # print(msg_counter)
        if msg_counter>2e5:
            print('ego_tf exceed max number, data lost!')
            break
        t_loc.append(loc_timestamp)
        world_x_loc.append(x)
        world_y_loc.append(y)
        world_yaw_loc.append(yaw)
    dataMap = {
        '__time':t_loc,
        'metadata/loc/ego_tf_relative/x':world_x_loc,
        'metadata/loc/ego_tf_relative/y':world_y_loc,
        'metadata/loc/ego_tf_relative/yaw':world_yaw_loc
    }
    
    return dataMap



if __name__ == '__main__':

    bag_file = r"/zdrive/task01_simulatePath/datas/rosbag2_80km/rosbag2_2022_03_29-13_08_53/rosbag2_2022_03_29-13_08_53_0.db3"
    dataMap = eg_tf_reader(bag_file)
    # dataMap = eg_tf_relative_reader(bag_file)
    print(dataMap)

    # 转换为csv并保存
    # df = pd.DataFrame(dataMap)
    # df.to_csv('./test.csv', index=False)

