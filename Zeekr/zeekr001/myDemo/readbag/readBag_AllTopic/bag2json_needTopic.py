#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# @Author: monomania_cjk
# @Data: 2022.04.21

import math
import os
import sys
import time

import numpy as np
import pandas as pd

"""
支持绝对路径与相对路径----可搭配GUI
"""


if os.environ.get("ROSBAG2_PY_TEST_WITH_RTLD_GLOBAL", None) is not None:
    # This is needed on Linux when compiling with clang/libc++.
    # TL;DR This makes class_loader work when using a python extension compiled with libc++.
    #
    # For the fun RTTI ABI details, see https://whatofhow.wordpress.com/2015/03/17/odr-rtti-dso/.
    sys.setdlopenflags(os.RTLD_GLOBAL | os.RTLD_LAZY)


def handlePath(path):
    """
    判断路径是否存在,同时将相对位置转换为绝对位置
    """
    fatherPath = os.path.dirname(os.path.abspath(__file__))

    # 同时支持相对路径与绝对路径
    if os.path.exists(path):
        return path
    elif os.path.exists(fatherPath + os.sep + path):
        return fatherPath + os.sep + path
    else:
        print("=" * 80)
        print("\t\t=== Bag path do not exist===")
        print("\t\t=== Stop ===")
        print(path)
        print("=" * 80)
        import sys

        sys.exit()


def getDataByClass_recursion(obj):
    """
    递归获取类内元素--类中类中类
    """
    attr_list = [
        i
        for i in dir(obj)
        if not callable(getattr(obj, i)) and not i.startswith("_") and i.islower()
    ]
    # tmp_dataMap.update()
    tmp_dataMap = {}
    for attr in attr_list:
        value = getattr(obj, attr)
        if isinstance(value, (list)):
            valueList = []
            for v in value:
                valueList.append(getDataByClass_recursion(v))
            tmp_dataMap[attr] = valueList
            continue

        if not isinstance(value, (str, float, int, list, np.ndarray)):
            # print(value)
            value = getDataByClass_recursion(value)

        elif isinstance(value, (np.ndarray)):
            # print(value)
            # sys.exit()
            value = list(value.astype(float))

        tmp_dataMap[attr] = value

    return tmp_dataMap


def serialize_instance(obj):
    """解析数据类型----递归
    通用
    @obj: class的实例对象
    @return: map
    """
    tmp_dataMap = {"__className__": type(obj).__name__}
    attr_list = [
        i
        for i in dir(obj)
        if not callable(getattr(obj, i)) and not i.startswith("_") and i.islower()
    ]
    # tmp_dataMap.update()
    for attr in attr_list:
        value = getattr(obj, attr)
        if isinstance(value, (list)):
            valueList = []
            for v in value:
                valueList.append(getDataByClass_recursion(v))
            tmp_dataMap[attr] = valueList
            continue

        if not isinstance(value, (str, float, int, np.ndarray)):
            value = getDataByClass_recursion(value)
            # print(value)
        elif isinstance(value, (np.ndarray)):
            # print(value)
            # sys.exit()
            value = list(value.astype(float))

        tmp_dataMap[attr] = value

    return tmp_dataMap


def getData(bagPath):
    startTime = time.time()
    # 这里只能是绝对路径
    # ——————————————————————————————————————————————————————————————————————
    import json

    bagName = os.path.basename(bagPath).split(".")[0]
    bagDir = os.path.dirname(bagPath)

    dataType = os.path.basename(bagPath).split(".")[-1]
    if dataType == "json":
        jsonPath = bagPath
    else:
        jsonPath = bagDir + os.sep + "bagData_" + bagName + ".json"

    #     if os.path.exists(jsonPath):
    #         print("=" * 80)
    #         print("\t\t=== JSON is exist ===")
    #         print("=" * 80)
    #         sys.exit()
    #     else:
    #         print("=" * 80)
    #         print("\t\t=== JSON is not exist, Reading bag ===")
    #         print("\t\t=== Please wait a minute ===")
    #         print("=" * 80)
    #         startTime = time.time()
    #         dataMaps, _ = read_bag(bagPath)
    #         # https://blog.csdn.net/qq_43523725/article/details/104770458
    #         # https://www.cnblogs.com/tizer/p/11067098.html
    #         # with open(jsonPath,'w',encoding='utf-8') as f:
    #         #     json.dump(dataMaps,f)
    #         # 标准格式存储
    #         with open(jsonPath, "w", encoding="utf-8") as f:
    #             f.write(json.dumps(dataMaps, ensure_ascii=False, indent=1))
    print("=" * 80)
    print("\t\t=== JSON is not exist, Reading bag ===")
    print("\t\t=== Please wait a minute ===")
    print("=" * 80)
    startTime = time.time()
    dataMaps, _ = read_bag(bagPath)
    # https://blog.csdn.net/qq_43523725/article/details/104770458
    # https://www.cnblogs.com/tizer/p/11067098.html
    # with open(jsonPath,'w',encoding='utf-8') as f:
    #     json.dump(dataMaps,f)
    # 标准格式存储
    with open(jsonPath, "w", encoding="utf-8") as f:
        f.write(json.dumps(dataMaps, ensure_ascii=False, indent=1))

    print("read bag time: ", time.time() - startTime)
    # ——————————————————————————————————————————————————————————————————————
    print("read data time:", time.time() - startTime)


def get_rosbag_options(path, serialization_format="cdr"):
    import rosbag2_py

    storage_options = rosbag2_py.StorageOptions(uri=path, storage_id="sqlite3")
    converter_options = rosbag2_py.ConverterOptions(
        input_serialization_format=serialization_format,
        output_serialization_format=serialization_format,
    )
    return storage_options, converter_options


def read_bag(bagPath, needTopicList=None):
    """一次性读取所有数据并返回map
    return  {topic1: [], topic2:[]}
    """
    import rosbag2_py
    from rclpy.serialization import deserialize_message
    from rosidl_runtime_py.utilities import get_message

    storage_options, converter_options = get_rosbag_options(bagPath)
    reader = rosbag2_py.SequentialReader()
    reader.open(storage_options, converter_options)
    topic_types = reader.get_all_topics_and_types()
    # Create a map for quicker lookup
    type_map = {
        topic_types[i].name: topic_types[i].type for i in range(len(topic_types))
    }
    # Set filter for topic of string type

    if not needTopicList:
        # all topics of bag
        dataMap = {topicName: [] for topicName in type_map.keys()}
    else:
        storage_filter = rosbag2_py.StorageFilter(topics=list(needTopicList))
        reader.set_filter(storage_filter)
        # needTopicList
        dataMap = {topicName: [] for topicName in needTopicList}

    while reader.has_next():
        (topic, data, t) = reader.read_next()
        if needTopicList:
            if topic not in needTopicList:
                continue

        msg_type = get_message(type_map[topic])
        msg = deserialize_message(data, msg_type)

        loc_timestamp = t * 1.0e-9
        tmpMap = serialize_instance(msg)
        tmpMap.update({"__time": loc_timestamp})

        dataMap[topic].append(tmpMap)
    from pprint import pprint

    # pprint(dataMap[topicList[0]][0])
    topicList = list(dataMap.keys())
    return dataMap, topicList


if __name__ == "__main__":
    startTime = time.time()

    if len(sys.argv) == 2:
        bag_file = sys.argv[1]
    else:
        print("=" * 80)
        bagName = os.path.basename(os.path.abspath(__file__))
        print(f"Run with: python3 {bagName} <bag path>")
        print("=" * 80)
        import sys

        sys.exit()

    if not bag_file:
        sys.exit()

    getData(bag_file)
