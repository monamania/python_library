from distutils.core import setup
from Cython.Build import cythonize

setup(ext_modules = cythonize(["BagReader_AllTopic.py"]))

# cd testing
# python3 setup.py build_ext