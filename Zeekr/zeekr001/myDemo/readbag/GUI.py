from tkinter import Tk
from tkinter.filedialog import askopenfilename
root = Tk()
root.withdraw()
bag_file = askopenfilename(title='Please choose a ros2 bag file', 
                initialdir='~', filetypes=[('ros2 bag file','*.db3')])
if bag_file:
    plot_multi_traj_inner(bag_file, 0.0)

# GUI界面获取bag包