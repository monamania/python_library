#!/bin/env python3
# -*- coding:utf-8 -*-
# @Author: monomania_cjk
# @Data: 2022.03.31

import math
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import numpy.linalg as LA  # 计算曲率
import pandas as pd

import copy

import rclpy
from rclpy.node import Node

import os
# from pathlib import Path
import sys
import time

from rclpy.serialization import deserialize_message
from rosidl_runtime_py.utilities import get_message

import rosbag2_py


if os.environ.get('ROSBAG2_PY_TEST_WITH_RTLD_GLOBAL', None) is not None:
    # This is needed on Linux when compiling with clang/libc++.
    # TL;DR This makes class_loader work when using a python extension compiled with libc++.
    #
    # For the fun RTTI ABI details, see https://whatofhow.wordpress.com/2015/03/17/odr-rtti-dso/.
    sys.setdlopenflags(os.RTLD_GLOBAL | os.RTLD_LAZY)

def get_rosbag_options(path, serialization_format='cdr'):
    storage_options = rosbag2_py.StorageOptions(uri=path, storage_id='sqlite3')
    converter_options = rosbag2_py.ConverterOptions(
        input_serialization_format=serialization_format,
        output_serialization_format=serialization_format)
    return storage_options, converter_options


"""
@tagert: Get the data of csv
numpy: https://www.numpy.org.cn/user/quickstart.html
"""
class ReadData:
    def __init__(self, bag_path, saveData=False, savePath=None) -> None:
        storage_options, converter_options = get_rosbag_options(bag_path)
        self.reader = rosbag2_py.SequentialReader()
        self.reader.open(storage_options, converter_options)
        self.bag_path = bag_path
        topic_types = self.reader.get_all_topics_and_types()
        # Create a map for quicker lookup
        self.type_map = {topic_types[i].name: topic_types[i].type for i in range(len(topic_types))}

        self.saveData=saveData
        self.savePath=savePath
        if savePath:
            assert savePath
        

    def eg_tf_reader(self):
        # Set filter for topic of string type
        msg_counter = 0
        
        t_loc = []
        world_x_loc =[]
        world_y_loc =[]
        world_yaw_loc =[]
        storage_filter = rosbag2_py.StorageFilter(topics=['/loc/ego_tf'])
        self.reader.set_filter(storage_filter)

        while self.reader.has_next():
            (topic, data, t) = self.reader.read_next()
            # print(topic)
            # print('t'); print(type(t)); print(t*1e-9)
            msg_type = get_message(self.type_map[topic])
            msg_localizaztion = deserialize_message(data, msg_type)
            # if topic == '/loc/ego_tf':
            # print(msg_localizaztion)
            tem_loc = []
            # loc_timestamp = msg_localizaztion.header.stamp.sec + msg_localizaztion.header.stamp.nanosec*1e-9
            loc_timestamp = t*1.0e-9
            # print(msg_localizaztion.header.stamp.sec, msg_localizaztion.header.stamp.nanosec)
            world_x = msg_localizaztion.world_x
            world_y = msg_localizaztion.world_y
            world_yaw = msg_localizaztion.world_yaw
            # print(tem_loc)
            # print(world_x)
            # print(world_y)
            # print(yaw)
            # print(planning_timestamp)
            # print(msg_counter)  
            msg_counter += 1
            # print(msg_counter)
            if msg_counter>2e5:
                print('ego_tf exceed max number, data lost!')
                break
            t_loc.append(loc_timestamp)
            world_x_loc.append(world_x)
            world_y_loc.append(world_y)
            world_yaw_loc.append(world_yaw)
        dataMap = {
            '__time':t_loc,
            'metadata/loc/ego_tf/world_x':world_x_loc,
            'metadata/loc/ego_tf/world_y':world_y_loc,
            'metadata/loc/ego_tf/world_yaw':world_yaw_loc
        }
        df = pd.DataFrame(dataMap)
        if self.saveData:
            df.to_csv(self.savePath, index=False)
        return df, dataMap
    

    def getPandaData(self) -> pd.DataFrame:
        """
        默认需要: __time
        :topic_wX: name of topic----world_x
        :topic_wY: name of topic----world_y
        """
        df, _ = self.eg_tf_reader()
        return df
    

    def getNumpyDate(self):
        df_w = self.getPandaData()
        return df_w.values, df_w.columns




class PreHandle:
    def __init__(self):
        pass

    def preHandleData_smooth_base(self, predata):
        """
        双指针
        :predata: numpy
        """
        delList = []
        startIdx = 0
        step = 1
        col_world_x=1
        col_world_y=2
        while startIdx < predata.shape[0]-step:
            # if startIdx%1000 == 0:
            #     print(startIdx)
            endIdx = startIdx+step
            startX = predata[startIdx][col_world_x]
            startY = predata[startIdx][col_world_y]
            while endIdx < predata.shape[0]:
                endX = predata[endIdx][col_world_x]
                endY = predata[endIdx][col_world_y] 
                if self.isTooClose((startX,startY), (endX, endY), 0.01):
                    delList.append(endIdx)
                    endIdx += step
                else:
                    startIdx = endIdx
                    break
            if endIdx >= predata.shape[0]:
                break
        newData = np.delete(predata, delList, axis=0)
        xy = newData[:, 1:3]
        kappa = self.get_kappa(xy)
        return newData, kappa
        

    @staticmethod
    def isTooClose(node1:tuple, node2:tuple, threshold) -> bool: 
        """
        :threshold: 阈值
        """
        # print(node1, node2)
        dX = node1[0]-node2[0]
        dY = node1[1]-node2[1]
        # print(dX, dY)
        # print(math.sqrt(threshold))

        return math.hypot(dX, dY) <= threshold
        # return dX**2 + dY**2 <= threshold**2
    

    def cal_kappa(self, x,y):
        """
        input  : the coordinate of the three point
        output : the curvature and norm direction
        refer to https://github.com/Pjer-zhang/PJCurvature for detail
        """
        t_a = LA.norm([x[1]-x[0],y[1]-y[0]])
        t_b = LA.norm([x[2]-x[1],y[2]-y[1]])
        
        M = np.array([
            [1, -t_a, t_a**2],
            [1, 0,    0     ],
            [1,  t_b, t_b**2]
        ])

        a = np.matmul(LA.inv(M),x)
        b = np.matmul(LA.inv(M),y)

        kappa = 2*(a[2]*b[1]-b[2]*a[1])/(a[1]**2.+b[1]**2.)**(1.5)
        return kappa #, [b[1],-a[1]]/np.sqrt(a[1]**2.+b[1]**2.)
    


    def get_kappa(self, xy):
        xy = np.array(xy)
        kappa = [0]
        for i in range(1, len(xy)-1):
            x = xy[i-1:i+2, 0]
            y = xy[i-1:i+2, 1]
            kappa.append(self.cal_kappa(x,y))
        kappa[0] = kappa[1]
        kappa.append(kappa[-1])
        return kappa



# def plotData(preData,newData,startTimeIdx, width=100, figsize=(11,6)):
#     plt.figure(figsize=figsize)
#     plt.suptitle('Before')
#     plt.subplot(121)
#     endTimeIdx = startTimeIdx + width
#     plt.plot(preData[startTimeIdx:endTimeIdx,0], preData[startTimeIdx:endTimeIdx,1])
#     plt.title('x')
#     plt.subplot(122)
#     plt.plot(preData[startTimeIdx:endTimeIdx,0], preData[startTimeIdx:endTimeIdx,2])
#     plt.title('y')

#     plt.figure(figsize=figsize)
#     plt.suptitle('After')
#     plt.subplot(121)
#     endTimeIdx = startTimeIdx + width
#     plt.plot(newData[startTimeIdx:endTimeIdx,0], newData[startTimeIdx:endTimeIdx,1])
#     plt.title('x')
#     plt.subplot(122)
#     plt.plot(newData[startTimeIdx:endTimeIdx,0], newData[startTimeIdx:endTimeIdx,2])
#     plt.title('y')
#     # plt.xlabel('x')
#     # plt.ylabel('y')
#     plt.show()

if __name__ == '__main__':
    import os
    fatherpath = os.path.dirname(os.path.abspath(__file__))
    dataPath =fatherpath + r"/test_eg_tf_data.csv"


    bag_file = r"/zdrive/task01/datas/rosbag2_80km/rosbag2_2022_03_29-13_08_53/rosbag2_2022_03_29-13_08_53_0.db3"

    rd = ReadData(bag_file, saveData=True, savePath=dataPath)
    preData, colName = rd.getNumpyDate()

    ph = PreHandle()
    newData = ph.preHandleData_smooth_base(preData)
    
    
    newdf = pd.DataFrame(newData)
    newdf.columns = colName    # topicNames
    newdf.to_csv(fatherpath + os.sep + 'datas/traj_afterHandle.csv', index=False)


    

