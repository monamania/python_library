#!/bin/env python3
# -*- coding:utf-8 -*-
# @Author: monomania_cjk
# @Data: 2022.04.06

import math
import numpy as np
import numpy.linalg as LA  # 计算曲率
import pandas as pd

import rclpy
from rclpy.node import Node

import os
import sys
import time

from rclpy.serialization import deserialize_message
from rosidl_runtime_py.utilities import get_message

import rosbag2_py



if os.environ.get('ROSBAG2_PY_TEST_WITH_RTLD_GLOBAL', None) is not None:
    # This is needed on Linux when compiling with clang/libc++.
    # TL;DR This makes class_loader work when using a python extension compiled with libc++.
    #
    # For the fun RTTI ABI details, see https://whatofhow.wordpress.com/2015/03/17/odr-rtti-dso/.
    sys.setdlopenflags(os.RTLD_GLOBAL | os.RTLD_LAZY)

def get_rosbag_options(path, serialization_format='cdr'):
    storage_options = rosbag2_py.StorageOptions(uri=path, storage_id='sqlite3')
    converter_options = rosbag2_py.ConverterOptions(
        input_serialization_format=serialization_format,
        output_serialization_format=serialization_format)
    return storage_options, converter_options


def eg_tf_data_reader(bag_path):
    storage_options, converter_options = get_rosbag_options(bag_path)
    reader = rosbag2_py.SequentialReader()
    reader.open(storage_options, converter_options)
    topic_types = reader.get_all_topics_and_types()
    # Create a map for quicker lookup
    type_map = {topic_types[i].name: topic_types[i].type for i in range(len(topic_types))}
    # Set filter for topic of string type
    

    storage_filter = rosbag2_py.StorageFilter(topics=['/loc/ego_tf', '/loc/ego_tf_relative'])
    reader.set_filter(storage_filter)

    # 数据定义
    mapNameList = ['t','x','y','yaw']
    #——————————————————————————————————————————————————————————————————————————————————————————————————————————
    # ego_tf
    ego_tf_msg_counter = 0
    ego_tf_msgList = ['__time', 
                    'metadata/loc/ego_tf/world_x', 
                    'metadata/loc/ego_tf/world_y', 
                    'metadata/loc/ego_tf/world_yaw']
    msgNameMap = {mapNameList[i]: ego_tf_msgList[i] for i in range(len(mapNameList))}   # 便于后面程序编写--可以不要，只是一个名字映射
    ego_tf_dataMap = {msg:[] for msg in ego_tf_msgList}
    #——————————————————————————————————————————————————————————————————————————————————————————————————————————
    # ego_tf_relative
    ego_tf_relative_msg_counter = 0
    ego_tf_relative_msgList = ['__time', 
                            'metadata/loc/ego_tf_relative/x', 
                            'metadata/loc/ego_tf_relative/y', 
                            'metadata/loc/ego_tf_relative/yaw']
    msgNameMap_r = {mapNameList[i]: ego_tf_relative_msgList[i] for i in range(len(mapNameList))}
    ego_tf_relative_dataMap = {msg:[] for msg in ego_tf_relative_msgList}
    #——————————————————————————————————————————————————————————————————————————————————————————————————————————


    while reader.has_next():
        (topic, data, t) = reader.read_next()

        msg_type = get_message(type_map[topic])
        msg_data = deserialize_message(data, msg_type)
        if topic == '/loc/ego_tf':
            # loc_timestamp = msg_data.header.stamp.sec + msg_data.header.stamp.nanosec*1e-9
            loc_timestamp = t*1.0e-9
            world_x = msg_data.world_x
            world_y = msg_data.world_y
            world_yaw = msg_data.world_yaw

            ego_tf_msg_counter += 1
            # print(msg_counter)
            if ego_tf_msg_counter>2e5:
                print('ego_tf exceed max number, data lost!')
                break
            ego_tf_dataMap[msgNameMap['t']].append(loc_timestamp)
            ego_tf_dataMap[msgNameMap['x']].append(world_x)
            ego_tf_dataMap[msgNameMap['y']].append(world_y)
            ego_tf_dataMap[msgNameMap['yaw']].append(world_yaw)

        elif topic == '/loc/ego_tf_relative':
            # loc_timestamp = msg_data.header.stamp.sec + msg_data.header.stamp.nanosec*1e-9
            loc_timestamp = t*1.0e-9
            x = msg_data.x
            y = msg_data.y
            yaw = msg_data.yaw

            ego_tf_relative_msg_counter += 1
            # print(msg_counter)
            if ego_tf_relative_msg_counter>2e5:
                print('ego_tf exceed max number, data lost!')
                break
            ego_tf_relative_dataMap[msgNameMap_r['t']].append(loc_timestamp)
            ego_tf_relative_dataMap[msgNameMap_r['x']].append(x)
            ego_tf_relative_dataMap[msgNameMap_r['y']].append(y)
            ego_tf_relative_dataMap[msgNameMap_r['yaw']].append(yaw)

    return (ego_tf_dataMap, ego_tf_relative_dataMap)



if __name__ == '__main__':
    startTime = time.time()
    fatherPath = os.path.dirname(os.path.abspath(__file__))

    bag_file = r"/zdrive/task01_simulatePath/datas/rosbag2_80km/rosbag2_2022_03_29-13_08_53/rosbag2_2022_03_29-13_08_53_0.db3"
    ego_tf_dataMap, ego_tf_relative_dataMap = eg_tf_data_reader(bag_file)
    print(len(ego_tf_dataMap['__time']), len(ego_tf_dataMap['metadata/loc/ego_tf/world_x']))

    print(len(ego_tf_relative_dataMap['__time']), len(ego_tf_relative_dataMap['metadata/loc/ego_tf_relative/x']))

    # 转换为csv并保存
    # df = pd.DataFrame(dataMap)
    # df.to_csv('./test.csv', index=False)
    print("time:", time.time()-startTime)