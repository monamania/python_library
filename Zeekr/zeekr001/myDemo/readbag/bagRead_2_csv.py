#!/bin/env python3
# -*- coding:utf-8 -*-
# @Author: monomania_cjk
# @Data: 2022.04.06

import math
import numpy as np
import numpy.linalg as LA  # 计算曲率
import pandas as pd

import rclpy
from rclpy.node import Node

import os
import sys
import time

from rclpy.serialization import deserialize_message
from rosidl_runtime_py.utilities import get_message

import rosbag2_py

subtopic_control_command = '/pnc/control_command'
subtopic_chassis_report = '/pnc/chassis_report'

if os.environ.get('ROSBAG2_PY_TEST_WITH_RTLD_GLOBAL', None) is not None:
    # This is needed on Linux when compiling with clang/libc++.
    # TL;DR This makes class_loader work when using a python extension compiled with libc++.
    #
    # For the fun RTTI ABI details, see https://whatofhow.wordpress.com/2015/03/17/odr-rtti-dso/.
    sys.setdlopenflags(os.RTLD_GLOBAL | os.RTLD_LAZY)

def get_rosbag_options(path, serialization_format='cdr'):
    storage_options = rosbag2_py.StorageOptions(uri=path, storage_id='sqlite3')
    converter_options = rosbag2_py.ConverterOptions(
        input_serialization_format=serialization_format,
        output_serialization_format=serialization_format)
    return storage_options, converter_options


def eg_tf_data_reader(bag_path):
    storage_options, converter_options = get_rosbag_options(bag_path)
    reader = rosbag2_py.SequentialReader()
    reader.open(storage_options, converter_options)
    topic_types = reader.get_all_topics_and_types()
    # Create a map for quicker lookup
    type_map = {topic_types[i].name: topic_types[i].type for i in range(len(topic_types))}
    # Set filter for topic of string type
    

    storage_filter = rosbag2_py.StorageFilter(topics=[subtopic_control_command, subtopic_chassis_report])
    reader.set_filter(storage_filter)

    # 数据定义
    mapNameList = ['t','acc']
    #——————————————————————————————————————————————————————————————————————————————————————————————————————————
    # control_command
    control_command_msg_counter = 0
    control_command_msgList = ['__time', 
                    'metadata/pnc/control_command/chassis_command/acceleration']
    msgNameMap = {mapNameList[i]: control_command_msgList[i] for i in range(len(mapNameList))}   # 便于后面程序编写--可以不要，只是一个名字映射
    control_command_dataMap = {msg:[] for msg in control_command_msgList}
    #——————————————————————————————————————————————————————————————————————————————————————————————————————————
    # chassis_report
    chassis_report_msg_counter = 0
    chassis_report_msgList = ['__time', 
                            'metadata/pnc/chassis_report/imu_motion_master/acceleration_x/value',
                            'metadata/pnc/chassis_report/auto_driving_mode',
                            'metadata/pnc/chassis_report/speed']
    msgNameMap_r = {mapNameList[i]: chassis_report_msgList[i] for i in range(len(mapNameList))}
    chassis_report_dataMap = {msg:[] for msg in chassis_report_msgList}
    #——————————————————————————————————————————————————————————————————————————————————————————————————————————


    while reader.has_next():
        (topic, data, t) = reader.read_next()

        msg_type = get_message(type_map[topic])
        msg_data = deserialize_message(data, msg_type)
        if topic == subtopic_control_command:
            # loc_timestamp = msg_data.header.stamp.sec + msg_data.header.stamp.nanosec*1e-9
            loc_timestamp = t*1.0e-9
            acc = msg_data.chassis_command.acceleration

            control_command_msg_counter += 1
            # print(msg_counter)
            if control_command_msg_counter>2e5:
                print('control_command exceed max number, data lost!')
                break
            control_command_dataMap[msgNameMap['t']].append(loc_timestamp)
            control_command_dataMap[msgNameMap['acc']].append(acc)

        elif topic == subtopic_chassis_report:
            # loc_timestamp = msg_data.header.stamp.sec + msg_data.header.stamp.nanosec*1e-9
            loc_timestamp = t*1.0e-9
            acc = msg_data.imu_motion_master.acceleration_x.value
            mode = msg_data.auto_driving_mode
            speed = msg_data.speed.value

            chassis_report_msg_counter += 1
            # print(msg_counter)
            if chassis_report_msg_counter>2e5:
                print('chassis_report exceed max number, data lost!')
                break
            chassis_report_dataMap[msgNameMap_r['t']].append(loc_timestamp)
            chassis_report_dataMap[msgNameMap_r['acc']].append(acc)
            chassis_report_dataMap['metadata/pnc/chassis_report/auto_driving_mode'].append(mode)
            chassis_report_dataMap['metadata/pnc/chassis_report/speed'].append(speed)

    return (control_command_dataMap, chassis_report_dataMap)



if __name__ == '__main__':
    startTime = time.time()
    fatherPath = os.path.dirname(os.path.abspath(__file__))
    
    bag_file = fatherPath + r"/data/slice_581_20220403152308_20220403152508.db3"
    control_command_dataMap, chassis_report_dataMap = eg_tf_data_reader(bag_file)
    print(len(control_command_dataMap['__time']), len(control_command_dataMap['metadata/pnc/control_command/chassis_command/acceleration']))

    print(len(chassis_report_dataMap['__time']), len(chassis_report_dataMap['metadata/pnc/chassis_report/imu_motion_master/acceleration_x/value']))

    # 转换为csv并保存
    df = pd.DataFrame(control_command_dataMap)
    df.to_csv(fatherPath + '/data/control_command.csv', index=False)
    df = pd.DataFrame(chassis_report_dataMap)
    df.to_csv(fatherPath + '/data/chassis_report.csv', index=False)
    print("time:", time.time()-startTime)