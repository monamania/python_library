#!/bin/env python3
# -*- coding:utf-8 -*-
# @Author: monomania_cjk
# @Data: 2022.04.06

import math
import numpy as np
import numpy.linalg as LA  # 计算曲率
import pandas as pd

import rclpy
from rclpy.node import Node

import os
import sys
import time

from rclpy.serialization import deserialize_message
from rosidl_runtime_py.utilities import get_message

import rosbag2_py

"""
支持绝对路径与相对路径----可搭配GUI
"""


if os.environ.get('ROSBAG2_PY_TEST_WITH_RTLD_GLOBAL', None) is not None:
    # This is needed on Linux when compiling with clang/libc++.
    # TL;DR This makes class_loader work when using a python extension compiled with libc++.
    #
    # For the fun RTTI ABI details, see https://whatofhow.wordpress.com/2015/03/17/odr-rtti-dso/.
    sys.setdlopenflags(os.RTLD_GLOBAL | os.RTLD_LAZY)


def get_rosbag_options(path, serialization_format='cdr'):
    storage_options = rosbag2_py.StorageOptions(uri=path, storage_id='sqlite3')
    converter_options = rosbag2_py.ConverterOptions(
        input_serialization_format=serialization_format,
        output_serialization_format=serialization_format)
    return storage_options, converter_options



class BagReader:
    def __init__(self, bag_path=None) -> None:
        # GUI————————————————————————————————————————————————————————————————
        from tkinter import Tk
        from tkinter.filedialog import askopenfilename
        root = Tk()
        root.withdraw()
        ffatherPath = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        bag_path = askopenfilename(title='Please choose a ros2 bag file', 
                        initialdir=ffatherPath, filetypes=[('ros2 bag file','*.db3')])
        if bag_file:
            self.bagPath = bag_path
        # GUI————————————————————————————————————————————————————————————————
        self.bagPath = self.handlePath(bag_path)
        

    @staticmethod
    def handlePath(path):
        """
        判断路径是否存在,同时将相对位置转换为绝对位置
        """
        fatherPath = os.path.dirname(os.path.abspath(__file__))

        # 同时支持相对路径与绝对路径
        if os.path.exists(path):
            return path
        elif os.path.exists(fatherPath + os.sep + path):
            return fatherPath + os.sep + path
        else:
            print('='*80)
            print("\t\t=== Bag path do not exist===")
            print("\t\t=== Stop ===")
            print(path)
            print('='*80)
            import sys
            sys.exit()



    def getData_ego_tf(self):
        startTime = time.time()
        # 这里只能是绝对路径
        # ——————————————————————————————————————————————————————————————————————
        import json
        bagName = os.path.basename(self.bagPath).split('.')[0]
        bagDir = os.path.dirname(self.bagPath)

        jsonPath = bagDir + os.sep + "pltData_" + bagName + '.json'

        if os.path.exists(jsonPath):
            print('='*80)
            print("\t\t=== JSON is exist ===")
            print('='*80)
            startTime = time.time()
            with open(jsonPath) as file_obj:
                maps = json.load(file_obj)
            ego_tf_dataMap = maps["ego_tf_dataMap"]
            ego_tf_relative_dataMap = maps["ego_tf_relative_dataMap"]
            print("read JSON time: ",time.time()-startTime)
        else:
            print('='*80)
            print("\t\t=== JSON is not exist, Reading bag ===")
            print("\t\t=== Please wait a minute ===")
            print('='*80)
            startTime = time.time()
            ego_tf_dataMap, ego_tf_relative_dataMap = self.__ego_tf_data_reader()
            maps = {
                "ego_tf_dataMap" : ego_tf_dataMap,
                "ego_tf_relative_dataMap" : ego_tf_relative_dataMap
            }
            # https://blog.csdn.net/qq_43523725/article/details/104770458
            # https://www.cnblogs.com/tizer/p/11067098.html
            with open(jsonPath,'w',encoding='utf-8') as f:
                json.dump(maps,f)
            # 标准格式存储
            # with open(jsonPath,'w',encoding='utf-8') as f:
            #     f.write(json.dumps(maps,ensure_ascii=False,indent=1))

            print("read bag time: ",time.time()-startTime)
        # ——————————————————————————————————————————————————————————————————————
        import sys
        if len(ego_tf_dataMap['__time']) == 0:
            print('='*80)
            print("============== ego_tf_dataMap is empty! ==============")
            print('='*80)
            sys.exit()
        if len(ego_tf_relative_dataMap['__time']) == 0:
            print('='*80)
            print("============== ego_tf_relative_dataMap is empty! ==============")
            print('='*80)
            sys.exit()
        
        print("read data time:",time.time()-startTime)
        return ego_tf_dataMap, ego_tf_relative_dataMap
    


    def __ego_tf_data_reader(self):
        storage_options, converter_options = get_rosbag_options(self.bagPath)
        reader = rosbag2_py.SequentialReader()
        reader.open(storage_options, converter_options)
        topic_types = reader.get_all_topics_and_types()
        # Create a map for quicker lookup
        type_map = {topic_types[i].name: topic_types[i].type for i in range(len(topic_types))}
        # Set filter for topic of string type
        

        storage_filter = rosbag2_py.StorageFilter(topics=['/loc/ego_tf', '/loc/ego_tf_relative'])
        reader.set_filter(storage_filter)

        # 数据定义
        mapNameList = ['t','x','y','yaw']
        #——————————————————————————————————————————————————————————————————————————————————————————————————————————
        # ego_tf
        ego_tf_msg_counter = 0
        ego_tf_msgList = ['__time', 
                        'metadata/loc/ego_tf/world_x', 
                        'metadata/loc/ego_tf/world_y', 
                        'metadata/loc/ego_tf/world_yaw']
        msgNameMap = {mapNameList[i]: ego_tf_msgList[i] for i in range(len(mapNameList))}   # 便于后面程序编写--可以不要，只是一个名字映射
        ego_tf_dataMap = {msg:[] for msg in ego_tf_msgList}
        #——————————————————————————————————————————————————————————————————————————————————————————————————————————
        # ego_tf_relative
        ego_tf_relative_msg_counter = 0
        ego_tf_relative_msgList = ['__time', 
                                'metadata/loc/ego_tf_relative/x', 
                                'metadata/loc/ego_tf_relative/y', 
                                'metadata/loc/ego_tf_relative/yaw']
        msgNameMap_r = {mapNameList[i]: ego_tf_relative_msgList[i] for i in range(len(mapNameList))}
        ego_tf_relative_dataMap = {msg:[] for msg in ego_tf_relative_msgList}
        #——————————————————————————————————————————————————————————————————————————————————————————————————————————
        while reader.has_next():
            (topic, data, t) = reader.read_next()
            
            msg_type = get_message(type_map[topic])
            msg_localizaztion = deserialize_message(data, msg_type)

            if topic == '/loc/ego_tf':
                # loc_timestamp = msg_localizaztion.header.stamp.sec + msg_localizaztion.header.stamp.nanosec*1e-9
                loc_timestamp = t*1.0e-9
                world_x = msg_localizaztion.world_x
                world_y = msg_localizaztion.world_y
                world_yaw = msg_localizaztion.world_yaw

                ego_tf_msg_counter += 1
                # print(msg_counter)
                if ego_tf_msg_counter>2e5:
                    print('ego_tf exceed max number, data lost!')
                    break
                ego_tf_dataMap[msgNameMap['t']].append(loc_timestamp)
                ego_tf_dataMap[msgNameMap['x']].append(world_x)
                ego_tf_dataMap[msgNameMap['y']].append(world_y)
                ego_tf_dataMap[msgNameMap['yaw']].append(world_yaw)

            elif topic == '/loc/ego_tf_relative':
                # loc_timestamp = msg_localizaztion.header.stamp.sec + msg_localizaztion.header.stamp.nanosec*1e-9
                loc_timestamp = t*1.0e-9
                x = msg_localizaztion.x
                y = msg_localizaztion.y
                yaw = msg_localizaztion.yaw

                ego_tf_relative_msg_counter += 1
                # print(msg_counter)
                if ego_tf_relative_msg_counter>2e5:
                    print('ego_tf exceed max number, data lost!')
                    break
                ego_tf_relative_dataMap[msgNameMap_r['t']].append(loc_timestamp)
                ego_tf_relative_dataMap[msgNameMap_r['x']].append(x)
                ego_tf_relative_dataMap[msgNameMap_r['y']].append(y)
                ego_tf_relative_dataMap[msgNameMap_r['yaw']].append(yaw)

        return (ego_tf_dataMap, ego_tf_relative_dataMap)



if __name__ == '__main__':
    startTime = time.time()
    # bag_file = "/zdrive/task_cjk/task01_simulatePath/datas/rosbag2_80km/rosbag2_2022_03_29-13_08_53/rosbag2_2022_03_29-13_08_53_0.db3"

    if len(sys.argv) == 2:
        bag_file = sys.argv[1]
    else:
        print("="*80)
        bagName = os.path.basename(os.path.abspath(__file__))
        print(f"Run with: python3 {bagName} <bag path> [<weith_t>]")
        print("="*80)
        import sys
        sys.exit()

    reader = BagReader(bag_file)
    ego_tf_dataMap, ego_tf_relative_dataMap = reader.getData_ego_tf()


    # 转换为csv并保存
    # df = pd.DataFrame(ego_tf_dataMap)
    # df.to_csv(fatherPath + '/ego_tf_dataMap_rosbag2_2022_04_11-14_59_32_0.csv', index=False)
    # df = pd.DataFrame(ego_tf_relative_dataMap)
    # df.to_csv(fatherPath + '/ego_tf_relative_dataMap.csv', index=False)



    # if len(sys.argv) == 2:
    #     file_path = str(sys.argv[1])
    # else:
    #     from tkinter import Tk
    #     from tkinter.filedialog import askopenfilename
    #     root = Tk()
    #     root.withdraw()
    #     # ffatherPath = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    #     ffatherPath = os.path.dirname(os.path.dirname(os.getcwd()))
    #     file_path = askopenfilename(title='Please choose a npz file', 
    #                     initialdir = ffatherPath, filetypes=[('npz file','*.npz')])

    # if not file_path:
    #     sys.exit()
