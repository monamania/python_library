#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# @Author: monomania_cjk(mono_cjk@qq.com)
# @Data: 2022.04.28

import os
import sqlite3
import time

# import matplotlib.pyplot as plt
from rclpy.serialization import deserialize_message
from rosidl_runtime_py.utilities import get_message

"""
"""


class BagFileParser:
    def __init__(self, bag_file):
        self.bag_file = self.handlePath(bag_file)
        basename = os.path.basename(self.bag_file)
        # suffix 后缀
        self.fileName, self.fileSuffix = os.path.splitext(basename)
        # 路径
        self.fileDir = os.path.dirname(self.bag_file)
        # ——————————————————————————————————————————————————————————————
        startTime = time.time()
        print("=" * 80)
        print("\t\t=== Reading bag ===")
        print("\t\t=== Please wait a minute ===")
        print("=" * 80)
        self.conn = sqlite3.connect(self.bag_file)
        self.cursor = self.conn.cursor()

        ## create a message type map
        topics_data = self.cursor.execute(
            "SELECT id, name, type FROM topics"
        ).fetchall()
        self.topic_type = {name_of: type_of for id_of, name_of, type_of in topics_data}
        self.topic_id = {name_of: id_of for id_of, name_of, type_of in topics_data}
        self.topic_msg_message = {
            name_of: get_message(type_of) for id_of, name_of, type_of in topics_data
        }
        print("connect db time: ", time.time() - startTime)

    @staticmethod
    def handlePath(path: str):
        """
        判断路径是否存在,同时将相对位置转换为绝对位置
        """
        fatherPath = os.path.dirname(os.path.abspath(__file__))
        import getpass

        user_name = getpass.getuser()
        # print("user name: ", user_name)
        if os.path.exists(os.sep + path.strip(f"/home/{user_name}")):
            return os.sep + path.strip(f"/home/{user_name}")

        # 同时支持相对路径与绝对路径
        if os.path.exists(path):
            return path
        elif os.path.exists(fatherPath + os.sep + path):
            return fatherPath + os.sep + path
        elif os.path.exists(f"/home/{user_name}" + os.sep + path):
            return f"/home/{user_name}" + os.sep + path
        elif os.path.exists(path.strip(f"home/{user_name}")):
            return path.strip(f"home/{user_name}")
        else:
            print("=" * 80)
            print("\t\t=== data path do not exist===")
            print("\t\t=== Stop ===")
            print(path)
            print("=" * 80)
            import sys

            sys.exit()

    def __del__(self):
        try:
            self.conn.close()
        except AttributeError:
            pass

    # Return [(timestamp0, message0), (timestamp1, message1), ...]
    def get_messages(self, topic_name):

        topic_id = self.topic_id[topic_name]
        # Get from the db
        rows = self.cursor.execute(
            "SELECT timestamp, data FROM messages WHERE topic_id = {}".format(topic_id)
        ).fetchall()
        # Deserialise all and timestamp them
        return [
            (timestamp, deserialize_message(data, self.topic_msg_message[topic_name]))
            for timestamp, data in rows
        ]


if __name__ == "__main__":

    # bag_file = '/zdrive/task_cjk/bagData/rosbag2_2022_04_07-14_40_04/rosbag2_2022_04_07-14_40_04_0.db3'
    bag_file = r"/zdrive/task_cjk/bagData/Download-Slice-Data-1512695047210745858/slice-metadata/1649991008387/slice_581_20220408163359_20220408163449.db3"
    startTime = time.time()
    parser = BagFileParser(bag_file)

    trajectory = parser.get_messages("/loc/ego_tf")[0][1]
    print("readerData time:", time.time() - startTime)
    print("=" * 80)
    print(trajectory.world_x)
    print(trajectory.world_y)

    startTime = time.time()
    actual = parser.get_messages("/loc/ego_tf_relative")
    print("readerData time:", time.time() - startTime)

    # plt.plot(t_des, p_des_1)

    # plt.show()
