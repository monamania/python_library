#!/bin/env python3
# -*- coding:utf-8 -*-
# @Author: monomania_cjk(mono_cjk@qq.com)
# @Data: 2022.04.28

import numpy as np
import pandas as pd

import os
import sys
import time

from BagFileParser_Sqlite3 import BagFileParser

if os.environ.get('ROSBAG2_PY_TEST_WITH_RTLD_GLOBAL', None) is not None:
    # This is needed on Linux when compiling with clang/libc++.
    # TL;DR This makes class_loader work when using a python extension compiled with libc++.
    #
    # For the fun RTTI ABI details, see https://whatofhow.wordpress.com/2015/03/17/odr-rtti-dso/.
    sys.setdlopenflags(os.RTLD_GLOBAL | os.RTLD_LAZY)


def eg_tf_data_reader(bag_path):
    reader = BagFileParser(bag_path)
    ego_tf_dataList = reader.get_messages("/loc/ego_tf")
    ego_tf_relative_dataList = reader.get_messages("/loc/ego_tf_relative")

    # 数据定义与读取
    #——————————————————————————————————————————————————————————————————————————————————————————————————————————
    ego_tf_msgList = [
        '__time', 'ego_tf/world_x', 'ego_tf/world_y', 'ego_tf/world_yaw'
    ]
    ego_tf_dataMap = {msg: [] for msg in ego_tf_msgList}
    # ego_tf
    ego_tf_msg_counter = 0
    for t, msg in ego_tf_dataList:
        ego_tf_msg_counter += 1
        # print(msg_counter)
        if ego_tf_msg_counter > 2e5:
            print('ego_tf exceed max number, data lost!')
            break
        ego_tf_dataMap["__time"].append(t)
        ego_tf_dataMap["ego_tf/world_x"].append(msg.world_x)
        ego_tf_dataMap["ego_tf/world_y"].append(msg.world_y)
        ego_tf_dataMap["ego_tf/world_yaw"].append(msg.world_yaw)
    #——————————————————————————————————————————————————————————————————————————————————————————————————————————
    # ego_tf_relative
    ego_tf_ref_msgList = [
        '__time', 'ego_tf_ref/x', 'ego_tf_ref/y', 'ego_tf_ref/yaw'
    ]
    ego_tf_relative_dataMap = {msg: [] for msg in ego_tf_ref_msgList}

    ego_tf_relative_msg_counter = 0
    for t, msg in ego_tf_relative_dataList:
        ego_tf_relative_msg_counter += 1
        # print(msg_counter)
        if ego_tf_relative_msg_counter > 2e5:
            print('ego_tf exceed max number, data lost!')
            break
        ego_tf_relative_dataMap["__time"].append(t)
        ego_tf_relative_dataMap["ego_tf_ref/x"].append(msg.x)
        ego_tf_relative_dataMap["ego_tf_ref/y"].append(msg.y)
        ego_tf_relative_dataMap["ego_tf_ref/yaw"].append(msg.yaw)

    return (ego_tf_dataMap, ego_tf_relative_dataMap)


if __name__ == '__main__':
    startTime = time.time()
    fatherPath = os.path.dirname(os.path.abspath(__file__))
    bag_file = r"/zdrive/task_cjk/bagData/apa_bagData/145134_l/1651130342853/slice_582_20220427145137_20220427145406.db3"
    # bag_file = r"/zdrive/task_cjk/bagData/rosbag2_2022_04_07-14_40_04/rosbag2_2022_04_07-14_40_04_0.db3"
    ego_tf_dataMap, ego_tf_relative_dataMap = eg_tf_data_reader(bag_file)
    print(len(ego_tf_dataMap['__time']), len(ego_tf_dataMap['ego_tf/world_x']))

    print(len(ego_tf_relative_dataMap['__time']),
          len(ego_tf_relative_dataMap['ego_tf_ref/x']))

    # 转换为csv并保存
    # df = pd.DataFrame(dataMap)
    # df.to_csv('./test.csv', index=False)
    print("time:", time.time() - startTime)
