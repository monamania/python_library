#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# @Author: monomania_cjk
# @Data: 2022.04.25

import os
import sys
import math
import time
from pprint import pprint
import numpy as np
import pandas as pd


"""
bag包数据读取
1. 依据needTopicList读取bag包, 并将每个topic单独存储为Json文件, 后续不在docker环境下，依旧可以使用
2. 支持绝对路径与相对路径---路径处理
"""


class DataReader:
    def __init__(self, dataPath) -> None:
        self.dataPath = self.handlePath(dataPath)
        basename = os.path.basename(self.dataPath)

        # suffix 后缀
        self.fileName, self.fileSuffix = os.path.splitext(basename)
        # 路径
        self.fileDir = os.path.dirname(self.dataPath)

    @staticmethod
    def handlePath(path: str):
        """
        判断路径是否存在,同时将相对位置转换为绝对位置
        """
        fatherPath = os.path.dirname(os.path.abspath(__file__))
        import getpass

        user_name = getpass.getuser()
        # print("user name: ", user_name)

        # 同时支持相对路径与绝对路径
        if os.path.exists(path):
            return path
        elif os.path.exists(fatherPath + os.sep + path):
            return fatherPath + os.sep + path
        elif os.path.exists(f"/home/{user_name}" + os.sep + path):
            return f"/home/{user_name}" + os.sep + path
        elif os.path.exists(path.strip(f"/home/{user_name}")):
            return path.strip(f"/home/{user_name}")
        else:
            print("=" * 80)
            print("\t\t=== data path do not exist===")
            print("\t\t=== Stop ===")
            print(path)
            print("=" * 80)
            import sys

            sys.exit()


class BagReader(DataReader):
    def __init__(self, bag_path, needTopicList=None):
        super().__init__(bag_path)
        self.bagPath = self.dataPath

        self.needTopicList = needTopicList
        # 获取数据
        self.__dataMaps, self.__topicList = self.__getData()

    @property
    def dataMaps(self):
        return self.__dataMaps

    def dataMapsOfTopic(self, topic):
        return self.__dataMaps[topic]

    @property
    def topicList(self):
        return self.__topicList

    def getPandaData(self, topicName: str):
        return pd.DataFrame(self.dataMaps[topicName])

    def getNumpyData(self, topic: str, msgList: list = None):
        """
        @topic:
        @timeList: 默认包括timeList
        @msg: msg name list
        """
        df = self.getPandaData(topic)
        # if timeList:
        #     return pd.concat([ df['__time'],df[msgList] ], axis=1).values
        # else:
        if msgList:
            return df[msgList].values
        else:
            return df.values

    @staticmethod
    def getDataByClass_recursion(obj):
        """
        递归获取类内元素--类中类中类
        """
        attr_list = [
            i
            for i in dir(obj)
            if not callable(getattr(obj, i)) and not i.startswith("_") and i.islower()
        ]
        # tmp_dataMap.update()
        tmp_dataMap = {}
        for attr in attr_list:
            value = getattr(obj, attr)
            if isinstance(value, (list)):
                valueList = []
                for v in value:
                    valueList.append(BagReader.getDataByClass_recursion(v))
                tmp_dataMap[attr] = valueList
                continue

            if not isinstance(value, (str, float, int, list, np.ndarray)):
                # print(value)
                value = BagReader.getDataByClass_recursion(value)

            if isinstance(value, (np.ndarray)):
                # print(value)
                # sys.exit()
                value = list(value.astype(float))
            tmp_dataMap[attr] = value
        return tmp_dataMap

    @staticmethod
    def serialize_instance(obj):
        """解析数据类型----递归
        getDataByClass_recursion
        通用
        @obj: class的实例对象
        @return: map
        额外增加一层 __className__  信息
        """
        dataMap = {"__className__": type(obj).__name__}
        tmp_dataMap = BagReader.getDataByClass_recursion(obj)
        dataMap.update(tmp_dataMap)
        return tmp_dataMap

    def __getJsonData(self, jsonPath):
        import json

        if os.path.exists(jsonPath):
            # print("=" * 80)
            # print("\t\t=== JSON is exist ===")
            # print(jsonPath)
            # print("=" * 80)
            # startTime = time.time()
            with open(jsonPath) as file_obj:
                dataMaps = json.load(file_obj)
            # print("read JSON time: ", time.time() - startTime)
            return dataMaps
        else:
            return None

    def __saveJsonData(self, jsonData: dict, jsonPath: str, standard: True):
        # https://blog.csdn.net/qq_43523725/article/details/104770458
        # https://www.cnblogs.com/tizer/p/11067098.html
        import json

        if standard:
            # 标准格式存储
            with open(jsonPath, "w", encoding="utf-8") as f:
                f.write(json.dumps(jsonData, ensure_ascii=False, indent=1))
        else:
            with open(jsonPath, "w", encoding="utf-8") as f:
                json.dump(jsonData, f)

        print("=" * 80)
        print("\t\t=== Standard format: {} ===".format(standard))
        print("\t\t=== JSON sava path: %s ===" % jsonPath)
        print("=" * 80)

    def __saveJsonDataEachTopic(self, jsonData: dict, standard: True):
        bagDir = self.fileDir
        for topic in jsonData.keys():
            jsonPath = bagDir + os.sep + "topicData_" + topic.split("/")[-1] + ".json"
            tmpMap = {topic: jsonData[topic]}
            self.__saveJsonData(tmpMap, jsonPath, standard=standard)

    def __getDataNeedTopic(self):
        """获取topic list 的相关数据"""
        bagDir = self.fileDir
        dataMaps = {}
        notExistTopicList = []
        for topic in self.needTopicList:
            jsonPath = bagDir + os.sep + "topicData_" + topic.split("/")[-1] + ".json"
            tmpDataMap = self.__getJsonData(jsonPath)
            if tmpDataMap:
                try:
                    dataMaps.update(tmpDataMap)
                except:
                    print(type(tmpDataMap))
                    print("func __getDataNeedTopic(): update has something wrong!")
                    notExistTopicList.append(topic)
            else:
                print("json of " + topic + " not exist")
                notExistTopicList.append(topic)

        if notExistTopicList:
            print("=" * 80)
            print(f"{notExistTopicList}")
            print(f"\t\t=== Json is not exist, Reading bag ===")
            print("\t\t=== Please wait a minute ===")
            print("=" * 80)
            tmpDataMaps, _ = self.__read_bag(notExistTopicList)
            self.__saveJsonDataEachTopic(tmpDataMaps, standard=True)
            dataMaps.update(tmpDataMaps)
        return dataMaps

    def __getData(self):
        startGetTime = time.time()
        # 这里只能是绝对路径
        # ——————————————————————————————————————————————————————————————————————

        bagName = self.fileName
        bagDir = self.fileDir

        if self.fileSuffix == "json":
            bagJsonPath = self.bagPath
        else:
            bagJsonPath = bagDir + os.sep + "bagData_" + bagName + ".json"

        if self.needTopicList:
            dataMaps = self.__getDataNeedTopic()
        else:
            if os.path.exists(bagJsonPath):
                print("=" * 80)
                print("\t\t=== BAG JSON is exist ===")
                print("=" * 80)
                startTime = time.time()
                dataMaps = self.__getJsonData(bagJsonPath)
                print("read JSON time: ", time.time() - startTime)
            else:
                print("=" * 80)
                print("\t\t=== BAG JSON is not exist, Reading bag ===")
                print("\t\t=== Please wait a minute ===")
                print("=" * 80)
                startTime = time.time()
                dataMaps, _ = self.__read_bag()
                self.__saveJsonData(dataMaps, bagJsonPath, standard=True)
                print("read bag time: ", time.time() - startTime)
        # ——————————————————————————————————————————————————————————————————————
        print("read data time:", time.time() - startGetTime)

        topicList = list(dataMaps.keys())
        if self.needTopicList:
            topicList = self.needTopicList
        return dataMaps, topicList

    def __read_bag(self, needTopicList=None):
        """一次性读取所有数据并返回map
        return  {topic1: [], topic2:[]}
        """
        from BagFileParser_Sqlite3 import BagFileParser
        from rclpy.serialization import deserialize_message

        bg_parser = BagFileParser(self.bagPath)

        # Create a map for quicker lookup
        type_map = bg_parser.topic_type
        # Set filter for topic of string type

        if not needTopicList:
            needTopicList = list(type_map.keys())
        else:
            notExitTopicListOfBag = [
                topic for topic in needTopicList if topic not in list(type_map.keys())
            ]
            if notExitTopicListOfBag:
                print("those topics do not exit in the bag: ", notExitTopicListOfBag)
                needTopicList = [
                    topic
                    for topic in needTopicList
                    if topic not in notExitTopicListOfBag
                ]
                print("new needTopicList:", needTopicList)

        # needTopicList
        dataMap = {topicName: [] for topicName in needTopicList}

        for topic in needTopicList:
            topic_id = bg_parser.topic_id[topic]
            # Get from the db
            rows = bg_parser.cursor.execute(
                "SELECT timestamp, data FROM messages WHERE topic_id = {}".format(
                    topic_id
                )
            ).fetchall()
            for timestamp, data in rows:
                msg = deserialize_message(data, bg_parser.topic_msg_message[topic])

                loc_timestamp = timestamp * 1.0e-9
                tmpMap = self.serialize_instance(msg)
                tmpMap.update({"__time": loc_timestamp})
                dataMap[topic].append(tmpMap)

        from pprint import pprint

        # pprint(dataMap[topicList[0]][0])
        topicList = list(dataMap.keys())  # 暂时没用
        return dataMap, topicList

    def __new__(cls, bag_path, needTopicList=None):
        """单例"""
        if not hasattr(BagReader, "_instance"):
            print("create bag reader")
            cls._instance = super().__new__(cls)
        else:
            print("bag reader instance exist")

        return BagReader._instance


if __name__ == "__main__":
    startTime = time.time()
    # bag_file = "/zdrive/task_cjk/task01_simulatePath/datas/rosbag2_80km/rosbag2_2022_03_29-13_08_53/rosbag2_2022_03_29-13_08_53_0.db3"

    if len(sys.argv) == 2:
        bag_file = str(sys.argv[1])
    else:
        from tkinter import Tk
        from tkinter.filedialog import askopenfilename

        root = Tk()
        root.withdraw()
        ffatherPath = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        # ffatherPath = os.path.dirname(os.path.dirname(os.getcwd()))
        bag_file = askopenfilename(
            title="Please choose a bag file",
            initialdir=ffatherPath,
            filetypes=[("bag file", "*.db3"), ("json file", "*.json")],
        )

    # needTopicList = ["/loc/ego_tf", "/pnc/control_debug", "/pnc/planning_result"]
    needTopicList = [
        "/loc/ego_tf",
        "/loc/ego_tf_relative",
        "/pnc/control_debug",
        "/pnc/planning_result",
    ]
    reader = BagReader(bag_file, needTopicList)
    # print(reader.topicList)
    # print(reader.bagTopicList)

    # print(reader.dataMaps)
    # print(reader.getPandaData("/loc/ego_tf"))
    # df = reader.getPandaData('/loc/ego_tf')
    # print(reader.getNumpyData('/loc/ego_tf', ['__time', 'world_x']))
    # print(reader.getNumpyData('/loc/ego_tf'))

    pprint(reader.dataMaps["/pnc/control_debug"][0])
