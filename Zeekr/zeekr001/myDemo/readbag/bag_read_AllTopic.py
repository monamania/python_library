#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# @Author: monomania_cjk
# @Data: 2022.04.06

import math
from pprint import pprint
import numpy as np
import numpy.linalg as LA  # 计算曲率
import pandas as pd

import rclpy
from rclpy.node import Node

import os
import sys
import time
from interfaces.msg import Localization
from interfaces.msg import Localization
# print(Localization.__dict__)

from rclpy.serialization import deserialize_message
from rosidl_runtime_py.utilities import get_message

import rosbag2_py
"""
支持绝对路径与相对路径----可搭配GUI
"""
# 序列化
# def serialize_instance(obj):
#     """解析数据类型"""
#     d = {'__classname__': type(obj).__name__}
#     d.update(vars(obj))
#     return d
# # 反序列化
# def unserialize_object(d):
#     clsname = d.pop('__className__', None)
#     if clsname:
#         # 需要改进的地方，定义类；也可以通过getattr创建对象
#         cls = corpora.Dictionary  # 测试用的类，可更改为其他类
#         obj = cls.__new__(cls)
#         for key, value in d.items():
#             setattr(obj, key, value)
#             return obj
#     else:
#         return d
# ————————————————
# 版权声明：本文为CSDN博主「yangtom249」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
# 原文链接：https://blog.csdn.net/weixin_44153121/article/details/106075577

# globals()["字符串"]---转换为类名
# globals()[type(msg)__name__]---转换为类名
# def serialize_instance(obj):
#     """解析数据类型---具备__dict__的情况
#     @obj: class的实例对象
#     @return: map
#     """
#     d = {'__classname__': type(obj).__name__}
#     d.update(vars(obj))
#     return d


def getDataByClass_recursion(obj):
    """
    递归获取类内元素--类中类中类
    """
    attr_list = [
        i for i in dir(obj) if not callable(getattr(obj, i))
        and not i.startswith('_') and i.islower()
    ]
    # tmp_dataMap.update()
    tmp_dataMap = {}
    for attr in attr_list:
        value = getattr(obj, attr)

        if not isinstance(value, (str, float, int, list, np.ndarray)):
            # print(value)
            value = getDataByClass_recursion(value)

        if isinstance(value, (np.ndarray)):
            # print(value)
            # sys.exit()
            value = list(value.astype(float))
        tmp_dataMap[attr] = value

    return tmp_dataMap


def serialize_instance(obj):
    """解析数据类型----递归
    通用
    @obj: class的实例对象
    @return: map
    """
    tmp_dataMap = {'__className__': type(obj).__name__}
    attr_list = [
        i for i in dir(obj) if not callable(getattr(obj, i))
        and not i.startswith('_') and i.islower()
    ]
    # tmp_dataMap.update()
    for attr in attr_list:
        value = getattr(obj, attr)
        if not isinstance(value, (str, float, int, list, np.ndarray)):
            # print(value)
            value = getDataByClass_recursion(value)
            print(value)
            tmp_dataMap[attr] = serialize_instance(value)

        if isinstance(value, (np.ndarray)):
            # print(value)
            # sys.exit()
            value = list(value.astype(float))

        tmp_dataMap[attr] = value

    return tmp_dataMap


if os.environ.get('ROSBAG2_PY_TEST_WITH_RTLD_GLOBAL', None) is not None:
    # This is needed on Linux when compiling with clang/libc++.
    # TL;DR This makes class_loader work when using a python extension compiled with libc++.
    #
    # For the fun RTTI ABI details, see https://whatofhow.wordpress.com/2015/03/17/odr-rtti-dso/.
    sys.setdlopenflags(os.RTLD_GLOBAL | os.RTLD_LAZY)


def get_rosbag_options(path, serialization_format='cdr'):
    storage_options = rosbag2_py.StorageOptions(uri=path, storage_id='sqlite3')
    converter_options = rosbag2_py.ConverterOptions(
        input_serialization_format=serialization_format,
        output_serialization_format=serialization_format)
    return storage_options, converter_options


def bag_reader(bagPath):
    """一次性读取所有数据并返回map
    return  {topic1: [], topic2:[]}
    """
    storage_options, converter_options = get_rosbag_options(bagPath)
    reader = rosbag2_py.SequentialReader()
    reader.open(storage_options, converter_options)
    topic_types = reader.get_all_topics_and_types()
    # Create a map for quicker lookup
    type_map = {
        topic_types[i].name: topic_types[i].type
        for i in range(len(topic_types))
    }
    topicList = list(type_map.keys())
    # Set filter for topic of string type

    storage_filter = rosbag2_py.StorageFilter(
        topics=['/loc/ego_tf', '/loc/ego_tf_relative'])
    reader.set_filter(storage_filter)

    # msgMap = {'topic': topic_types[i].name, 'topic_type': topic_types[i].type , 'data':[]  for i in range(len(topic_types)}
    dataMap = {topicName: [] for topicName in type_map.keys()}

    while reader.has_next():
        (topic, data, t) = reader.read_next()

        msg_type = get_message(type_map[topic])
        msg = deserialize_message(data, msg_type)

        loc_timestamp = t * 1.0e-9
        tmpMap = serialize_instance(msg)
        tmpMap.update({'__time': loc_timestamp})

        dataMap[topic].append(tmpMap)

    pprint(dataMap[topicList[0]][0])

    return dataMap


def bag_reader_2(bagPath):
    """一次性读取所有数据并返回map
    return  {topic1: {time, dataMap}, topic2: {time, dataMap}}
    """
    storage_options, converter_options = get_rosbag_options(bagPath)
    reader = rosbag2_py.SequentialReader()
    reader.open(storage_options, converter_options)
    topic_types = reader.get_all_topics_and_types()
    # Create a map for quicker lookup
    type_map = {
        topic_types[i].name: topic_types[i].type
        for i in range(len(topic_types))
    }
    topicList = list(type_map.keys())
    # Set filter for topic of string type

    storage_filter = rosbag2_py.StorageFilter(
        topics=['/loc/ego_tf', '/loc/ego_tf_relative'])
    reader.set_filter(storage_filter)

    # msgMap = {'topic': topic_types[i].name, 'topic_type': topic_types[i].type , 'data':[]  for i in range(len(topic_types)}
    dataMap = {
        topicName: {
            '__time': [],
            'data': []
        }
        for topicName in type_map.keys()
    }

    while reader.has_next():
        (topic, data, t) = reader.read_next()

        msg_type = get_message(type_map[topic])
        msg = deserialize_message(data, msg_type)

        loc_timestamp = t * 1.0e-9
        tmpMap = serialize_instance(msg)

        dataMap[topic]['data'].append(tmpMap)
        dataMap[topic]['__time'].append(loc_timestamp)

    pprint(dataMap[topicList[0]]['__time'])

    return dataMap


if __name__ == '__main__':
    startTime = time.time()
    # bagPath = r"/zdrive/task_cjk/bagData/Download-Slice-Data-1512695047210745858/slice-metadata/1649991008387/slice_581_20220408163359_20220408163449.db3"
    bag_file = "/zdrive/task_cjk/task01_simulatePath/datas/rosbag2_80km/rosbag2_2022_03_29-13_08_53/rosbag2_2022_03_29-13_08_53_0.db3"

    # reader = bag_reader(bag_file)
    bagMap = bag_reader(bag_file)

    jsonPath = r'./test.json'

    import json
    with open(jsonPath, 'w', encoding='utf-8') as f:
        json.dump(bagMap, f)

    # 转换为csv并保存
    # df = pd.DataFrame(ego_tf_dataMap)
    # df.to_csv(fatherPath + '/ego_tf_dataMap_rosbag2_2022_04_11-14_59_32_0.csv', index=False)
    # df = pd.DataFrame(ego_tf_relative_dataMap)
    # df.to_csv(fatherPath + '/ego_tf_relative_dataMap.csv', index=False)