```python
#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# @Author: monomania_cjk
# @Data: 2022.04.08
```

## os 目录
```python
 print("os.get.cwd",os.getcwd())             #获取当前工作目录：    D:\PythonPrj\Test
 print("os.path:",os.path)                   #获取的是ntpath      'D:\\ProgramData\\Python\\Python38\\lib\\ntpath.py'>
 print(os.path.abspath('.'))                 # 获取当前工作目录  D:\PythonPrj\Test
 print(os.path.abspath('test.xls'))          # 获取当前目录文件下的工作目录路径 
 D:\PythonPrj\Test\test.xls
   print(os.path.abspath('..'))            # 获取当前工作的父目录 ！注意是父目录路径  D:\PythonPrj
   print(os.path.abspath(os.curdir))       # 获取当前工作目录路径  D:\PythonPrj\Test
```
# matplotlib

## 交互模式
```python
plt.ion()：开启交互模式
plt.ioff()：关闭交互模式
plt.clf()：清理残留图像
plt.pause()：暂停

# 子图间距
plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=0.5)
```

scipy.optimize优化器——————如：最小二乘
https://blog.csdn.net/jiang425776024/article/details/87885969

# 计算四分位点----箱型图，去除异常值
## pandas
[pandas quantile 寻找四分位数及判断离群点](https://blog.csdn.net/SXxtyz/article/details/115372447)

https://blog.csdn.net/DDxuexi/article/details/116031223

`DataFrame.quantile(q=0.5, axis=0, numeric_only=True,interpolation=’linear’)`
interpolation（插值方法）:可以是 {‘linear’, ‘lower’, ‘higher’, ‘midpoint’, ‘nearest’}之一，默认是linear。
> 这五个插值方法是这样的：当选中的分为点位于两个数数据点 i and j 之间时:
> linear: i + (j - i) * fraction, fraction由计算得到的pos的小数部分（后面有例子）；
> lower: i.
> higher: j.
> nearest: i or j whichever is nearest.
> midpoint: (i + j) / 2.


```python
import pandas as pd
train_df = pd.read_csv("train.csv")
q1, q3 = train_df['price'].quantile([0.25, 0.75]) # 线性方法时与numpy percentile是一样的
iqr = q3 - q1
outlier = train_df[(train_df['price'] > q3 + iqr * 1.5) | (train_df['price'] < q1 - iqr * 1.5)]
```
## numpy
https://www.cnblogs.com/cgmcoding/p/13713171.html
[numpy : percentile百分位数使用（多用于去除 离群点）](https://blog.csdn.net/u011630575/article/details/79451357)
[python numpy quantile求四分位距](https://blog.csdn.net/weixin_30633507/article/details/97373292)

```python
import numpy as np
# percentile
np.percentile(array, q, axis)  # q 要计算的百分位数，在 0 ~ 100 之间  axis 沿着它计算百分位数的轴   ，二维取值0，1

# 
import numpy as np
ages=[3,3,6,7,7,10,10,10,11,13,30]
lower_q=np.quantile(ages,0.25,interpolation='lower')#下四分位数 
higher_q=np.quantile(ages,0.75,interpolation='higher')#上四分位数
int_r=higher_q-lower_q#四分位距
``` 


# numpy


## 插值、填充、合并
```python
import os
import sys
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import cufflinks as cf

df_control = pd.read_csv(controlPath)
df_control.columns = ['t','acc_c']
df_report = pd.read_csv(reportPath)
df_report.columns = ['t','acc_r','mode', 'speed']

# 扩充
np_control = df_control.values
np_report = df_report.values

# 扩充轴内容
np_control_x = np.linspace(np_control[0,0], np_control[-1,0], len(np_report))
# 插值
np_control_y = np.interp(np_control_x, np_control[:,0], np_control[:,1])
np_control_x = np_control_x.reshape(len(np_control_x), 1)
np_control_y = np_control_y.reshape(len(np_control_y), 1)
# 合并
np_control = np.concatenate((np_control_x,np_control_y),axis=1)
```

## 删除指定值
```python
import numpy as np
newData = np.delete(self.np_need, delList, axis=0)

# 逆序
if backFlag:
    newData = np.flipud(newData)
    # newData = newData[::-1, ::-1]
```


## 通过相对地址获取绝对地址
```python
@staticmethod
def handlePath(path):
    """
    判断路径是否存在,同时将相对位置转换为绝对位置
    """
    fatherPath = os.path.dirname(os.path.abspath(__file__))

    # 同时支持相对路径与绝对路径
    if os.path.exists(path):
        return path
    elif os.path.exists(fatherPath + os.sep + path):
        return fatherPath + os.sep + path
    else:
        print('='*80)
        print("\t\t=== Bag path do not exist===")
        print("\t\t=== Stop ===")
        print(path)
        print('='*80)
        import sys
        sys.exit()
```


## 获取当前path的父目录及当前文件名
```python
# 这里只能是绝对路径
# ——————————————————————————————————————————————————————————————————————
import json
import os
bagName = os.path.basename(self.bagPath).split('.')[0]
bagDir = os.path.dirname(self.bagPath)

jsonPath = bagDir + os.sep + "pltData_" + bagName + '.json'

if os.path.exists(jsonPath):
    print('='*80)
    print("\t\t=== JSON is exist ===")
    print('='*80)
    startTime = time.time()
    with open(jsonPath) as file_obj:
        maps = json.load(file_obj)
    ego_tf_dataMap = maps["ego_tf_dataMap"]
    ego_tf_relative_dataMap = maps["ego_tf_relative_dataMap"]
    print("read JSON time: ",time.time()-startTime)
else:
    print('='*80)
    print("\t\t=== JSON is not exist, Reading bag ===")
    print("\t\t=== Please wait a minute ===")
    print('='*80)
    startTime = time.time()
    ego_tf_dataMap, ego_tf_relative_dataMap = self.__ego_tf_data_reader()
    maps = {
        "ego_tf_dataMap" : ego_tf_dataMap,
        "ego_tf_relative_dataMap" : ego_tf_relative_dataMap
    }
    # https://blog.csdn.net/qq_43523725/article/details/104770458
    # https://www.cnblogs.com/tizer/p/11067098.html
    with open(jsonPath,'w',encoding='utf-8') as f:
        json.dump(maps,f)
    # 标准格式存储
    # with open(jsonPath,'w',encoding='utf-8') as f:
    #     f.write(json.dumps(maps,ensure_ascii=False,indent=1))

    print("read bag time: ",time.time()-startTime)
# ——————————————————————————————————————————————————————————————————————
import sys
if len(ego_tf_dataMap['__time']) == 0:
    print('='*80)
    print("============== ego_tf_dataMap is empty! ==============")
    print('='*80)
    sys.exit()
if len(ego_tf_relative_dataMap['__time']) == 0:
    print('='*80)
    print("============== ego_tf_relative_dataMap is empty! ==============")
    print('='*80)
    sys.exit()

```

## 查找共同元素、差异元素、共有元素
https://blog.csdn.net/tyro_java/article/details/81360322
```python
import numpy as np
# 创建一维 ndarray x
x = np.array([1,2,3,4,5])
# 创建一维 ndarray y
y = np.array([6,5,4,8,7])
# 输出 x
print('x = ', x)
# 输出 y
print('y = ', y)
# 比较x和y
print('使用intersect1d输出在x和y中共有交集的元素:', np.intersect1d(x,y))
print('使用setdiff1d输出在x中不在y中的元素:', np.setdiff1d(x,y))
print('使用union1d输出在x和y并集的元素:',np.union1d(x,y))


# 输出————————————————————————————————
x =  [1 2 3 4 5]
y =  [6 5 4 8 7]
使用intersect1d输出在x和y中共有的元素: [4 5]
使用setdiff1d输出在x中不在y中的元素: [1 2 3]
使用union1d输出在x和y共有的元素: [1 2 3 4 5 6 7 8]

```


# pandas

按行索引：https://blog.csdn.net/ls13552912394/article/details/79349809