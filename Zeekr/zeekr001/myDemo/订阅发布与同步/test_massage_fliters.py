#!/bin/env python3
# -*- coding:utf-8 -*-
# @Author: monomania_cjk
# @Data: 2022.04.02
import os
import sys
import rclpy
from rclpy.node import Node
import message_filters

#msg————————————————————————————————————————————
# from interfaces.msg import ChassisCommand
from interfaces.msg import ChassisReport
from interfaces.msg import ControlCommand
# from interfaces.msg import Localization
# from interfaces.msg import RelativeLocalization


if os.environ.get('ROSBAG2_PY_TEST_WITH_RTLD_GLOBAL', None) is not None:
    # This is needed on Linux when compiling with clang/libc++.
    # TL;DR This makes class_loader work when using a python extension compiled with libc++.
    #
    # For the fun RTTI ABI details, see https://whatofhow.wordpress.com/2015/03/17/odr-rtti-dso/.
    sys.setdlopenflags(os.RTLD_GLOBAL | os.RTLD_LAZY)


"""
同步时间戳
"""

subtopic_control_command = '/pnc/control_command'
subtopic_chassis_report = '/pnc/chassis_report'


class AccSubscriber(Node):
    def __init__(self):
        super().__init__('acc_subscriber')

        self.subscription_Command = message_filters.Subscriber(self, ControlCommand, subtopic_control_command)  # Localization
        self.subscription_Command  # prevent unused variable warning

        self.subscription_Report = message_filters.Subscriber(self, ChassisReport, subtopic_chassis_report) # RelativeLocalization
        self.subscription_Report  # prevent unused variable warning

        # message_filters.TimeSynchronizer
        self.sync = message_filters.ApproximateTimeSynchronizer([self.subscription_Command, self.subscription_Report], \
            queue_size=10,slop=0.1) # 同步时间戳，具体参数含义需要查看官方文档。
        self.sync.registerCallback(self.multi_callback)#执行反馈函数


        
    def multi_callback(self, commandMsg, reportMsg):

        print(commandMsg.chassis_command.acceleration, commandMsg.header)
        print(reportMsg.imu_motion_master.acceleration_x.value, reportMsg.header)
        print('='*50)


    # def listener_callback_command(self, msg):
    #     """
    #         acceleration
    #     """
    #     # self.get_logger().info('I heard: "%s"' % msg)
    #     print(msg.acceleration)
    #     print('='*50)

    # def listener_callback_report(self, msg):
    #     """/pnc/chassis_report
    #         imu_motion_master
    #             Float32WithValid acceleration_x
    #                 float32 value
    #                 uint8 valid
    #     """
    #     # self.get_logger().info('I heard: "%s"' % msg)
    #     print(msg.imu_motion_master.acceleration_x.value)
    #     print('='*50)


def main(args=None):
    rclpy.init(args=args)

    acc_subscriber = AccSubscriber()

    rclpy.spin(acc_subscriber)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    acc_subscriber.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()