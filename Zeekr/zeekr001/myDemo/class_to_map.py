# 序列化
# def serialize_instance(obj):
#     """解析数据类型"""
#     d = {'__classname__': type(obj).__name__}
#     d.update(vars(obj))
#     return d
# # 反序列化
# def unserialize_object(d):
#     clsname = d.pop('__className__', None)
#     if clsname:
#         # 需要改进的地方，定义类；也可以通过getattr创建对象
#         cls = corpora.Dictionary  # 测试用的类，可更改为其他类
#         obj = cls.__new__(cls)
#         for key, value in d.items():
#             setattr(obj, key, value)
#             return obj
#     else:
#         return d
# ————————————————
# 版权声明：本文为CSDN博主「yangtom249」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
# 原文链接：https://blog.csdn.net/weixin_44153121/article/details/106075577


# globals()["字符串"]---转换为类名
# globals()[type(msg)__name__]---转换为类名
# def serialize_instance(obj):
#     """解析数据类型---具备__dict__的情况
#     @obj: class的实例对象
#     @return: map
#     """
#     d = {'__classname__': type(obj).__name__}
#     d.update(vars(obj))
#     return d


def getDataByClass_recursion(obj):
    """
    递归获取类内元素--类中类中类
    """
    attr_list = [i for i in dir(obj) if not callable(getattr(obj, i)) and not i.startswith('_') and i.islower()]
    # tmp_dataMap.update()
    tmp_dataMap = {}
    for attr in attr_list:
        value = getattr(obj, attr)

        if not isinstance(value,(str, float ,int,list, np.ndarray)):
            # print(value)
            value = getDataByClass_recursion(value)

        if isinstance(value,(np.ndarray)):
            # print(value)
            # sys.exit()
            value = list(value.astype(float))
        tmp_dataMap[attr] = value

    return tmp_dataMap
    


def serialize_instance(obj):
    """解析数据类型----递归
    通用
    @obj: class的实例对象
    @return: map
    """
    tmp_dataMap = {'__className__': type(obj).__name__}
    attr_list = [i for i in dir(obj) if not callable(getattr(obj, i)) and not i.startswith('_') and i.islower()]
    # tmp_dataMap.update()
    for attr in attr_list:
        value = getattr(obj, attr)
        if not isinstance(value,(str, float ,int,list, np.ndarray)):
            # print(value)
            value = getDataByClass_recursion(value)
            print(value)
            tmp_dataMap[attr] = serialize_instance(value)

        if isinstance(value,(np.ndarray)):
            # print(value)
            # sys.exit()
            value = list(value.astype(float))

        tmp_dataMap[attr] = value
    
    return tmp_dataMap
