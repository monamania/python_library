#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# @Author: monomania_cjk
# @Data: 2022.04.06
from scipy import interpolate
import numpy as np

"""
https://blog.csdn.net/qq_41365597/article/details/90676249
"""


def calc_spline_course(x, y, der):
    # 进行样条插值
    tck = interpolate.splrep(x, y)
    xx = np.linspace(min(x), max(x), 100)
    yy = interpolate.splev(xx, tck, der=0)
    return xx, yy
