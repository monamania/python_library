#ifndef __PTINTCOLOR_H
#define __PTINTCOLOR_H
 
#include <stdio.h>

#ifndef LOG_DBG
 
//颜色宏定义
#define NONE         "\033[m"
#define RED          "\033[0;32;31m"
#define LIGHT_RED    "\033[1;31m"
#define GREEN        "\033[0;32;32m"
#define LIGHT_GREEN  "\033[1;32m"
#define BLUE         "\033[0;32;34m"
#define LIGHT_BLUE   "\033[1;34m"
#define DARY_GRAY    "\033[1;30m"
#define CYAN         "\033[0;36m"
#define LIGHT_CYAN   "\033[1;36m"
#define PURPLE       "\033[0;35m"
#define LIGHT_PURPLE "\033[1;35m"
#define BROWN        "\033[0;33m"
#define YELLOW       "\033[1;33m"
#define LIGHT_GRAY   "\033[0;37m"
#define WHITE        "\033[1;37m"
#define HEADER       "\033[95m"
 
#define LOGFUNC(fmt,...) do{printf(fmt" line:%d - %s/%s   \n",##__VA_ARGS__,__LINE__,__TIME__,__DATE__);}while(0)
// //可变参数
//     LOGFUNC("i am C++ :%d name:%s age:%d",112,"C语言教程",18);// ok

//     //字符串常量
//     LOGFUNC("i am C++ ");// ok
#define LOG_DBG(...)     do{printf("[%s Line-%-3d]| ",__TIME__,__LINE__);printf(DARY_GRAY "DEBUG  | %s : " NONE,__FILE__); printf(__VA_ARGS__);printf("\n");}while(0)
#define LOG_INFO(...)    do{printf("[%s Line-%-3d]| ",__TIME__,__LINE__);printf(    GREEN "INFO   | %s : " NONE,__FILE__); printf(__VA_ARGS__);printf("\n");}while(0)
#define LOG_WARN(...)    do{printf("[%s Line-%-3d]| ",__TIME__,__LINE__);printf(   YELLOW "WARN   | %s : " NONE,__FILE__); printf(__VA_ARGS__);printf("\n");}while(0)
#define LOG_ERR(...)     do{printf("[%s Line-%-3d]| ",__TIME__,__LINE__);printf(LIGHT_RED "ERROR  | %s : " NONE,__FILE__); printf(__VA_ARGS__);printf("\n");}while(0)
#define LOG_HEADER(...)  do{printf("[%s Line-%-3d]| ",__TIME__,__LINE__);printf(HEADER    "HEADER | %s : " NONE,__FILE__); printf(__VA_ARGS__);printf("\n");}while(0)
 
#endif
 
#endif