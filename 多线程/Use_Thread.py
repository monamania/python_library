'''
多线程的使用
模块
1. _thread模块   低级模块
2. threading模块 高级模块
'''

'''第一个案例'''
# #实例：启动一个线程
#导包
# import threading,time

# #第一步：定义一个函数,把num作为参数
# def run(num):
#     '''run函数用于打印信息'''
#     #打印线程的名字 %s代表字符串
#     print('子线程（%s）开始' % (threading.current_thread().name)) 
#     #延时两秒
#     time.sleep(2)
#     #打印num
#     print('打印', num)  #逗号会自动打印成空格
#     #延时两秒
#     time.sleep(2)
#     #结束子线程
#     print('子线程（%s）结束' % (threading.current_thread().name))


# #写程序入口
# #一个python文件如何使用，有两种方法
# #1. 直接作为脚本运行
# #2. 作为模块被其他py文件import
# #下面的语句就是用于控制这两种使用方法，只有作为脚本执行的时候，后面的语句才会被运行
# if __name__ == "__main__":
#     #主线程开始
#     print('主线程（%s）开始' % (threading.current_thread().name))
    
#     #开始创建线程   
#     # 参数说明：
#     # target--线程所要执行的函数名字 
#     # name--给所执行的线程命名（遵循驼峰原则） 
#     # args--传给所执行函数的参数(需要传一个元组，最好有逗号)
#     # 用t这个变量来接收线程
#     t = threading.Thread(target=run, name='runThread', args=(1,))
#     #运行这个线程
#     t.start()
    
#     #等待线程结束。join()必须要有，不然在子线程没结束前，主线程就结束了
#     t.join()
#     print('主线程（%s）结束' % (threading.current_thread().name))

'''
第二个案例：如果我多个线程同时对数据进行修改，听谁的？-数据的一致性、完整性
'''
# import threading, time

# #定义一个num变量
# num = 0
# #定义一个函数,传一个参数n
# def run(n):
#     #把num变量声明成全局的
#     global num
#     #第一步，写一个循环,一百万次
#     for index in range(1000000):
#         #对数据进行修改
#         num = num + n
#         num = num - n

# #定义主入口-赋值--判断是否相等
# if __name__ == "__main__":
#     #开始创建线程 //名字可以不取
#     t1 = threading.Thread(target=run, args=(6,))
#     t2 = threading.Thread(target=run, args=(9,))
#     #开启线程
#     t1.start()
#     t2.start()
#     #等待线程结束
#     t1.join()
#     t1.join()
#     print('num = ', num)  #发现两个线程同时对变量num修改，输出结果每次运行都不同


'''
如何解决：加锁！！
一个线程操作数据的时候，其他线程就不允许操作
'''
# import threading, time

# #首先，定义一个线程锁
# lock = threading.Lock()

# #定义一个num变量
# num = 0
# def run(n):
#     #把num变量声明成全局的
#     global num
#     for index in range(1000000):
#         #通过上锁的方式，对数据进行修改
#         #with lock可以自动上锁、自动解锁
#         with lock:
#             num = num + n
#             num = num - n

# #定义主入口-赋值--判断是否相等
# if __name__ == "__main__":
#     #开始创建线程 //名字可以不取
#     t1 = threading.Thread(target=run, args=(6,))
#     t2 = threading.Thread(target=run, args=(9,))
#     #开启线程
#     t1.start()
#     t2.start()
#     #等待线程结束
#     t1.join()
#     t1.join()
#     print('num = ', num)
'''
案例4：一起过马路
'''
# import threading,time
# #凑够了3个人才能过马路（凑够三个线程才会执行）
# bar = threading.Barrier(3)

# def run():
#     print('%s---start' % (threading.current_thread().name))
#     time.sleep(1)
#     #等人
#     bar.wait()
#     print('%s---end' % (threading.current_thread().name))


# if __name__ == "__main__":
#     for i in range(6):
#         #偷懒，不用变量过度，直接开始线程
#         threading.Thread(target=run).start()
'''
定时线程
'''
import threading, time
def run():
    print("***********")
#创建线程 //等待5秒后执行线程
t = threading.Timer(5, run)
t.start()
t.join()
print('end!')

