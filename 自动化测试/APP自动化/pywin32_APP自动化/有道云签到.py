#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2021-07-02
"""
import ctypes
import os
import time
import win32gui
import win32api
import win32con
import pyautogui


''' 
@ 功能：
'''

# from pymouse import PyMouse
hwnd_title = {}

def get_all_hwnd(hwnd, mouse):
    """ 获取窗口句柄 """
    if (win32gui.IsWindow(hwnd) and
        win32gui.IsWindowEnabled(hwnd) and
        win32gui.IsWindowVisible(hwnd)):
        hwnd_title.update({hwnd: win32gui.GetWindowText(hwnd)})

def open_app(app_dir):
    os.startfile(app_dir)


def get_current_size(hWnd=None):
    try:
        f = ctypes.windll.dwmapi.DwmGetWindowAttribute
    except WindowsError:
        f = None
    if f:
        rect = ctypes.wintypes.RECT()
        DWMWA_EXTENDED_FRAME_BOUNDS = 9
        f(ctypes.wintypes.HWND(hWnd),
          ctypes.wintypes.DWORD(DWMWA_EXTENDED_FRAME_BOUNDS),
          ctypes.byref(rect),
          ctypes.sizeof(rect)
          )
        # size = (rect.right - rect.left, rect.bottom - rect.top)
        # return size
        return rect.left, rect.top, rect.right, rect.bottom



def YouDao_sign_in(handle):
    """有道云签到"""
    left, top, right, bottom = win32gui.GetWindowRect(handle)
    print(left, top, right, bottom)
    # size = get_current_size(handle)
    # print(size)
    # print("当前屏幕分辨率", pyautogui.size())
    # 点击头像
    pyautogui.click(left + 50, top + 60, duration=0.4)
    # 点击签到
    pyautogui.click(1150, 290, duration=0.5)
    #  当前鼠标的坐标
    print(pyautogui.position())
    # 点击叉叉
    pyautogui.moveRel(270, -60, duration=0.2)
    # 鼠标当前位置单击左键
    pyautogui.click()
    # 关闭有道云
    pyautogui.click(right - 20, top + 20, duration=0.4)



def main():
    # 设置appdict
    app_dir = r'D:\My_User_App\有道云笔记\YoudaoNote\YoudaoNote.exe'
    open_app(app_dir)

    time.sleep(2)
    # 获取所有窗口句柄
    win32gui.EnumWindows(get_all_hwnd, 0)

    for handle, title in hwnd_title.items():
        if title:
            # print(handle, title)
            if title == '有道云笔记':
                print(handle, title)
                YouDao_sign_in(handle)


if __name__ == '__main__':
    main()
