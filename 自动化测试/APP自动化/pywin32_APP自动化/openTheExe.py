#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2021-07-02
"""
import os

'''
@ 功能：用于打开应用程序
'''

def open_app(app_dir):
    os.startfile(app_dir)


if __name__ == "__main__":
    app_dir = r'D:\My_User_App\有道云笔记\YoudaoNote\YoudaoNote.exe'
    open_app(app_dir)