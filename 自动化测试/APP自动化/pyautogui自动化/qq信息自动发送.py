#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2021-08-20
"""
import os
import time
import pag as pag
import pyperclip
''' 
@ 功能：
1. 消息自动发送
2. 消息自定义
实现的代码文件主要有两个，目的分别是：获取聊天窗口位置和实现自动发送消息功能
'''



# 获取聊天窗口位置
try:
    while True:
        print("Press Ctrl-C to end")
        x, y = pag.position()  # 返回鼠标的坐标
        posStr = "Position:" + str(x).rjust(4) + ',' + str(y).rjust(4)
        print(posStr)  # 打印坐标
        time.sleep(0.2)
        os.system('cls')  # 清楚屏幕
except KeyboardInterrupt:
    print('end....')




# 待发送消息
content = """   
呼叫龙叔！
第二遍！
第三遍！
第四遍！
第五遍！
"""

for line in list(content.split("\n"))*10:
    if line:
        pag.click(669,687)  #鼠标点击并定位到聊天窗口
        pyperclip.copy(line)    #复制该行
        pag.hotkey("ctrl","v") #粘贴，mac电脑则把ctrl换成command
        pag.typewrite("\n")   #发送
        time.sleep(5) #每次发完间隔5s
