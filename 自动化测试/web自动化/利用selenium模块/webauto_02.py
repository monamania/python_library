#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2021-06-16
"""
from selenium import webdriver
''' 
http://www.byhy.net/tut/auto/selenium/03/
@ 功能：操控元素的基本方法
1. 点击元素 element.click()
2. 在元素中输入字符串，通常是对输入框这样的元素
    element.clear() # 清除输入框已有的字符串
    element.send_keys('白月黑羽') # 输入新字符串
3. 获取元素包含的信息，比如文本内容，元素的属性
    获取文本内容【<>之间的内容】
    element = wd.find_element_by_id('animal')
    print(element.text)
4. 获取输入框里面的文字
    element = wd.find_element_by_id("input1")
    element.get_attribute('value')

'''


# 创建 WebDriver 对象，指明使用chrome浏览器驱动
wd = webdriver.Chrome(r'D:\WebDrivers\chromedriver.exe')
# 设置最大等待时长为 10秒  ----- 没有这句会报错
wd.implicitly_wait(10)

# 调用WebDriver 对象的get方法 可以让浏览器打开指定网址
wd.get('https://www.baidu.com')

# 如果想关闭浏览器窗口可以调用WebDriver对象的 quit 方法，像这样 wd.quit()


'''输入字符串 与 点击操作'''
# 根据id选择元素，返回的就是该元素对应的WebElement对象
element = wd.find_element_by_id('kw')   # 通过F12可知，id为'kw'的地方就是文本输入框s
# 调用这个对象的 send_keys 方法就可以在对应的元素中 输入字符串，
element.send_keys('白月黑羽\n')  # 由于\n是回车，所以相当于直接搜索了，就不用点击了
# # element.send_keys('白月黑羽')
# # element = wd.find_element_by_id('su')
# # element.click()

'''获取文本内容'''
element = wd.find_element_by_id('1')   # 通过F12可知，id为'kw'的地方就是文本输入框s
element = element.find_element_by_tag_name('h3')
em = element.find_elements_by_tag_name('em')
print("id为1，<tag>为<h3>下的<em>的文本内容如下：")
for em in em:
    print(em.text)

''' 获取元素属性 '''
element = wd.find_element_by_id('1')
print('属性名为srcid的值为：', element.get_attribute('srcid'))  # 获取元素属性名为srcid的值


'''获取整个元素对应的HTML'''
# # 要获取整个所选元素对应的HTML文本内容，可以使用 element.get_attribute('outerHTML')
# # 如果，只是想获取某个元素 内部 的HTML文本内容，可以使用 element.get_attribute('innerHTML')
# element = wd.find_element_by_id('1')
# # 整个<div>的内容
# print(element.get_attribute('outerHTML'))
# # 去掉本身<div>的内容，拿内部信息
# print(element.get_attribute('innerHTML'))


'''获取输入框内容'''
element = wd.find_element_by_id('kw')
print('输入框内容:', element.get_attribute('value'))

# # 如果想关闭浏览器窗口可以调用WebDriver对象的 quit 方法
# wd.quit()


'''获取元素文本内容2
通过WebElement对象的 text 属性，可以获取元素 展示在界面上的 文本内容。
但是，有时候，元素的文本内容没有展示在界面上，或者没有完全展示在界面上。 
这时，用WebElement对象的text属性，获取文本内容，就会有问题。
出现这种情况，可以尝试使用 element.get_attribute('innerText') ，或者 element.get_attribute('textContent')
'''
