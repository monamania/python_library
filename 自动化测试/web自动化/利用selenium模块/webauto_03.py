#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2021-06-16
"""
from selenium import webdriver

''' 
http://www.byhy.net/tut/auto/selenium/css_1/
@ 功能：根据css表达式选择元素-上篇
'''
# 创建 WebDriver 对象，指明使用chrome浏览器驱动
wd = webdriver.Chrome(r'D:\WebDrivers\chromedriver.exe')
# 设置最大等待时长为 10秒  ----- 没有这句会报错
wd.implicitly_wait(10)

# 调用WebDriver 对象的get方法 可以让浏览器打开指定网址
wd.get('https://www.baidu.com')

wd.quit()
