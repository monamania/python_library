#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2021-06-16
"""
from selenium import webdriver

''' 
【推荐】复杂的建议网上搜索Playwright-python 教程，可以实现录制脚本的功能（就是自己手动操作，会自动生成python脚本代码）

http://www.byhy.net/tut/auto/selenium/02/
@ 功能：
1. 实现自动的打开浏览器的指定网页功能
2. 实现元素选择基本方法：根据id、class属性、tag名、WebElement对象 选择元素
3. 使用wd.implicitly_wait(10)，实现等待界面元素出现功能，不至于运行太快导致，报NoSuchElementException错

'''

# 创建 WebDriver 对象，指明使用chrome浏览器驱动
wd = webdriver.Chrome(r'D:\WebDrivers\chromedriver.exe')
# 设置最大等待时长为 10秒  ----- 没有这句会报错
wd.implicitly_wait(10)

# 调用WebDriver 对象的get方法 可以让浏览器打开指定网址
wd.get('https://www.baidu.com')

# 如果想关闭浏览器窗口可以调用WebDriver对象的 quit 方法，像这样 wd.quit()

# ----------------------------------------------------
# # 创建 WebDriver 对象，指明使用edge浏览器驱动
# wd = webdriver.Edge(r'D:\WebDrivers\msedgedriver.exe')
#
# # 调用WebDriver 对象的get方法 可以让浏览器打开指定网址
# wd.get('https://www.baidu.com')
# ----------------------------------------------------

'''（首选）根据ID选择元素  id通过浏览器的F12可以查看到，且ID唯一'''
# # 根据id选择元素，返回的就是该元素对应的WebElement对象
# element = wd.find_element_by_id('kw')   # 通过F12可知，id为'kw'的地方就是文本输入框s
#
# # 调用这个对象的 send_keys 方法就可以在对应的元素中 输入字符串，
# element.send_keys('白月黑羽\n')  # 由于\n是回车，所以相当于直接搜索了，就不用点击了
# # 上面相当于先输入后电机
# # element.send_keys('白月黑羽')
# # element = wd.find_element_by_id('su')
# # element.click()
# # 调用这个对象的 click 方法就可以 点击 该元素。


'''
根据class选择元素  class通过浏览器的F12可以查看到
使用 find_elements 选择的是符合条件的 所有 元素， 如果没有符合条件的元素， 返回空列表
使用 find_element 选择的是符合条件的 第一个 元素， 如果没有符合条件的元素， 抛出 NoSuchElementException 异常
'''
# # 调用WebDriver 对象的get方法 可以让浏览器打开指定网址
# wd.get('http://cdn1.python3.vip/files/selenium/sample1.html')
# # 根据 class name 选择元素，返回的是 一个列表
# # 里面 都是class 属性值为 animal的元素对应的 WebElement对象
# elements = wd.find_elements_by_class_name('animal')  # 注意 find_elements_by_class_name返回列表  find_element_by_class_name返回第一个
#
# # 取出列表中的每个 WebElement对象，打印出其text属性的值
# for element in elements:
#     print(element.text)   # text属性就是该 WebElement对象对应的元素在网页中的文本内容


'''
根据tag选择元素
如<span>山羊</span>
'''
# # 调用WebDriver 对象的get方法 可以让浏览器打开指定网址
# wd.get('http://cdn1.python3.vip/files/selenium/sample1.html')
# # 根据 class name 选择元素，返回的是 一个列表
# # 里面 都是class 属性值为 animal的元素对应的 WebElement对象
# elements = wd.find_elements_by_tag_name('span')  # 注意 find_elements_by_class_name返回列表  find_element_by_class_name返回第一个
#
# # 取出列表中的每个 WebElement对象，打印出其text属性的值
# for element in elements:
#     print(element.text)   # text属性就是该 WebElement对象对应的元素在网页中的文本内容


'''
@ 功能：通过WebElement对象选择元素
'''
# # 调用WebDriver 对象的get方法 可以让浏览器打开指定网址
# wd.get('http://cdn1.python3.vip/files/selenium/sample1.html')
#
# element = wd.find_element_by_id('container')
#
# # 限制 选择元素的范围是 id 为 container 元素的内部。
# spans = element.find_elements_by_tag_name('span')
# for span in spans:
#     print(span.text)


''' 
@ 功能：等待界面元素出现
我们进行网页操作的时候， 有的元素内容不是可以立即出现的，可能会等待一段时间。
由于元素未出现导致报错：NoSuchElementException
为什么不用sleep，因为等待时间随机，并不知道具体需要多少时间
'''
# wd = webdriver.Chrome(r'D:\WebDrivers\chromedriver.exe')
#
# # 当发现元素没有找到的时候， 并不 立即返回 找不到元素的错误。
# # 而是周期性（每隔半秒钟）重新寻找该元素，直到该元素找到，
# # 或者超出指定最大等待时长，这时才 抛出异常（如果是 find_elements 之类的方法， 则是返回空列表）。
# # 设置最大等待时长为 10秒  ----- 没有这句会报错
# wd.implicitly_wait(10)
#
# wd.get('https://www.baidu.com')
#
# element = wd.find_element_by_id('kw')
#
# element.send_keys('白月黑羽\n')
#
# # id 为 1 的元素 就是第一个搜索结果
# element = wd.find_element_by_id('1')
#
# # 打印出 第一个搜索结果的文本字符串
# print(element.text)


wd.quit()