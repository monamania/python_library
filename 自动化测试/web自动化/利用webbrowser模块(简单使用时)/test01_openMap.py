#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2021-08-13
"""

import sys
import webbrowser

import pyperclip

''' 
@ 功能：
webbrowser：用于打开浏览器
pyperclip：用于获取剪贴板的内容

在Windows 上，第一行可以是 #! python3。
在OS X，第一行可以是 #! /usr/bin/env python3。
在Linux 上，第一行可以是 #! /usr/bin/python3。
'''

if len(sys.argv) > 1:
    address = ' '.join(sys.argv[1:])
else:
    # 获取剪贴板内容
    address = pyperclip.paste()

webbrowser.open("https://www.amap.com/search?query=" + address)
