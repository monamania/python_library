#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2021-06-25
"""
import platform
import time
import ctypes
import os
import time

import requests
import win32gui
import pyautogui
from selenium import webdriver

''' 
@ 功能：自动签到
'''
hwnd_title = {}


# 下载图片
def download_img(url, filename):
    r = requests.get(url)
    with open(filename + '.png', 'wb') as f:
        # 对于图片类型的通过r.content方式访问响应内容，将响应内容写入yanzheng.png中
        f.write(r.content)
        print(filename + '下载完成')


def VPN_sign_in():
    wd = webdriver.Edge(r'D:\WebDrivers\msedgedriver.exe')
    wd.maximize_window()  # 窗口最大化
    # 设置最大等待时长为 10秒  ----- 没有这句会报错
    wd.implicitly_wait(10)
    # 调用WebDriver 对象的get方法 可以让浏览器打开指定网址
    wd.get(r'https://ggme.xyz/auth/login')
    wd.switch_to.alert.accept()  # 处理弹窗

    text = wd.find_element_by_class_name('geetest_radar_tip_content')
    text.click()  # 点击按钮进行验证
    # 输入账号密码
    email = wd.find_element_by_id('email')
    email.send_keys(r'202060239@hdu.edu.cn')
    row_login = wd.find_element_by_id('passwd')
    row_login.send_keys(r'q1830309412,./')

    os.system('clc')
    print('账号密码输入完成。')
    time.sleep(1)


    # 验证码
    cnt = 0
    while wd.find_element_by_class_name('geetest_success_radar_tip_content').text != '验证成功':
        time.sleep(2)
        cnt = cnt + 1
        if cnt == 300:
            print("超时，退出系统")
            wd.quit()
            wd.quit()
            exit(0)

    # 点击登录按键
    login = wd.find_element_by_id('login')
    login.click()
    time.sleep(5)
    # 点击签到
    btn = wd.find_element_by_class_name('btn-brand')
    btn.click()
    print("签到完毕")
    time.sleep(5)

    input('输入任意键退出：')
    # 关闭
    wd.quit()
    exit(0)
    # 获取alert对话框的按钮，点击按钮，弹出alert对话框
    # wd.find_element_by_id('alert').click()
    # time.sleep(1)
    # # 获取alert对话框
    # dig_alert = wd.switch_to.alert
    # time.sleep(1)
    # # 打印警告对话框内容
    # print(dig_alert.text)
    # # alert对话框属于警告对话框，我们这里只能接受弹窗
    # dig_alert.accept()


def get_all_hwnd(hwnd, mouse):
    """ 获取窗口句柄 """
    if (win32gui.IsWindow(hwnd) and
            win32gui.IsWindowEnabled(hwnd) and
            win32gui.IsWindowVisible(hwnd)):
        hwnd_title.update({hwnd: win32gui.GetWindowText(hwnd)})


def open_app(app_dir):
    os.startfile(app_dir)


def get_current_size(hWnd=None):
    try:
        f = ctypes.windll.dwmapi.DwmGetWindowAttribute
    except WindowsError:
        f = None
    if f:
        rect = ctypes.wintypes.RECT()
        DWMWA_EXTENDED_FRAME_BOUNDS = 9
        f(ctypes.wintypes.HWND(hWnd),
          ctypes.wintypes.DWORD(DWMWA_EXTENDED_FRAME_BOUNDS),
          ctypes.byref(rect),
          ctypes.sizeof(rect)
          )
        # size = (rect.right - rect.left, rect.bottom - rect.top)
        # return size
        return rect.left, rect.top, rect.right, rect.bottom


def YouDao_handle(hwnd):
    """有道云签到"""
    left, top, right, bottom = win32gui.GetWindowRect(hwnd)
    print(left, top, right, bottom)
    # size = get_current_size(handle)
    # print(size)
    # print("当前屏幕分辨率", pyautogui.size())
    # 点击头像
    pyautogui.click(left + 50, top + 60, duration=0.4)
    # 点击签到
    pyautogui.click(1150, 290, duration=0.6)
    #  当前鼠标的坐标
    print(pyautogui.position())
    # 点击叉叉
    pyautogui.moveRel(270, -60, duration=0.2)
    # 鼠标当前位置单击左键
    pyautogui.click()
    # 关闭有道云
    pyautogui.click(right - 20, top + 20, duration=0.4)


def YouDao_sign_in():
    # 设置appdict
    app_dir = r'D:\My_User_App\有道云笔记\YoudaoNote\YoudaoNote.exe'
    open_app(app_dir)

    time.sleep(3)
    # 获取所有窗口句柄
    win32gui.EnumWindows(get_all_hwnd, 0)

    for hwnd, title in hwnd_title.items():
        if title:
            # print(handle, title)
            if title == '有道云笔记':
                print(hwnd, title)
                YouDao_handle(hwnd)


def clear():
    """该函数用于清屏"""
    OS = platform.system()
    if OS == 'Windows':
        os.system('cls')
    else:
        os.system('clear')


def main():
    # 清屏
    clear()
    print("=" * 50)
    print('------ 开始运行自动签到程序 ------')
    print("=" * 50)
    # 有道云签到
    YouDao_sign_in()
    # VPN签到
    VPN_sign_in()


if __name__ == '__main__':
    main()
