#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2022-06-16
"""
import os
import random
import time
import re
import pandas as pd
#输入11位手机号码，判断是否是移动有效手机号
phone_pat = r'.*(13[0-9]|14[01456879]|15[0-35-9]|16[2567]|17[0-8]|18[0-9]|19[0-35-9])\d{8}'  #正则


from selenium import webdriver

''' 
@ 功能：
'''


class Spider:
    def __init__(self, base_url):
        # 创建 WebDriver 对象，指明使用chrome浏览器驱动
        self.wd = webdriver.Chrome(r'D:\WebDrivers\chromedriver.exe')
        # 设置最大等待时长为 10秒  ----- 没有这句会报错
        self.wd.implicitly_wait(10)
        # 调用WebDriver 对象的get方法 可以让浏览器打开指定网址
        self.wd.get(base_url)
        time.sleep(2)
        self.fatherPath = os.path.dirname(os.path.abspath(__file__))

    # 用来控制页面滚动
    @staticmethod
    def Transfer_Clicks(browser):
        # 获取内嵌div的位置 并设置滚动条移动的值
        js_1 = "document.getElementById('agreementMain').scrollTop=12000"
        js_2 = "window.scrollTo(0,10000)"

        try:
            # 调用js方法
            # browser.execute_script(js_2)
            browser.execute_script("window.scrollBy(0,document.body.scrollHeight)", "")
        except:
            pass
        return "Transfer successfully \n"

    # 获取当前页面的数据
    def get_current_weibo_data(self, maxWeibo=100, excel_name="result.xlsx"):
        dataMap = {
            "cost": [],
            "phone": [],
            "msg": []
        }
        after = 0
        n = 0
        while True:
            before = after
            timeToSleep = random.randint(1, 6)
            self.Transfer_Clicks(self.wd)
            print('sleep:', timeToSleep)
            time.sleep(timeToSleep)
            # elems = self.wd.find_elements_by_css_selector('div.card.m-panel.card9')
            # print("当前包含微博最大数量：%d,n当前的值为：%d, n值到5说明已无法解析出新的微博" % (len(elems), n))
            elements = self.wd.find_elements_by_class_name(
                'weibo-text')  # 注意 find_elements_by_class_name返回列表  find_element_by_class_name返回第一个
            after = len(elements)
            if after > before:
                n = 0
            if after == before:
                n = n + 1
            if n == 5:
                print("当前关键词最大微博数为：%d" % after)
                break
            if len(elements) > maxWeibo:
                print("当前微博数以达到%d条" % maxWeibo)
                break

            for i in range(before, after):
                msg = elements[i].text  # text属性就是该 WebElement对象对应的元素在网页中的文本内容

                phone_match = re.match(phone_pat, msg)
                nums = re.findall(r"\d+\.?\d*", msg)
                for num in nums:
                    if len(num) < 5 and len(num) > 3:
                        cost = num
                if phone_match == None:
                    phone = None
                else:
                    nums = re.findall(r"\d+\.?\d*", phone_match.string)
                    for num in nums:
                        if len(num) > 10:
                            phone = num
                dataMap['cost'].append(cost)
                dataMap['phone'].append(phone)
                print("phone:", phone)
                print("cost:", cost)
                print(msg)
                print('=' * 80)
                dataMap['msg'].append(msg)

        df = pd.DataFrame(dataMap)
        writer = pd.ExcelWriter(self.fatherPath + os.sep + excel_name)
        df.to_excel(writer, sheet_name='data1', index=False)
        # df.to_excel(writer, sheet_name='data2', index=False)
        writer.save()
        writer.close()
        self.wd.quit()

if __name__ == '__main__':
    # # 滨江租房超话
    # base_url = '''
    #         https://m.weibo.cn/p/index?containerid=100808cb93ca799f92d3ec54b11802c0a30481&luicode=10000011&lfid=100103type%3D1%26q%3D%E6%BB%A8%E6%B1%9F%E7%A7%9F%E6%88%BF
    #         '''
    # 杭州租房超话
    # base_url = '''
    # https://m.weibo.cn/p/index?extparam=%E6%9D%AD%E5%B7%9E%E7%A7%9F%E6%88%BF&containerid=10080882b9c9ad3f652b72697beb0dfd7e8cb6&luicode=10000011&lfid=100103type%3D98%26q%3D%E6%9D%AD%E5%B7%9E%E7%A7%9F%E6%88%BF%26t%3D
    # '''
    # # 杭州滨江租房超话
    base_url = '''
    https://m.weibo.cn/p/index?extparam=%E6%9D%AD%E5%B7%9E%E6%BB%A8%E6%B1%9F%E7%A7%9F%E6%88%BF&containerid=100808f5f66e2ad92dbe11747726c309b3abbb&luicode=10000011&lfid=100103type%3D98%26q%3D%E6%9D%AD%E5%B7%9E%E7%A7%9F%E6%88%BF%26t%3D
    '''
    spider = Spider(base_url)
    # spider.get_current_weibo_data(maxWeibo=200, excel_name='weibo_zufang2.xlsx')
    # spider.get_current_weibo_data(maxWeibo=200, excel_name='杭州租房超话.xlsx')
    spider.get_current_weibo_data(maxWeibo=200, excel_name='杭州滨江租房超话.xlsx')
