#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ Author：CJK_Monomania
@ Data：2022-06-16
"""
import re
''' 
@ 功能：
'''
pattern = r'.*(13[0-9]|14[01456879]|15[0-35-9]|16[2567]|17[0-8]|18[0-9]|19[0-35-9])\d{8}'  #正则
# msg = r"滨江租房 刚刚到期，江南国际城整租3房2厅2卫，5800，有需要的抓紧联系我，房子很好 同行勿扰看房15024113417萧山租房杭州租房小孟#租房# 杭州·江南国际"
msg = r" daf 15024113417 "
match = re.match(pattern, msg)
print(re.findall(r"\d+\.?\d*", match.string))
